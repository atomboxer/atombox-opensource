!isEmpty(ATOMBOXER_PRI_INCLUDED):error("atomboxer.pri already included")

ATOMBOXER_EVALUATION      = 1

ATOMBOXER_BUILD_CRYPTO    = 1
#ATOMBOXER_BUILD_DDL       = 1
ATOMBOXER_BUILD_HTTP      = 1
ATOMBOXER_BUILD_NET       = 1
ATOMBOXER_BUILD_SQL       = 1
ATOMBOXER_BUILD_XML       = 1

ATOMBOXER_INCLUDE_TESTS_SUNSPIDER = 1

ATOMBOXER_PRI_INCLUDED   = 1
ATOMBOXER_SOURCE_TREE    = $$PWD

ATOMBOXER_BUILD_SSL        = 1
AB_STATIC_PLUGINS         = 1
#USE_SCRIPT_CLASSIC        = 1
#IS_TNSR                   = 1

!tandem {
    ATOMBOXER_BUILD_CTREE  = 1
    !linux-g++ {
        ATOMBOXER_BUILD_MQ     = 1
    }
}

!isEmpty(ATOMBOXER_EVALUATION) {
    QMAKE_CXXFLAGS += -DAB_EVALUATION
}

!tandem {   
    !isEmpty(ATOMBOXER_BUILD_CTREE) {
	win32 {
	    CTREE_DIR            = c:/tmpuser/tools/ctree/win32/	
	}
    unix {
        CTREE_DIR            = /opt/faircom/ctree/
	}
        QMAKE_CXXFLAGS += -DAB_HAVE_CTREE
    }
}

!isEmpty(ATOMBOXER_BUILD_MQ) {
    win32 {
        MQ_DIR            = C:/tmpuser/tools/WebSphere MQ
    } else {
            MQ_DIR            = /usr/mqm/
    }

    QMAKE_CXXFLAGS   += -DAB_HAVE_MQ
}


!isEmpty(USE_SCRIPT_CLASSIC) {
    QMAKE_CXXFLAGS += -DAB_USE_SCRIPT_CLASSIC
    INCLUDEPATH +=  $$ATOMBOXER_SOURCE_TREE/src/3rdparty/qtscriptclassic/src/

}

isEmpty(ATOMBOXER_BUILD_SSL) {
    QMAKE_CXXFLAGS += -DQT_NO_OPENSSL
} else {
   !linux-g++ {
        OPENSSL_DIR    = $$QMAKE_LIBDIR_QT/../src/3rdparty/openssl-1.0.1k/
        QMAKE_CXXFLAGS += -I$$OPENSSL_DIR/include/
        QMAKE_LFLAGS   += -L$$OPENSSL_DIR -L$$OPENSSL_DIR/lib
    }
}   

!isEmpty(ATOMBOXER_INCLUDE_TESTS_SUNSPIDER) {
    QMAKE_CXXFLAGS += -DAB_INCLUDE_TESTS
}

tandem {
#    USE_SCRIPT_CLASSIC = 1
}

CONFIG      += debug
CONFIG      -= release

#CONFIG      += release
#CONFIG      -= debug

debug {
    
}
#
#  DEBUG
#
linux-g++ {
#    QMAKE_CXXFLAGS +=  -fno-omit-frame-pointer
#    QMAKE_CXXFLAGS +=  -fsanitize=address
#    QMAKE_LFLAGS   += -lasan
}

#AB_HAVE_MPATROL = 1

!isEmpty(AB_STATIC_PLUGINS) {
    QMAKE_CXXFLAGS += -DAB_STATIC_PLUGINS
}

LIBS =-L.

win32 {
    !tandem {
        QMAKE_CXXFLAGS += -Wno-unused-variable -Wno-unused-parameter -Wno-unknown-pragmas 
        QMAKE_LFLAGS  += -static-libgcc -static-libstdc++ 
    } else {
        QMAKE_CXXFLAGS += -Wnowarn=252,262,304,734,770,1065,1255,1506 
        !isEmpty(IS_TNSR) {
            QMAKE_CXXFLAGS += -Wilp32
        }
    }

    !isEmpty(AB_HAVE_MPATROL) {
        QMAKE_CXXFLAGS += -DAB_HAVE_MPATROL
#        QMAKE_CXXFLAGS += -DAB_HAVE_MPATROL 
        QMAKE_CXXFLAGS += -I$$ATOMBOXER_SOURCE_TREE/src/3rdparty/mpatrol/src/lib/
    }
}

tandem { #or if you need static plugins (for deployment)
    QMAKE_LINK_OBJECT_MAX = 1000

    DLLDESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/

    QMAKE_RESOURCE_FLAGS += -no-compress
}

QMAKE_INCDIR +=  $$ATOMBOXER_SOURCE_TREE/src/3rdparty/ $$ATOMBOXER_SOURCE_TREE/src/lib/scripttools/debugging/
QMAKE_LFLAGS += -L$$ATOMBOXER_SOURCE_TREE/lib/

!isEmpty(USE_SCRIPT_CLASSIC) {
    QSCRIPTCLASSIC_DIR_TREE = $$ATOMBOXER_SOURCE_TREE/src/3rdparty/qtscriptclassic/
    INCLUDEPATH += $$QSCRIPTCLASSIC_DIR_TREE/include $$QSCRIPTCLASSIC_DIR_TREE/src/ $$PWD
}

QT          += core network
isEmpty(USE_SCRIPT_CLASSIC) {
     INCLUDEPATH += $$QMAKE_LIBDIR_QT/../include/QtScript $$PWD
}


win32 {
    CONFIG += console 
}


builddir      =  $$PWD/tmp
DLLDESTDIR    =  $$PWD/bin/

OBJECTS_DIR    = $$builddir
MOC_DIR        = $$builddir
UI_DIR         = $$builddir
RCC_DIR        = $$builddir
DESTDIR_TARGET = $$PWD/bin 

INCLUDEPATH += \ 
$$PWD/src/3rdparty/ \
	$$PWD/src/lib/atombox \
	$$PWD/src/lib/ddl \
	$$PWD/src/plugins/ddl \
	$$PWD/src/plugins/core \
	$$PWD/src/plugins/ \
	$$PWD/src/main/ 

aix-xlc-64 {    
    QT_BUILD_DIR = /aci/atombox/build/qt-everywhere-opensource-src-4.8.6/src
    QMAKE_INCDIR += $$QT_BUILD_DIR/../include/QtCore $$QT_BUILD_DIR/../include/QtSql
    QMAKE_LFLAGS  += -L/usr/lib
    QMAKE_RESOURCE_FLAGS += -compress 0
    QMAKE_LFLAGS += -brtl -bdynamic 
    QMAKE_CXXFLAGS += -qrtti=all
}

solaris-cc-64-stlport {    
    QT_BUILD_DIR = /aci/atombox/build/qt-everywhere-opensource-src-4.8.6/src
    QMAKE_INCDIR += $$QT_BUILD_DIR/../include/QtCore $$QT_BUILD_DIR/../include/QtSql
    QMAKE_LFLAGS  += -L/usr/lib
    QMAKE_RESOURCE_FLAGS += -compress 0
}

!isEmpty(ATOMBOXER_BUILD_CRYPTO) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_CRYPTO
}
!isEmpty(ATOMBOXER_BUILD_DDL) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_DDL
}
!isEmpty(ATOMBOXER_BUILD_HTTP) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_HTTP
}
!isEmpty(ATOMBOXER_BUILD_NET) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_NET
}
!isEmpty(ATOMBOXER_BUILD_SQL) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_SQL
}
!isEmpty(ATOMBOXER_BUILD_XML) {
    QMAKE_CXXFLAGS += -DATOMBOXER_BUILD_XML
}

