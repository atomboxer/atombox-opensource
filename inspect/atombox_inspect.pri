ATOMBOX_INSPECT_PRI_INCLUDED = 1
ATOMBOX_INSPECT_SOURCE_TREE = $$PWD

CONFIG      += release #console
CONFIG      += gui network

builddir      =  $$PWD/tmp

OBJECTS_DIR    = $$builddir
MOC_DIR        = $$builddir
UI_DIR         = $$builddir
RCC_DIR        = $$builddir
DESTDIR_TARGET = $$PWD/bin 

INCLUDEPATH += \ 
	   $$PWD/src/3rdparty/qtscripttools/debugging \
	   $$PWD/src/lib/ \
	   $$PWD/src/lib/remotedebugger/ \
	   $$PWD/src/main/ 
