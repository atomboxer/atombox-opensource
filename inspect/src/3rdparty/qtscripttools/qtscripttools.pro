isEmpty(ATOMBOX_INSPECT_PRI_INCLUDED) {
  include(../../../atombox_inspect.pri)
}

QT          = core gui network

isEmpty(USE_SCRIPT_CLASSIC) {
      QT         += script
}

DESTDIR = $$ATOMBOX_INSPECT_SOURCE_TREE/lib/

TEMPLATE = lib
TARGET     = QtScriptTools1
QPRO_PWD   = $$PWD

DEFINES   += QT_BUILD_SCRIPTTOOLS_LIB QT_BUILD_INTERNAL
DEFINES   += QT_NO_USING_NAMESPACE
DEFINES   += QT_MAKEDLL



include(debugging/debugging.pri)

symbian:TARGET.UID3=0x2001E625
