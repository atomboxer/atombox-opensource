#include "connectdialog.h"

#include <QtGui/QGridLayout>
#include <QIntValidator>
#include <QSettings>
#include <QApplication>

IPCtrl::IPCtrl(QWidget *parent) : QFrame(parent)
{
    QHBoxLayout* pLayout = new QHBoxLayout( this );
    setLayout( pLayout );
    pLayout->setContentsMargins( 0, 0, 0, 0 );
    pLayout->setSpacing( 0 );

    for ( int i = 0; i != QTUTL_IP_SIZE; ++i )
    {
        if ( i != 0 )
        {
            QLabel* pDot = new QLabel( ".", this );
            pDot->setStyleSheet( "background: white" );
            pLayout->addWidget( pDot );
            pLayout->setStretch( pLayout->count(), 0 );
        }

        m_pLineEdit[i] = new QLineEdit( this );
        QLineEdit* pEdit = m_pLineEdit[i];
        pEdit->installEventFilter( this );

        pLayout->addWidget( pEdit );
        pLayout->setStretch( pLayout->count(), 1 );

        pEdit->setFrame( false );
        pEdit->setAlignment( Qt::AlignCenter );

        QFont font = pEdit->font();
        font.setStyleHint( QFont::Monospace );
        font.setFixedPitch( true );
        pEdit->setFont( font );

        QRegExp rx ( "^(0|[1-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$" );
        QValidator *validator = new QRegExpValidator(rx, pEdit);
        pEdit->setValidator( validator );

    }

    setMaximumWidth( 40 * QTUTL_IP_SIZE );

    connect( this, SIGNAL(signalTextChanged(QLineEdit*)),
             this, SLOT(slotTextChanged(QLineEdit*)),
             Qt::QueuedConnection );

    //setFrameShape( QFrame::StyledPanel );
    //setFrameShadow( QFrame::Sunken );


}

IPCtrl::~IPCtrl()
{

}

void
IPCtrl::setText(QString text) 
{
    QStringList ipl = text.split('.');

    int idx=0;
    foreach(QString i, ipl) {
        if (idx >= QTUTL_IP_SIZE)
            return;

        m_pLineEdit[idx]->setText(i);
        idx++;
    }
}

QString
IPCtrl::text() const
{
    QString ip;
    for (int i=0;i<QTUTL_IP_SIZE; i++) {
        ip+=m_pLineEdit[i]->text()+".";
    }
    ip.resize(ip.length()-1);
    return ip;
}


void IPCtrl::slotTextChanged( QLineEdit* pEdit )
{
    for ( unsigned int i = 0; i != QTUTL_IP_SIZE; ++i )
    {
        if ( pEdit == m_pLineEdit[i] )
        {
            if ( ( pEdit->text().size() == MAX_DIGITS &&  pEdit->text().size() == pEdit->cursorPosition() ) || ( pEdit->text() == "0") )
            {
                // auto-move to next item
                if ( i+1 != QTUTL_IP_SIZE )
                {
                   m_pLineEdit[i+1]->setFocus();
                   m_pLineEdit[i+1]->selectAll();
                }
            }
        }
    }
}

bool IPCtrl::eventFilter(QObject *obj, QEvent *event)
{
    bool bRes = QFrame::eventFilter(obj, event);

    if ( event->type() == QEvent::KeyPress )
    {
        QKeyEvent* pEvent = dynamic_cast<QKeyEvent*>( event );
        if ( pEvent )
        {
            for ( unsigned int i = 0; i != QTUTL_IP_SIZE; ++i )
            {
                QLineEdit* pEdit = m_pLineEdit[i];
                if ( pEdit == obj )
                {
                    switch ( pEvent->key() )
                    {
                    case Qt::Key_Left:
                        if ( pEdit->cursorPosition() == 0 )
                        {
                            // user wants to move to previous item
                            movePrevLineEdit(i);
                        }
                        break;

                    case Qt::Key_Right:
                        if ( pEdit->text().isEmpty() || (pEdit->text().size() == pEdit->cursorPosition()) )
                        {
                            // user wants to move to next item
                            moveNextLineEdit(i);
                        }
                        break;

                    case Qt::Key_0:
                        if ( pEdit->text().isEmpty() || pEdit->text() == "0" )
                        {
                            pEdit->setText("0");
                            // user wants to move to next item
                            moveNextLineEdit(i);
                        }
                        emit signalTextChanged( pEdit );
                        break;

                    case Qt::Key_Backspace:
                        if ( pEdit->text().isEmpty() || pEdit->cursorPosition() == 0)
                        {
                            // user wants to move to previous item
                            movePrevLineEdit(i);
                        }
                        break;

                    case Qt::Key_Comma:
                    case Qt::Key_Period:
                        moveNextLineEdit(i);
                        break;

                    default:
                        emit signalTextChanged( pEdit );
                        break;

                    }
                }
            }
        }
    }

    return bRes;
}

void IPCtrl::moveNextLineEdit(int i)
{
    if ( i+1 != QTUTL_IP_SIZE )
    {
        m_pLineEdit[i+1]->setFocus();
        m_pLineEdit[i+1]->setCursorPosition( 0 );
        m_pLineEdit[i+1]->selectAll();
    }
}

void IPCtrl::movePrevLineEdit(int i)
{
    if ( i != 0 )
    {
        m_pLineEdit[i-1]->setFocus();
        m_pLineEdit[i-1]->setCursorPosition( m_pLineEdit[i-1]->text().size() );
        //m_pLineEdit[i-1]->selectAll();
    }
}

ConnectDialog::ConnectDialog(QWidget *parent) : QDialog(parent),
    label_ip_(0),
    label_port_(0),
    button_connect_(0),
    lineedit_port_(0),
    ip_(0) 

{
    setModal( true );
    setWindowTitle( tr("AtomBox Inspector") );
    setupGUI();
    setFixedSize(sizeHint());
    
    QObject::connect(button_connect_, SIGNAL(clicked()), this, SLOT(slotGo()));
}

ConnectDialog::~ConnectDialog() {
}

void ConnectDialog::setupGUI()
{
    label_port_  = new QLabel(tr("TCP/IP Port Number"),this);
    label_ip_    = new QLabel(tr("TCP/IP IP Address"),this);

    ip_          = new IPCtrl(this);
    lineedit_port_ = new QLineEdit(this);

    QValidator *validator = new QIntValidator(1024, 65535, this);
    lineedit_port_->setValidator(validator);
    lineedit_port_->setFixedSize(lineedit_port_->sizeHint());

    QGridLayout *fl = new QGridLayout(this);
    fl->addWidget(label_port_,0,0);
    fl->addWidget(lineedit_port_,0,1);

    fl->addWidget(label_ip_,1,0);
    fl->addWidget(ip_,1,1);

    fl->setSpacing(6);

    button_connect_ = new QPushButton(tr("Inspect"),this);
    fl->addWidget(button_connect_, 2, 1);

    QSettings settings(QSettings::UserScope, QLatin1String("AtomBox"));
    QVariant port = settings.value(QLatin1String("/debugging/port"));
    QVariant ip = settings.value(QLatin1String("/debugging/ip"));
    if (port.isValid()) {
        lineedit_port_->setText(port.toString());
    }

    if (port.isValid()) {
        ip_->setText(ip.toString());
    }

    setFocus();
}

void
ConnectDialog::slotGo()
{
    QSettings settings(QSettings::UserScope, QLatin1String("AtomBox"));
    settings.setValue(QLatin1String("/debugging/port"), lineedit_port_->text());
    settings.setValue(QLatin1String("/debugging/ip"), ip_->text());

    button_connect_->setText(tr("Connecting..."));
    button_connect_->setEnabled(false);
    QApplication::processEvents();
    emit signalGo();
}
