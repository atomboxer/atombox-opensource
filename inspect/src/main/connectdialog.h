#ifndef CONNECT_DIALOG
#define CONNECT_DIALOG

#include <QPushButton>
#include <QDialog>
#include <QFrame>
#include <QLineEdit>
#include <QIntValidator>
#include <QHBoxLayout>
#include <QFont>
#include <QLabel>
#include <QKeyEvent>

class IPCtrl : public QFrame
{
    Q_OBJECT

public:
    IPCtrl(QWidget *parent = 0);
    ~IPCtrl();

    virtual bool eventFilter( QObject *obj, QEvent *event );
    
    void setText(QString ip);
    QString text() const;
    
public slots:
    void slotTextChanged( QLineEdit* pEdit );

signals:
    void signalTextChanged( QLineEdit* pEdit );

private:
    enum {
        QTUTL_IP_SIZE   = 4,
        MAX_DIGITS      = 3 
    };

    QLineEdit *(m_pLineEdit[QTUTL_IP_SIZE]);
    void moveNextLineEdit (int i);
    void movePrevLineEdit (int i);
    void setupGUI();
};

class ConnectDialog : public QDialog {
    Q_OBJECT
public:
    ConnectDialog(QWidget *parent=0);
    ~ConnectDialog();
    QString ip() const { return ip_->text(); }
    QString port() const { return lineedit_port_->text(); }

public slots:
    void slotGo();

signals:
    void signalGo();

private:
    void setupGUI();
private:
    QLabel *label_ip_;
    QLabel *label_port_;
    QPushButton *button_connect_;
    QLineEdit *lineedit_port_;
    IPCtrl *ip_;
};



#endif
