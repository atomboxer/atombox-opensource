#ifndef SCRIPTREMOTETARGETDEBUGGER_H
#define SCRIPTREMOTETARGETDEBUGGER_H

#include <QtCore/qobject.h>
#include <QtNetwork/qabstractsocket.h>
#include <QtNetwork/qhostaddress.h>

class ScriptRemoteTargetDebuggerFrontend;
class QScriptDebugger;

class QAction;
class QWidget;
class QMainWindow;
class QMenu;
class QToolBar;

class ScriptRemoteTargetDebugger
    : public QObject
{
    Q_OBJECT

public:
    enum Error {
        NoError,
        HostNotFoundError,
        ConnectionRefusedError,
        HandshakeError,
        SocketError
    };

    enum DebuggerWidget {
        ConsoleWidget,
        StackWidget,
        ScriptsWidget,
        LocalsWidget,
        CodeWidget,
        CodeFinderWidget,
        BreakpointsWidget,
        DebugOutputWidget,
        ErrorLogWidget
    };

    enum DebuggerAction {
        InterruptAction,
        ContinueAction,
        StepIntoAction,
        StepOverAction,
        StepOutAction,
        RunToCursorAction,
        RunToNewScriptAction,
        ToggleBreakpointAction,
        ClearDebugOutputAction,
        ClearErrorLogAction,
        ClearConsoleAction,
        FindInScriptAction,
        FindNextInScriptAction,
        FindPreviousInScriptAction,
        GoToLineAction
    };

    ScriptRemoteTargetDebugger(QObject *parent = 0);
    ~ScriptRemoteTargetDebugger();

    void attachTo(const QHostAddress &address, quint16 port);
    void detach();

    bool listen(const QHostAddress &address = QHostAddress::Any, quint16 port = 0);

    bool autoShowStandardWindow() const;
    void setAutoShowStandardWindow(bool autoShow);

    QMainWindow *standardWindow() const;
    QToolBar *createStandardToolBar(QWidget *parent = 0);
    QMenu *createStandardMenu(QWidget *parent = 0);

    QWidget *widget(DebuggerWidget widget) const;
    QAction *action(DebuggerAction action) const;

Q_SIGNALS:
    void attached();
    void detached();
    void error(ScriptRemoteTargetDebugger::Error error);

    void evaluationSuspended();
    void evaluationResumed();

private Q_SLOTS:
    void showStandardWindow();

private:
    void createDebugger();
    void createFrontend();

private:
    ScriptRemoteTargetDebuggerFrontend *m_frontend;
    QScriptDebugger *m_debugger;
    bool m_autoShow;
    QMainWindow *m_standardWindow;

    Q_DISABLE_COPY(ScriptRemoteTargetDebugger)
};

#endif
