TEMPLATE = lib

isEmpty(ATOMBOX_INSPECT_PRI_INCLUDED) {
  include(../../../atombox_inspect.pri)
}

DESTDIR = $$ATOMBOX_INSPECT_SOURCE_TREE/lib/

CONFIG += static

INCLUDEPATH += .

DEPENDPATH += $$PWD

SOURCES +=    \
              $$PWD/scriptremotetargetdebugger.cpp \
              $$PWD/scriptdebuggermetatypes.cpp

HEADERS += \
              $$PWD/scriptremotetargetdebugger.h
              
DEFINES += QT_BUILD_INTERNAL
