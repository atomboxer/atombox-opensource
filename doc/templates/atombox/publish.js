/*global env: true */
var template = require('jsdoc/template'),
fs = require('jsdoc/fs'),
path = require('jsdoc/path'),
taffy = require('taffydb').taffy,
handle = require('jsdoc/util/error').handle,
helper = require('jsdoc/util/templateHelper'),
htmlsafe = helper.htmlsafe,
linkto = helper.linkto,
resolveAuthorLinks = helper.resolveAuthorLinks,
scopeToPunc = helper.scopeToPunc,
hasOwnProp = Object.prototype.hasOwnProperty,
data,
view,
outdir = env.opts.destination;


//
//  @mb
//
function setMarkdownConf(hash) {
    if (!env.conf.markdown) {
        env.conf.markdown = {};
    }
    var keys = Object.keys(hash);
    var storage = {};
    for (var i = 0; i < keys.length; ++i) {
        storage[keys[i]] = env.conf.markdown[keys[i]];
        // works because hash[key] is a scalar not an array/object
        env.conf.markdown[keys[i]] = hash[keys[i]];
    }
    return storage;
}
        
function CUSTOM_generateTheIndexPage() {
    
    // Generate custom index
    var indexData = {
        title:"Index",
    }
    var markdown = require('jsdoc/util/markdown');
    var markdown_index = path.join(env.dirname,"../index.md");
    var buffer = fs.readFileSync(markdown_index,'utf8');
    var storage = setMarkdownConf({parser: 'evilstreak'});
    var parser = markdown.getParser();
    
    var html = parser(buffer);
    html = html.replace(/<pre class="prettyprint source lang-js"><code>(.*)/g, '<pre class="brush: js;gutter:false;collapse:false">$1'); 
    html = html.replace(/<pre class="prettyprint source lang-ddl"><code>(.*)/g, '<pre class="brush: ddl;gutter:false;collapse:false">$1');
    html = html.replace(/<pre class="prettyprint source lang-plain"><code>(.*)/g, '<pre class="brush: plain;gutter:false;collapse:false">$1');
    indexData.content = html;
    html = view.render('index.tmpl', indexData);
    html = helper.resolveLinks(html);
    var indexPath = path.join(outdir, "index.html");
    fs.writeFileSync(indexPath, html,'utf8');
}

function find(spec) {
    return helper.find(data, spec);
}

function tutoriallink(tutorial) {
    return helper.toTutorial(tutorial, null, { tag: 'em', classname: 'disabled', prefix: 'Tutorial: ' });
}

function getAncestorLinks(doclet) {
    return helper.getAncestorLinks(data, doclet);
}

function hashToLink(doclet, hash) {
    if ( !/^(#.+)/.test(hash) ) { return hash; }
    
    var url = helper.createLink(doclet);
    
    url = url.replace(/(#.+|$)/, hash);
    return '<a href="' + url + '">' + hash + '</a>';
}

function needsSignature(doclet) {
    var needsSig = false;

    // function and class definitions always get a signature
    if (doclet.kind === 'function' || doclet.kind === 'class') {
        needsSig = true;
    }
    // typedefs that contain functions get a signature, too
    else if (doclet.kind === 'typedef' && doclet.type && doclet.type.names &&
             doclet.type.names.length) {
        for (var i = 0, l = doclet.type.names.length; i < l; i++) {
            if (doclet.type.names[i].toLowerCase() === 'function') {
                needsSig = true;
                break;
            }
        }
    }

    return needsSig;
}
function addSignatureParams(f) {
    var params = helper.getSignatureParams(f, 'optional');
    
    f.signature = (f.signature || '') + '('+params.join(', ')+')';
}

function addSignatureReturns(f) {
    var returnTypes = helper.getSignatureReturns(f);
    
    f.signature = '<span class="signature">'+(f.signature || '') + '</span>' + '<span class="type-signature">'+(returnTypes.length? ' &rarr; {'+returnTypes.join('|')+'}' : '')+'</span>';
}

function addSignatureTypes(f) {
    var types = helper.getSignatureTypes(f);
    
    f.signature = (f.signature || '') + '<span class="type-signature">'+(types.length? ' :'+types.join('|') : '')+'</span>';
}

function addAttribs(f) {
    var attribs = helper.getAttribs(f);
    
    f.attribs = '<span class="type-signature">'+htmlsafe(attribs.length? '<'+attribs.join(', ')+'> ' : '')+'</span>';
}
function shortenPaths(files, commonPrefix) {
    // always use forward slashes
    var regexp = new RegExp('\\\\', 'g');

    Object.keys(files).forEach(function(file) {
        files[file].shortened = files[file].resolved.replace(commonPrefix, '')
            .replace(regexp, '/');
    });

    return files;
}

function resolveSourcePath(filepath) {
    return path.resolve(process.cwd(), filepath);
}

function getPathFromDoclet(doclet) {
    if (!doclet.meta) {
        return;
    }

    var filepath = doclet.meta.path && doclet.meta.path !== 'null' ?
        doclet.meta.path + '/' + doclet.meta.filename :
        doclet.meta.filename;

    return filepath;
}

function generate(title, docs, filename, resolveLinks) {
    resolveLinks = resolveLinks === false ? false : true;

    var docData = {
        title: title,
        docs: docs
    };
    
    if (filename.match(/.*module.ddl.html.*/g)) {
        // Generate custom ddldoc
        var markdown = require('jsdoc/util/markdown');
        var markdown_index = path.join(env.dirname,"../ddl.md");
        var buffer = fs.readFileSync(markdown_index,'utf8');
        var storage = setMarkdownConf({parser: 'evilstreak'});
        var parser = markdown.getParser();
        var html1 = parser(buffer);
        html1 = html1.replace(/<pre class="prettyprint source lang-js"><code>(.*)/g, '<pre class="brush: js;gutter:false;collapse:false">$1'); 
        html1 = html1.replace(/<pre class="prettyprint source lang-ddl"><code>(.*)/g, '<pre class="brush: ddl;gutter:false;collapse:false">$1');
        html1 = html1.replace(/<pre class="prettyprint source lang-plain"><code>(.*)/g, '<pre class="brush: plain;gutter:false;collapse:false">$1');
       
        //html1 = view.render('index.tmpl', {title:"Atom/Box Data Model", content:html1});
        //html1 = helper.resolveLinks(html1);
        docs[0].description = html1;
        docs[0].name = "";

    }

    var outpath = path.join(outdir, filename),
    html = view.render('container.tmpl', docData);
    
    if (resolveLinks) {
        html = helper.resolveLinks(html); // turn {@link foo} into <a href="foodoc.html">foo</a>
    }
    
    fs.writeFileSync(outpath, html, 'utf8');
}

function generateSourceFiles(sourceFiles) {
    Object.keys(sourceFiles).forEach(function(file) {
        var source;
        // links are keyed to the shortened path in each doclet's `meta.filename` property
        var sourceOutfile = helper.getUniqueFilename(sourceFiles[file].shortened);
        helper.registerLink(sourceFiles[file].shortened, sourceOutfile);

        try {
            source = {
                kind: 'source',
                code: helper.htmlsafe( fs.readFileSync(sourceFiles[file].resolved, 'utf8') )
            };
        }
        catch(e) {
            handle(e);
        }

        generate('Source: ' + sourceFiles[file].shortened, [source], sourceOutfile,
                 false);
    });
}
/**
 * Create the navigation sidebar.
 * @param {object} members The members that will be used to create the sidebar.
 * @param {array<object>} members.classes
 * @param {array<object>} members.externals
 * @param {array<object>} members.globals
 * @param {array<object>} members.mixins
 * @param {array<object>} members.modules
 * @param {array<object>} members.namespaces
 * @param {array<object>} members.tutorials
 * @param {array<object>} members.events
 * @return {string} The HTML for the navigation sidebar.
 */
function buildNav(members) {
    //@mb 
    var nav = '<div id="multiAccordion">';
    //nav += '<a href="http://www.atombox.org"><img src="images/logo.png" style="border:none;" /></a>',
    //nav += '<a href="http://www.atombox.org"><img src="images/logo.png" style="border:none;" /></a>',
    seen = {};

    if (members.modules.length) {

        nav += '<h3><a href="#">Modules</a></h3><div><ul>';
        members.modules.forEach(function(m) {
            if ( !hasOwnProp.call(seen, m.longname) ) {
                nav += '<li>'+linkto(m.longname, m.name)+' </li>';
            }
            seen[m.longname] = true;
        });
        
        nav += '</ul></div>';
    }
    
    if (members.externals.length) {
        nav += '<h3><a href="#">Externals</a></h3><div><ul>';
        members.externals.forEach(function(e) {
            if ( !hasOwnProp.call(seen, e.longname) ) {
                nav += '<li>'+linkto( e.longname, e.name.replace(/(^"|"$)/g, '') )+'</li>';
            }
            seen[e.longname] = true;
        });
        
        nav += '</ul></div>';
    }

    if (members.classes.length) {
        var moduleClasses = 0;
        members.classes = members.classes.sort( function(a,b) {
            if (a.name > b.name)
                return 1;
            else if (a.name == b.name)
                return 0;
            else 
                return -1;
        });

        members.classes.forEach(function(c) {
            var skip = false;
            if (c.scope != undefined && c.scope == "global")
                skip = true;

            var moduleSameName = find({kind: 'module', longname: c.longname});
            if (moduleSameName.length) {
                c.name = c.name.replace('module:', 'require("')+'")';
                moduleClasses++;
                moduleSameName[0].module = c;
            }
            if (moduleClasses !== -1 && moduleClasses < members.classes.length) {
                nav += '<h3><a href="#">Classes</a></h3><div><ul>';
                moduleClasses = -1;
            }
            if ( !hasOwnProp.call(seen, c.longname) && !skip) {
                nav += '<li>'+linkto(c.longname, c.name)+'</li>';
            }

            if (!skip) 
                seen[c.longname] = true;
        });
        
        nav += '</ul></div>';
    }
    
    if (members.namespaces.length) {
        nav += '<h3>Namespaces</h3><ul>';
        members.namespaces.forEach(function(n) {
            if ( !hasOwnProp.call(seen, n.longname) ) {
                nav += '<li>'+linkto(n.longname, n.name)+'</li>';
            }
            seen[n.longname] = true;
        });
        
        nav += '</ul>';
    }
    
    if (members.mixins.length) {
        nav += '<h3>Mixins</h3><ul>';
        members.mixins.forEach(function(m) {
            if ( !hasOwnProp.call(seen, m.longname) ) {
                nav += '<li>'+linkto(m.longname, m.name)+'</li>';
            }
            seen[m.longname] = true;
        });
        
        nav += '</ul></div>';
    }

    // if (members.tutorials.length) {
    //     nav += '<h3>Tutorials</h3><ul>';
    //     members.tutorials.forEach(function(t) {
    //         nav += '<li>'+tutoriallink(t.name)+'</li>';
    //     });
    
    //     nav += '</ul></div>';
    // }

    if ( members.classes.length) {
        nav += '<h3><a href="#">Builtin Classes</a></h3><div><ul>';

        members.classes.forEach(function(g) {

            if ( g.tags != undefined) {
                var ok = false;
                g.tags.forEach(function(t) {
                    if (t.title == "builtin") {
                        ok = true;
                    }
                });

                if ( ok && g.kind !== 'typedef' && !hasOwnProp.call(seen, g.longname) ) {
                    nav += '<li>'+linkto(g.longname, g.name)+'</li>';
                }
            }

            seen[g.longname] = true;
        });

        nav += '</ul></div>';
    }

    if (members.globals.length) {
        nav += '<h3><a href="#">Builtin functions</a></h3><div><ul>';

        members.globals.forEach(function(g) {

            if ( g.tags != undefined) {
                var ok = false;
                g.tags.forEach(function(t) {
                    if (t.title == "builtin") {
                        ok = true;
                    }
                });

                if ( ok && g.kind !== 'typedef' && !hasOwnProp.call(seen, g.longname) ) {
                    nav += '<li>'+'<a href="global.html#'+g.name+'" >'+g.name+'</a></li>';
                }
            }

            seen[g.longname] = true;
        });

        nav += '</ul></div>';
    }

    
    
    return nav;
}


/**
   @param {TAFFY} taffyData See <http://taffydb.com/>.
   @param {object} opts
   @param {Tutorial} tutorials
*/
exports.publish = function(taffyData, opts, tutorials) {
    data = taffyData;

    var conf = env.conf.templates || {};
    conf['default'] = conf['default'] || {};
    var templatePath = opts.template;
    view = new template.Template(templatePath + '/tmpl');
    
    // claim some special filenames in advance, so the All-Powerful Overseer of Filename Uniqueness
    // doesn't try to hand them out later
    helper.getUniqueFilename('index');
    //helper.getUniqueFilename('global');

    // set up templating
    view.layout = 'layout.tmpl';
    //view.layout = 'layout_tutorial.tmpl';

    // set up tutorials for helper
    helper.setTutorials(tutorials);

    data = helper.prune(data);
    data.sort('longname, version, since');

    data().each(function(doclet) {
        doclet.attribs = '';
        
        if (doclet.examples) {
            doclet.examples = doclet.examples.map(function(example) {
                var caption, code;
                
                if (example.match(/^\s*<caption>([\s\S]+?)<\/caption>(\s*[\n\r])([\s\S]+)$/i)) {
                    caption = RegExp.$1;
                    code    = RegExp.$3;
                }
                
                return {
                    caption: caption || '',
                    code: code || example
                };
            });
        }
        if (doclet.see) {
            doclet.see.forEach(function(seeItem, i) {
                doclet.see[i] = hashToLink(doclet, seeItem);
            });
        }
    });
    
    // update outdir if necessary, then create outdir
    var packageInfo = ( find({kind: 'package'}) || [] ) [0];
    if (packageInfo && packageInfo.name) {
        outdir = path.join(outdir, packageInfo.name, packageInfo.version);
    }
    fs.mkPath(outdir);

    // copy static files to outdir
    var fromDir = path.join(templatePath, 'static'),
    staticFiles = fs.ls(fromDir, 3);

    staticFiles.forEach(function(fileName) {
        var toDir = fs.toDir( fileName.replace(fromDir, outdir) );
        fs.mkPath(toDir);
        fs.copyFileSync(fileName, toDir);
    });
    
    data().each(function(doclet) {
        var url = helper.createLink(doclet);
        helper.registerLink(doclet.longname, url);
    });
    
    data().each(function(doclet) {
        var url = helper.longnameToUrl[doclet.longname];

        if (url.indexOf('#') > -1) {
            doclet.id = helper.longnameToUrl[doclet.longname].split(/#/).pop();
        }
        else {
            doclet.id = doclet.name;
        }
        
        if (doclet.kind === 'function' || doclet.kind === 'class') {
            addSignatureParams(doclet);
            addSignatureReturns(doclet);
            addAttribs(doclet);
        }
    });
    
    // do this after the urls have all been generated
    data().each(function(doclet) {
        doclet.ancestors = getAncestorLinks(doclet);

        if (doclet.kind === 'member') {
            addSignatureTypes(doclet);
            addAttribs(doclet);
        }
        
        if (doclet.kind === 'constant') {
            addSignatureTypes(doclet);
            addAttribs(doclet);
            doclet.kind = 'member';
        }
    });
    

    var members = helper.getMembers(data);
    //members.tutorials = tutorials.children;

    var _obj = find({kind: 'function', scope: 'global'});
    for (var n in _obj) {
        members.globals[n] = _obj[n];
    }

    // add template helpers
    view.find = find;
    view.linkto = linkto;
    //view.tutoriallink = tutoriallink;
    view.htmlsafe = htmlsafe;

    // once for all
    view.nav = buildNav(members);
    if (members.globals.length ) { generate('Global', members.globals, 'global' + helper.fileExtension); }
    
    // index page displays information from package.json and lists files
    // var files = find({kind: 'file'}),
    // packages = find({kind: 'package'});

    //@mb
    // generate('Index',
    //          packages.concat(
    //              [{kind: 'mainpage', readme: opts.readme, longname: (opts.mainpagetitle) ? opts.mainpagetitle : 'Main Page'}]
    //          ).concat(files),
    //          'index' + helper.fileExtension);
    
    // set up the lists that we'll use to generate pages
    var classes = taffy(members.classes);
    var modules = taffy(members.modules);
    var namespaces = taffy(members.namespaces);
    var mixins = taffy(members.mixins);
    var externals = taffy(members.externals);
    
    for (var longname in helper.longnameToUrl) {
        if ( hasOwnProp.call(helper.longnameToUrl, longname) ) {
            var myClasses = helper.find(classes, {longname: longname});
            if (myClasses.length) {
                generate('Class: ' + myClasses[0].name, myClasses, helper.longnameToUrl[longname]);
            }
            
            var myModules = helper.find(modules, {longname: longname});
            if (myModules.length) {
                generate('Module: ' + myModules[0].name, myModules, helper.longnameToUrl[longname]);
            }

            var myNamespaces = helper.find(namespaces, {longname: longname});
            if (myNamespaces.length) {
                generate('Namespace: ' + myNamespaces[0].name, myNamespaces, helper.longnameToUrl[longname]);
            }
            
            var myMixins = helper.find(mixins, {longname: longname});
            if (myMixins.length) {
                generate('Mixin: ' + myMixins[0].name, myMixins, helper.longnameToUrl[longname]);
            }

            var myExternals = helper.find(externals, {longname: longname});
            if (myExternals.length) {
                generate('External: ' + myExternals[0].name, myExternals, helper.longnameToUrl[longname]);
            }
        }
    }

    CUSTOM_generateTheIndexPage();

    // TODO: move the tutorial functions to templateHelper.js
    function generateTutorial(title, tutorial, filename) {
        var tutorialData = {
            title: title,
            header: tutorial.title,
            content: tutorial.parse(),
            children: tutorial.children
        };
        
        var tutorialPath = path.join(outdir, filename),
        html = view.render('tutorial.tmpl', tutorialData);
        
        // yes, you can use {@link} in tutorials too!
        html = helper.resolveLinks(html); // turn {@link foo} into <a href="foodoc.html">foo</a>
        
        fs.writeFileSync(tutorialPath, html,'utf8');
    }
    
    // tutorials can have only one parent so there is no risk for loops
    function saveChildren(node) {
        node.children.forEach(function(child) {
            generateTutorial('Tutorial: ' + child.title, child, helper.tutorialToUrl(child.name));
            saveChildren(child);
        });
    }
    saveChildren(tutorials);
};
