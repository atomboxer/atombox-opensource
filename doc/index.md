# What is AtomBox?

AtomBox is a data modelling language and interpreter interconnected with a versatile JavaScript platform.

## AtomBox JavaScript platform

* A command line tool that interprets JavaScript built entirely in C++
* Runs natively on various platforms (AIX, Solaris, Windows, HP Nonstop - Guardian (tns/r and tns/e))
* Has an internal model based on atoms and boxes that binds the JavaScript platform to any domain specific languages used for describing messages and other arbitrary text or binary data
* Enables you to run standalone JavaScript programs on the server side by using a __asynchronous (non-blocking) JavaScript API__ to access file systems, terminals, databases, network, web, cryptography, and others
* Highly optimized and fully compliant with the latest ECMA 262 standard (5'th edition) 
* Based on JavaScriptCore that is part of the Apple's Webkit
* Graphical User Interface for remote debugging
* Nothing in common with JAVA

## AtomBox DDL 

* Custom data modeling language inspired from COBOL data division, fully compatible with HP NonStop DDL. This means that most of the COBOL data declarations or HP NonStop DDL can be translated with ease to AtomBox DDL and further be used in JavaScript.
* Used for describing various types of messages and other dynamic and unstructured data through:
       * Dynamic message definition (__VLENGTH DEPENDING ON__ clause)
       * Speculative parsing (__CHOICE__ clause)
       * Built-in and automatic validation (__MUST BE__ clause)
       * Default initial values (__DEFAULT__ clause)
       * Box redefines (__REDEFINES__ clause)
       * ISO-8583 messages (__ISOBITMAP__ clause) 
       * And others

* __Maps directly to the Atom/Box internal model so it can further be used by the JavaScript platform__

## Other parsers and interpretors

* MDBCSV parser. MDBCSV is a custom CSV model created by ACI Worldwide, common in the payments industry.
* IBM DFDL parser (not released yet). DFDL is an XTD based model used by the IBM Integration Bus.
 

# Getting started with AtomBox

## Download
Go to the [Download Page](http://www.inductum.com/atombox/download/) to download the latest version. 

## Install

### Windows
* Follow the installer steps 
* Optionally add the atombox\bin folder to the PATH Environment Variable

### HP NonStop - Guardian
* Make sure you download the correct version. AB for Guardian comes in two flavours:
    - AB for HP Nonstop - Guardian H06.20+ and J (newer machines, tns/e, file code 800). If in doubt, download this version.
    - AB for HP Nonstop - Guardian G06.20+ (older S series, tns/r, file code 700)

* Transfer the `abpak` archive to a sub-volume of your choice (in binary mode)
* Unpack the archive by running the following command (and replace $vol.subvol accordingly):
```
UNPAK abpak, *.*.*, vol $vol.subvol 
```

### UNIX
* Transfer the `atombox.tar.gz` archive to the UNIX box (in binary mode)
* Unpack the archive by running the following commands:
```
> gunzip atombox.tar.gz
> tar xf atombox.tar
```
* Add the `atombox\lib` directory to the `LD_LIBRARY_PATH` by replacing `<path>` accordingly. Note that you will have to execute this step for all the shell sessions where you want to run AtomBox. Also note that the `LD_LIBRAY_PATH` will be lost when the session is closed.
```
> export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<path>/atombox/lib
```
* Alternatively you can position to the atombox\bin directory and execute `. ./setenv` helper script
* __If the `mq` or `ctree` modules are needed the necessary library files need to be in `LD_LIBRARY_PATH` also__


## Hello World
In programming literature it has become the standard to create a hello world program as the first example. Here it is:

Create a text document called hellojs with the following content:

```js
//`require` (load) the console module
var console = require("console");
//`require` (load) the system module
var system = require("system");

//invoke the `write` function
console.write("Hello world!\n");

//exit the main event loop
system.exit(0);
```

### Executing `hellojs` on Windows
Go to the command prompt and run the following command after replacing `<path>` accordingly:
```
<path>\atombox\bin\ab.exe hellojs
```

### Executing `hellojs` on HP NonStop - Guardian
Go to the TACL console and run the following command after replacing `$vol.subvol` accordingly
```
run $vol.subvol.ab hellojs
```

### Executing `hellojs` on UNIX
Go to the shell and run the following command after replacing `<path>` accordingly:

```
./<path>/atombox/bin/ab hellojs
```

# Key AtomBox concepts

## Modules
AtomBox implements the CommonJS [`Modules/1.1`](http://wiki.commonjs.org/wiki/Modules/1.1) specification that addresses how modules should be written.

In a module, there is a:

* `require` function that accepts a module identifier and returns the exported API of the foreign module

* free variable called `exports` that is an Object that the module may add its API. This is the only means of exporting from a module.

The following is an example of a user created AtomBox module:

```js
//file math.js
exports.add = function() {
    var sum = 0, i = 0, args = arguments, l = args.length;
    while (i < l) {
        sum += args[i++];
    }
    return sum;
};
```

```js
//file increment.js
var add = require('math').add;
exports.increment = function(val) {
    return add(val, 1);
};
```

```js
//file program.js
var inc = require('increment').increment;
var a = 1;
inc(a); // 2
```

### Built-in modules

AtomBox has several modules compiled into the binary. These modules are described in greater detail elsewhere in this documentation (menu on the right) and are always preferentially loaded if their identifier is passed to the required function. 
## DDL AtomBox DDL

Dealing with arbitrary data has never been the strength of any scripting languages. This is especially true in the case of JavaScript which does not even have built-in capabilities to deal with raw data.

As already mentioned, the AtomBox DDL (together with `binary` module) provides a parser and interpreter for a custom language (DSL) called AtomBox DDL that is aiming at describing various types of unstructured binary data that maps directly to JavaScript classes. 

AtomBox DDL is compatible with HP NonStop DDL and provides additional language extensions.

The central point of loading an AtomBox DDL file is still the `required` function with a second string parameter "ddl" that indicates a DDL and not a JavaScript module. (for ACI's MDBCSV, the second string parameter needs to be  "mdbcsv")

There is a separate section dedicated to AtomBox DDL and data models at the following address (http://www.atombox.org/jsdoc/module-ddl.html)[http://www.atombox.org/jsdoc/module-ddl.html] (http://www.atombox.org/jsdoc/module-ddl.html)*

### A small DDL example

```ddl
!file helloddl!
DEFINITION hello_class.
    02 message.
        04 part1 PIC X(5).
        04 part2 PIC X(5) DEFAULT "world" MUST BE "world".
END.
```

```js
//file hellojs//
var console = require("console");
var system = require("system");

//load the ddl that describe the message
var Hello = require("helloddl","ddl").hello_class;

var o = new Hello(); //create an object of class Hello
o.message.part1.value = "Hello"; //change the value of the part1

//unpack the message, decode the resulting ByteArray to a String 
//and invoke the `write` function on the 'console' object
console.write(o.unpack().decodeToString());

system.exit(0);
```

## AtomBox event processing

In AtomBox, the asynchronicity is achieved through SIGNAL/SLOT event based constructs that are used for communicating between objects that makes it easy to implement the `Observer pattern` 

All activities in AtomBox are non-blocking. They return immediately and emit `SIGNALS` to all the connected `SLOT` functions (by using the `connect` function on an event).

Because AtomBox is single-threaded, it keeps an event loop internally that query the completion of the initiated operations. This is the reason why the `system.exit` needs to be invoked at the end of the AtomBox scripts where signals are not being required.

Node.js `Event Emitter` pattern can still be used in AtomBox by using the `events` module, but the internal objects will never emit this event types.

### A small example
The Timer class from the `timers` module provides repetitive timers. A [`timeout`](http://www.atombox.org/doc/html/module-timers.Timer.html#timeout) event is emitted when the timer set in the object expires.

```js
var console = require("console");
var Timer = require("timers").Timer;
//...
 
var t = new Timer();
 
//Connect the timeout() signal
t.timeout.connect(function() { 
    console.write("Timeout! \n");
});
 
t.start(2); // Will display "Timeout!" every two seconds
``` 

## Remote GUI debugging

Another key utility in AtomBox is the `bin\abinspect.exe` executable that is contained in the AtomBox Windows installer that enables you to initiate remote debugging sessions from the Windows box.

In order to start a remote debugging session:

1. Start AtomBox in debugging state by using -d and -p <port> switch as in the example below:

```
./ab -d -p 8150 hellojs
```

2. Start abinspect.exe, type in the `IP` and `port` of the remote server and click `connect`

!AtomBox debugger img](images/debugger.png)

## Compatibility mode

Historically, the AtomBox was relying on two backend JavaScript engines JavaScriptCore, part of WebKit and a custom built-in JavaScript engine. Nowadays (since version 0.4) only AtomBox for HP NonStop S-series (TNS/R) still uses the custom implementation.

To address any possible compatibility with the scripts built for older versions of AtomBox, __abcompat__ executable is still provided for all the platforms.


