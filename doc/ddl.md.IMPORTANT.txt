#AtomBox DDL Language Elements

A DDL source consists of statements that define data models (boxes) for further use by the AtomBox JavaScript Interpreter through the __require__ statement.

The language is similar to the HP DDL which is also based on COBOL data layout definition.

##Names
Names that are used in the The AtomBox DDL Interpreter are the names of the following
constructs:

* Constant Names
* Definition Names __(box names)__
* Field Names __(atom names)__
* Group Names  __(box names)__
* Enumeration item names

A name need to start with a letter or underscore (_) and can contain:

* Lowecase and uppercase letters [a-zA-Z]
* Decimal numeric digits [0-9]
* Hyphen (-)
* Underscore (_)

```
NOTES:
1. Even though the AtomBox DDL is not case sensitive in regards to keywords, the AtomBox JavaScript interpretor _is_
case sensitive, therefore the names are to be considered case sensitive as well.
2. Because the hyphens (-) have different meanings in JavaScript, when DDLs are imported into the JavaScript
interpretor all the hypens are replaced with underscores.
```

##Numbers
The DDL interepreter recognizes the following:

* Decimal numbers
An unsigned numbers are positive by default; positive or negative numbers can also be specified with a + or - sign.
Any plus or minus sign _must_ precede the number.
* Octal numbers
Prefixed by %0, a sign is not permitted.
* Hex numbers
Prefixed by %h or %H, a sign is not permitted. 
* Binary numbers
Prefixed by %b or %B, a sign is not permitted.

Some number examples:
```
    44        <--- positive decimal value
    -1        <--- negative decimal value
   %0115      <--- octal value (77 in decimal)
   %h4d       <--- hex value (77 decimal)
   %b1001101  <--  binary value (77 decimal)
```

##Strings
A DDL string is a combination of any ASCII characters enclosed in quotation marks.

##Keywords

The following table depicts the keywords currently implemented in AtomBox DDL.

```ddl
--- AtomBox DDL keywords (compatible with HP DDL)
BEGIN BE BINARY BIT CHARACTER COMP COMPUTATIONAL COMP-3 COMPUTATIONAL-3 CONSTANT DEF DEFINITION FILLER RECORD
DEPENDING DISPLAY END ENUM FLOAT IS JUST JUSTIFIED MUST NO NOT OCCURS ON PACKED PACKED-DECIMAL PIC PICTURE REDEFINES
RIGHT THROUGH THRU TIMES TO TYPE UNSIGNED UPSHIFT USAGE VALUE

---Non HP DDL AtomBox keywords
ASCII BCD CHARSET CHOICE DEFAULT DELIMITER LEFT LPAD EBCDIC INCLUSIVE ISOBITMAP REGEX RPAD VALUES VLENGTH 
```

In addition to the keywords above, in order to provide compatibilty with the HP DDL files, the next keywords are also 
to be considered keywords in AtomBox DDL. 

```
--- HP DDL Keywords (for compatibility only)
ALL ALLOWED ARE AS ASCENDING ASSIGNED AUDIT AUDITCOMPRESS BLOCK BUFFERED BUFFERSIZE BY CFIELDALIGN_MATCHED2 
C_MATCH_HISTORIC_TAL CODE COMPLEX COMPRESS CRTPID CURRENT DATE DATETIME DAY DCOMPRESS DELETE DESCENDING DEVICE 
DUPLICATES EDIT-PIC ENTRY-SEQUENCED EXIT EXT EXTERNAL FILE FIELDALIGN_SHARED8 FNAME32 FOR FRACTION HEADING HELP
HIGH-NUMBER HIGH-VALUE HOUR ICOMPRESS INDEX INDEXED INTERVAL KEY KEY-SEQUENCED KEYTAG LOGICAL LOW-NUMBER LOW-VALUE 
LOW-VALUES MAXEXTENTS MINUTE MONTH NOVALUE NOVERSION NULL ODDUNSTR OF OUTPUT PHANDLE QUOTE QUOTES REFRESH RELATIVE 
RENAMES SECOND SEQ SEQUENCE SERIALWRITES SETLOCALENAME SHOW SPACE SPACES SPI-NULL SQL SQLNULL SQL-NULLABLE SSID SUBVOL
SYSTEM TACL TALUNDERSCORE TEMPORARY TIME TIMESTAMP TOKEN-CODE TOKEN-MAP TOKEN-TYPE TRANSIDTSTAMP UNSTRUCTURED UPDATE
USE USERNAME VARCHAR VARYING VERIFIEDWRITESVERSION YEAR ZERO ZEROES ZEROS KEY-SEQUENCED RELATIVE ENTRY-SEQUENCED
UNSTRUCTURED AUDIT AUDITCOMPRESS BLOCK BUFFERED BUFFERSIZE CODE COMPRESS DCOMPRESS ICOMPRESS EXT MAXEXTENTS ODDUNSTR
REFRESS SERIALWRITES VERIFYWRITES DUPLICATES ALLOWED UPDATE SEQUENCE ASCENDING DESCENDING
```

##Comments

* asterisk (*) at the beggining of a line specifies that the whole line is a comment
* double dash (--) marks the beginning of a comment that untill the end of the line
* exclamation mark (!) marks the begining of a comment that can be ended with another exclamation mark sign(!) or end of line.

```ddl
    --This is a comment
    CONSTANT pi VALUE !this is another comment! IS "3.14". 
    
```

##Statements
@todo









































--------------------------------------------------------------------
Defining atoms:

You can have Atoms that store values of the following data types:


* Atoms with text representation
* String   
* Numeric   
* Alphabetic
* Atoms with binary representation

   

      * Unsigned

        * uint8, uint16, uint32, uint64, float, double

      * Signed

          * int8, int16, int32, int64

    * Bitmap

      * BCD (Binary Coded Decimal)

* Packed BCD (Packed Binary Coded Decimal)

* Packed hex


-----------------------------------------------------------------------------------------

PICTURE CLAUSE

In AtomBox DDL this is a way to describe atoms, through the use of COBOL like constructs. A picture is a means of describing data by using simple characters that indicate the atom (item) characteristics and size.
Each PIC item is then mapped to one of AtomBox Atoms depending on the data being defined.


-----------------------------------------------------------------------------------------

PICTURE_clause ::= (PIC | PICTURE) ["] (pic-string-ascii | pic-string-numeric) ["]

*

* This is a character string that specifies the data type and size of an atom (field).

*

pic-string-ascii  ::= [“] (('A' | 'X')+ ( '(' 'length' ')' )* )+ [“]

   - X represents an ASCII character.

   - A represents a letter or a blank.

   - length is a one to five digit integer, enclosed in parentheses that specifies the number of times the preceding symbol is repeated.



*

* This is a character string that specifies the data type and size of an atom (field).

*/

pic-string-numeric  ::= ('S')* (('9')+ ( '(' 'length' ')' )*)+ ('V')* ('S')*

   - 9 represents an ASCII digit [0-9]

   - S represents a separate sign character in a signed numeric field. It can be either in the front of the pic-string or at the end.

   - T represents a numeric character with an implied embedded sign. It can be either in the front of the pic-string or at the end.

   - length is a one to five digit integer, enclosed in parentheses that specifies the number of times the preceding symbol is repeated.

   - V represents an implied decimal point location in a numeric field.

@note: If two or more symbols X, A or 9 are used in the same picture string, the type of atom being created is a String.



Examples:

------------------------------------------------------------------------------------



Field_Definition ::= ('DEF'|'DEFINITION') name (PICTURE_clause|TYPE_clause) 
                     (AS_clause)?
                     ('BEGIN')?
                     (DISPLAY_clause)?
                     (JUSTIFIED_clause|LPAD_clause|RPAD_clause)?
                     (MUST_BE_clause)?
                     (NULL_clause)?
                     (UPSHIFT_clause)?
                     (USAGE_clause)?
                     (VALUE_clause)?  
                      '.'
                     (enum_89_clause)* ('END' ('.')?)?

Group_Definition ::= ('DEF'|'DEFINITION') name
                     (DISPLAY_clause)*
                     (CHARSET_clause)*
                     line_item_specification+
                     'END' '.'?

line_item_specification ::= level_number (name | FILLER)
                            (PICTURE_clause | TYPE_clause)
                            (AS_clause)?
                            (DISPLAY_clause)?
                            (JUSTIFIED_clause|LPAD_clause|RPAD_clause)?
                            (MUST_BE_clause)?
                            (NULL_clause)?
                            (OCCURS_clause | OCCURS_DEPENDING_ON_clause)?
                            (REDEFINES_clause)?
                            (USAGE_clause)?
                            (VALUE_clause)?
                            (enum_89_clause '.')*

PICTURE_clause ::= (PIC | PICTURE) ["] (pic-string-ascii | pic-string-numeric) ["]

pic-string-ascii  ::= [“] (('A' | 'X')+ ( '(' 'length' ')' )* )+ [“]
pic-string-numeric  ::= ('S')? (('9')+ ( '(' 'length' ')' )*)+ ('V')? ('S')?
