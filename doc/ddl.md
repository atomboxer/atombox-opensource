# AtomBox Data Model

While on the web JSON is the de-facto standard for data interoperability, for historical reasons many industries still use general text and binary data for electronic data interchange.

The real strength of AtomBox doesn’t come from the cross-platform JavaScript interpreter, and the comprehensive APIs, but from the AtomBox Data Model that aims at providing a means of modelling various types of complex data structures that can be mapped to JavaScript classes. 

Currently AtomBox main way through which you can define an Atom/Box mode is through a custom DSL (Domain Specific Language) called AtomBox DDL, which is based on HP NonStop DDL with custom AtomBox vocabulary extensions (the second option is MDBCSV parsing of the ACI Worldwide Base24-eps application). See @todo for more detail on the AtomBox DDL syntax.

AtomBox Data model has two key abstractisations: the Atom and the Box. 

The model can be imagined as a binary tree made with Boxes and Atoms, where the Atoms represent a leaf.

## The Box

The Box is the container of other Boxes and Atoms and it does not have a value.

It packs (consumes) binary data into the atoms and boxes it contains which in turn will pack the data into their internal contents.

It unpacks (reconstructs) binary data from the atoms and boxes it contains, which in turn unpack their internal contents.

A Box has one internal state depending on their content states as below:

 * `OK_STATE` – all atoms and boxes contained are in the `OK_STATE`, all data passed validation
 * `WARNING_STATE` – at least one boxes or atoms contained is in `WARNING_STATE`,  by failing validation against constraints or data types
 * `ERROR_STATE` - at least one of the boxes or atoms contained is in `ERROR_STATE`, by falling to pack (or set) some data into the internal structures.  The content of a Box in `ERROR_STATE` is unreliable. 
 * `DEFAULT_STATE` – when no data has been packed into the structure, all of its contents are in the initial state.

A Box has a name that is used to address a specific leaf in the Atom/Box tree, and a length which is the cumulative length of its contents.

__The Box artifact maps to the [module:ddl.Box](module-ddl.Box.html) class __

## The Atom

The Atom is the data unit storage which actually consumes the data from the data stream, performs data validation, and applies constraints.

There are various types of Atoms, depending on the data types it packs as depicted in the class hierarchy below:
![AtomBox class hierarchy](images/atombox_cl.png)

As the Box, an Atom also packs (consumes) binary data into the internal structure and applies the validation constraints.

It unpacks (reconstructs) the binary data from the internal storage.

It has an internal state depending on their content states as below:

 *	`OK_STATE` –all data passed validation (length, delimiter, type and constraints).
 *	`WARNING_STATE` –  failed validation against constraints
 *	`ERROR_STATE` – The atom has failed validation, and the internal data stored is unreliable (length or type validation has failed).
 *	`DEFAULT_STATE` –When the atom is in the initial state, and stores default values.


__The Atom artifact maps to the [module:ddl.Atom](module-ddl.Atom.html) JavaScript class__
