#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include <QtCore/QStringList>

#ifdef _GUARDIAN_TARGET
#include <QtSql/QSqlDriverPlugin>
#include <QtSql/QSQLiteDriver>
#endif

#include "sqlplugin.h"

#include "databaseprototype.h"
#include "queryprototype.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptsql_compat, SqlPlugin )
#else
Q_EXPORT_PLUGIN2( abscriptsql, SqlPlugin )
#endif


SqlPlugin::SqlPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
    
SqlPlugin::~SqlPlugin()
{
}

Q_DECLARE_METATYPE (Database*)
Q_DECLARE_METATYPE (Query*)

QScriptValue 
Database_Constructor(QScriptContext *ctx, QScriptEngine *engine)
{
    if (ctx->argument(0).isUndefined()) {
        ctx->throwError("Database.contructor. Database type not specified (try sqlite)");
        return QScriptValue();
    }

    QString dbType = ctx->argument(0).toString();

    Database *d = NULL;
    if (ctx->argument(1).isUndefined())
        d = new Database(dbType);
    else
        d = new Database(dbType, ctx->argument(1).toString());

    if(!d->isValid()) {
        ctx->throwError("Database.contructor. Database driver not found");
        return QScriptValue();
    }

    QScriptValue obj =  engine->newQObject(d, QScriptEngine::AutoOwnership,
                                           QScriptEngine::SkipMethodsInEnumeration
                                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
    return obj;
}

QScriptValue 
Query_Constructor(QScriptContext *ctx, QScriptEngine *engine)
{
    Query *query_obj = NULL;

    if (ctx->argument(0).isUndefined()) {
        query_obj = new Query();
    } else {
        Database *d = NULL;    
        d = qscriptvalue_cast<Database*>(ctx->argument(0));
        if (d) {
            query_obj = new Query(d);
        } else {
            ctx->throwError("Query.contructor. Unrecognized parameter.");
            return QScriptValue();
        }
    }

    QScriptValue obj =  engine->newQObject(query_obj, QScriptEngine::AutoOwnership,
                                           QScriptEngine::SkipMethodsInEnumeration
                                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
    return obj;
}

void
SqlPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("sql_compat") ) {
#else
    if ( key == QString("sql") ) {
#endif
#ifdef AB_STATIC_PLUGINS
    Q_INIT_RESOURCE(sql);
#endif
    Q_INIT_RESOURCE(jssqlfiles);

    QScriptValue __internal__;

    __internal__ = engine->globalObject().property("__internal__");
    if (!__internal__.isObject()) {
        __internal__ = engine->newObject();
        engine->globalObject()
        .setProperty("__internal__",__internal__);
    }
        
        engine->setDefaultPrototype(qMetaTypeId<Database*>(),
                                    engine->newQObject(new DatabasePrototype(this)));
    engine->globalObject().property("__internal__").
        setProperty("Database", engine->newFunction(Database_Constructor));
        engine->globalObject().property("__internal__").property("Database").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Database*>()));

        engine->setDefaultPrototype(qMetaTypeId<Query*>(),
                                    engine->newQObject(new QueryPrototype(this)));
    engine->globalObject().property("__internal__").
        setProperty("Query", engine->newFunction(Query_Constructor));
        engine->globalObject().property("__internal__").property("Query").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Query*>()));
    }
}
    
QStringList
SqlPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("sql_compat");
#else
    keys << QString("sql");
#endif
    
    return keys;
}

#ifdef _GUARDIAN_TARGET
QT_BEGIN_NAMESPACE

class QSQLiteDriverPlugin : public QSqlDriverPlugin
{
public:
    QSQLiteDriverPlugin();

    QSqlDriver* create(const QString &);
    QStringList keys() const;
};

QSQLiteDriverPlugin::QSQLiteDriverPlugin()
    : QSqlDriverPlugin()
{
}

QSqlDriver* QSQLiteDriverPlugin::create(const QString &name)
{
    if (name == QLatin1String("QSQLITE")) {
        QSQLiteDriver* driver = new QSQLiteDriver();
        return driver;
    }
    return 0;
}

QStringList QSQLiteDriverPlugin::keys() const
{
    QStringList l;
    l  << QLatin1String("QSQLITE");
    return l;
}

Q_EXPORT_STATIC_PLUGIN(QSQLiteDriverPlugin)
Q_EXPORT_PLUGIN2(qsqlite, QSQLiteDriverPlugin)

QT_END_NAMESPACE 

#endif
