#ifndef QUERYPROTOTYPE_H
#define QUERYPROTOTYPE_H

#include "query.h"

#include <QtCore/QObject>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptContext>

class QueryPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    QueryPrototype(QObject *parent = 0);
    ~QueryPrototype();

public slots:
    inline int at() const { return thisQuery()->at(); }
    inline void clear() const { thisQuery()->clear(); }
    inline bool exec( const QString &q ) const  { bool ret=thisQuery()->exec(q); 
        if (!ret) context()->throwError(thisQuery()->error()); return ret;}
    inline bool exec() const { bool ret=thisQuery()->exec(); 
        if (!ret) context()->throwError(thisQuery()->error()); return ret;}
    inline QString executedQuery() const { return thisQuery()->executedQuery(); }
    inline bool first() const { return thisQuery()->first(); }
    inline bool isActive() const { return thisQuery()->isActive(); }
    inline bool isNull(int index) const { return thisQuery()->isNull(index); }
    inline bool isSelect() const { return thisQuery()->isSelect(); }
    inline bool isValid() const { return thisQuery()->isValid(); }
    inline bool last() const { return thisQuery()->last(); }
    inline QString error() const { return thisQuery()->error(); }
    inline bool next() const { return thisQuery()->next(); }
    inline int numRowsAffected() const { return thisQuery()->numRowsAffected(); }
    inline bool prepare(const QString &q) const { bool ret=thisQuery()->prepare(q);
        if (!ret) context()->throwError(thisQuery()->error()); return ret; }
    inline bool previous() const { return thisQuery()->previous(); }
    inline bool seek(int index, bool relative=false) const { bool ret=thisQuery()->seek(index, relative);
        if (!ret) context()->throwError(thisQuery()->error()); return ret;}
    inline QVariant value(int index) const { return thisQuery()->value(index); }

private:
    Query *thisQuery() const;
};

#endif //QUERYPROTOTYPE_H
