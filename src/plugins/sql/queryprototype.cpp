#include "databaseprototype.h"
#include "queryprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


Q_DECLARE_METATYPE(Database*)
Q_DECLARE_METATYPE(Query*)

QueryPrototype::QueryPrototype(QObject *parent)
{
}

QueryPrototype::~QueryPrototype()
{

}

Query*
QueryPrototype::thisQuery() const
{
    Query *q = qscriptvalue_cast<Query*>(thisObject());
    Q_ASSERT(q);
    return q;
}


