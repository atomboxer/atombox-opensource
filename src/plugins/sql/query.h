#ifndef QUERY_H
#define QUERY_H

#include "database.h"

#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

class Query : public QObject
{
    Q_OBJECT

    friend class Database;

public:
    Query();
    Query(const QString &q);
    Query(const QString &q, Database *d);
    Query(Database *d);

    virtual ~Query();
    
    inline int at() const { return q_.at(); }
    inline void clear() const { return q_.clear(); }
    inline bool exec( const QString &q ) const  { return q_.exec(q); }
    inline bool exec() const { return q_.exec(); }
    inline QString executedQuery() const { return q_.executedQuery(); }
    inline bool first() const { return q_.first(); }
    inline bool isActive() const { return q_.isActive(); }
    inline bool isNull(int index) const { return q_.isNull(index); }
    inline bool isSelect() const { return q_.isSelect(); }
    inline bool isValid() const { return q_.isValid(); }
    inline bool last() const { return q_.last(); }
    inline QString error() const { return q_.lastError().text(); }
    inline bool next() const { return q_.next(); }
    inline int numRowsAffected() const { return q_.numRowsAffected(); }
    inline bool prepare(const QString &q) const { return q_.prepare(q); }
    inline bool previous() const { return q_.previous(); }
    inline bool seek(int index, bool relative=false) const { return q_.seek(index, relative); }
    inline QVariant value(int index) const { return q_.value(index); }

private:
    mutable QSqlQuery q_;
};

#endif // query.h 
