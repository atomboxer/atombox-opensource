
include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptsql
} else {
    TARGET         = abscriptsql_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script

QT += sql

!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core sql
    }
} else {
    QT            += core script 
}

DEPENDPATH += .
INCLUDEPATH += .
#LIBS += -L$$QT_BUILD_DIR/../plugins/sqldrivers/ -lqsqldb2

HEADERS  = \
           sqlplugin.h \
           database.h \
           databaseprototype.h \
           query.h \
           queryprototype.h

SOURCES  = \
           sqlplugin.cpp \
           database.cpp \
           databaseprototype.cpp \
           query.cpp \
           queryprototype.cpp

RESOURCES += jssqlfiles.qrc

static {
    RESOURCES += sql.qrc
}
else {
    CONFIG   += qt plugin
    isEmpty(USE_SCRIPT_CLASSIC) {
         QT += script sql
    }
}

OTHER_FILES += \
    js/sqlite.js
