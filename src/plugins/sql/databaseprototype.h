#ifndef DATABASEPROTOTYPE_H
#define DATABASEPROTOTYPE_H

#include "database.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class DatabasePrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    DatabasePrototype(QObject *parent = 0);
    ~DatabasePrototype();

public slots:
    inline void close() const { return thisDatabase()->close(); } 
    inline bool commit() const { return thisDatabase()->commit(); } 
    inline QString databaseName() const { return thisDatabase()->databaseName(); } 
    inline QSqlQuery exec(QString q = QString()) const { return thisDatabase()->exec(q); } 
    inline QString hostName() const { return thisDatabase()->hostName(); } 
    inline bool isOpen() const { return thisDatabase()->isOpen(); }
    inline QString error() const { return thisDatabase()->error(); }
    inline bool open() const { return thisDatabase()->open(); }
    inline bool open(const QString user, const QString password) const { return thisDatabase()->open(user,password); }
    inline QString password() const { return thisDatabase()->password(); }
           void setDatabaseName(QString name) const;
    inline void setHostName(const QString name) const { return thisDatabase()->setHostName(name); }
    inline void setPassword(const QString pass) const { return thisDatabase()->setPassword(pass); }
    inline void setUserName(const QString name) const { return thisDatabase()->setUserName(name); }
    inline QStringList tables() const { return thisDatabase()->tables(); }
    inline bool transaction() const { return thisDatabase()->transaction(); }
    inline QString userName() const { return thisDatabase()->userName(); }

private:
    Database *thisDatabase() const;
};

#endif //DATABASEPROTOTYPE_H
