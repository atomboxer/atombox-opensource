#include "databaseprototype.h"
#include "queryprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Database*)
Q_DECLARE_METATYPE(Query*)

DatabasePrototype::DatabasePrototype(QObject *parent)
{
}

DatabasePrototype::~DatabasePrototype()
{

}

Database*
DatabasePrototype::thisDatabase() const
{
    Database *d = qscriptvalue_cast<Database*>(thisObject());
    Q_ASSERT(d);
    return d;
}

void 
DatabasePrototype::setDatabaseName(QString name) const 
{ 
#ifdef _GUARDIAN_TARGET
    if (thisDatabase()->db().driverName() == "QSQLITE") {
        if (name.length() >=8) {
            context()->throwError("Database.prototype.setDatabaseName. Length should be maximum of 7 for sqlite");
            return;
        }
    }
#endif
    return thisDatabase()->setDatabaseName(name); 
}
