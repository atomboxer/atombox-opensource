#ifndef DATABASE_H
#define DATABASE_H


#include <QtCore/QObject>
#include <QtCore/QStringList>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

class Query;
class DatabasePrototype;
class Database : public QObject
{
    Q_OBJECT

    friend class Query;
    friend class DatabasePrototype;
public:
    Database(const QString dbType);
    Database(const QString dbType, const QString conn);
 
    virtual ~Database();

    inline void close() const { return db_.close(); } 
    inline bool commit() const { return db_.commit(); } 
    inline QString databaseName() const { return db_.databaseName(); } 
    inline QSqlQuery exec(QString q = QString()) const { return db_.exec(q); } 
    inline QString hostName() const { return db_.hostName(); } 
    inline bool isOpen() const { return db_.isOpen(); }
    inline QString error() const { return db_.lastError().databaseText(); }
    inline bool open() const { return db_.open(); }
    inline bool open(const QString &user, const QString &password) const { return db_.open(user,password); }
    inline QString password() const { return db_.password(); }
    inline void setDatabaseName(QString &name) const { return db_.setDatabaseName(name); }
    inline void setHostName(const QString &name) const { return db_.setHostName(name); }
    inline void setPassword(const QString &pass) const { return db_.setPassword(pass); }
    inline void setUserName(const QString &name) const { return db_.setUserName(name); }
    inline QStringList tables() const { return db_.tables(); }
    inline bool transaction() const { return db_.transaction(); }
    inline QString userName() const { return db_.userName(); }


    inline bool isValid() const { return db_.isValid(); }
    
private:
    QSqlDatabase db() const { return db_; }
    mutable QSqlDatabase db_;
};

#endif // database.h 
