
#include "ctreeprototype.h"
#include "ctreestream.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Ctree*)
Q_DECLARE_METATYPE(CtreeStream*)

CtreePrototype::CtreePrototype(QObject *parent)
    : QObject(parent)
{
}

CtreePrototype::~CtreePrototype()
{
}

void
CtreePrototype::connect(QString server, QString username, QString password) const
{
    if (server == "") {
        context()->throwError("<<ctree/ctree.connect>> constructor requires at least one argument");
    }

    thisCtree()->connect(server, username, password);

    if (!thisCtree()->isConnected()) {
        context()->throwError( thisCtree()->lastErrorString() );
    }
}

void
CtreePrototype::disconnect() const
{
    thisCtree()->disconnect();
}

bool
CtreePrototype::isConnected() const
{
    return thisCtree()->isConnected();
}


QScriptValue
CtreePrototype::open( QString filename, bool exclusive, QString pass )
{
    CtreeStream *cs = new CtreeStream();

    cs->open(filename, exclusive, pass);

    if (cs->isOpened())
        return engine()->newQObject( cs, 
                                     QScriptEngine::AutoOwnership,
                                     QScriptEngine::SkipMethodsInEnumeration);
    else {
        context()->throwError( cs->lastErrorString() );
        delete cs;
        return QScriptValue();
    }
}

Ctree *
CtreePrototype::thisCtree() const
{
    Ctree *p = qscriptvalue_cast<Ctree*>(thisObject());

    if (!p) {
        qFatal("Programmatic error CtreePrototype::thisCtree");
    }

    return p;
}

void
CtreePrototype::beginTransaction()
{
    if (!thisCtree()->beginTransaction()) {
        context()->throwError( thisCtree()->lastErrorString() );
    }
}

void
CtreePrototype::endTransaction()
{
    if (!thisCtree()->endTransaction()) {
        context()->throwError( thisCtree()->lastErrorString() );
    }
}

void
CtreePrototype::abortTransaction()
{
    if (!thisCtree()->abortTransaction()) {
        context()->throwError( thisCtree()->lastErrorString() );
    }
}
