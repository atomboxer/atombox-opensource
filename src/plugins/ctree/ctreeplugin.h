#ifndef CTREEPLUGIN_H
#define CTREEPLUGIN_H

#include <QScriptExtensionPlugin>

class CtreePlugin : public QScriptExtensionPlugin
{
public:
    CtreePlugin( QObject *parent = 0);
    ~CtreePlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //CTREEPLUGIN_H
