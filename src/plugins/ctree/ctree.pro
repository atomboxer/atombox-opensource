include(../plugins.pri)

!isEmpty(ATOMBOXER_BUILD_CTREE) {
    isEmpty(USE_SCRIPT_CLASSIC) {
        TARGET         = abscriptctree
    } else {
        TARGET         = abscriptctree_compat
    }

    #DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
    
    DEPENDPATH  += . 
    INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/main/ $$ATOMBOXER_SOURCE_TREE/src/plugins/core . $$CTREE_DIR/include 

    win32 {
	    INCLUDEPATH += $$CTREE_DIR/include/sdk/ctree.isam/
    }

    aix-xlc-64 {
	    INCLUDEPATH += $$CTREE_DIR/include/sdk/ctree.isam/multithreaded/static/
        QMAKE_CXXFLAGS += -DDH_OS_UNIX -DDH_64BIT_MODEL
    }

    solaris-cc-64-stlport {
        INCLUDEPATH += $$CTREE_DIR/include/sdk/ctree.isam/multithreaded/dynamic/
        QMAKE_CXXFLAGS += -DDH_OS_UNIX -DDH_OS_UNIX_SOLARIS -DDH_64BIT_MODEL
    }

    linux-g++ {
        INCLUDEPATH += $$CTREE_DIR/include/sdk/ctree.isam/multithreaded/dynamic/
                       QMAKE_CXXFLAGS += -DDH_64BIT_MODEL -DDH_OS_UNIX -DDH_OS_LINUX -fPIC
    }


    HEADERS  = \
        ctreeplugin.h \
        ctreeclass.h \
        ctreeprototype.h \
        ctreestream.h \
        ctreestreamprototype.h \

    
    SOURCES  = \
        ctreeplugin.cpp \
        ctreeclass.cpp \
        ctreestream.cpp \
        ctreestreamprototype.cpp \
        ctreeprototype.cpp

    RESOURCES += ctreefiles.qrc

    static {
        RESOURCES  += ctree.qrc
        LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/
    } else {
        CONFIG += qt plugin

        isEmpty(USE_SCRIPT_CLASSIC) {
            QT += script
            LIBS += -labscriptcore
        } else {
            LIBS += -labscriptcore_compat
        }

        win32:!tandem {
            LIBS +=  $$quote($$CTREE_DIR/lib/ctree.isam/Microsoft Visual Studio 2010/mtclient.lib)
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/runtmchk.lib  
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/ftol2.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/lldiv.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/ulldiv.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/ullshr.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/llmul.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/llrem.obj \
                $$ATOMBOXER_SOURCE_TREE/lib/lldvrm.obj 

            LIBS +=-lmingw32 -lmoldname -lmingwex -lgcc
            LIBS +=  -lgcc -ladvapi32 -lshell32 -luser32 -lkernel32  -lcrypt32 -lwsock32 -lgdi32
	    }

	    aix-xlc-64  {
            LIBS +=  $$CTREE_DIR/lib/ctree.isam/multithreaded/static/libmtclient.a
	    } 
            solaris-cc-64-stlport {
            LIBS += -L$$CTREE_DIR/lib/ctree.isam/multithreaded/dynamic/ -lmtclient -lsocket -lrt -ldl
	    }
            linux-g++ {
            LIBS += $$CTREE_DIR/lib/ctree.isam/multithreaded/dynamic/libmtclient.a -lrt -ldl
	    }

    }
}
