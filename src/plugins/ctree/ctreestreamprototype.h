#ifndef CSTREAMPROTOTYPE_H
#define CSTREAMPROTOTYPE_H

#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"
#include "stream.h"

#include "streamprototype.h"
#include "ctreestream.h"

class CtreeStreamPrototype : public StreamPrototype
{
    Q_OBJECT

public:
    CtreeStreamPrototype(QObject *parent = 0);
    ~CtreeStreamPrototype();

public slots:
    void  close();
    bool  atEnd();
    bool  isClosed();
    bool  isReadable();
    bool  isSequential();
    bool  isWritable();
    QScriptValue read(unsigned bytes = 4096);
    bool lockRecord();
    bool unlockRecord();
    bool setKey(QScriptValue key, short spec = 0);
    QScriptValue getKey();

public slots:
    //not applicable
    void  copy(QIODevice *io);
    void  flush();
    QScriptValue readInto(QScriptValue buffer, QScriptValue begin=QScriptValue(), 
                          QScriptValue end = QScriptValue() );

    bool  skip(int bytes);
    void  write(QScriptValue binary, QScriptValue begin=QScriptValue(), 
                QScriptValue end = QScriptValue() );

    void   __internal__setPosition( QScriptValue position );
    qint64 __internal__getPosition();

    qint64 bytesAvailable();
 
private:
    virtual QString errorString();
    virtual CtreeStream* thisCtreeStream();
};

#endif //CTREESTREAMPROTOTYPE_H
