
#include <QtCore/QByteArray>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>


#include "ctreestream.h"

#define NO_ctFeatOPENSSL
#include <ctreep.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define CTREESTREAM_DEBUG

#define MAX_IFILL_SIZE  2000
#define MAX_RECORD_SIZE 8192
#define MAX_KEY_SIZE    999

CtreeStream::CtreeStream ( QObject * parent ) :
    QObject(parent),
    last_error_(""),
    at_end_(false),
    need_reread_(false)
{
    ifil_ptr_ = new char[MAX_IFILL_SIZE];
    memset((void*)ifil_ptr_, 0, MAX_IFILL_SIZE);
    fileno_ = -1;
}

CtreeStream::~CtreeStream()
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::~CtreeStream()";
#endif
    if (isOpened()&&ifil_ptr_) {
        CloseIFile((ifil*)ifil_ptr_);
    }

    if (ifil_ptr_)
        delete []ifil_ptr_;
}

bool
CtreeStream::deleteCurrentRecord()
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::deleteCurrentRecord()";
#endif

   COUNT retval;

   retval = DeleteVRecord(fileno_);

   if (retval != NO_ERROR) {
       if (retval == TTYP_ERR) {
           last_error_ = QString("deleteCurrentRecord: retval: = %1 nisam_err = %2, isam_fil = %3, sysiocod = %4. Invoke transactionBegin()... \n").arg(retval).arg(isam_err).arg(isam_fil).arg(sysiocod);
       } else {
           last_error_ = QString("deleteCurrentRecord: retval: = %1 nisam_err = %2, isam_fil = %3, sysiocod = %4\n").arg(retval).arg(isam_err).arg(isam_fil).arg(sysiocod);
       }
       return false;
   }
   return true;
}


bool
CtreeStream::lockRecord()
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::lockRecord()";
#endif

    LONG  ofst = CurrentFileOffset(fileno_);

    COUNT ret;
    if (ret = LockCtData(fileno_, ctENABLE, ofst) != NO_ERROR) {
        last_error_ = QString("LockCtData: return %1 nisam_err = %2, isam_fil = %3, sysiocod = %4\n").arg(ret).arg(isam_err).arg(isam_fil).arg(sysiocod);
        return false;
    }

    return true;
}

bool
CtreeStream::unlockRecord()
{
    LONG   ofst = CurrentFileOffset(fileno_);

    COUNT ret;
    if (LockCtData(fileno_, ctFREE, ofst) != NO_ERROR) {
        last_error_ = QString("LockCtData: return %1 nisam_err = %2, isam_fil = %3, sysiocod = %4\n").arg(ret).arg(isam_err).arg(isam_fil).arg(sysiocod);
        return false;
    }

    return true;
}

bool
CtreeStream::open( QString filename, bool exclusive, QString pass)
{
    COUNT mode = ctSHARED;
    if (exclusive)
        mode = ctEXCLUSIVE;

    if (pass != "")
        fileno_ = OpenFileWithResourceXtd(-1, filename.toUtf8().data(),
                                          mode, pass.toUtf8().data());
    else
        fileno_ = OpenFileWithResource(-1, filename.toUtf8().data(),mode);

    if ( fileno_ < 0) {
        last_error_ = QString("open: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
        return false;
    }

    int len = GetIFile(fileno_, MAX_IFILL_SIZE, (void*)ifil_ptr_);
    CloseCtFile(fileno_,0);

    if (len <= 0 || len >= MAX_IFILL_SIZE) {
        last_error_ = QString("getIFile: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
        fileno_ = -1;
        return false;
    }
    
    QString path = QFileInfo(filename).path();
    if (path == ".")
        path = "";

#ifdef CTREESTREAM_DEBUG
    qDebug()<<"path="<<path;
#endif

    if ( !QFileInfo(((ifil*)(ifil_ptr_))->pfilnam).exists() ) {
        //
        //  File has moved, so cycle through ifil and change the names
        //
        QString sfn;
        if (path == "")
            sfn = QFileInfo(((ifil*)(ifil_ptr_))->pfilnam).completeBaseName();
        else 
            sfn = path+"/"+QFileInfo(((ifil*)(ifil_ptr_))->pfilnam).completeBaseName();
        strncpy(((ifil*)(ifil_ptr_))->pfilnam, sfn.toLatin1().data(), sfn.size());
        ((ifil*)(ifil_ptr_))->pfilnam[ sfn.size()] = '\0';

        ssize_t offset = sizeof (ifil);

#ifdef CTREESTREAM_DEBUG
        qDebug()<<"((ifil*)(ifil_ptr_))->pfilnam="<< ((ifil*)(ifil_ptr_))->pfilnam;
#endif

        for (int i=0; i<((ifil*)(ifil_ptr_))->dnumidx; i++) {
            //
            // Go through the indices
            //
            IIDX *iidx_ptr = (IIDX*)(ifil_ptr_+offset);
            QString file_name = QFileInfo(iidx_ptr->aidxnam).completeBaseName();
#ifdef CTREESTREAM_DEBUG
            qDebug()<<"old iidx_ptr->aidxnam="<<iidx_ptr->aidxnam;
#endif
            
            if (path != "")
                file_name = (path+"/"+file_name).toLatin1().data();

            strncpy(iidx_ptr->aidxnam, file_name.toLatin1().data(), file_name.size());
            iidx_ptr->aidxnam[file_name.size()] = '\0';
            
#ifdef CTREESTREAM_DEBUG
            qDebug()<<"iidx_ptr->aidxnam="<< iidx_ptr->aidxnam;
#endif
            offset += sizeof(IIDX);
        }
    }

    COUNT ret = OpenIFile((ifil*)ifil_ptr_);
    if (ret != NO_ERROR) {
        CloseIFile((ifil*)ifil_ptr_);
        ssize_t offset = sizeof (ifil);
        //lets go again and put .idx extension
        for (int i=0; i<((ifil*)(ifil_ptr_))->dnumidx; i++) {
            //
            // Go through the indices
            //
            IIDX *iidx_ptr = (IIDX*)(ifil_ptr_+offset);
            QString file_name = QFileInfo(iidx_ptr->aidxnam).completeBaseName()+".idx";
            if (path != "")
                file_name = (path+"/"+file_name).toLatin1().data();

            strncpy(iidx_ptr->aidxnam, file_name.toLatin1().data(), file_name.size());
            iidx_ptr->aidxnam[file_name.size()] = '\0';
            
            offset += sizeof(IIDX);
        }
        
        COUNT ret = OpenIFile((ifil*)ifil_ptr_);
        if (ret != NO_ERROR) {
            last_error_ = QString("OpenIFileXtd: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
            fileno_ = -1;
            return false;
        }

    }
   
    fileno_ = ((ifil*)ifil_ptr_)->tfilno;
    for (int i=1;i<=((ifil*)(ifil_ptr_))->dnumidx; i++) {
        indexes_.push_back(fileno_+i);
    }

#ifdef CTREESTREAM_DEBUG
    qDebug()<<"opened with"<<indexes_.size()<<"indexes";
#endif

    //UpdateHeader(fileno_, 1, ctISAMKBUFhdr);

    return true;
}

bool 
CtreeStream::atEnd()
{
#ifdef  CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::atEnd()";
#endif
    return at_end_;
}

void
CtreeStream::close()
{
    if (isOpened()) {
        CloseIFile((ifil*)ifil_ptr_);
    }

    fileno_ = -1;
}

ByteArray
CtreeStream::read(unsigned bytes)
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::read";
#endif
   COUNT retval;
   TEXT  recbuf[MAX_RECORD_SIZE];
   VRLEN reclen = MAX_RECORD_SIZE;

   if (need_reread_) {
       reclen = VRecordLength(fileno_);
       retval = ReReadVRecord(fileno_, &recbuf, reclen);
       need_reread_ = false;
   } else {
       retval = NextVRecord(fileno_, &recbuf, &reclen);
   }

   if (retval != NO_ERROR) {
       if (retval == INOT_ERR) {
           at_end_ = true;
       } else {
           if (retval == ICUR_ERR) {
               reclen = MAX_RECORD_SIZE;
               retval = FirstVRecord( fileno_, &recbuf, &reclen);
           }
       }
       
       if (retval != NO_ERROR && retval != INOT_ERR) {
           last_error_ = QString("NextVRecord: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);

           return ByteArray();
       } else if (retval == INOT_ERR) 
           at_end_ = true;
   }

   if (reclen > bytes) {
       last_error_ = QString("CtreeStream::read error, bytes > record length");
       return ByteArray();
   }

   return QByteArray((const char*)recbuf, reclen);
}

ByteArray
CtreeStream::getKey(short idx)
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::getKey";
#endif

    if (indexes_.size() == 0) {
        last_error_ = "getKey, there are no index files opened";
        return QByteArray();
    }

    if (indexes_.size() < idx) {
        last_error_ = "getKey, invalid index specified";
        return QByteArray();
    }
    
    TEXT  keybuf[MAX_KEY_SIZE];
    VRLEN keylen = MAX_KEY_SIZE;

    void *bptr;
    if ((bptr = CurrentISAMKey( indexes_.at(idx),&keybuf,&keylen )) == NULL) {
        // Go again
        VRLEN  varlen = VRecordLength(fileno_);
        TEXT *recptr = new TEXT[varlen];
        
        if (ReReadVRecord(fileno_,recptr,varlen)) {
            last_error_ = QString("ReReadVRecord: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
            delete[]recptr;
            return QByteArray();
        }
        delete[]recptr;
        
        if ((bptr = CurrentISAMKey( indexes_.at(idx),&keybuf,&keylen )) == NULL) {
            last_error_ = QString("CurrentISAMKey: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
            return ByteArray();
        }
    }

    return QByteArray((const char*)(bptr), keylen);
}

bool
CtreeStream::setKey( ByteArray ba, short idx )
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::setKey";
#endif

    if (indexes_.size() == 0) {
        last_error_ = "setKey, there are no index files opened";
        return false;
    }

    if (indexes_.size() < idx) {
        last_error_ = "setKey, invalid index specified";
        return false;
    }

    LONG    recbyt;
    TEXT    buf[MAX_KEY_SIZE];

    if (!(recbyt = GetGTEKey(indexes_.at(idx),ba.data(),buf))) {
        if (uerr_cod)
            last_error_ = QString("setKey:Error while searching");
        else
            last_error_ = QString("setKey:Error keyValue not found");

        return false;
    }
        
    //
    //  position
    //
    if (SetRecord(fileno_,recbyt,NULL,0) != NO_ERROR) {
        last_error_ = QString("SetRecord: nisam_err = %1, isam_fil = %2, sysiocod = %3\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
        return false;
    }

    need_reread_ = true; /*ne next read we need to reread*/

    return true;
}


bool
CtreeStream::write(TEXT *data, VRLEN length)
{
#ifdef CTREESTREAM_DEBUG
    qDebug()<<"CtreeStream::write";
#endif
   COUNT retval;

   retval = AddVRecord(fileno_, data, length);
   if (retval == INOT_ERR) {
       at_end_ = true;
   }

   if (retval != NO_ERROR) {
       if (retval == TTYP_ERR) {
          last_error_ = QString("AddVRecord:  nisam_err = %1, isam_fil = %2, sysiocod = %3. Invoke transactionBegin()... \n").arg(isam_err).arg(isam_fil).arg(sysiocod);
       } else {
          last_error_ = QString("AddVRecord:  nisam_err = %1, isam_fil = %2, sysiocod = %3.\n").arg(isam_err).arg(isam_fil).arg(sysiocod);
      }
      return false;
}
   return true;
}
