#ifndef CTREESTREAM_H
#define CTREESTREAM_H

#include <QtCore/QObject>

#include "binary.h"

#define NO_ctFeatOPENSSL
#include <ctreep.h>

class CtreeStream : public QObject
{
    Q_OBJECT

    friend class CtreeStreamPrototype;

public:
    CtreeStream ( QObject * parent = 0 );
    ~CtreeStream();

    bool atEnd(); 
    void close();
    bool isOpened() const { return (fileno_ == -1)?false:true;}
    
    QString lastErrorString() const { return last_error_; }
    bool deleteCurrentRecord();
    bool lockRecord();
    bool unlockRecord();
    bool open( QString filename, bool exclusive = false, QString pass="");
    ByteArray read(unsigned bytes);
    ByteArray getKey( short idx=0 );
    bool setKey( ByteArray ba, short idx=0);
    bool write(TEXT *data, VRLEN length);

private:
    COUNT         fileno_;
    QList<COUNT>  indexes_;
    char         *ifil_ptr_;

    QString      last_error_;
    bool         at_end_;
    bool         need_reread_;
};

#endif
