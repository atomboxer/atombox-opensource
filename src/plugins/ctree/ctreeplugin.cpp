#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "ctreeplugin.h"

#include "ctreeclass.h"
#include "ctreeprototype.h"
#include "ctreestreamprototype.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptctree_compat, CtreePlugin )
#else
Q_EXPORT_PLUGIN2( abscriptctree, CtreePlugin )
#endif

#if defined(AB_HAVE_CTREE) && defined(Q_OS_WIN32) 
#if !defined(AB_STATIC_PLUGINS) 
int __sse2_available; /*exern*/
#else
extern int __sse2_available;
#endif
#endif

CtreePlugin::CtreePlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
 	
CtreePlugin::~CtreePlugin()
{
}

Q_DECLARE_METATYPE(Ctree*)
Q_DECLARE_METATYPE(CtreeStream*)

QScriptValue 
Ctree_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 0) {
        ctx->throwError("<<ctree/ctree.Ctree>> constructor does not require arguments");
        return QScriptValue();
    }

    return eng->newQObject(new Ctree(),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue 
CtreeStream_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    // if (ctx->argumentCount() != 0) {
    //     ctx->throwError("<<ctree/ctree.Ctree>> constructor does not require arguments");
    //     return QScriptValue();
    // }
    return eng->newQObject(new CtreeStream(),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

 	
void
CtreePlugin::initialize( const QString &key, QScriptEngine *engine )
{
#if defined(QT_PLUGIN) && defined(AB_HAVE_CTREE) && defined(Q_OS_WIN32)
    __sse2_available = 0;
#endif

    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("ctree_compat") ) {
#else
  if ( key == QString("ctree") ) {
#endif

#ifdef AB_STATIC_PLUGINS
        Q_INIT_RESOURCE(ctree);
#endif
        Q_INIT_RESOURCE(ctreefiles);
    }

    if (globalObject.property("__internal__").isUndefined()) {
        globalObject.setProperty("__internal__", engine->newObject());
    }

    engine->setDefaultPrototype(qMetaTypeId<Ctree*>(),
                                 engine->newQObject(new CtreePrototype(this)));


    engine->globalObject().property("__internal__").
        setProperty("Ctree", engine->newFunction(Ctree_Constructor));

    engine->globalObject().property("__internal__").property("Ctree").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Ctree*>()));

    engine->setDefaultPrototype(qMetaTypeId<CtreeStream*>(),
                                 engine->newQObject(new CtreeStreamPrototype(this)));

    engine->globalObject().property("__internal__").
        setProperty("CtreeStream", engine->newFunction(CtreeStream_Constructor));

    engine->globalObject().property("__internal__").property("CtreeStream").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<CtreeStream*>()));

}
 	
QStringList
CtreePlugin::keys() const
{
    QStringList keys;

#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("ctree_compat");
#else
    keys << QString("ctree");
#endif

    return keys;
}
