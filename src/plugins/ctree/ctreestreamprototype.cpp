#include "ctreestreamprototype.h"
#include "bytearrayprototype.h"
#include "bytearrayclass.h"

#include <QtCore/QByteArray>
#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define CTREESTREAMPROTOTYPE_DEBUG

Q_DECLARE_METATYPE(Stream*)
Q_DECLARE_METATYPE(CtreeStream*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

CtreeStreamPrototype::CtreeStreamPrototype(QObject *parent)
    : StreamPrototype(parent)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::CtreeStreamPrototype(QObject *parent)";
#endif
}

CtreeStreamPrototype::~CtreeStreamPrototype()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::~CtreeStreamPrototype()";
#endif
}

void
CtreeStreamPrototype::close()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::close()";
#endif
    thisCtreeStream()->close();
}

bool
CtreeStreamPrototype::atEnd()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::atEnd()";
#endif
    return thisCtreeStream()->atEnd();
}

bool
CtreeStreamPrototype::isClosed()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::isClosed()";
#endif
    
    return !(thisCtreeStream()->isOpened());
}

bool
CtreeStreamPrototype::isReadable()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::isReadable()";
#endif
    if (thisCtreeStream()->isOpened())
        return true;

    return false;
}

bool
CtreeStreamPrototype::isSequential()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::isSequential()";
#endif

    return false;
}

bool
CtreeStreamPrototype::isWritable()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::isWritable()";
#endif
    if (thisCtreeStream()->isOpened())
        return true;

    return false;
}

QScriptValue
CtreeStreamPrototype::read(unsigned bytes)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::read(unsigned bytes)";
#endif
    QByteArray ba(thisCtreeStream()->read(bytes));
    return ByteArrayClass::toScriptValue(engine(), ba);
}

bool
CtreeStreamPrototype::lockRecord()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::lockRecord()";
#endif
    if (!thisCtreeStream()->lockRecord()) {
        engine()->currentContext()->throwError(errorString());
        return false;
    }
    return true;
}

bool
CtreeStreamPrototype::unlockRecord()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::unlockRecord()";
#endif
    if (!thisCtreeStream()->unlockRecord()) {
        engine()->currentContext()->throwError(errorString());
        return false;
    }
    return true;
}

bool
CtreeStreamPrototype::setKey(QScriptValue key, short spec)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::setKey(QScriptValue key, short spec)";
#endif

    //
    //  Looking to see if the parameter is Binary
    //
    ByteArray   *ba = NULL;
    ByteString  *bs = NULL;
    
    bool ret = false;
    ba = qscriptvalue_cast<ByteArray*>(key.data());
    if (!ba) {
        bs = qscriptvalue_cast<ByteString*>(key.data());
    }
    
    if (!ba && !bs) {
        engine()->currentContext()->throwError("CtreeStream.prototype.setKey. should be instanceof Binary");
        return false;
    }

    if (ba)
        ret = thisCtreeStream()->setKey(*ba, spec);
    else 
        ret = thisCtreeStream()->setKey(*bs, spec);

    if (!ret) {
        engine()->currentContext()->throwError(errorString());
        return false;
    }

    return true;
}

QScriptValue
CtreeStreamPrototype::getKey()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::getKey()";
#endif
    ByteArray ret = thisCtreeStream()->getKey();
    if (ret.size() == 0) {
        engine()->currentContext()->throwError(errorString());
        return false;
    }
    return ByteArrayClass::toScriptValue(engine(), ret);
}

QString
CtreeStreamPrototype::errorString()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::errorString()";
#endif
    return thisCtreeStream()->lastErrorString();
}

void
CtreeStreamPrototype::write(QScriptValue binary, QScriptValue begin, 
                            QScriptValue end )
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::write(QScriptValue binary, QScriptValue begin,";
#endif
    ByteArray  *ba = qscriptvalue_cast<ByteArray*>(binary.data());
    ByteString *bs = 0;
    QByteArray *b  = ba;

    if (binary.isNumber() && binary.toInt32() == 0) {
        if (!thisCtreeStream()->deleteCurrentRecord()) {
            engine()->currentContext()->throwError(errorString());
            return;
        }
        return;
    }

    if (!b) {
        // Maybe is a ByteString
        bs = qscriptvalue_cast<ByteString*>( binary.data() );
        b = bs;
    }

    if (!b) {
        context()->throwError("CtreeStream.prototype.write: arg(0) "\
                              "not instance of ByteArray or ByteString");
        return;
    }

    int start = 0;
    if (begin.isNumber()) {
        start = begin.toNumber();
        Q_ASSERT(start > b->length());
        return;
    }

    int stop = b->length();
    if (end.isNumber()) {
        stop = end.toNumber();
        Q_ASSERT(stop-start> b->length());
        return;
    }

    if (b->size() == 0) {
        if (!thisCtreeStream()->deleteCurrentRecord()) {
            engine()->currentContext()->throwError(errorString());
            return;
        }
    } else {
        char *data = b->data();
        if (!thisCtreeStream()->write(data+start, stop-start )) {
            engine()->currentContext()->throwError(errorString());
            return;
        }
    }

    return;
}


CtreeStream*
CtreeStreamPrototype::thisCtreeStream()
{
    CtreeStream *p = qscriptvalue_cast<CtreeStream*>(thisObject());
    if (!p) {
        qFatal("programatic error CtreeStreamPrototype::thisCtreeStream");
        return 0;
    }
    return p;
}

//NOT APPLICABLE
void
CtreeStreamPrototype::copy(QIODevice *io)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::copy(QIODevice *io)";
#endif
    context()->throwError("CtreeStreamPrototype::copy method not applicable");
}

void
CtreeStreamPrototype::flush()
{
    context()->throwError("CtreeStreamPrototype::flush() method not applicable");
}

QScriptValue
CtreeStreamPrototype::readInto(QScriptValue buffer, QScriptValue begin, 
                               QScriptValue end)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::readInto(QScriptValue buffer...";
#endif
    context()->throwError("CtreeStreamPrototype::readInto method not applicable");
    return QScriptValue();
}

bool
CtreeStreamPrototype::skip(int bytes)
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"";
#endif
    context()->throwError("CtreeStreamPrototype::skip method not applicable");
    return false;
}

void
CtreeStreamPrototype::__internal__setPosition( QScriptValue position )
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"__internal__setPosition( QScriptValue position )";
#endif
    context()->throwError("CtreeStreamPrototype::__internal__setPosition method not applicable");
}

qint64
CtreeStreamPrototype::__internal__getPosition()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"__internal__getPosition()";
#endif
    context()->throwError("CtreeStreamPrototype::__internal__getPosition() method not applicable");
    return -1;
}

qint64
CtreeStreamPrototype::bytesAvailable()
{
#ifdef CTREESTREAMPROTOTYPE_DEBUG
    qDebug()<<"CtreeStreamPrototype::bytesAvailable()";
#endif
    context()->throwError("CtreeStreamPrototype::bytesAvailable() method not applicable");
    return -1;
}
