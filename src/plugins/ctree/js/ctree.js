
/**
 * @desc This module provides a simple API for accessing a Faircom Ctree - ACE database. The central class of this module is module:ctree/ace.Ctree, that enables you to connect/disconnect to the database. After invoking the `open` method you will get an instance of module:io.Stream for further usage.

 * @module ctree/ace
 * @summary Faircom Ctree ACE database access
 * @example
var system  = require("system");

//Load the ctree/ace module
var Ctree = require("ctree/ace").Ctree;

//Load some arbitrary Atom/Box model
var Interface_Manager = require("mdb.csv", "mdbcsv").Interface_Manager;
var im = new Interface_Manager();
im.setByteOrder(LITTLE_ENDIAN);

var c = new Ctree();
//Connect to the database
c.connect("#5597@127.0.0.1","ADMIN","ADMIN");

//Open the file
var stream = c.open("ifmgrd.dat");

im.intf_nam.value = "HISO93_INTF"; //this is the key

c.beginTransaction();
//write a record
try {
    stream.write(im.unpack()); 
} catch(e) {
    //position
    stream.setKey(im.intf_nam.bytes);
    //reread the record
    stream.read();
    //delete it
    stream.write(0);
}

//try again
stream.write(im.unpack()); 
c.endTransaction()

c.disconnect();
system.exit(0);
 */


/**
 * @class
 * @name Ctree
 * @see module:io
 * @classdesc This class provides as a common implementation for raw data stream classes. 
 * @summary Ctree/ACE database access API
 * @memberof module:ctree/ace
 * @requires module:io
 * @example
var Ctree = require("ctree/ace").Ctree;

var conn = new Ctree();

try {
    conn.connect("#5597@127.0.0.1","ADMIN","ADMIN");

    var stream = conn.open("somefile.dat");
    //...
} catch(e) {
    //...
} 

conn.disconnect();

 */


/**
 * Connects to the Ctree server
 * @method module:ctree/ace.Ctree.prototype.connect
 * @arg {String} server
 * @arg {String} [username=""]
 * @arg {String} [password=""]
 * @throws {Error}
 */

/**
 * Disconnects from the server
 * @method module:ctree/ace.Ctree.prototype.disconnect
 */

/**
 * Returns true if the object is connected, false otherwise
 * @method module:ctree/ace.Ctree.prototype.isConnected
 * @returns {Boolean}
 */

/**
 * Opens a Ctree file
 * @method module:ctree/ace.Ctree.prototype.open
 * @arg {String}  filename
 * @arg {Boolean} [exclusive=false] - Set to true, if you need exclusive access 
 * @arg {String}  [password=""] - in case the file is password protected
 * @returns {Stream} an instance of module:io.Stream
 * @note To close the file, invoke the close method on the Stream object
 * @red
 * @see  module:io.Stream
 */

/**
 * Begins a Ctree transaction
 * @method module:ctree/ace.Ctree.prototype.beginTransaction
 */

/**
 * Complete a Ctree transaction
 * @method module:ctree/ace.Ctree.prototype.endTransaction
 */

/**
 * Aborts the Ctree transaction
 * @method module:ctree/ace.Ctree.prototype.abortTransaction
 */

exports.Ctree       = Ctree = __internal__.Ctree;
exports.CtreeStream = CtreeStream = __internal__.CtreeStream;
