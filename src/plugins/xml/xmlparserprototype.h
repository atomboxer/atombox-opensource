#ifndef XMLPARSERPROTOTYPE_H
#define XMLPARSERPROTOTYPE_H


#include "xmlparser.h"

#include <QtCore/QString>
#include <QtCore/QObject>
#include <QtScript/QScriptable>

#include <QtScript/QScriptValue>

class XmlParserPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    XmlParserPrototype(QObject *parent = 0);
    ~XmlParserPrototype();

    //public slots:
    //;

private:
    XmlParser *thisXmlParser();
};

#endif //XMLPARSERPROTOTYPE_H
