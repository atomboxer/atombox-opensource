
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "xmlplugin.h"

#include "xmlparser.h"
#include "xmlparserprototype.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptxml_compat, XmlPlugin )
#else
Q_EXPORT_PLUGIN2( abscriptxml, XmlPlugin )
#endif


XmlPlugin::XmlPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
 	
XmlPlugin::~XmlPlugin()
{
}

Q_DECLARE_METATYPE(XmlParser*)

QScriptValue 
XmlParser_Constructor(QScriptContext *context, QScriptEngine *engine)
{
    
    QScriptValue obj =  engine->newQObject(new XmlParser(), QScriptEngine::AutoOwnership,
                                           QScriptEngine::SkipMethodsInEnumeration
                                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                                           /*| QScriptEngine::ExcludeSuperClassProperties*/);

    return obj;
}

void
XmlPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("xml_compat") ) {
#else
    if ( key == QString("xml") ) {
#endif

#ifdef AB_STATIC_PLUGINS
	Q_INIT_RESOURCE(xml);
#endif
	Q_INIT_RESOURCE(jsxmlfiles);

	QScriptValue __internal__;

	__internal__ = engine->globalObject().property("__internal__");
	if (!__internal__.isObject()) {
	    __internal__ = engine->newObject();
	    engine->globalObject()
		.setProperty("__internal__",__internal__);
	}

    engine->setDefaultPrototype(qMetaTypeId<XmlParser*>(),
                                engine->newQObject(new XmlParserPrototype(this)));
    
	engine->globalObject().property("__internal__").
	    setProperty("XmlParser", engine->newFunction(XmlParser_Constructor));
	engine->globalObject().property("__internal__").property("XmlParser").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<XmlParser*>()));
    }
}
 	
QStringList
XmlPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("xml_compat");
#else
    keys << QString("xml");
#endif
 	
    return keys;
}
