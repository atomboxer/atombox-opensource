#include "xmlparser.h"
#include "xmlparserprototype.h"

#include <QtCore/QDebug>
#include <QtCore/QList>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(XmlParser*)

XmlParserPrototype::XmlParserPrototype(QObject *parent)
    : QObject(parent)
{
}

XmlParserPrototype::~XmlParserPrototype()
{
}

XmlParser *
XmlParserPrototype::thisXmlParser()
{
    XmlParser *pt = qscriptvalue_cast<XmlParser*>(thisObject());
    if (!pt) {
        qFatal("Programmatic error XmlParserPrototype::thisXmlParser");
    }

    return pt;
}
