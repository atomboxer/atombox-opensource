/**
 *  @module xml/sax
 *  @deprecated Please don't use this module for new scripts. AtomBox has powerfull XML libraries that can be exposed to JavaScript but at this moment there are no new XML future plans. Please contact us if you would need an XML module.
 *  @desc The "xml/sax" module provides a sax-style parser for XML and HTML. This is a port of [sax-js](https://github.com/isaacs/sax-js/) Node.js module. 
 *  @summary SAX parsing - pure JS implementation
 *  @example
var console = require("console");
var sax = require("xml/sax.js"),
strict = true, // set to false for html-mode
parser = sax.parser(strict);

parser.onerror = function (e) {
    // an error happened.
};
parser.ontext = function (t) {
    // got some text.  t is the string of text.

    console.write("onText");
    console.dir(t);
};
parser.onopentag = function (node) {
    // opened a tag.  node has "name" and "attributes"
    console.write("onopentag");
    console.dir(node);
};
parser.onattribute = function (attr) {
    // an attribute.  attr has "name" and "value"
    console.dir(attr.name);
};
parser.onend = function () {
    // parser stream is done, and ready to have more stuff written to it.
    console.write("end");
};

var xml = '<xml>Hello, <who name="world" some="other">world<other></other></who>!</xml>';
parser.write(xml).close();

 */

/**
 * @module xml/xml2js
 * @deprecated Please don't use this module for new scripts. AtomBox has powerfull XML libraries that can be exposed to JavaScript but at this moment there are no new XML future plans. Please contact us if you would need an XML module.
 * @desc The "xml/xml2js" module provides a simple XML to JavaScript object converter. It supports bi-directional conversion. This is a port of [xml2js](https://github.com/Leonidas-from-XIV/node-xml2js) Node.js module. 
 * @summary XML to JavaScript object converter - pure JS implementation
 * @see module:events
 * @example 
var xml = '<xml>Hello, <who name="world" some="other">world<other></other></who>!</xml>';
var parseString = require('xml/xml2js').parseString;
var obj = undefined;
parseString(xml, function (err, result) {
    obj = result;
});

var Builder = new require('xml/xml2js').Builder;
var builder = new Builder();

var xml = builder.buildObject(obj);
console.write(xml);
 */
exports.XmlParser = __internal__.XmlParser;
