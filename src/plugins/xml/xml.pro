
include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptxml
} else {
    TARGET         = abscriptxml_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}


DEPENDPATH += .
INCLUDEPATH += .

HEADERS  = \
           xmlplugin.h \
           xmlparser.h \
           xmlparserprototype.h

SOURCES  = \
           xmlplugin.cpp \
           xmlparser.cpp \
           xmlparserprototype.cpp

RESOURCES += jsxmlfiles.qrc

static {
    RESOURCES += xml.qrc
}
else {
    CONFIG   += qt plugin
    
    isEmpty(USE_SCRIPT_CLASSIC) {
         QT += script
         LIBS += -labscriptcore
    } else {
         LIBS += -labscriptcore_compat
    }

}

OTHER_FILES += \
    js/xml.js
