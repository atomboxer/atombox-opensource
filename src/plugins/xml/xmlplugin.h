#ifndef XMLPLUGIN_H
#define XMLPLUGIN_H

#include <QScriptExtensionPlugin>

class XmlPlugin : public QScriptExtensionPlugin
{
public:
    XmlPlugin( QObject *parent = 0);
    ~XmlPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //XMLPLUGIN_H
