#ifndef TCPSERVERPROTOTYPE_H
#define TCPSERVERPROTOTYPE_H

#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QObject>
#include <QtCore/QCoreApplication>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include <QtNetwork/QTcpServer>

class TcpServerPrototype : public QObject, protected QScriptable
{
    Q_OBJECT

public:
    TcpServerPrototype(QObject *parent = 0);
    ~TcpServerPrototype();

public slots:
    QString address();
    void close();
    QString errorString();
    bool isListening(); 
    bool listen(quint16 port, QScriptValue host=QScriptValue::UndefinedValue);
    QScriptValue nextPendingConnection();
    quint16 port();

signals:
    void newConnection();

private:
    QTcpServer *thisTcpServer();
    mutable bool one_time_;
    
private slots:
    inline void notify_newConnection() { emit newConnection(); }
};

#endif //TCPSERVERPROTOTYPE_H
