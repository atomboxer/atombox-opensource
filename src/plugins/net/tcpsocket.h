#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>

class TcpSocket : public QObject
{
    friend class TcpSocketPrototype;

    Q_OBJECT

public:
    TcpSocket(QTcpSocket *priv, QObject *parent = 0);
    ~TcpSocket();

signals:
    void aboutToClose ();
    void bytesWritten ( qint64 bytes );
    void readStreamFinished ();
    void readyRead();

    void connected();
    void disconnected();
    void error();

private:
    QTcpSocket *priv_;
};

#endif //TCPSOCKET_H
