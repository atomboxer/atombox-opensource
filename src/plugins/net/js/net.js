/**
 *  AtomBox networking module that provides a set of APIs for programming applications that use TCP/IP. The classes in this module allows you to write clients ans servers in just a few lines of code.
 *  @module net
 *  @note UDP is yet to be implemented in AtomBox
 *  @summary TCP/IP networking API
 *  @example
//
//  TCP/Server Fortune Server example (examples/sfortune.js)
//

var net     = require("net");
var console = require("console");

var fortunes = ["You've been leading a dog's life. Stay off the furniture",
                "You've got to think about tomorrow",
                "You will be surprised by a loud noise",
                "You will feel hungry again in another hour",
                "You might have mail",
                "You cannot kill time without injuring eternity",
                "Computers are not intelligent. They only think they are"];


// Create a new instance of TcpServer
var server =  new net.TcpServer();

// Start listening on port 8150
server.listen(8150);

// Connect the newConnection signal
server.newConnection.connect(function() {

    // Accept the client connection
    var client = server.nextPendingConnection();

    // Send the fortune 
    client.write(fortunes[Math.floor(Math.random()*7+1)].toByteArray());
    
    // Wait for the server to send the message and then disconnect
    client.bytesWritten.connect(function() {
        console.write("fortune delivered");

        client.disconnectFromHost(); //close the connection 
    });
});


 * @example
//
//  TCP/Client Fortune example (examples/cfortune.js)
//

var TcpSocket     = require("net").TcpSocket;
var console       = require("console");

//Create a new TcpSocket object
var socket = new TcpSocket();

//Connect the readyRead signal
socket.readyRead.connect( function() {
    //Read what the server sends us
    var fortune = socket.read();

    console.write(fortune.decodeToString());
});

//Connect the disconnected signal
socket.disconnected.connect( function() {
    console.write("disconnected\n");
});

socket.connectToHost("localhost", 8150);

 */


/**
 * @class 
 * @name TcpSocket
 * @classdesc The TcpSocket class represents a TCP/IP Socket.
The TcpSocket class is inherited from the {@link module:io.Stream}.
Newly created sockets need to be connected to a remote address before being able to send and receive data.
 * @summary TCP/IP Socket class
 * @memberof module:net
 * @requires module:net
 * @example
//
//  TCP/Socket HTTP connect example 
//
var console = require("console");
var net    = require("net");

var socket =  new net.TcpSocket();

//Connect the 'connected' signal to a custom function
socket.connected.connect( function() {
    console.write("<slot>connected\n");
    socket.write('GET index.html HTTP/1.1\r\n'.toByteArray());
    socket.write('host: www.google.ro\r\n'.toByteArray());
    socket.write('\r\n'.toByteArray());
});

//Connect the 'readyRead' signal to a custom function
socket.readyRead.connect(function() {
    console.write("<slot>ready read\n");
    console.write(socket.read().decodeToString());
})

//Connect the 'error' signal to a custom function
socket.error.connect(function() {
    console.write("error connecting to host:"+this.errorString());
});

socket.connectToHost("www.google.ro",80);
*/

/**
 * Flushes the bytes written to the stream to the underlying medium
 * @method module:net.TcpSocket.prototype.flush
 */

/**
 * Read up to n bytes from the stream, or until the end of the stream has been reached. 
An empty ByteString is returned when the end of the stream has been reached
 * @method module:net.TcpSocket.prototype.read
 * @arg {Number} [n=4096]
 * @return {ByteString}
 */

/**
 * Read bytes from the stream into the ByteArray buffer. This method does not increase the length of the ByteArray buffer. 
 * @method module:net.TcpSocket.prototype.readInto
 * @arg {ByteArray} buffer
 * @arg {Number} [begin = 0]
 * @arg {Number} [end = length of buffer]
 * @return {Number} The number of bytes read.
 */

/**
 * Write bytes from b to the stream. If `begin` and `end` are specified, only the range starting at `begin` and ending before `end` are written.
 * @method module:net.TcpSocket.prototype.write
 * @arg {ByteArray|ByteString} b
 * @arg {Number} [begin = 0]
 * @arg {Number} [end = length of b]
 * @throws {Error}
 */

/**
 * Closes the stream, freeing the resources it is holding
 * @method module:net.TcpSocket.prototype.close
 */

/**
 * Returns the number of bytes that are available for reading. This method is commonly used with 
sequential devices to determine the number of bytes to allocate in a buffer before reading.
 * @method module:net.TcpSocket.prototype.bytesAvailable
 * @return {Number}
 * @throws {Error}
 */

/**
 * This signal is emitted when the device is about to close
 * @event module:net.TcpSocket.prototype.aboutToClose
 */

/**
 * This signal is emitted every time a payload of data has been written to the device
 * @event module:net.TcpSocket.prototype.bytesWritten
 * @arg {Number} bytes
 */

/**
 * This signal is emitted when the input (reading) stream is closed in this device
 * @event module:net.TcpSocket.prototype.readStreamFinished
 */

/**
 * This signal is emitted once every time new data is available for reading from the device. 
It will only be emitted again once new data is available.
 * @event module:net.TcpSocket.prototype.readyRead
 */

/*..................*/

/**
 * This signal is emitted when the socket gets into the 'connected' state
 * @event module:net.TcpSocket.prototype.connected
 */

/**
 * This signal is emitted when the socket gets into the 'disconnected' state
 * @event module:net.TcpSocket.prototype.disconnected
 */

/**
 * This signal is emitted when the socket gets into the 'error' state. For example unabel to connect.
 * @event module:net.TcpSocket.prototype.error
 */

/**
 * Creates a TCP/IP connection to `host` and `port`. 
 * @note Because of the asynchronous nature of AtomBox this method returns immediately. It is your responsability to connect your custom functions to the needed signals.
 * @method module:net.TcpSocket.prototype.connectToHost
 * @arg {String} host The host (or IP address) of the port on which to connect
 * @arg {Number} port The port on which to connect
 */

 /**
 * Disconnects the TCP/IP connection.
 * @method module:net.TcpSocket.prototype.disconnectFromHost
 */

/**
 * Returns `true` if the socket is valid, `false` otherwise.
 * @method module:net.TcpSocket.prototype.isValid
 * @returns {Boolean}
 */

/**
 * Returns the peer address
 * @method module:net.TcpSocket.prototype.peerAddress
 * @returns {String} peer The peer address
 */

/**
 * Returns the peer port
 * @method module:net.TcpSocket.prototype.peerPort
 * @returns {String} peer The peer port
 */

/**
 * Returns the error string of a socket in `error` state
 * @method module:net.TcpSocket.prototype.errorString
 * @returns {String} error The error string
 */


exports.TcpSocket = TcpSocket = __internal__.TcpSocket;


/**
 * @class 
 * @name TcpServer
 * @classdesc The TcpServer class provides a TCP-based server. This class makes it possible to accept incoming TCP connections. Call the [listen]{@link module:net.TcpServer#listen} method to start listening for the incomming connections. Connect the [newConnection]{@link module:net.TcpServer#newConnection] signal to a custom function. Call the [nextPendingConnection]{@link module:net.TcpServer#nextPendingConnection} to accept the pending connection as a connected `TcpSocket` that you can use to communicate with the client
 * @summary TCP Server class
 * @memberof module:net
 * @requires module:net
 * @example
//
//  TCP/Server Fortune Server example
//

var net     = require("net");
var console = require("console");

var fortunes = ["You've been leading a dog's life. Stay off the furniture",
                "You've got to think about tomorrow",
                "You will be surprised by a loud noise",
                "You will feel hungry again in another hour",
                "You might have mail",
                "You cannot kill time without injuring eternity",
                "Computers are not intelligent. They only think they are"];


// Create a new instance of TcpServer
var server =  new net.TcpServer();

// Start listening on port 8150
server.listen(8150);

// Connect the newConnection signal
server.newConnection.connect(function() {

    // Accept the client connection
    var client = server.nextPendingConnection();

    // Send the fortune 
    client.write(fortunes[Math.floor(Math.random()*7+1)].toByteArray());
    
    // Wait for the server to send the message and then disconnect
    client.bytesWritten.connect(function() {
        console.write("fortune delivered");

        client.disconnectFromHost(); //close the connection
    });
});

*/

/**
 * This signal is emitted when there is a new connection available
 * @event module:net.TcpServer.prototype.newConnection
 */

/**
 * Returns the server's address if the server is listening for connections
 * @method module:net.TcpServer.prototype.address
 * @returns {String} address
 */

/**
 * Returns the server's port if the server is listening for connections
 * @method module:net.TcpServer.prototype.port
 * @returns {Number} port
 */


/**
 * Returns true if the server is currently listening for incomming connections; otherwise returns false;
 * @method module:net.TcpServer.prototype.isListening
 * @returns {Boolean} listening
 */

/**
 * Tells the server to listen for incomming connections on address and port specified
 * @method module:net.TcpServer.prototype.listen
 * @arg {Number} port The port on which to connect
 * @arg {String} host The host (or IP address) on which to bind the listening port. If ommited, it will bind the port on all the available interfaces
 * @returns {Boolean} success 
 */

/**
 * Returns the next pending connection as a new TcpSocket in `connected` state.
 * @method module:net.TcpServer.prototype.nextPendingConnection
 * @returns {TcpSocket} socket The new client socket
 */

/**
 * Closes the server, freeing the resources it is holding
 * @method module:net.TcpServer.prototype.close
 */

exports.TcpServer = TcpServer = __internal__.TcpServer;
