#ifndef QT_NO_OPENSSL
#include "sslserver.h"

#include <QtNetwork/QSslSocket>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QAbstractSocket>

#include <QtCore/QFile>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

SslServer::SslServer(QObject *parent)
    : QTcpServer(parent)
{
    {
    QFile file("key.pem");
    
    file.open(QIODevice::ReadOnly);
    if (!file.isOpen()) {
        qDebug()<<"cannot open";
        socket->disconnectFromHost();
        return;
    }
   
    QSslKey key(&file, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
    key_ = key;
    qDebug()<<"key is:"<<key.isNull();
    }

    {
        QFile file("cert.pem");
        
        file.open(QIODevice::ReadOnly);
        if (!file.isOpen()) {
            qDebug()<<"cannot open";
            socket->disconnectFromHost();
            return;
        }
        QSslCertificate cert(&file);

    cert_ = cert;
    }
}

void
SslServer::incomingConnection (int socketDescriptor)
{   
   qDebug("SslServer::incomingConnection(%d)", socketDescriptor);

   socket = new QSslSocket;

   if (!socket) {
      return;
   }
   socket->setProtocol(QSsl::AnyProtocol);

   _connectSocketSignals();

   if (!socket->setSocketDescriptor(socketDescriptor)) {
      delete socket;
      return;
   }
   _startServerEncryption();
}

QSslSocket *
SslServer::nextPendingConnection()
{
    QAbstractSocket *clearSocket = QTcpServer::nextPendingConnection();
    if (!clearSocket) {
        return 0;
    }

    QSslSocket *sslSocket = new QSslSocket;
    sslSocket->setSocketDescriptor(clearSocket->socketDescriptor());
    return sslSocket;
}

void SslServer::_startServerEncryption ()
{

   bool b;
   if (key_.isNull()) {
      qWarning("key is null");
      socket->disconnectFromHost();
      return;
   }

   // b = socket->addCaCertificates(CACERTIFICATES_FILE);
   // if (!b) {
   //    qWarning("Couldn't add CA certificates (\"%s\")"
   //       , CACERTIFICATES_FILE);
   // }
   //else {
      socket->setLocalCertificate(cert_);
      socket->setPrivateKey(key_);
      socket->startServerEncryption();
   //}
}

void SslServer::_connectSocketSignals ()
{
   connect(socket, SIGNAL(encrypted()), this, SLOT(slot_encrypted()));
   connect(socket, SIGNAL(encryptedBytesWritten(qint64)),
      this, SLOT(slot_encryptedBytesWritten(qint64)));
   connect(socket, SIGNAL(modeChanged(QSslSocket::SslMode)),
      this, SLOT(slot_modeChanged(QSslSocket::SslMode)));
   connect(socket, SIGNAL(peerVerifyError(const QSslError &)),
      this, SLOT(slot_peerVerifyError (const QSslError &)));
   connect(socket, SIGNAL(sslErrors(const QList<QSslError> &)),
      this, SLOT(slot_sslErrors(const QList<QSslError> &)));
   connect(socket, SIGNAL(readyRead()),
      this, SLOT(slot_readyRead()));
   connect(socket, SIGNAL(connected()),
      this, SLOT(slot_connected()));
   connect(socket, SIGNAL(disconnected()),
      this, SLOT(slot_disconnected()));
   connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
      this, SLOT(slot_error(QAbstractSocket::SocketError)));
   connect(socket, SIGNAL(hostFound()),
      this, SLOT(slot_hostFound()));
   connect(socket, SIGNAL(proxyAuthenticationRequired(const QNetworkProxy &, QAuthenticator *)),
      this, SLOT(slot_proxyAuthenticationRequired(const QNetworkProxy &, QAuthenticator *)));
   connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
      this, SLOT(slot_stateChanged(QAbstractSocket::SocketState)));
}

void SslServer::slot_encrypted ()
{
   qDebug("SslServer::slot_encrypted");
}

void SslServer::slot_encryptedBytesWritten (qint64 written)
{
   qDebug("SslServer::slot_encryptedBytesWritten(%ld)", (long) written);
}

void SslServer::slot_modeChanged (QSslSocket::SslMode mode)
{
   qDebug("SslServer::slot_modeChanged(%d)", mode);
}

void SslServer::slot_peerVerifyError (const QSslError &)
{
   qDebug("SslServer::slot_peerVerifyError");
}

void SslServer::slot_sslErrors (const QList<QSslError> &)
{
   qDebug("SslServer::slot_sslErrors");
}

void SslServer::slot_readyRead ()
{
   qDebug("SslServer::slot_readyRead");
}

void SslServer::slot_connected ()
{
   qDebug("SslServer::slot_connected");
}

void SslServer::slot_disconnected ()
{
   qDebug("SslServer::slot_disconnected");
}

void SslServer::slot_error (QAbstractSocket::SocketError err)
{
   qDebug() << "SslServer::slot_error(" << err << ")";
   qDebug() << socket->errorString();
}

void SslServer::slot_hostFound ()
{
   qDebug("SslServer::slot_hostFound");
}

void SslServer::slot_proxyAuthenticationRequired (const QNetworkProxy &, QAuthenticator *)
{
   qDebug("SslServer::slot_proxyAuthenticationRequired");
}

void SslServer::slot_stateChanged (QAbstractSocket::SocketState state)
{
   qDebug() << "SslServer::slot_stateChanged(" << state << ")";
}

#endif
