#include "sslsocketprototype.h"

#include <QtCore/QDebug>
#include <QtCore/QList>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QNetworkProxyQuery>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QAuthenticator>

#include <QtNetwork/QSslCipher>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(QSslSocket*)
Q_DECLARE_METATYPE(QAbstractSocket::SocketError)

SslSocketPrototype::SslSocketPrototype(QObject *parent)
: StreamPrototype(parent)
{
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
}

SslSocketPrototype::~SslSocketPrototype()
{
}

void
SslSocketPrototype::connectToHost(QString host, quint16 port)
{
    QNetworkProxyQuery npq(host, port);
    QList<QNetworkProxy> listOfProxies = QNetworkProxyFactory::systemProxyForQuery(npq);

    if (listOfProxies.size()) {
	QNetworkProxy prx = listOfProxies.at(0);
	thisSslSocket()->setProxy(prx);	
    }

    QSslSocket *s = thisSslSocket();
    s->abort();
    s->connectToHostEncrypted(host, port);
}

void
SslSocketPrototype::disconnectFromHost()
{
    thisSslSocket()->disconnectFromHost();
}

bool
SslSocketPrototype::isValid()
{
    return thisSslSocket()->isValid();
}

QString
SslSocketPrototype::peerAddress()
{
    return thisSslSocket()->peerAddress().toString();
}

qint16
SslSocketPrototype::peerPort()
{
    return thisSslSocket()->peerPort();
}

QString
SslSocketPrototype::errorString() 
{
    QAbstractSocket::SocketError err = thisSslSocket()->error();
    QString ret = "";

    switch(err) {
    case QAbstractSocket::ConnectionRefusedError: 
	ret =  QString("The connection was refused by the peer (or timed out)"); break;
    case QAbstractSocket::RemoteHostClosedError:
	ret = QString("The remote host closed the connection"); break;
    case QAbstractSocket::HostNotFoundError: 
	ret = QString("The host address was not found"); break;
    case QAbstractSocket::SocketAccessError: 
	ret = QString("The socket operation failed because the application lacked the required privileges"); break;
    case QAbstractSocket::SocketResourceError: 
	ret = QString("The local system ran out of resources (e.g., too many sockets)"); break;
    case QAbstractSocket::SocketTimeoutError: 
	ret = QString("The socket operation timed out"); break;
    case QAbstractSocket::DatagramTooLargeError: 
	ret = QString("The datagram was larger than the operating system's limit (which can be as low as 8192 bytes)"); break;
    case QAbstractSocket::NetworkError: 
	ret = QString("An error occurred with the network (e.g., the network cable was accidentally plugged out)"); break;
    case QAbstractSocket::AddressInUseError: 
	ret = QString("The address specified to bind() is already in use and was set to be exclusive"); break;
    case QAbstractSocket::SocketAddressNotAvailableError: 
	ret = QString("The address specified to bind() does not belong to the host"); break;
    case QAbstractSocket::UnsupportedSocketOperationError: 
	ret = QString("The requested socket operation is not supported by the local operating system"); break;
    case QAbstractSocket::ProxyAuthenticationRequiredError: 
	ret = QString("The socket is using a proxy, and the proxy requires authentication"); break;
    case QAbstractSocket::SslHandshakeFailedError: 
	ret = QString("The SSL/TLS handshake failed, so the connection was closed"); break;
    case QAbstractSocket::UnfinishedSocketOperationError: 
	ret = QString("The last operation attempted has not finished yet (still in progress in the background)"); break;
    case QAbstractSocket::ProxyConnectionRefusedError: 
	ret = QString("Could not contact the proxy server because the connection to that server was denied"); break;
    case QAbstractSocket::ProxyConnectionClosedError: 
	ret = QString("The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)"); break;
    case QAbstractSocket::ProxyConnectionTimeoutError: 
	ret = QString("The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase"); break;
    case QAbstractSocket::ProxyNotFoundError: 
	ret = QString("The proxy address set was not found"); break;
    case QAbstractSocket::ProxyProtocolError: 
	ret = QString(QString("The connection negotiation with the proxy server because the response from the proxy server could not be understood")); break;
    default: ret = QString("An unidentified error occurred"); break;
    };

    return ret;
}

QSslSocket *
SslSocketPrototype::thisSslSocket()
{
    QSslSocket *s = qscriptvalue_cast<QSslSocket*>(thisObject());
    Q_ASSERT(s);

    if (init_flags_.find(s) == init_flags_.end()) {
        connect_notifiers(s);
        init_flags_.insert(s, true);
    }
    return s;
}

void
SslSocketPrototype::connect_notifiers(QSslSocket *s) 
{   
    QObject::connect(s, SIGNAL(connected()), 
                     this, SIGNAL(connected()));
    QObject::connect(s, SIGNAL(disconnected()), 
                     this, SIGNAL(disconnected()));
    QObject::connect(s, SIGNAL(error(QAbstractSocket::SocketError)), 
                     this, SIGNAL(error()));
    QObject::connect(s, SIGNAL(bytesWritten(qint64)), 
                     this, SIGNAL(bytesWritten(qint64)));
    QObject::connect(s, SIGNAL(readyRead()), 
                     this, SIGNAL(readyRead()));

    //
    // QSslSocket events
    //
    QObject::connect(s, SIGNAL(encrypted()),
                     this, SIGNAL(encrypted()));
    QObject::connect(s, SIGNAL(encryptedBytesWritten(qint64)),
                     this, SIGNAL(encryptedBytesWritten(qint64)));

    
}
