#ifndef NETPLUGIN_H
#define NETPLUGIN_H

#include <QScriptExtensionPlugin>

class NetPlugin : public QScriptExtensionPlugin
{
public:
    NetPlugin( QObject *parent = 0);
    ~NetPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //NETPLUGIN_H
