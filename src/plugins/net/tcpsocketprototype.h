#ifndef TCPSOCKETPROTOTYPE_H
#define TCPSOCKETPROTOTYPE_H

#include "binary.h"

#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QObject>
#include <QtCore/QCoreApplication>
#include <QtScript/QScriptable>
#include "streamprototype.h"

#include <QtScript/QScriptValue>
#include <QtNetwork/QTcpSocket>

class TcpSocketPrototype : public StreamPrototype
{
    Q_OBJECT

public:
    TcpSocketPrototype(QObject *parent = 0);
    ~TcpSocketPrototype();

public slots:
    void connectToHost(QString host, quint16 port);
    void disconnectFromHost();
    QString errorString();
    bool isValid();
    QString peerAddress();
    qint16  peerPort();
    
private:
    QTcpSocket *thisTcpSocket();
    virtual QIODevice* thisIODevice();
};

#endif //TCPSOCKETPROTOTYPE_H
