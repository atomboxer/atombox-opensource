#include "tcpserver.h"
#include "tcpsocket.h"
#include "tcpserverprototype.h"


#include <QtCore/QDebug>
#include <QtCore/QList>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QNetworkProxyQuery>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QAuthenticator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(TcpServer*)
Q_DECLARE_METATYPE(TcpSocket*)

TcpServerPrototype::TcpServerPrototype(QObject *parent)
: QObject(parent)
{
}

TcpServerPrototype::~TcpServerPrototype()
{
}

QString
TcpServerPrototype::address()
{
    return thisTcpServer()->serverAddress().toString();
}

void
TcpServerPrototype::close()
{
    return thisTcpServer()->close();
}

QString
TcpServerPrototype::errorString()
{
    return thisTcpServer()->errorString();
}

bool
TcpServerPrototype::isListening()
{
    return thisTcpServer()->isListening();
}

bool
TcpServerPrototype::listen( quint16 port, QScriptValue host)
{
    QHostAddress addr = QHostAddress::Any;
    if (!host.isUndefined()) {
        addr = QHostAddress(host.toString());
    }

    if (port == 0) {
        context()->throwError("Server.prototype.listen, invalid port specified");
        return false;
    }

    return thisTcpServer()->listen(addr, port);
}

QScriptValue
TcpServerPrototype::nextPendingConnection()
{          
    return engine()->newQObject(new TcpSocket(thisTcpServer()->nextPendingConnection()),
                                QScriptEngine::AutoOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                /*| QScriptEngine::ExcludeSuperClassMethods
                                  | QScriptEngine::ExcludeSuperClassProperties*/);
}

quint16
TcpServerPrototype::port()
{
    return thisTcpServer()->serverPort();
}

QTcpServer *
TcpServerPrototype::thisTcpServer()
{
 
    TcpServer *ts = qscriptvalue_cast<TcpServer*>(thisObject());
    if (!ts) {
        qFatal("Programmatic error TcpSocketPrototype::thisTcpSocket");
    }

    return ts->priv_;
}
