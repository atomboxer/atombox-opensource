#ifndef SSLSOCKETPROTOTYPE_H
#define SSLSOCKETPROTOTYPE_H

#include "binary.h"

#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QObject>
#include <QtCore/QCoreApplication>
#include <QtScript/QScriptable>
#include "streamprototype.h"

#include <QtScript/QScriptValue>
#include <QtNetwork/QSslSocket>

class SslSocketPrototype : public StreamPrototype
{
    Q_OBJECT

public:
    SslSocketPrototype(QObject *parent = 0);
    ~SslSocketPrototype();

public slots:
    void connectToHost(QString host, quint16 port);
    void disconnectFromHost();
    QString errorString();
    bool isValid();
    QString peerAddress();
    qint16  peerPort();
    
signals:
    void connected();
    void disconnected();
    void error();

    void encrypted();
    void encryptedBytesWritten(qint64 written);

private:
    QSslSocket *thisSslSocket();
    QHash<QSslSocket*, bool> init_flags_;

public:
    void connect_notifiers(QSslSocket *s);    
};

#endif //SSLSOCKETPROTOTYPE_H
