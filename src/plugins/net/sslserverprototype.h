#ifndef SSLSERVERPROTOTYPE_H
#define SSLSERVERPROTOTYPE_H

#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QObject>
#include <QtCore/QCoreApplication>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "sslserver.h"

class SslServerPrototype : public QObject, protected QScriptable
{
    Q_OBJECT

public:
    SslServerPrototype(QObject *parent = 0);
    ~SslServerPrototype();

public slots:
    QString address();
    void close();
    QString errorString();
    bool isListening(); 
    bool listen(quint16 port, QScriptValue host);
    QScriptValue nextPendingConnection();
    quint16 port();

signals:
    void newConnection();

private:
    SslServer *thisSslServer();
    mutable bool one_time_;
    
private slots:
    inline void notify_newConnection() { emit newConnection(); }
};

#endif //SSLSERVERPROTOTYPE_H
