
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "netplugin.h"

#include "tcpsocket.h"
#include "tcpsocketprototype.h"
#include "tcpserver.h"
#include "tcpserverprototype.h"

#ifndef QT_NO_OPENSSL
#include "sslsocketprototype.h"
#include "sslserverprototype.h"
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptnet_compat, NetPlugin )
#else
Q_EXPORT_PLUGIN2( abscriptnet, NetPlugin )
#endif



NetPlugin::NetPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
 	
NetPlugin::~NetPlugin()
{
}

Q_DECLARE_METATYPE(TcpSocket*)
Q_DECLARE_METATYPE(TcpServer*)
Q_DECLARE_METATYPE(Stream*)
Q_DECLARE_METATYPE( QAbstractSocket::SocketError);
Q_DECLARE_METATYPE( QAbstractSocket::SocketState);

#ifndef QT_NO_OPENSSL
//Q_DECLARE_METATYPE(QSslSocket*)
//Q_DECLARE_METATYPE(SslServer*)
#endif


QScriptValue 
TcpSocket_Constructor(QScriptContext *context, QScriptEngine *engine)
{
    QScriptValue obj =  engine->newQObject(new TcpSocket(new QTcpSocket()), 
                                           QScriptEngine::AutoOwnership,
                                           QScriptEngine::SkipMethodsInEnumeration
                                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                                           /*| QScriptEngine::ExcludeSuperClassProperties*/);

    return obj;
}

// #ifndef QT_NO_OPENSSL
// QScriptValue 
// SslSocket_Constructor(QScriptContext *context, QScriptEngine *engine)
// {
//     QScriptValue obj =  engine->newQObject(new QSslSocket(), QScriptEngine::AutoOwnership,
//                                            QScriptEngine::SkipMethodsInEnumeration
//                                            /*| QScriptEngine::ExcludeSuperClassMethods(*/
//                                            /*| QScriptEngine::ExcludeSuperClassProperties*/);

//     return obj;
// }
// #endif

QScriptValue 
TcpServer_Constructor(QScriptContext *context, QScriptEngine *engine)
{
    return engine->newQObject(new TcpServer(new QTcpServer()), QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration
                              /*| QScriptEngine::ExcludeSuperClassMethods*/
                              /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

// #ifndef QT_NO_OPENSSL
// QScriptValue 
// SslServer_Constructor(QScriptContext *context, QScriptEngine *engine)
// {
//     return engine->newQObject(new SslServer(), QScriptEngine::AutoOwnership,
//                               QScriptEngine::SkipMethodsInEnumeration
//                               /*| QScriptEngine::ExcludeSuperClassMethods*/
//                               /*| QScriptEngine::ExcludeSuperClassProperties*/);
// }
// #endif

void
NetPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("net_compat") ) {
#else
    if ( key == QString("net") ) {
#endif

#ifdef AB_STATIC_PLUGINS
	Q_INIT_RESOURCE(net);
#endif
	Q_INIT_RESOURCE(jsnetfiles);

	QScriptValue __internal__;

	__internal__ = engine->globalObject().property("__internal__");
	if (!__internal__.isObject()) {
	    __internal__ = engine->newObject();
	    engine->globalObject()
		.setProperty("__internal__",__internal__);
	}

        qRegisterMetaType<QAbstractSocket::SocketError>(); 
        qRegisterMetaType<QAbstractSocket::SocketState>(); 

        engine->setDefaultPrototype(qMetaTypeId<TcpSocket*>(),
                                engine->newQObject(new TcpSocketPrototype(this)));

// #ifndef QT_NO_OPENSSL
//         engine->setDefaultPrototype(qMetaTypeId<QSslSocket*>(),
//                                 engine->newQObject(new SslSocketPrototype(this)));
//         engine->setDefaultPrototype(qMetaTypeId<SslServer*>(),
//                                 engine->newQObject(new SslServerPrototype(this)));
// #endif
        engine->setDefaultPrototype(qMetaTypeId<TcpServer*>(),
                                engine->newQObject(new TcpServerPrototype(this)));



	engine->globalObject().property("__internal__").
	    setProperty("TcpSocket", engine->newFunction(TcpSocket_Constructor));
// #ifndef QT_NO_OPENSSL
// 	engine->globalObject().property("__internal__").
// 	    setProperty("SslSocket", engine->newFunction(SslSocket_Constructor));
// 	engine->globalObject().property("__internal__").
// 	    setProperty("SslServer", engine->newFunction(SslServer_Constructor));
// #endif
	engine->globalObject().property("__internal__").
	    setProperty("TcpServer", engine->newFunction(TcpServer_Constructor));

	engine->globalObject().property("__internal__").property("TcpSocket").
	     setProperty("prototype", engine->defaultPrototype(qMetaTypeId<TcpSocket*>()));
// #ifndef QT_NO_OPENSSL
// 	engine->globalObject().property("__internal__").property("SslSocket").
// 	     setProperty("prototype", engine->defaultPrototype(qMetaTypeId<QSslSocket*>()));
// #endif
	engine->globalObject().property("__internal__").property("TcpServer").
	    setProperty("prototype", engine->defaultPrototype(qMetaTypeId<TcpServer*>()));
// #ifndef QT_NO_OPENSSL
// 	engine->globalObject().property("__internal__").property("SslServer").
// 	    setProperty("prototype", engine->defaultPrototype(qMetaTypeId<SslServer*>()));
// #endif

    }
}
 	
QStringList
NetPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("net_compat");
#else
    keys << QString("net");
#endif

    return keys;
}
