#ifndef BYTEARRAYPROTOTYPE_H
#define BYTEARRAYPROTOTYPE_H

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"

class ByteArrayPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    ByteArrayPrototype(QObject *parent = 0);
    ~ByteArrayPrototype();

public slots:
    QScriptValue codeAt(int offset) const;
    ByteArray concat( QScriptValue /*other*/) const;
    ByteArray compress() const;
    ByteArray uncompress() const;
    ByteArray hash32() const;
    ByteArray hash64() const;
    ByteArray hash64WithSeed(ByteArray seed) const;
    ByteArray hash128() const;
    ByteArray hash128WithSeed(ByteArray seed) const;
    QScriptValue copy(QScriptValue target,
     		      int start,
     		      int stop,
     		      int targetStart = 0) const;
    QString decodeToString(QScriptValue charset=QScriptValue()) const;
    QScriptValue get(int offset) const;   
    int indexOf( QScriptValue arg, int from = 0 ) const ;
    bool isPrint() const;
    int lastIndexOf( QScriptValue arg, int from = -1 ) const;
    int pop() const;
    int push(QScriptValue values) const;
    ByteArray reverse() const;
    int shift() const;
    ByteArray slice() const;
    ByteArray slice(QScriptValue /*args*/) const;
    QList<int> toArray(QString charset = "utf-8")  const;
    ByteArray toByteArray() const;
    ByteArray toByteArray(QString src_charset,
                          QString dst_charset) const;
    ByteString toByteString() const;
    ByteString toByteString(QString src_charset,
                            QString dst_charset) const;

    int unshift(QScriptValue values) const;
    QScriptValue valueOf() const;

    //
    // Non CommonJS
    //
    ByteArray replace( int pos, 
			int len,
			QScriptValue arg) const;

    QScriptValue readInt8( qint32 offset = 0 ) const;
    QScriptValue readInt16( qint32 offset = 0 ) const;
    QScriptValue readInt32( qint32 offset = 0 ) const;
    QScriptValue readUInt8( qint32 offset = 0 ) const;
    QScriptValue readUInt16 (qint32 offset = 0 ) const;
    QScriptValue readUInt32( qint32 offset = 0 ) const;

    QScriptValue writeInt8 ( qint8 value, qint32 offset = 0 ) const;
    QScriptValue writeInt16 ( qint16 value, qint32 offset = 0 ) const;
    QScriptValue writeInt32 ( qint32 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt8 ( quint8 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt16( quint16 value, qint32 offset = 0 ) const;
    QScriptValue writeUInt32( quint32 value, qint32 offset = 0 ) const;

    QScriptValue toNumber() const;

 private:
    ByteArray *thisByteArray() const;
};

#endif //BYTEARRAYPROTOTYPE_H
