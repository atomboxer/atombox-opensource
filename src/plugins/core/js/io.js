/**
 *  The io module.
 *  @module io
 *  @see module:fs
 *  @see module:net
 *  @see module:ctree/ace
 *  @desc
This module provides implementations for the Stream/TextStream classes, based on the [IO/A CommonJS] ( 
http://wiki.commonjs.org/wiki/IO/A) proposal.

Stream class serves as a base implementation for other classes from modules that need to support (a)synchronous 
reading and writing of blocks of data, such as "fs", "ctree/ace", "net" or "http".

The signals of the Stream objects can be connected to functions in order to check the status of operations as in the example below:

 * @example
var console = require("console");
require("io");

var stream = new Stream( new ByteString("Hello World!") );

var ba = stream.read(5); // read 5 bytes from the stream

//Connect the aboutToClose signal
stream.aboutToClose.connect(function() { 
    console.write("About to close!\n");
});

//Connect the bytesWritten signal
stream.bytesWritten.connect(function(num) { 
    console.write("We have written"+num+" bytes!\n");
});

stream.write("fancy".toByteString()); // We have written 5 bytes!

// seek to position 0 using the position property
stream.position = 0;
console.write(stream.read().decodeToString()); // Hellofancyd!

stream.close(); //About to close!
 *
 */

ByteArray = require("binary").ByteArray;

/**
 * @class
 * @name Stream
 * @classdesc This class provides as a common implementation for raw data stream classes. 
You can either create a binary Stream object from a ByteArray or ByteString or you can get a new instance from the `fs`, `ctree/ace`, `net` and `http` modules.
 * @summary Common implementation for raw data stream classes. 
 * @arg {ByteArray|ByteString} ba
 * @memberof module:io
 * @requires module:binary
 * @requires module:io
 * @see module:fs
 * @see module:net
 * @see module:ctree/ace
 * @example
var ByteArray = require("binary").ByteArray;
var Stream = require("io").Stream;

var ba = ByteArray("123456789");
var s = new Stream(ba);

print(s.read(1).decodeToString("hex")); // 31
 */

/**
 * Position property. The position within the Stream at which the next read or write operation occurs, in bytes. 
This property is only defined for seekable streams and is only writable for streams that are both seekable and 
writable. Setting a new value moves the stream posititon to the given byte offset. Accesing this property may 
throw an error if the underlying operation did not succeed.
 * @member {Number} module:io.Stream.prototype.position
 * @see Stream.prototype.isSequential
 * @throws Error
 * @example 

var console = require("console");
require("binary");
require("io");

var s = new Stream(new ByteString("Hello World"));
s.read(5);

console.write(s.position); //5

s.position = 1; // seek to position 1

console.write(s.read(1).decodeToString()); //e
 */

/**
 * Key property. The current primary key at which the stream is positioned. Setting a new value positiones the stream to the key provided.
 * @note In Guardian the setKey method uses the 'aproximate' algorithm and never fails. You need to read a new record and check the currentKey in order to do a proper positioning.
 * @note In streams created by `ctree/ace`, this method uses the 'GTEKey' which returns a greater or equal key, therefore the new key can be totally unrelated.
 * @member {ByteArray} module:io.Stream.prototype.key
 * @see Stream.prototype.getKey
 * @see Stream.prototype.setKey
 */

/**
 * Closes the stream, freeing the resources it is holding
 * @method module:io.Stream.prototype.close
 */

/**
 * Reads from this stream with read(), writing the results to the target stream and flushing, until the source has been exhausted.
 * @method module:io.Stream.prototype.copy
 * @arg {Stream} target
 * @throws {Error}
 */

/**
 * Flushes the bytes written to the stream to the underlying medium
 * @method module:io.Stream.prototype.flush
 */

/**
 * Returns true if the stream is at the end
 * @method module:io.Stream.prototype.atEnd
 * @return {Boolean}
 */

/**
 * Returns true if the stream is closed, false otherwise
 * @method module:io.Stream.prototype.isClosed
 * @return {Boolean}
 */

/**
 * Returns true if the stream is readable, false otherwise
 * @method module:io.Stream.prototype.isReadable
 * @return {Boolean}
 */

/**
 * Returns true if the stream is sequential, false otherwise
 * @method module:io.Stream.prototype.isSequential
 * @return {Boolean}
 */

/**
 * Returns true if the stream is writable, false otherwise
 * @method module:io.Stream.prototype.isWritable
 * @return {Boolean}
 */

/**
 * Read up to n bytes from the stream, or until the end of the stream has been reached. 
An empty ByteString is returned when the end of the stream has been reached.

@note When dealing with Enscribe files opended in structured mode, or ctree/ace streams, this method reads the next record after the current key.

 * @method module:io.Stream.prototype.read
 * @arg {Number} [n=4096]
 * @return {ByteString}
 */

/**
 * Read bytes from the stream into the ByteArray buffer. This method does not increase the length 
of the ByteArray buffer. 
 * @method module:io.Stream.prototype.readInto
 * @arg {ByteArray} buffer
 * @arg {Number} [begin = 0]
 * @arg {Number} [end = length of buffer]
 * @return {Number} The number of bytes read.
 */

/**
 * Try to skip over n bytes in the stream.
 * @method module:io.Stream.prototype.skip
 * @arg {Number} bytes
 * @return {Boolean}
 */

/**
 * Write bytes from b to the stream. If begin and end are specified, only the range starting at begin 
and ending before end are written.
 * @method module:io.Stream.prototype.write
 * @arg {ByteArray|ByteString} b
 * @arg {Number} [begin = 0]
 * @arg {Number} [end = length of b]
 * @note In case of structured files, instead of a binary argument, a numeric value of 0 can be used to
specify a record deletion.
 * @throws {Error}
 */

/**
 * Returns the number of bytes that are available for reading. This method is commonly used with 
sequential devices to determine the number of bytes to allocate in a buffer before reading.
 * @method module:io.Stream.prototype.bytesAvailable
 * @return {Number}
 * @note Throws Error (and returns -1) for Guardian Enscribe files opened with structured access and `ctree/ace`
 * @throws {Error}
 */

/**
 * Locks the record at current position.
 * @node In Guardian locks the current record. It does not lock the whole stream.
 * @method module:io.Stream.prototype.lockRecord
 * @return {Boolean}
 * @note Guardian (Enscribe files)/structured files only
 */

/**
 * Unlocks the record at current position.
 * @node In Guardian it unlocks the current record
 * @method module:io.Stream.prototype.unlockRecord
 * @return {Boolean}
 * @note Guardian (Enscribe files) and ctree/ace structured files only
 */

/**
 * Try to position to the specified key. For alternate keys, you can provide a numeric key specifier.
 * @note In Guardian the setKey method uses the 'aproximate' algorithm and never fails. 
You need to read a new record and check the currentKey in order to do a proper positioning.
 * @note In `ctree/ace` the setKey method positions to the record having the key equal to the value 
returned by the 'GetGTEKey' function, therefore the new key can be totally unrelated.

 * @method module:io.Stream.prototype.setKey
 * @arg {ByteArray|ByteString} key
 * @arg {Number} [key_spec=0] Key specifier
 * @return {Boolean}
 * @throws {Error}
 * @note Guardian (Enscribe files) and ctree/ace structured files only
 */

/**
 * Returns the current key at which the stream is positioned
 * @method module:io.Stream.prototype.getKey
 * @arg {Number} [key_spec=0] Key specifier 
 * @return {ByteArray}
 * @note Guardian (Enscribe files) and ctree/ace structured files only
 */

/**
 * This signal is emitted when the device is about to close
 * @event module:io.Stream.prototype.aboutToClose
 * @note This is never emitted for the Enscribe/Ctree streams
 */

/**
 * This signal is emitted every time a payload of data has been written to the device
 * @event module:io.Stream.prototype.bytesWritten
 * @arg {Number} bytes
 * @note This is never emitted for the Enscribe/Ctree streams
 */

/**
 * This signal is emitted when the input (reading) stream is closed in this device
 * @event module:io.Stream.prototype.readStreamFinished
 * @note This is never emitted for the Enscribe/Ctree streams
 */

/**
 * This signal is emitted once every time new data is available for reading from the device. 
It will only be emitted again once new data is available.
 * @note This is never emitted for the Enscribe/Ctree streams
 * @event module:io.Stream.prototype.readyRead
 */

exports.Stream = Stream = __internal__.Stream;

/**
 * @class
 * @name TextStream
 * @classdesc This class wraps a raw Stream and exposes methoods that deal with strings.
 * @summary Text wrapper for a raw data stream classes. 
 * @arg {Stream} stream
 * @arg {Object} options May have the following properties: charset: a string containing the name of the encoding to use, newline: a string containing the newline character sequence to use, delimiter: a string containing the delimiter to use in print() 
 * @memberof module:io
 * @requires module:binary
 * @requires module:io
 * @example
var console = require("console");
require("io");

var stream = new Stream("Hello world\n".toByteString());

var text_stream = new TextStream(stream);

text_stream.write("GAGA ");

console.write(text_stream.raw.position); //5

//seek back to position 0
text_stream.raw.position = 0; 

console.write(text_stream.readLine()); //GAGA  world

text_stream.raw.close();
 */

/**
 * A readonly property containing the raw byte stream wrapped by this text stream.
 * @member {Number} module:io.TextStream.prototype.raw
 */

/**
 * Reads a line from the stream. If the end of the stream is reached before any data is gathered, returns an empty string. Otherwise, returns the line including the newline.
 * @method module:io.TextStream.prototype.readLine
 * @return {String}
 */

/**
 * Returns an Array of Strings accumulated by calling readLine until an empty string turns up. Does not include the final empty string, and does include newline at the end of every line.
 * @method module:io.TextStream.prototype.readLines
 * @return {Array} array of strings
 */

/**
 * Returns the next line of input without the newline
 * @method module:io.TextStream.prototype.next
 * @return {String}
 */

/**
 * Writes a delimiter delimited array of values as Strings terminated with a newline, then flushes.
 * @method module:io.TextStream.prototype.print
 * @arg {Array} array array of strings
 */

/**
 * Writes a string.
 * @method module:io.TextStream.prototype.write
 * @arg {String} string
 */

/**
 * Writes line followed by a newline.
 * @method module:io.TextStream.prototype.writeLine
 * @arg {String} line
 */

/**
 * Similar to print except it doesn't flush.
 * @method module:io.TextStream.prototype.writeLines
 * @arg {Array} array array of strings
 */

/**
 * Reads from this stream, writing the results to the target stream and flushing, until the source has been exhausted. 
 * @method module:io.TextStream.prototype.copy
 * @arg {Stream} target
 */
exports.TextStream = TextStream = __internal__.TextStream;
