require("io");

/**
 *  @module fs
 *  @desc The "fs" module provides a file system API for the manipulation of paths, directories, files, links, and the construction of file streams.
    The most useful function is `open` that opens an external file and returns an instance of class module:io.Stream or module:io.TextStream. In case of an error it throws an exception.
 *  @summary File System module
 *  @example

 //open a file in text mode
 var fs = require("fs");

 var stream = undefined;
 try {
      stream = fs.open("somefile", "r"); //read and text(default)
 } catch(e) {};

 //stream instanceof TextStream
 var line = stream.readLine();
 stream.raw.close();

*  @example

 //opens a file in binary mode
 var fs = require("fs");

 var stream = undefined;
 try {
      stream = fs.open("somefile", {read:true, binary:true}); //read and binary (alternative notation)
 } catch(e) {};

 //read five bytes and return a ByteArray object (stream instanceof Stream)
 var ba = stream.read(5); 
 stream.close();

* @example
  //opens a HP NonStop - Guardian Enscribe file (or Windows and Unix c-tree)
  var fs = require("fs");
  
  var stream = undefined;
  try {
  stream = fs.open("somefile", "rbs"); //read binary and structured mode
  } catch(e) {};
  
  while(!stream.atEnd()) {
       var row = stream.read(); //reads a record up to 4096 bytes
       //..
  }
  stream.close();
  * @see module:io.Stream
  * @see module:io.TextStream
 */


/**
 *  Moves a file at one path to another.
 *  @function module:fs.move
 *  @arg {String} source
 *  @arg {String} target
 *  @return {Boolean}
 *  @example 
var fs = require("fs");
fs.move("/G/user/file", "/G/user/newname"); //unix 
fs.move("$user.subvol.file", "$user.subvol.newname"); //guardian
fs.move("C:\\tmp.txt", "C:/newname.txt"); //windows
 */
exports.move = move = __internal__.move;

/**
 * Returns an IO stream that supports an appropriate interface for the given options and mode

    The _options_ parameter is an Object with the following properties:
        read:      Boolean - Open for reading, do not create, set position to beginning of the file. Throws if the file does not exist
        write:     Boolean - Open for writing, create or truncate, set position to the beginning of the file
        append:    Boolean - Open for appending, create if it doesn't exist, do not truncate, set position to end of the file 
        update:    Boolean - Open for updating, create if it doesn't exist, do not truncate, set position to the beginning of the file
        binary:    Boolean - Return a raw Stream instead of a TextStream
        exclusive: Boolean - Open for write only if it does not already exist, otherwise throw an error. 
###### 

    The _mode_ parameter can substitute the _options_ and it is any substring of "rwa+bxs" in any order, with the following meaning:
        r - Read
        w - Write
        a - Append
        + - Update
        b - Binary
        x - Exclusive Access
        s - Structured Access (ex: Tandem/Enscribe or c-tree)

 *  @function module:fs.open
 *  @arg {String} path Any string value that can be interpreted as a fully-qualified path, or a path relative to the current working directory
 *  @arg {String|Object} mode|options See above description
 *  @return {Stream|TextStream}
 *  @note Opening an Enscribe file on HP NonStop Guardian/ without the structured mode set, will result in a raw binary stream that ignores the file structure, blocks, block sizes, and keys.
 *  @red
 */
exports.open = open = __internal__.open;

/**
 *  Removes the file at the given path. Returns a Boolean representing the result of the operation.
 *  @function module:fs.remove
 *  @arg {String} target
 *  @return {Boolean}
 */
exports.remove = remove = __internal__.remove;

/**
 *  @function module:fs.touch
 *  @arg {String} target
 *  @note Not implemented yet
 *  @return {Boolean}
 */
exports.touch  = touch = __internal__.touch;


//tests
/**
 *  @function module:fs.exists
 *  @arg {String} target
 *  @return {Boolean}
 */
exports.exists = exists = __internal__.exists;

/**
 *  Returns true if the file exists, false otherwise
 *  @function module:fs.isFile
 *  @arg {String} target
 *  @return {Boolean}
 */
exports.isFile = isFile = __internal__.isFile;

/**
 *  @function module:fs.isDirectory
 *  @arg {String} path
 *  @return {Boolean}
 *  @note For platforms where there is no concept of "directory", undefined is returned.
 */
exports.isDirectory = isDirectory = __internal__.isDirectory;

/**
 *  Returns true whether a symbolic link/shortcut exists.
 *  @arg {String} target
 *  @function module:fs.isLink
 *  @return {Boolean}
 *  @note For platforms where there is no concept of "symbolic link", undefined is returned.
 */
exports.isLink = isLink = __internal__.isLink;

/**
 *  @function module:fs.isReadable
 *  @arg {String} target
 *  @return {Boolean}
 */
exports.isReadable = isReadable = __internal__.isReadable;

/**
 *  @function module:fs.isWritable
 *  @arg {String} target
 *  @return {Boolean}
 */
exports.isWritable = isWritable = __internal__.isWritable;

/**
 *  @function module:fs.same
 *  @arg {String} pathA
 *  @arg {String} pathB
 *  @return {Boolean}
 */
exports.same = same = __internal__.same;

//paths as text
/**
 *  Returns the part of the path that is after the last directory (or subvolume) separator. If there is a "file extension" concept, the extension is removed.
 *  @function module:fs.base
 *  @arg {String} path
 *  @return {String}
 */
exports.base = base = __internal__.base;

/**
 *  Returns the absolute path, starting with the root of this file system object, for the given path.
 *  @function module:fs.absolute
 *  @arg {String} path
 *  @return {String}
 */
exports.absolute = absolute = __internal__.absolute;

//attributes
/**
 *  Returns the time that a file was last modified as a Date object. 
 *  @function module:fs.lastModified
 *  @arg {String} target
 *  @return {Date}
 */
exports.lastModified = lastModified = __internal__.lastModified;

/**
 *  Returns the time that a file was last opened for reading accesses
 *  @function module:fs.lastRead
 *  @arg {String} target
 *  @return {Date}
 */
exports.lastRead = lastRead = __internal__.lastRead;

/**
 *  Returns the size opf the file 
 *  @function module:fs.size
 *  @arg {String} target
 *  @return {Number}
 */
exports.size = size = __internal__.size;

//list
/**
 * Returns the names of all the files in a directory (or subvolume)
 *  @function module:fs.list
 *  @arg {String} path
 *  @return {Array} array of strings
 */
exports.list = list = __internal__.list;
