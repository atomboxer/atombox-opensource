require("io");

/**
 *  @module system
 *  @desc The "system" module provides operating-system related functions and utilities
 *  @summary System module
 */

/**
 *  The standard input stream, an object with the same API as a file opened with the mode "r"
 *  @member {Stream} module:system.stdin
 */

exports.stdin   = __internal__.stdin;
/**
 *  The standard output stream, an object with the same API as a file opened with the mode "w"
 *  @member {Stream} module:system.stdout
 */
exports.stdout  = __internal__.stdout;

/**
 *  The standard error stream, an object with the same API as a file opened with the mode "w"
 *  @member {Stream} module:system.stderr
 */
exports.stderr  = __internal__.stderr;

/**
 *  Array containing posix-style environment variables
 *  @member {Array} module:system.env
 */
exports.env  = __internal__.env;

/**
 *  An Array containing the command line arguments for the invoking arguments, not including the atombox 
 *  interpreter executable.
 *  @member {Array} module:system.args
 */
exports.args = __internal__.args;

/**
 *  Exits the event loop and returns.
 *  @method module:system.exit
 *  @arg {Number} exit_code Platform exit code
 */
exports.exit = __internal__.exit;

/**
 *  Suspends execution of the event loop for the given seconds
 *  @method module:system.sleep
 *  @arg {Number} seconds
 *  @note This freezes the event loop. Use timers instead.
 */
exports.sleep = __internal__.sleep;

/**
 *  A string that contains the platform name
 *  @member {String} module:system.platform
 */
exports.platform = __internal__.platform;

/**
 *  A string that contains the local host name
 *  @member {String} module:system.hostname
 */
exports.hostname = __internal__.hostname;

/**
 *  A string that contains the group identifier(if available)
 *  @member {String} module:system.gid
 */
exports.gid = __internal__.gid;

/**
 *  A string that contains the user identifier(if available)
 *  @member {String} module:system.uid
 */
exports.uid = __internal__.uid;

/**
 *  A string that contains the process identifier
 *  @member {String} module:system.pid
 */
exports.pid = __internal__.pid;

/**
 *  A string that contains the login name
 *  @member {String} module:system.login
 */
exports.login = __internal__.login;

/**
 *  Returns usefull stats about the current process (heap, stack, etc)
 *  @method module:system.processStats
 *  @returns {Object}
 */
exports.processStats = __internal__.processStats;
