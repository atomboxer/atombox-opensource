/**
 *  @module args
 *  @desc This module provides the Parser class for parsing the command line arguments.
 */
var fs = require("fs");

function startsWith(string, substring) {
    return string.indexOf(substring) == 0;
}

function pad(string, fill, length, mode) {
    if (typeof string !== "string") {
        string = string.toString();
    }
    if (fill == null || length == null) {
        return string;
    }
    var diff = length - string.length;
    if (diff == 0) {
        return string;
    }
    var left, right = 0;
    if (mode == null || mode > 0) {
        right = diff;
    } else if (mode < 0) {
        left = diff;
    } else if (mode == 0) {
        right = Math.round(diff / 2);
        left = diff - right;
    }
    var list = [];
    for (var i = 0; i < left; i++) {
        list[i] = fill[i % fill.length];
    }
    list.push(string);
    for (i = 0; i < right; i++) {
        list.push(fill[i % fill.length]);
    }
    return list.join('');
}

function toCamelCase(string) {
    return string;
}

/**
 * @class
 * @name Parser
 * @memberof module:args
 * @classdesc The Parser class provides a high level API for parsing the command line arguments (system.args) in AtomBox scripts.
 * @summary Parser class for parsing the system.args (command line arguments)
 * @see module:system.args
 * @example

 //gotst file
 var console = require("console");
 var system  = require("system");
 var Parser  = require("args").Parser;

 var p = new Parser();
 p.addOption("h" , "help", "", "This is the help text", false);

 var options = p.parse(system.args);

 if (options["help"] != undefined) {
     console.write("help switch activated!");
     system.exit(0);
 } else {
     console.write(p.usage());
 }
 
 // run ab gotst --help : displays "help switch activated!" and exits
 // run ab gotst        : displays:
 
 // SCRIPT USAGE:
 //   gotst   [--help]
 //
 //  Where:
 //    -h --help
 //     This is the help text
 */
exports.Parser = function() {
    var options = [];
    
    /**
     *  Adds a recognizable switch to the Parser object.
     *  @method module:args.Parser.prototype.addOption
     *  @arg {String}  shortName - Short variant of the switch (ex: -h)
     *  @arg {String}  longName  - Long variant of the switch (ex: --help)
     *  @arg {String}  argument  - Argument name. Used by .usage() and help()
     *  @arg {String}  helpText  - Help Text. Used by .usage() and help()
     *  @arg {Boolean} required  - true if the switch is required, false otherwise
     */
    this.addOption = function(shortName, longName, argument, helpText, required) {
        if (shortName && shortName.length != 1) {
            throw new Error("Short option must be a string of length 1");
        }

	if (required == undefined || required == null) {
	    required = false; 
	}

        longName = longName || "";
        argument = argument || "";
        options.push({
            shortName: shortName,
            longName: longName,
            argument: argument,
	    required: required,
            helpText: helpText
        });

        return this;
    };

    /**
     * Returns a generated "help" string
     * @method module:args.Parser.prototype.help
     * @return {String}
     */
    this.help = function() {
        var lines = new Array();
        for (var opt in options) {
            var flags;
            if (options[opt].shortName !== undefined && options[opt].shortName !== null) {
                flags = " -" + options[opt].shortName;
            } else {
                flags = "   ";
            }
            if (options[opt].longName) {
                flags += " --" + options[opt].longName;
            }
            if (options[opt].argument) {
                flags += " " + options[opt].argument;
            }
            lines.push({flags: flags, helpText: options[opt].helpText});
        }

	var maxlength = lines.reduce(function(prev, val) {
	     return Math.max(val.flags.length, prev)
	}, 0 );

	return lines.map(
             function(s) {return pad(s.flags, " ", 2 + maxlength) + s.helpText}
        ).join("\n");

    };

    /**
     * Returns a generated "usage" string
     * @method module:args.Parser.prototype.usage
     * @return {String}
     */
    this.usage = function() {
	var text = "\n";
	text += "SCRIPT USAGE:\n    "+fs.base(require.main)+"   ";
	for (var opt in options) {
	    if (options[opt].longName != undefined && options[opt].longName) {
		text += "[--"+options[opt].longName;
		
		if (options[opt].argument) 
		    text += " "+options[opt].argument+"] ";
		else
		    text += "] ";
	    }
	    else if (options[opt].shortName != undefined) {
		text += "[-"+options[opt].shortName;

		if (options[opt].argument) 
		    text += " "+options[opt].argument+"] ";
		else
		    text += "] ";

	    }
	}
	text += "\n\nWhere:\n"

        var lines = new Array();
        for (var opt in options) {
            var flags;
            if (options[opt].shortName !== undefined && options[opt].shortName !== null) {
                flags = " -" + options[opt].shortName;
            } else {
                flags = "   ";
            }
            if (options[opt].longName) {
                flags += " --" + options[opt].longName;
            }
            if (options[opt].argument) {
                flags += " " + options[opt].argument;
            }
            lines.push({flags: flags, helpText: options[opt].helpText});
        }

	lines = lines.map(
            function(s) {return "    " + s.flags+"\n      "+s.helpText+"\n";}
        ).join("\n");

	text += lines;

	return text;
	
    }

    /**
     * This method is invoked afther all the options are added to the parser object, and returns an object with the coresponding "options" properties for the provided arguments to be parsed.
     * @method module:args.Parser.prototype.parse
     * @arg {Array} args - arguments (usualy it is system.args)
     * @return {Object}
     */
    this.parse = function(args, result) {
        result = result || {};
        while (args.length > 0) {
            var option = args[0];

            if (!startsWith(option, "-")) {
		if (require.main.search(option) == -1) {
		    if (result['unlabeled'] == undefined)
			result['unlabeled'] = new Array();
		    
		    result['unlabeled'].push(option);
		}
		args.shift();
		continue;
            }
            if (startsWith(option, "--")) {
                parseLongOption(option.substring(2), args, result);
            } else {
                parseShortOption(option.substring(1), args, result);
            }
        }
        return result;
    };

    function parseShortOption(opt, args, result) {
        var length = opt.length;
        var consumedNext = false;
        for (var i = 0; i < length; i++) {
            var def = null;
            var c = opt.charAt(i);

            for (var d in options) {
                if (options[d].shortName == c) {
                    def = options[d];
                    break;
                }
            }
            if (def == null) {
                unknownOptionError("-" + c);
            }
            var optarg = null;
            if (def.argument) {
                if (i == length - 1) {
                    if (args.length <= 1) {
                        missingValueError("-" + def.shortName);
                    }
                    optarg = args[1];
                    consumedNext = true;
                } else {
                    optarg = opt.substring(i + 1);
                    if (optarg.length == 0) {
                        missingValueError("-" + def.shortName);
                    }
                }
                i = length;
            }

            var propertyName = def.longName || def.shortName;
            result[toCamelCase(propertyName)] = optarg || true;
        }
        args.splice(0, consumedNext ? 2 : 1);
    }

    function parseLongOption(opt, args, result) {
        var def = null;
        for(var d in options) {
            if (opt == options[d].longName || (startsWith(opt, options[d].longName)
                    && opt.charAt(options[d].longName.length) == '=')) {
                def = options[d];
                break;
            }
        }
        if (def == null) {
            unknownOptionError("--" + opt);
        }
        var optarg = null;
        var consumedNext = false;
        if (def.argument) {
            if (opt == def.longName) {
                if (args.length <= 1) {
                    missingValueError("--" + def.longName);
                }
                optarg = args[1];
                consumedNext = true;
            } else {
                var length = def.longName.length;
                if (opt.charAt(length) != '=') {
                    missingValueError("--" + def.longName);
                }
                optarg = opt.substring(length + 1);
            }
        }
        result[toCamelCase(def.longName)] = optarg || true;
        args.splice(0, consumedNext ? 2 : 1);
    }
};

function missingValueError(option) {
    throw new Error(option + " option requires a value.");
}


function unknownOptionError(option) {
    throw new Error("Unknown option: " + option);
}
