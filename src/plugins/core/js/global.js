/**
 * @constant
 * @name NaN
 * @global
 * @builtin
 */

/**
 * @constant
 * @name Infinity
 * @global
 * @builtin
 */

/**
 * @constant
 * @name undefined
 * @global
 * @builtin
 */

/**
 * Process all events in the event loop. Very important when all signals need to be processed in order.
 * @function
 * @name processEvents
 * @global
 * @builtin
 */

/**
 * Evaluates a string of JavaScript code without reference to a particular object.
 * @function
 * @name eval
 * @arg {String} code
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/eval}
 */

/**
 * Parses a string argument and returns an integer of the specified radix or base.
 * @function
 * @name parseInt
 * @arg {String} string
 * @arg {Number} [radix]
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/parseInt}
 */

/**
 * Parses a string argument and returns a floating point number.
 * @function
 * @name parseFloat
 * @arg {String} string
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/parseFloat}
 */

/**
 * Evaluates an argument to determine whether it is a finite number.
 * @function
 * @name isFinite
 * @arg {Number} number
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/isFinite}
 */

/**
 * Decodes a Uniform Resource Identifier (URI) previously created by encodeURI or by a similar routine
 * @function
 * @name decodeURI
 * @arg {String} encodedURI
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/decodeURI}
 */

/**
 * Decodes a Uniform Resource Identifier (URI) component previously created by encodeURIComponent or by a similar routine
 * @function
 * @name decodeURIComponent
 * @arg {String} encodedURIComponent
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/decodeURIComponent}
 */

/**
 * Encodes a Uniform Resource Identifier (URI) by replacing each instance of certain characters by one, two, three, or four escape sequences representing the UTF-8 encoding of the character (will only be four escape sequences for characters composed of two "surrogate" characters)
 * @function
 * @name encodeURI
 * @arg uri
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/encodeURI}
 */

/********************CONSTRUCTOR FUNCTIONS**********************************************************************/

/**
 * Creates an object wrapper.
 * @constructor
 * @name Object
 * @arg [value] Any value
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object}
 */

/**
 * Returns the prototype of the specified object.
 * @method Object.getPrototypeOf
 * @arg obj The object on which to define the property
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/getPrototypeOf}
 */

/**
 * Returns a property descriptor for an own property (that is, one directly present on an object, not present by dint of being along an object's prototype chain) of a given object.
 * @method Object.getOwnPropertyDescriptor
 * @arg obj The object in which to look for the property
 * @arg prop The name of the property whose description is to be retrieved
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/getOwnPropertyDescriptor}
 */

/**
 * Returns an array of all properties (enumerable or not) found upon a given object
 * @method Object.getOwnPropertyNames
 * @arg obj
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames}
 */

/**
 * Creates a new object with the specified prototype object and properties
 * @method Object.create
 * @arg obj
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/create}
 */

/**
 * Defines a new property directly on an object, or modifies an existing property on an object, and returns the object.
 * @method Object.defineProperty
 * @arg obj The object on which to define the property.
 * @arg prop The name of the property to be defined or modified.
 * @arg descriptor The descriptor for the property being defined or modified.
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/defineProperty}
 */

/**
 * Defines new or modifies existing properties directly on an object, returning the object
 * @method Object.defineProperties
 * @arg obj
 * @arg props
 * @returns {Object}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/defineProperties}
 */

/**
 * Returns an array of all own enumerable properties found upon a given object
 * @method Object.keys
 * @arg obj
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/keys}
 */

/**
 * Returns a string representing the object
 * @method Object.prototype.toString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/toString}
 */

/**
 * Returns a string representing the object
 * @method Object.prototype.toLocaleString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/toLocaleString}
 */

/**
 * Returns the primitive value of the specified object
 * @method Object.prototype.valueOf
 * @returns {primitive}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/valueOf}
 */

/**
 * Returns a boolean indicating whether the object has the specified property
 * @method Object.prototype.hasOwnProperty
 * @arg v
 * @returns {Boolean}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/hasOwnProperty}
 */

/**
 * Tests for an object in another object's prototype chain
 * @method Object.prototype.isPrototypeOf
 * @arg v
 * @returns {Boolean}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/isPrototypeOf}
 */

/**
 * Returns a Boolean indicating whether the specified property is enumerable
 * @method Object.prototype.propertyIsEnumerable
 * @arg v
 * @returns {Boolean}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/propertyIsEnumerable}
 */


/*
 * Function
 */

/**
 * Every function in JavaScript is actually a Function object
 * @constructor
 * @name Function
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function}
 */

/**
 * Returns a string representing the source code of the function
 * @method Function.prototype.toString
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/toString}
 */

/**
 * Calls a function with a given this value and arguments provided as an array
 * @method Function.prototype.apply
 * @arg thisArg
 * @arg argArray
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/apply}
 */

/**
 * Calls a function with a given this value and arguments provided individually
 * @method Function.prototype.call
 * @arg thisArg
 * @arg  {...Array} [argArray]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/call}
 */


/*
 * Array
 */

/**
 * The JavaScript Array global object is a constructor for arrays, which are high-level, list-like objects
 * @note The methods in color red, are AtomBox enhancements and are not part of the standard
 * @constructor
 * @name Array
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array}
 */

/**
 * Returns a string representing the specified array and its elements
 * @method Array.prototype.toString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/toString}
 */


/**
 * Returns a new array comprised of this array joined with other array(s) and/or value(s)
 * @method Array.prototype.concat
 * @arg {Array} str
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/concat}
 */

/**
 * Joins all elements of an array into a string
 * @method Array.prototype.join
 * @arg {String} [separator=","]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/join}
 */

/**
 * Removes the last element from an array and returns that element
 * @method Array.prototype.pop
 * @returns {Object}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/pop}
 */

/**
 * Mutates an array by appending the given elements and returning the new length of the array
 * @method Array.prototype.push
 * @arg {Object} item
 * @returns {Number} The length of the new array
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/push}
 */

/**
 * The reverse method transposes the elements of the calling array object in place, mutating the array, and returning a reference to the array
 * @method Array.prototype.reverse
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/reverse}
 */

/**
 * Removes the first element from an array and returns that element. This method changes the length of the array
 * @method Array.prototype.shift
 * @returns {Object} The removed element
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/shift}
 */

/**
 * Returns a one-level deep copy of a portion of an array
 * @method Array.prototype.slice
 * @arg start
 * @arg [end]
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/slice}
 */

/**
 * Sorts the elements of an array in place and returns the array.
 * @method Array.prototype.sort
 * @arg {Function} [compareFunction]
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/sort}
 */

/**
 * Changes the content of an array, adding new elements while removing old elements
 * @method Array.prototype.splice
 * @arg start
 * @arg [deleteCount]
 * @arg {Object} [item]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/splice}
 */

/**
 * Adds one or more elements to the beginning of an array and returns the new length of the array
 * @method Array.prototype.unshift
 * @arg item1[,item2[,...]]
 * @returns {Number} The new length of the array
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/unshift}
 */

/**
 * Returns the first index at which a given element can be found in the array, or -1 if it is not present
 * @method Array.prototype.indexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf}
 */

/**
 * Returns the last index at which a given element can be found in the array, or -1 if it is not present. The array is searched backwards, starting at fromIndex.
 * @method Array.prototype.lastIndexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/lastIndexOf}
 */

/**
 * Tests whether all elements in the array pass the test implemented by the provided function
 * @method Array.prototype.every
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/every}
 */

/**
 * Tests whether some element in the array passes the test implemented by the provided function
 * @method Array.prototype.some
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/some}
 */

/**
 * Executes a provided function once per array element
 * @method Array.prototype.forEach
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/forEach}
 */

/**
 * Creates a new array with the results of calling a provided function on every element in this array
 * @method Array.prototype.map
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/map}
 */

/**
 * Creates a new array with all elements that pass the test implemented by the provided function
 * @method Array.prototype.filter
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/filter}
 */

/**
 * Apply a function against an accumulator and each value of the array (from left-to-right) as to reduce it to a single value
 * @method Array.prototype.reduce
 * @arg {Function} callbackfn
 * @arg [initialValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/reduce}
 */

/**
 * Apply a function simultaneously against two values of the array (from right-to-left) as to reduce it to a single value
 * @method Array.prototype.reduceRight
 * @arg {Function} callbackfn
 * @arg [initialValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/reduceRight}
 */


/*
 * String
 */


/**
 * The String global object is a constructor for strings, or a sequence of characters
 * @note The methods in color red, are AtomBox enhancements and are not part of the standard
 * @constructor
 * @name String
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String}
 */

/**
 * Returns a string representing the specified object
 * @method String.prototype.toString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/toString}
 */

/**
 * Returns the primitive value of a String object
 * @method String.prototype.valueOf
 * @returns {primitive}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/valueOf}
 */

/**
 * Returns the specified character from a string
 * @method String.prototype.charAt
 * @arg {Number} pos
 * @returns {character}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/charAt}
 */

/**
 * Returns the numeric Unicode value of the character at the given index (except for unicode codepoints > 0x10000)
 * @method String.prototype.charCodeAt
 * @arg {Number} pos
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/charCodeAt}
 */

/**
 * Combines the text of two or more strings and returns a new string
 * @method String.prototype.concat
 * @arg string1
 * @arg string2
 * @arg {String} [strings]
 * @returns {String} the new string
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/concat}
 */

/**
 * Returns the index within the calling String object of the first occurrence of the specified value, starting the search at position,
returns -1 if the value is not found
 * @method String.prototype.indexOf
 * @arg {String} searchString
 * @arg {Number} [position=0]
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/indexOf}
 */

/**
 * Returns the index within the calling String object of the last occurrence of the specified value, or -1 if not found. The calling string is searched backward, starting at position.
 * @method String.prototype.lastIndexOf
 * @arg {String} searchString
 * @arg {Number} position
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/lastIndexOf}
 */

/**
 * Returns a number indicating whether a reference string comes before or after or is the same as the given string in sort order
 * @method String.prototype.localeCompare
 * @arg {String} that
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/localeCompare}
 */

/**
 * Used to retrieve the matches when matching a string against a regular expression
 * @method String.prototype.match
 * @arg {RegExp} regexp
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/match}
 */

/**
 * Returns a new string with some or all matches of a pattern replaced by a replacement
 * The pattern can be a string or a RegExp, and the replacement can be a string or a function to be called for each match
 * @method String.prototype.replace
 * @arg {String|RegExp} pattern
 * @arg {String|Function} replacement
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/replace}
 */

/**
 * Executes the search for a match between a regular expression and this String object
 * @method String.prototype.search
 * @arg {RegExp} regexp
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/search}
 */

/**
 * Extracts a section of a string and returns a new string
 * @method String.prototype.slice
 * @arg {Number} beginSlice
 * @arg {Number} [endSlice]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/slice}
 */

/**
 * Splits a String object into an array of strings by separating the string into substrings
 * @method String.prototype.split
 * @arg {String} separator
 * @returns {Array}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/split}
 */

/**
 * Returns a subset of a string between one index and another, or through the end of the string
 * @method String.prototype.substring
 * @arg {Number} fro
 * @arg {Number} [to]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/substring}
 */

/**
 * Returns the calling string value converted to lowercase
 * @method String.prototype.toLowerCase
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/toLowerCase}
 */

/**
 * Returns the calling string value converted to lower case, according to any locale-specific case mappings
 * @method String.prototype.toLocaleLowerCase
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/toLocaleLowerCase}
 */

/**
 * Returns the calling string value converted to uppercase
 * @method String.prototype.toUpperCase
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/toUpperCase}
 */

/**
 * Returns the calling string value converted to upper case, according to any locale-specific case mappings
 * @method String.prototype.toLocaleUpperCase
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/toLocaleUpperCase}
 */

/**
 * Removes whitespace from both ends of the string
 * @method String.prototype.trim
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/trim}
 */

/**
 * @todo Document this
 * @method String.prototype.lpad
 * @returns {String}
 * @red
 */

/**
 * @todo Document this
 * @method String.prototype.rpad
 * @returns {String}
 * @red
 */


/*
 * Boolean
 */


/**
 * The Boolean object is an object wrapper for a boolean value
 * @constructor
 * @name Boolean
 * @global
 * @builtin
 * @arg value The initial value
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Boolean}
 */

/**
 * Returns a string representing the specified Boolean object
 * @method Boolean.prototype.toString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Boolean/toString}
 */

/**
 * Returns the primitive value of a Boolean object.This method is usually called internally by JavaScript and not explicitly in code.
 * @method Boolean.prototype.valueOf
 * @returns {primitive}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Boolean/valueOf}
 */


/*
 * Number
 */


/**
 * Creates a wrapper object to allow you to work with numerical values
 * @constructor
 * @name Number
 * @global
 * @builtin
 * @arg value
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number}
 */

/**
 * Returns a string representing the specified Number object
 * @method Number.prototype.toString
 * @arg [radix=10]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number/toString}
 */

/**
 * Returns a locale string representing the specified Number object
 * @method Number.prototype.toLocaleString
 * @arg [radix=10]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number/toLocaleString}
 */

/**
 * Formats a number using fixed-point notation
 * @method Number.prototype.toFixed
 * @arg [fractionDigits=0]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number/toFixed}
 */

/**
 * Returns a string representing the Number object in exponential notation
 * @method Number.prototype.toExponential
 * @arg [fractionDigits]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number/toExponential}
 */

/**
 * Returns a string representing the Number object to the specified precision
 * @method Number.prototype.toPrecision
 * @arg [precision]
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Number/toPrecision}
 */


/*
 * Date
 */


/**
 * Creates JavaScript Date instances which let you work with dates and times
 * @constructor
 * @name Date
 * @global
 * @builtin
 * @arg ... undefined, milliseconds, etc. 
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date}
 */


/**
 * Returns a string representing the specified Date object
 * @method Date.prototype.toString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toString}
 */

/**
 * Converts a date to a string, using the universal time convention
 * @method Date.prototype.toUTCString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toUTCString}
 */

/**
 * JavaScript provides a direct way to convert a date object into a string in ISO format, the ISO 8601 Extended Format (YYYY-MM-DDTHH:mm:ss.sssZ)
 * @method Date.prototype.toISOString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toISOString}
 */

/**
 * Returns a JSON representation of the Date object.
 * @method Date.prototype.toJSON
 * @returns {JSON}
 * @note Not implemented on AtomBox Compat
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toJSON}
 */


/**
 * Returns the date portion of a Date object in human readable form in American English
 * @method Date.prototype.toDateString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toDateString}
 */

/**
 * Returns the time portion of a Date object in human readable form in American English
 * @method Date.prototype.toTimeString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toTimeString}
 */


/**
 * Converts a date to a string, using the operating system's locale's conventions
 * @method Date.prototype.toLocaleString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toLocaleString}
 */

/**
 * Converts a date to a string, returning the "date" portion using the operating system's locale's conventions
 * @method Date.prototype.toLocaleDateString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toLocaleDateString}
 */

/**
 * Converts a date to a string, returning the "time" portion using the current locale's conventions
 * @method Date.prototype.toLocaleTimeString
 * @returns {String}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/toLocaleTimeString}
 */

/**
 * Returns the primitive value of a Date object
 * @method Date.prototype.valueOf
 * @returns {primitive}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/valueOf}
 */

/**
 * Returns the numeric value corresponding to the time for the specified date according to universal time
 * @method Date.prototype.getTime
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getTime}
 */

/**
 * Returns the year of the specified date according to local time
 * @method Date.prototype.getFullYear
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getFullYear}
 */

/**
 * Returns the month in the specified date according to local time
 * @method Date.prototype.getMonth
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getMonth}
 */

/**
 * Returns the day of the month for the specified date according to local time
 * @method Date.prototype.getDate
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getDate}
 */

/**
 * Returns the day of the week for the specified date according to local time
 * @method Date.prototype.getDay
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getDay}
 */

/**
 * Returns the hour for the specified date according to local time
 * @method Date.prototype.getHours
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getHours}
 */

/**
 * Returns the minutes in the specified date according to local time
 * @method Date.prototype.getMinutes
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getMinutes}
 */

/**
 * Returns the seconds in the specified date according to local time
 * @method Date.prototype.getSeconds
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getSeconds}
 */

/**
 * Returns the milliseconds in the specified date according to local time
 * @method Date.prototype.getMilliseconds
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getMilliseconds}
 */

/**
 * Returns the year of the specified date according to universal time
 * @method Date.prototype.getUTCFullYear
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCFullYear}
 */

/**
 * Returns the month in the specified date according to universal time
 * @method Date.prototype.getUTCMonth
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCMonth}
 */

/**
 * Returns the day of the month for the specified date according to universal time
 * @method Date.prototype.getUTCDate
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCDate}
 */

/**
 * Returns the day of the week for the specified date according to universal time
 * @method Date.prototype.getUTCDay
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCDay}
 */

/**
 * Returns the hour for the specified date according to universal time
 * @method Date.prototype.getUTCHours
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCHours}
 */

/**
 * Returns the minutes in the specified date according to universal time
 * @method Date.prototype.getUTCMinutes
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCMinutes}
 */

/**
 * Returns the seconds in the specified date according to universal time
 * @method Date.prototype.getUTCSeconds
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCSeconds}
 */

/**
 * Returns the milliseconds in the specified date according to universal time
 * @method Date.prototype.getUTCMilliseconds
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getUTCMilliseconds}
 */

/**
 * Returns the time-zone offset from UTC, in minutes, for the current locale
 * @method Date.prototype.getTimeZoneOffset
 * @returns {Number}
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/getTimeZoneOffset}
 */

/**
 * Sets the Date object to the time represented by a number of milliseconds since January 1, 1970, 00:00:00 UTC
 * @method Date.prototype.setTime
 * @arg {Number} timeValue
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setTime}
 */

/**
 * Sets the milliseconds for a specified date according to local time
 * @method Date.prototype.setMilliseconds
 * @arg {Number} millisecondsValue
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setMilliseconds}
 */

/**
 * Sets the seconds for a specified date according to local time
 * @method Date.prototype.setSeconds
 * @arg {Number} secondsValue
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setSeconds}
 */

/**
 * Sets the minutes for a specified date according to local time
 * @method Date.prototype.setMinutes
 * @arg {Number} minutesValue
 * @arg {Number} [secondsValue]
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setMinutes}
 */

/**
 * Sets the hour for a specified date according to local time
 * @method Date.prototype.setHours
 * @arg {Number} hoursValue
 * @arg {Number} [minutesValue]
 * @arg {Number} [secondsValue]
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setHours}
 */

/**
 * Sets the day of the month for a specified date according to local time
 * @method Date.prototype.setDate
 * @arg {Number} dayValue
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setDate}
 */

/**
 * Sets the month for a specified date according to local time
 * @method Date.prototype.setMonth
 * @arg {Number} monthValue
 * @arg {Number} [dayValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setMonth}
 */

/**
 * Sets the full year for a specified date according to local time
 * @method Date.prototype.setFullYear
 * @arg {Number} yearValue
 * @arg {Number} [monthValue]
 * @arg {Number} [dayValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setFullYear}
 */


/**
 * Sets the milliseconds for a specified date according to universal time
 * @method Date.prototype.setUTCMilliseconds
 * @arg {Number} millisecondsValue
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCMilliseconds}
 */

/**
 * Sets the seconds for a specified date according to universal time
 * @method Date.prototype.setUTCSeconds
 * @arg {Number} secondsValue
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCSeconds}
 */

/**
 * Sets the minutes for a specified date according to universal time
 * @method Date.prototype.setUTCMinutes
 * @arg {Number} minutesValue
 * @arg {Number} [secondsValue]
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCMinutes}
 */

/**
 * Sets the hour for a specified date according to universal time
 * @method Date.prototype.setUTCHours
 * @arg {Number} hoursValue
 * @arg {Number} [minutesValue]
 * @arg {Number} [secondsValue]
 * @arg {Number} [msValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCHours}
 */

/**
 * Sets the day of the month for a specified date according to universal time
 * @method Date.prototype.setUTCDate
 * @arg {Number} dayValue
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCDate}
 */

/**
 * Sets the month for a specified date according to universal time
 * @method Date.prototype.setUTCMonth
 * @arg {Number} monthValue
 * @arg {Number} [dayValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCMonth}
 */

/**
 * Sets the full year for a specified date according to universal time
 * @method Date.prototype.setUTCFullYear
 * @arg {Number} yearValue
 * @arg {Number} [monthValue]
 * @arg {Number} [dayValue]
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Date/setUTCFullYear}
 */


/*
 * RegExp
 */

/**
 * Creates a regular expression object for matching text with a pattern
 * @constructor
 * @name RegExp
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/RegExp}
 */

/**
 * 
 * @method RegExp.prototype.exec
 * @arg {String} 
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/RegExp/exec}
 */

/**
 * 
 * @method RegExp.prototype.test
 * @arg {String} 
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/RegExp/test}
 */

/**
 * 
 * @method RegExp.prototype.toString
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/RegExp/toString}
 */








/**
 * @_todo
 * @constructor
 * @name Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Error}
 */

/**
 * @_todo
 * @constructor
 * @name EvalError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/EvalError}
 */

/**
 * @_todo
 * @constructor
 * @name RangeError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/RangeError}
 */

/**
 * @_todo
 * @constructor
 * @name ReferenceError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/ReferenceError}
 */

/**
 * @_todo
 * @constructor
 * @name SyntaxError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Syntax/Global_Objects/SyntaxError}
 */

/**
 * @_todo
 * @constructor
 * @name TypeError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/Type/Global_Objects/TypeError}
 */

/**
 * @_todo
 * @constructor
 * @name URIError
 * @extends Error
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/URI/Global_Objects/URIError}
 */

/**
 * @_todo
 * @class
 * @name Math
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/JavaScript/URI/Global_Objects/Math}
 */

/**
 * The JSON object contains methods for converting values to JavaScript Object Notation (JSON) and for converting JSON to values.
 * @class
 * @name JSON
 * @global
 * @builtin
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON}
 */

/**
 * Parse a string as JSON.
 * @method JSON.parse
 * @arg {String} the JSON string
 * @returns {Object} the parsed Object
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse}
 */

/**
 * Convert a value to JSON, optionally replacing values if a replacer function is specified.
 * @method JSON.parse
 * @arg {Object} one Object
 * @returns {String} the UTF encoded JSON string
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify}
 */

/**
 * Takes an Object and copies the values into the coresponding AtomBox object tree.
 * @method JSON.toAtomBox
 * @arg {Object} source
 * @arg {Box}    destination
 * @red
 * @throws {Error} 
 */
