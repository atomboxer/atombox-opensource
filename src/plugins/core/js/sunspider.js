var tests = [
    "3d-cube.js",
    "3d-morph.js",
    "3d-raytrace.js",
    "access-binary-trees.js",
    "access-fannkuch.js",
    "access-nbody.js",
    "access-nsieve.js",
    "bitops-3bit-bits-in-byte.js",
    "bitops-bits-in-byte.js",
    "bitops-bitwise-and.js",
    "bitops-nsieve-bits.js",
//    "controlflow-recursive.js",
    "crypto-aes.js",
    "crypto-md5.js",
    "crypto-sha1.js",
    "date-format-tofte.js",
    "date-format-xparb.js",
    "math-cordic.js",
    "math-partial-sums.js",
    "math-spectral-norm.js",
    "regexp-dna.js",
    "string-base64.js",
    "string-fasta.js",
    "string-tagcloud.js",
    "string-unpack-code.js",
    "string-validate-input.js",
    "v8-crypto.js",
    "v8-deltablue.js",
    "v8-earley-boyer.js",
    "v8-raytrace.js",
    "v8-regexp.js",
    "v8-richards.js",
    "v8-splay.js" ];

var fs      = require("fs");
var console = require("console");
var system  = require("system");

exports.runTests = runTests = function()
{
    for (var f=0; f<tests.length; f++) { 
        for (var f=0; f<tests.length; f++) {
            var line = ":/atombox/SunSpider/tests/"+tests[f]+":";
            var t1 = new Date();
            var s = undefined;
            require(":/atombox/SunSpider/tests/"+tests[f]);
            var t2 = new Date();
            console.writeln(line+((t2.getTime() - t1.getTime())+"ms").lpad(" ", 80-(line.length+20)));
        }
    }
}
