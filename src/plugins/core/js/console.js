/**
 *  @module console
 *  @desc The "console" module provides an API for logging/debugging AtomBox scripts.
 *  @summary Logging/debugging API 
 *
 */
var system = require("system");
var Stream = require("io").Stream;
var TextStream = require("io").TextStream;
var env = system.env;

// used to remove ANSI control sequences if disabled
var cleaner = /\u001B\[\d*(?:;\d+)?[a-zA-Z]/g;
// used to check if a string consists only of ANSI control sequences
var matcher = /^(?:\u001B\[\d*(?:;\d+)?[a-zA-Z])+$/;


var TermWriter =  function(out) {

    if (!(this instanceof TermWriter)) {
        return new TermWriter(out);
    }

    var _enabled = false /*no escapes*/;
    out = out || system.stdout;

    this.setEnabled = function(flag) {
        _enabled = flag;
    }

    this.isEnabled = function() {
        return _enabled;
    }

    this.write = function() {
        for (var i = 0; i < arguments.length; i++) {
            var arg = String(arguments[i]);
            if (!_enabled) {
                arg = arg.replace(cleaner, '');
            }
            out.write(arg);
            if (arg && !matcher.test(arg) && i < arguments.length - 1) {
                out.write(" ");
            }
        }
    };

    this.writeln = function() {
        this.write.apply(this, arguments);
        out.writeLine(_enabled ? exports.RESET : "");
    };
}

var stdout = new TermWriter(system.stdout);
exports.write = write = stdout.write.bind(stdout);
exports.writeln = writeln = stdout.writeln.bind(stdout);

function convert(value, nesting, visited) {
    var type = typeof value;
    var retval = {type: type};
    switch (type) {
    case 'number':
        retval.string = String(value);
        break;
    case 'string':
        retval.string  = "'" + value.replace(/'/g, "\\'") + "'";
        break;
    case 'function':
        retval.string = "[Function]";
        break;
    case 'boolean':
        retval.string =  String(value);
        break;
    case 'object':
        if (value === null) {
            retval.type = retval.string = 'null';
            break;
        }
        if (visited.indexOf(value) > -1) {
            retval.type = "cyclic";
            retval.string = "[CyclicRef]";
            break;
        }
        visited.push(value);
        if (value instanceof Date) {
            retval.type = "date";
            retval.string = String(value);
        } else if (Object.prototype.toString.call(value) == '[object Array]') {
	    // if (nesting > 1) {
            //     retval.string = "[...]";
            // } else {
                retval.type = "array";
                retval.items = [];
                var count = 0;
                for (var i = 0; i < value.length; i++) {
                    var part = convert(value[i], nesting + 1, visited);
                    count += (part.string && part.string.length || part.count || 0) + 2;
                    retval.items.push(part);
                }
                retval.count = count;
            //}
        }  /*else if (value.toString != Object.prototype.toString) {
            retval.type = "custom";
            retval.string = value.toString();
            //break;
        } */else {
            if (/*nesting > 1 &&*/ value.hasOwnProperty("value")) {
		var dispValue = "";
                try {
		    if (!/[\x00-\x1F\x80-\xFF]/.test(value.value)&&
		        value.value != undefined /*&& value.length == value.value.length*/) {
		        dispValue += value.value;
		    } else
		        dispValue += value.bytes.decodeToString("hex");
		} catch(e){
                    var val = value.bytes.decodeToString();
                    if (!/[\x00-\x1F\x80-\xFF]/.test(val)) {
		        dispValue += val;
		    } else {
                        dispValue += value.bytes.decodeToString("hex");
                    }
                }

		retval.string = "{"+dispValue+"}"+"<"+value.length+">";
                switch(value.state) {
                case 0: retval.string += "<OK>";break;
                case 1: retval.string += "<Warning>";break;
                case 2: retval.string += "<Error>"; break;
                case 3: retval.string += "<Default>"; break;
                }
            } else {
                retval.items = [];
                var keys = Object.keys(value);
                count = 0;
                for (i = 0; i < keys.length; i++) {
                    part = convert(value[keys[i]], nesting + 1, visited);
                    count += String(keys[i]).length + 4;
                    count += part.string && part.string.length || part.count || 0;
                    retval.items.push({
                        key: keys[i] + ": ",
                        value: part
                    });
                }
                retval.count = count;
            }
        }
        break;
    case 'undefined':
        retval = {};
        break;
    default:
        retval.string = String(value);
        break;
    }
    return retval;
}

function printResult(value, writer) {
    if (typeof value !== "undefined") {
        printValue(convert(value, 0, []), writer, 0);
        writer.writeln();
    }
}

function printValue(value, writer, nesting) {
    if (value.string) {
        var style = "";
        writer.write(style + value.string);
    } else if (value && value.items) {
        var multiline = value.count > 60;
        var isArray = value.type === "array";
        var length = value.items.length;
        if (length === 0) {
            writer.write(isArray ? "[]" : "{}");
            return;
        }
        var opener = isArray ? "[" : "{";
        if (multiline && nesting > 0) {
            writer.write(opener + "\n  ");
            for (j = 0; j < nesting; j++)
                writer.write("  ");
        } else {
            writer.write(opener + " ");
        }
        if (isArray) {
            for (var i = 0; i < length; i++) {
                printValue(value.items[i], writer, nesting + 1);
                if (i < length - 1) {
                    if (multiline) {
                        writer.write(",\n  ");
                        for (var j = 0; j < nesting; j++)
                            writer.write("  ");
                    } else {
                        writer.write(", ");
                    }
                }
            }
            writer.write(" ]");
        } else {
            for (i = 0; i < length; i++) {
                writer.write(value.items[i].key);
                printValue(value.items[i].value, writer, nesting + 1);
                if (i < length - 1) {
                    if (multiline) {
                        writer.write(",\n  ");
                        for (j = 0; j < nesting; j++)
                            writer.write("  ");
                    } else {
                        writer.write(", ");
                    }
                }
            }
            writer.write(" }");
        }
    }
}
//=====================================================================//
function format() {
    var msg = arguments[0] ? String(arguments[0]) : "";
    var pattern = /%[sdifo]/;
    for (var i = 1; i < arguments.length; i++) {
        msg = pattern.test(msg)
            ? msg.replace(pattern, String(arguments[i]))
            : msg + " " + arguments[i];
    }
    return msg;
}

var writer = new TermWriter(system.stderr);

/**
 * Logs a message to the console. The first argument to log may be a string containing printf-like string substitution patterns.
 * @method module:console.log
 * @arg {Object} object
 * @arg {Object} [,object...]
 * @example
require("console").log("The %s jumped over %d tall buildings", "lion", 3);
 */
exports.log = function() {
    var msg = format.apply(null, arguments);
    writer.writeln(msg);
};

/**
 * Writes to the console
 * @method module:console.write
 * @arg {Object} object
 * @arg {Object} [,object...]
 * @example
require("console").write("Hello world!");
 */

/**
 * Writes to the console and append a new line
 * @method module:console.writeln
 * @arg {Object} object
 * @arg {Object} [,object...]
 * @example
require("console").writeln("Hello world!");
 */


exports.debug = function() {
    var msg = format.apply(null, arguments);
    var err = new Error();
    var bt  = "?";
    if (err.backtrace != undefined)
	bt = err.backtrace()[1];

    bt = bt.substr(bt.lastIndexOf("/")+1);
    writer.writeln("[DEBUG <"+bt+">]: "+msg);
}
/**
 * Logs a debug message to the console. The first argument to log may be a string containing printf-like string substitution patterns. Debug messages are prefixed with a string like: [debug <file>:<Date/Time>:line number].
 * @method module:console.debug
 * @arg {Object} object
 * @arg {Object} [,object...]
 * @example
require("console").debug("variable is %d",3);
 */
exports.debug = function() {
    var msg = format.apply(null, arguments);
    var err = new Error();
    var bt  = "?";
    if (err.backtrace != undefined)
	bt = err.backtrace()[1];

    bt = bt.substr(bt.lastIndexOf("/")+1);
    writer.writeln("[DEBUG <"+bt+">]: "+msg);
}

/**
 * Logs an error message to the console. The first argument to log may be a string containing printf-like string substitution patterns. Debug messages are prefixed with a string like: [error <file>:<Date/Time>:line number].
 * @method module:console.error
 * @arg {Object} object
 * @arg {Object} [,object...]
 * @example
require("console").error("fatal error");
 */

exports.error = function() {
    var msg = format.apply(null, arguments);
    var err = new Error();
    var bt  = "?";
    if (err.backtrace != undefined)
	bt = err.backtrace()[1];

    bt = bt.substr(bt.lastIndexOf("/")+1);
    writer.writeln("[ERROR <"+bt+">]: "+msg);
};

/**
 * Logs a warning message to the console. The first argument to log may be a string containing printf-like string substitution patterns. Debug messages are prefixed with a string like: [WARN <file>:<Date/Time>:line number].
 * @method module:console.warn
 * @arg {Object} object
 * @arg {Object} [,object...]
 */
exports.warn = function() {
    var msg = format.apply(null, arguments);
    var msg = format.apply(null, arguments);
    var err = new Error();
    var bt  = "?";
    if (err.backtrace != undefined)
	bt = err.backtrace()[1];

    bt = bt.substr(bt.lastIndexOf("/")+1);
    writer.writeln("[WARN <"+bt+">]: "+msg);
};

/**
 * Logs a informational message to the console. The first argument to log may be a string containing printf-like string substitution patterns. Debug messages are prefixed with a string like: [INFO <file>:<Date/Time>:line number].
 * @method module:console.info
 * @arg {Object} object
 * @arg {Object} [,object...]
 */
exports.info = function() {
    var msg = format.apply(null, arguments);
    var msg = format.apply(null, arguments);
    var err = new Error();
    var bt  = "?";
    if (err.backtrace != undefined)
	bt = err.backtrace()[1];

    bt = bt.substr(bt.lastIndexOf("/")+1);
    writer.writeln("[INFO <"+bt+">]: "+msg);
};

/**
 * Tests that an expression is true. If not, logs a message and throws an exception.
 * @method module:console.assert
 * @arg {Boolean} expresssion
 * @arg {String} assertion message
 */
exports.assert = __internal__.__console_assertHelper;

/**
 * Logs an interactive listing of all properties of the object
 * @method module:console.dir
 * @arg {Object} obj
 * @note This function is especially usefull in conjuction with complex hierarchical objects like DDL, when the whole hierarchy needs to be displayed (including values)
 */
exports.dir = function(obj) {
    printResult(obj, writer);
};

var timers = {};

/**
 * Creates a new timer under the given name. Call timeEnd(name) with the same name to stop the timer and log the time elapsed.
 * @method module:console.time
 * @arg {String} timerName
 * @note This should not be confused with the "timers" module timer. This functions are used solely for debugging/logging purposes.
 */
exports.time = function(name) {
    if (name && !timers[name]) {
        timers[name] = (new Date()).getTime();
    }
};

/**
 * Stops a timer created by a call to require("console").time(name) and logs the time elapsed. 
 * @method module:console.timeEnd
 * @arg {String} timerName
 * @note This should not be confused with the "timers" module timer. This functions are used solely for debugging/logging purposes.
 */
exports.timeEnd = function(name) {
    var start = timers[name];
    if (start) {
        var time = (new Date()).getTime();
        writer.writeln(name + ": " + (time - start) + "ms");
        delete timers[name];
        return time;
    }
    return undefined;
};
