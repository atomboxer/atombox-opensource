exports.Process = __internal__.Process;

/**
 *  @module process
 *  @desc This module provides the functionality for starting external programs and 
 *  communicating with them.
 *  @example
//Run a process on HP NonStop Guardian
var console = require("console");
var Process = require("process").Process;

var p = new Process({'out':"del",'cpu': 1, 'name': '$AB01', 'priority':50, 'lib':"$bla.xnlib"});
//..


 *  @example
//Run a process on windows
var console = require("console");
var Process = require("process").Process;

//create a new Process object
var p = new Process();

//connect a function to the 'finished' signal
p.finished.connect(function(exit) {
	console.write("finished event:"+exit+"\n");
});

//
//  Start the Windows cmd process with some parameters
//
p.start("cmd",["/C echo Hello world"]);
*/

/**
 * @class
 * @name Process 
 * @memberof module:process
 * @classdesc The Process class provide the functionality for starting external programs and communicating with them.
To start a process, pass the name and command line arguments of the program you want to run as arguments to start(). Arguments are supplied as individual strings in an Array of strings.
 @note  HP Nonstop has particularities that does not allow the stdout and stderr abstractizations, therefore the readyReadStandardOutput and readyReadStandardError events are never emitted in Guardian OS. The output can be retrieved by using the parameters passed to the constructor ('in', 'out').
@note   the parameters have been quote because `in` is a keyword in JavaScript


 * @arg {Object} tandem_options The Tandem options. An object with any of the following properties: in (in file), out (out file), lib (library), priority and cpu.
 * @summary The main process module class
 * @see module:process

 * @example
//Run a process on HP NonStop Guardian
var console = require("console");
var Process = require("process").Process;

var p = new Process{'out':"outfile",'cpu': 1, 'priority':50, 'lib':"$bcng05.dev5xpnt.xnlibn"  }
//..

 * @example
//Run a process on Windows
var console = require("console");
var Process = require("process").Process;
var system  = require("system");


//create a new Process object
var p = new Process();

//connect a function to the 'error' signal
p.error.connect(function(err) {
	console.write("error event"+err+"\n");
});

//connect a function to the 'finished' signal
p.finished.connect(function(exit) {
	console.write("finished event:"+exit+"\n");
    system.exit(0);
});

//connect a function to the 'readyReadStandardError' signal
p.readyReadStandardError.connect(function() {
    //read all from the stderr and display it to the terminal
	console.write(p.readAllStandardError().decodeToString()+"\n");
});

//connect a function to the 'readyReadStandardOutput' signal
p.readyReadStandardOutput.connect(function() {
    //read all from the stdout and display it to the terminal
	console.write(p.readAllStandardOutput().decodeToString()+"\n");
});

//connect a function to the 'started' signal
p.started.connect(function() {
	console.write("started signal\n");
});

if (system.platform == "windows")
    //
    //  Start the Windows cmd process with some parameters
    //
	p.start("cmd",["/C echo Hello world"]);

// Outputs:

//started signal
//Hello world
//finished event:0
//
//

*/


/**
 * This signal is emitted when the process finishes. status is the exist status of the process that just finished.
 * @event module:process.Process.prototype.finished
 * @arg {Number} status The exit status
 */


/**
 * This signal is emitted when the process fails to start
 * @event module:process.Process.prototype.error
 * @arg {String} error The returned error
 */

/**
 * This signal is emitted when the process has made new data available through the standard error channel (stderr).
 * @event module:process.Process.prototype.readyReadStandardError
 * @note This event is not emitted for HP Nonstop Guardian processes
 */

/**
 * This signal is emitted when the process has made new data available through the standard output channel (stdout).
 * @event module:process.Process.prototype.readyReadStandardOutput
 * @note This event is not emitted for HP Nonstop Guardian processes
 */

/**
 * This signal is emitted when the process has started.
 * @event module:process.Process.prototype.started
 */

/**
 * This method starts a new process
 * @method module:process.Process.prototype.start
 */

/**
 * This method terminates the running process
 * @method module:process.Process.prototype.stop
 */

/**
 * Returns all data available from the standard error of the process as a ByteString.
 * @method module:process.Process.prototype.readAllStandardError
 * @returns {ByteString} stderr contents of the running process
 * @note Not available in Guardian HP NonStop
 */

/**
 * Returns all data available from the standard output of the process as a ByteString.
 * @method module:process.Process.prototype.readAllStandardOutput
 * @returns {ByteString} stdout contents of the running process
 * @note Not available in Guardian HP NonStop
 */



