
/**
 *  The Binary module.
 *  @module binary
 *  @desc

This module provides implementations of the Binary, ByteArray, and ByteString classes.
as defined by the [Binary/B CommonJS]( http://wiki.commonjs.org/wiki/Binary/B ) proposal.

JavaScript does not have a binary data type. However on the server side scenarious, binary 
data needs to be often processed when dealing with TCP streams or the file system.
This is why AtomBox provides two classes for working with the binary arrays or strings.

The Binary class serves as common base class for ByteArray and ByteString and can't be instantiated.

 - The ByteArray type resembles the interface of Array in that it is mutable, extensible, and 
indexing will return number values for the byte in the given position, zero by default, or
undefined if the index is out of bounds

 - The ByteString type resembles the interface of String in that it is immutable and indexing 
returns a ByteString of length 1.

###### String.prototype and Array.prototype have been enhanced with the following members:
        toByteArray(charset) - Converts a string to a ByteArray encoded in charset. 
        toByteString(charset) - Converts a string to a ByteString encoded in charset.

* @example
require("binary");
//...

// creates a ByteArray object of size 10
var ba = new ByteArray(10); 

// creates a ByteString object
var bs = new ByteString("Hello world"); 

// creates a ByteArray from hex string
var bh = new ByteArray("3031323334","hex");

// compress a ByteArray
var bc = new ByteArray("This is a long ByteArray").compress();
 */

/**
 * @class 
 * @name Binary
 * @ignore
 * @abstract
 * @classdesc This class serves as common base class for ByteArray and ByteString, and cannot
              be instantiated.
 * @summary Abstract common base class for ByteArray and ByteString
 * @see {@link module:binary.ByteArray}
 * @see {@link module:binary.ByteString}
 * @memberof module:binary
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @class
 * @name ByteArray
 * @extends module:binary.Binary
 * @requires module:binary
 * @summary A ByteArray is a mutable, flexible representation of a C unsigned char (byte) array
 * @classdesc 
This type resembles the interface of Array, and it is used for representing and manipulating binary data.
 
Instances of ByteArray are instances of ByteArray, Binary, and Object.
 * @memberof module:binary
 */

/**
 * Constructor. Creates a new, empty ByteArray
 * @method module:binary.ByteArray.prototype.ByteArray
 */

/**
 * Constructor. Creates a new ByteArray filled with length zero bytes
 * @method module:binary.ByteArray.prototype.ByteArray
 * @arg {Number} length
 * @example
var b = new ByteArray(10);
console.log(b.decodeToString("hex")); //0000000000 
 */

/**
 * Constructor. Creates a new ByteArray.
 * @method module:binary.ByteArray.prototype.ByteArray
 * @arg {ByteArray} other
 */

/**
 * Constructor. Creates a new ByteArray.
 * @method module:binary.ByteArray.prototype.ByteArray
 * @arg {Array} arrayOfBytes The Array of bytes that will represent the content of the new ByteArray
 */

/**
 * Mutable length property. Extending a byte array fills the new entries with 0.
 * @member {Number} module:binary.ByteArray.prototype.length
 */

/**
 * Converts the ByteArray into an Array (byte values)
 * @method module:binary.ByteArray.prototype.toArray
 * @arg {String} [charset="ascii"] one of "hex", "ascii", "base64", "ebcdic", "utf-8" or other IANA string defining charset
 * @returns {Array}
 */


/**
 *  Returns the decoded ByteArray as a string
 *  @method module:binary.ByteArray.prototype.decodeToString
 *  @arg {String} [charset="ascii"] one of "hex", "ascii", "base64", "ebcdic", "utf-8", or other IANA string defining charset
 *  @returns {String}
 *  @example
var ba = new ByteArray("3031323334");
console.log(ba.decodeToString()); //01234
console.log(ba.decodeToString("hex")); //3031323334
 */

/**
 *  Returns a transcoded copy in a ByteArray
 *  @method module:binary.ByteArray.prototype.toByteArray
 *  @arg {String} [fromCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic" or other IANA string defining charset
 *  @arg {String} [toCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic", or other IANA string defining charset
 *  @returns {ByteArray}
 */

/**
 *  Returns a transcoded copy in a ByteString
 *  @method module:binary.ByteArray.prototype.toByteString
 *  @arg {String} [fromCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic" or other IANA string defining charset
 *  @arg {String} [toCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic"  or other IANA string defining charset
 *  @returns {ByteString}
 */

/**
 *  Returns the byte at offset as a ByteString 
 *  @method module:binary.ByteArray.prototype.byteAt
 *  @arg {Number} offset
 *  @returns {ByteString}
 */

/**
 *  Returns the byte at offset as a ByteString 
 *  @method module:binary.ByteArray.prototype.valueAt
 *  @arg {Number} offset
 *  @returns {ByteString}
 */

/**
 *  Return the byte at offset as a Number. get(offset) is anlaogous to indexing with brackets
 *  @method module:binary.ByteArray.prototype.get
 *  @arg {Number} offset
 *  @returns {Number}
 */

/**
 *  Return the byte at offset as a Number
 *  @method module:binary.ByteArray.prototype.codeAt
 *  @arg {Number} offset
 *  @returns {Number}
 */

/**
 *  Copies the Number values from this ByteArray between start and stop to a target ByteArray or Array at the targetStart offset. start, stop, and targetStart must be Numbers, undefined, or omitted. If omitted, start is presumed to be 0. If omitted, stop is presumed to be the length of this ByteArray. If omitted, targetStart is presumed to be 0. 
 *  @method module:binary.ByteArray.prototype.copy
 *  @arg {ByteArray} target
 *  @arg {Number} [start=0]
 *  @arg {Number} [stop=length of this ByteArray]
 *  @arg {Number} [targetStart=0]
 *  @note @todo. Not implemented. Use "slice" instead.
 */

/**
 *  Concatenate two arrays
 *  @method module:binary.ByteArray.prototype.concat
 *  @arg {ByteArray|ByteString|Array} other
 *  @returns {ByteArray}
 */

/**
 *  The pop method removes the last element from an array and returns that value to the caller
 *  @method module:binary.ByteArray.prototype.pop
 *  @returns {Number}
 *  @see Array.prototype.pop
 */

/**
 *  Mutates a ByteArray by appending the given elements and returning the new length of the array
 *  @method module:binary.ByteArray.prototype.push
 *  @arg {Number} tem1[,item2[,...]]
 *  @returns {Number} the length of the new ByteArray
 *  @see Array.prototype.push
 */


/** 
 *  Removes the first element from an array and returns that element. This method changes the length
 *  @method module:binary.ByteArray.prototype.shift
 *  @returns {Number} the removed element
 *  @see Array.prototype.shift
 */

/** 
 *  Adds one or more elements to the beginning of an array and returns the new length of the array
 *  @method module:binary.ByteArray.prototype.unshift
 *  @arg item1[,item2[,...]]
 *  @returns {Number} The new length of the array
 *  @see Array.prototype.unshift
 */


/**
 * The reverse method transposes the elements of the calling array object in place, mutating the array, and returning a reference to the array
 * @method module:binary.ByteArray.prototype.reverse
 * @see Array.prototype.reverse
 */

/**
 * Returns a one-level deep copy of a portion of an array
 * @method module:binary.ByteArray.prototype.slice
 * @arg {Number} beginSlice
 * @arg {Number} [endSlice]
 * @returns {ByteArray}
 * @see Array.prototype.slice
 */


/**
 * Returns the first index at which a given element can be found in the array, or -1 if it is not present
 * @method module:binary.ByteArray.prototype.indexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see Array.prototype.indexOf
 */

/**
 * Returns true if the ByteArray contains printable characters
 * @method module:binary.ByteArray.prototype.isPrint
 * @returns {Boolean}
 */


/**
 * Returns the last index at which a given element can be found in the array, or -1 if it is not present. The array is searched backwards, starting at fromIndex.
 * @method module:binary.ByteArray.prototype.lastIndexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see Array.prototype.lastIndexOf
 */

/**
 *  Replaces the ByteArray at position for length with the value.
 *  @method module:binary.ByteArray.prototype.replace
 *  @arg {Number} pos
 *  @arg {Number} length
 *  @arg {ByteArray} value
 *  @returns {ByteArray}
 */


/**
 *  Reads 8 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readInt8
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 16 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readInt16
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 32 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readInt32
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 8 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readUInt8
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 16 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readUInt16
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 32 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteArray.prototype.readUInt32
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeInt8
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeInt16
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeInt32
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeUInt8
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeUInt16
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteArray.prototype.writeUInt32
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Tries to convert the ByteArray object to a (signed) number
 *  @method module:binary.ByteArray.prototype.toNumber
 *  @returns {Number|undefined}
 */

/**
 *  ZLIB compression
 *  @method module:binary.ByteArray.prototype.compress
 *  @returns {ByteArray}
 */
 
/**
 *  ZLIB uncompression
 *  @method module:binary.ByteArray.prototype.uncompress
 *  @returns {ByteArray}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteArray.prototype.hash32
 *  @returns {ByteArray}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteArray.prototype.hash64
 *  @returns {ByteArray}
 */

/*
 *  Non-cryptographic hash
 *  @method module:binary.ByteArray.prototype.hash128
 *  @returns {ByteArray}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteArray.prototype.hash64WithSeed
 *  @arg {ByteArray} seed - bytearray that contains a 64 bit number
 *  @returns {ByteArray}
 */

/*
 *  Non-cryptographic hash
 *  @method module:binary.ByteArray.prototype.hash128WithSeed
 *  @arg {ByteArray} seed - bytearray that contains two 64 bit numbers
 *  @returns {ByteArray}
 */


exports.ByteArray  = ByteArray  = __internal__.ByteArray;

////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @class
 * @name ByteString
 * @extends module:binary.Binary
 * @requires module:binary
 * @summary A ByteString is an immutable, fixed-width representation of a C unsigned char (byte) array that does not implicitly terminate at the first 0-valued byte. Indexing returns a byte substring of length 1.
 * @classdesc This type resembles the interface of String, and it is used for representing and manipulating binary data. ByteString instances are instances of ByteString, Binary, and Object.
 * @memberof module:binary
 */
exports.ByteString = ByteString = __internal__.ByteString;

/**
 * Constructor. Creates a new, empty ByteString
 * @method module:binary.ByteString.prototype.ByteString
 */

/**
 * Constructor. Creates a new ByteString.
 * @method module:binary.ByteString.prototype.ByteString
 * @arg {ByteString|ByteArray} other
 */

/**
 * Constructor. Creates a new ByteString.
 * @method module:binary.ByteString.prototype.ByteString
 * @arg {Array} arrayOfNumbers The Array of numbers that will represent the content of the new ByteString. Note that each number should be in the range [0...255]
 */

/**
 * Constructor. Converts a string into a ByteString. The ByteString will contain string encoded with charset.
 * @method module:binary.ByteString.prototype.ByteString
 * @arg {String} [charset="ascii"] one of "hex", "ascii", "ebcdic", "utf-8" or other IANA string defining charset
 */

/**
 * Constructor method.
 * @method module:binary.ByteString.join
 * @arg {Array} array Array of numbers
 * @arg {Number} delimiter
 * @returns {ByteString}
 * @example 
//The following are equivalent
ByteString.join([1,2,3], 0)
ByteString([1, 0, 2, 0, 3])
 * @note @todo Not implemented yet. 
 */

/**
 * Imutable length property. This property cannot be changed.
 * @member {Number} module:binary.ByteString.prototype.length
 */

/**
 * Returns an array containing the bytes as numbers.
 * @method module:binary.ByteString.prototype.toByteArray
 *  @arg {String} [fromCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic" or 
    other IANA string defining charset
 *  @arg {String} [toCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic", or 
    other IANA string defining charset
 *  @returns {ByteArray}
 */

/**
 * Returns a transcoded copy of itself. If fromCharset and toCharset parameters are not provided, returns itself.
 * @method module:binary.ByteString.prototype.toByteString
 *  @arg {String} [fromCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic" or 
    other IANA string defining charset
 *  @arg {String} [toCharset="ascii"] one of "hex", "ascii", "base64", "utf-8", "ebcdic", or 
    other IANA string defining charset
 *  @returns {ByteString}
 */

/**
 * Returns an array containing the bytes as numbers.
 * @method module:binary.ByteString.prototype.toArray
 * @arg {String} [charset="ascii"] one of "hex", "ascii", "base64", "ebcdic", "utf-8" or other IANA string defining charset
 * @returns {Array}
 */

/**
 *  Returns the decoded ByteString as a string
 *  @method module:binary.ByteString.prototype.decodeToString
 *  @arg {String} [charset="ascii"] one of "hex", "ascii", "base64", "ebcdic", "utf-8", or other IANA string 
defining charset
 *  @returns {String}
 *  @see ByteArray.prototype.decodeToString
 */

/**
 * Returns the first index at which a given element can be found in the array, or -1 if it is not present.
 * @method module:binary.ByteString.prototype.indexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see Array.prototype.indexOf
 */

/**
 * Returns true if the ByteString contains printable characters
 * @method module:binary.ByteString.prototype.isPrint
 * @returns {Boolean}
 */

/**
 * Returns the last index at which a given element can be found in the array, or -1 if it is not present. The array is searched backwards, starting at fromIndex.
 * @method module:binary.ByteString.prototype.lastIndexOf
 * @arg searchElement
 * @arg [fromIndex]
 * @returns {Number}
 * @see Array.prototype.lastIndexOf
 */

/**
 *  Return the byte at offset as a Number
 *  @method module:binary.ByteString.prototype.codeAt
 *  @arg {Number} offset
 *  @returns {Number}
 */

/**
 *  Returns the byte at offset as a ByteString 
 *  @method module:binary.ByteString.prototype.byteAt
 *  @arg {Number} offset
 *  @returns {ByteString}
 */

/**
 *  Returns the byte at offset as a ByteString 
 *  @method module:binary.ByteString.prototype.valueAt
 *  @arg {Number} offset
 *  @returns {ByteString}
 */

/**
 *  Return the byte at offset as a Number. get(offset) is anlaogous to indexing with brackets.
 *  @method module:binary.ByteString.prototype.get
 *  @arg {Number} offset
 *  @returns {Number}
 */

/**
 *  Copies the Number values from this ByteString between start and stop to a target ByteString or Array at the targetStart offset. start, stop, and targetStart must be Numbers, undefined, or omitted. If omitted, start is presumed to be 0. If omitted, stop is presumed to be the length of this ByteString. If omitted, targetStart is presumed to be 0. 
 *  @method module:binary.ByteString.prototype.copy
 *  @arg {ByteString} target
 *  @arg {Number} [start=0]
 *  @arg {Number} [stop=length of this ByteString]
 *  @arg {Number} [targetStart=0]
 *  @note @todo. Not implemented. Use "slice" instead.
 */


/**
 * Returns a one-level deep copy of a portion of an array
 * @method module:binary.ByteString.prototype.slice
 * @arg {Number} beginSlice
 * @arg {Number} [endSlice]
 * @returns {ByteString}
 * @see Array.prototype.slice
 */

/**
 *  Concatenate two binaries
 *  @method module:binary.ByteString.prototype.concat
 *  @arg {ByteArray|ByteString|Array} other
 *  @returns {ByteString}
 */

///// NON COMMONJS ////

/**
 *  Replaces the ByteString at position for length with the value.
 *  @method module:binary.ByteString.prototype.replace
 *  @arg {Number} pos
 *  @arg {Number} length
 *  @arg {ByteString} value
 *  @returns {ByteString}
 */


/**
 *  Reads 8 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readInt8
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 16 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readInt16
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 32 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readInt32
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 8 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readUInt8
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 16 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readUInt16
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Reads 32 bits from offset and returns the number that coresponds to the raw unencoded data representation.
 *  @method module:binary.ByteString.prototype.readUInt32
 *  @arg {Number} [offset = 0]
 *  @returns {Number}
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeInt8
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeInt16
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeInt32
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeUInt8
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeUInt16
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Writes the binay representation of the parameter value at offset.
 *  @method module:binary.ByteString.prototype.writeUInt32
 *  @arg {Number} value
 *  @arg {Number} [offset = 0]
 */

/**
 *  Tries to convert the ByteString object to a (signed) number
 *  @method module:binary.ByteString.prototype.toNumber
 *  @returns {Number|undefined}
 */

/**
 *  ZLIB compression
 *  @method module:binary.ByteString.prototype.compress
 *  @returns {ByteString}
 */
 
/**
 *  ZLIB uncompression
 *  @method module:binary.ByteString.prototype.uncompress
 *  @returns {ByteString}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteString.prototype.hash32
 *  @returns {ByteString}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteString.prototype.hash64
 *  @returns {ByteString}
 */

/*
 *  Non-cryptographic hash
 *  @method module:binary.ByteString.prototype.hash128
 *  @returns {ByteString}
 */

/**
 *  Non-cryptographic hash
 *  @method module:binary.ByteString.prototype.hash64WithSeed
 *  @arg {ByteString} seed - bytearray that contains a 64 bit number
 *  @returns {ByteString}
 */

/*
 *  Non-cryptographic hash
 *  @method module:binary.ByteString.prototype.hash128WithSeed
 *  @arg {ByteString} seed - bytearray that contains two 64 bit numbers
 *  @returns {ByteString}
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Conversion to ByteArray
 * @method String.prototype.toByteArray
 * @param {string} [charset="utf-8"] Converts a string to a ByteArray. Supported values are: 
   "utf-8", "ascii", "hex", "ebcdic", or other encoding. 
 * @red
 * @returns {ByteArray}
*/
Object.defineProperty(String.prototype, 'toByteArray', {
    value: function(charset) {
        charset = charset || 'utf8';
        return new ByteArray(String(this), charset);
    }, writable: true
});

/**
 * Conversion to ByteString
 * @method String.prototype.toByteString
 * @param {string} [charset="utf-8"] Converts a string to a ByteArray. Supported values are: 
   "utf-8", "ascii", "hex", "ebcdic", or other encoding.
 * @red
 * @returns {ByteString}
 */
Object.defineProperty(String.prototype, 'toByteString', {
    value: function(charset) {
        charset = charset || 'utf8';
        return new ByteString(String(this), charset);
    }, writable: true
});

if (!Array.prototype.every)
    Object.defineProperty(Array.prototype,"every", {
	value:function(func, thisVal) {
            var len = this.length;
            for (var i = 0; i < len; i++)
		if (!func.call(thisVal, this[i] || 
                               this.charAt && this.charAt(i), i, this))
                    return false;
            return true;
	}, writable:true
    })

if (!Array.prototype.filter)
    Object.defineProperty(Array.prototype,"filter",{
	value:function(func, thisVal) {
            var len = this.length;
            var ret = new Array();
            for (var i = 0; i < len; i++) {
		var val = this[i] || this.charAt && this.charAt(i);
		if(func.call(thisVal, val, i, this))
                    ret[ret.length] = val;
            }
            return ret;
	}, writable:true
    })

if (!Array.prototype.forEach)
    Object.defineProperty(Array.prototype,"forEach", {
	value:  function(func, thisVal) {
            var len = this.length;
            for (var i = 0; i < len; i++)
		func.call(thisVal, this[i] || 
			  this.charAt && this.charAt(i), i, this);
	}, writable:true
    })

if (!Array.prototype.indexOf)
    Object.defineProperty(Array.prototype,"indexOf", {
	value : function(item, startIndex) {
            var len = this.length;
            if (startIndex == null)
		startIndex = 0;
            else if (startIndex < 0) {
		startIndex += len;
		if (startIndex < 0)
                    startIndex = 0;
            }
            for (var i = startIndex; i < len; i++) {
		var val = this[i] || this.charAt && this.charAt(i);
		if (val == item)
                return i;
            }
            return -1;
	}, writable:true
    })

if (!Array.prototype.lastIndexOf)
    Object.defineProperty(Array.prototype,"lastIndexOf", {
	value: function(item, startIndex) {
            var len = this.length;
            if (startIndex == null || startIndex >= len)
		startIndex = len - 1;
            else if (startIndex < 0)
		startIndex += len;
            for (var i = startIndex; i >= 0; i--) {
		var val = this[i] || this.charAt && this.charAt(i);
		if (val == item)
                    return i;
            }
            return -1;
	}, writable:true
    })

if (!Array.prototype.map)
    Object.defineProperty(Array.prototype,"map", {
	value: function(func, thisVal) {
            var len = this.length;
            var ret = new Array(len);
            for (var i = 0; i < len; i++)
		ret[i] = func.call(thisVal, this[i] || 
				   this.charAt && this.charAt(i), i, this);
            return ret;
	}, writable:true
    })

if ( !Array.prototype.reduce )
    Object.defineProperty(Array.prototype,"reduce", {
	value: function(accumulator) {  
            var i, l = this.length, curr;  
            
            if(typeof accumulator !== "function") 
		throw new TypeError("First argument is not callable");  
            
            if((l == 0 || l === null) && (arguments.length <= 1))
		throw new TypeError("Array length is 0 and no second argument");  
            
            if(arguments.length <= 1){  
		curr = this[0]; 
		i = 1;
            }  
            else{  
		curr = arguments[1];  
            }
            for(i = i || 0 ; i < l ; ++i){  
		if(i in this)  
                    curr = accumulator.call(undefined, curr, this[i], i, this);  
            }
            return curr;  
	}, writable:true 
    })

if (!Array.prototype.reduceRight)
    Object.defineProperty(Array.prototype,"reduceRight", {
	value : function(callbackfn)  {
            if (this == null)  
		throw new TypeError();  
            
            var t = Object(this);  
            var len = t.length >>> 0;  
            if (typeof callbackfn != "function")  
		throw new TypeError();  
	    
            if (len === 0 && arguments.length === 1)  
		throw new TypeError();  
            
            var k = len - 1;  
            var accumulator;  
            if (arguments.length >= 2)  {  
		accumulator = arguments[1];  
            }  
            else  {  
		do  {  
                    if (k in this)  {  
			accumulator = this[k--];  
			break;  
                    }  
                    if (--k < 0)  
			throw new TypeError();  
		}
		while (true);  
            }
	    
            while (k >= 0)  {  
		if (k in t)  
                    accumulator = callbackfn.call(undefined, accumulator, 
						  t[k], k, t);  
		k--;  
            }
            return accumulator;  
	} , writable:true 
    })

if (!Array.prototype.some)
    Object.defineProperty(Array.prototype,"some",{
	value : function(func, thisVal) {
            var len = this.length;
            for (var i = 0; i < len; i++)
		if (func.call(thisVal, this[i] || 
                              this.charAt && this.charAt(i), i, this))
                    return true;
            return false;
	}, writable:true
    })


/**
 * Conversion to ByteArray
 * @method Array.prototype.toByteArray
 * @param {string} [charset="utf-8"] Converts a string to a ByteArray. Supported values are: 
   "utf-8", "ascii", "hex", "ebcdic", or other encoding. 
 * @red
 * @returns {ByteArray}
*/
if (!Array.prototype.toByteArray)
    Object.defineProperty(Array.prototype,"toByteArray", {
	value: function(charset) {
	    var str = String.fromCharCode.apply(null, this);
	    return new ByteArray(str, charset);
	}, writable:true
    })

/**
 * Conversion to ByteString
 * @method Array.prototype.toByteString
 * @param {string} [charset="utf-8"] Converts a string to a ByteArray. Supported values are: 
   "utf-8", "ascii", "hex", "ebcdic", or other encoding. 
 * @red
 * @returns {ByteString}
 */
if (!Array.prototype.toByteString)
    Object.defineProperty(Array.prototype,"toByteString", {
	value: function(charset) {
	    return new ByteString(this, charset);
	}, writable:true
    })


/** 
 *  Equivalent to this.unshift.apply(this, arguments)
 *  @method module:binary.ByteArray.prototype.extendLeft
 *  @arg {Number|Array|ByteArray|ByteString} item1[,item2[,...]]
 */
Object.defineProperty(ByteArray.prototype, "extendLeft", {
    value: ByteArray.prototype.unshift,
    writable:true
});

/**
 *  Equivalent to this.push.apply(this, arguments) 
 *  @method module:binary.ByteArray.prototype.extendRight
 *  @arg {Number|Array|ByteArray|ByteString} item1[,item2[,...]]
 */
Object.defineProperty(ByteArray.prototype, "extendRight", {
    value: ByteArray.prototype.push,
    writable:true
});

/**
 * Tests whether all elements in the array pass the test implemented by the provided function
 * @method module:binary.ByteArray.prototype.every
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see Array.prototype.every
 */
Object.defineProperty(ByteArray.prototype,"every", {
    value : function(fn, thisObj) {
	return ByteArray((this.toArray()).every(fn,thisObj));
    }, writable: true
});

/**
 *  Fills each of the contained bytes with the given value (either a unary ByteString, ByteArray, or a Number) from the "start" offset to the "stop" offset. "start" and "stop" must be numbers, undefined, or omitted. If omitted, "start" is presumed to be 0. If omitted, "stop" is presumed to beh the length of this ByteArray. 
 *  @method module:binary.ByteArray.prototype.fill
 *  @arg {Number} [start=0]
 *  @arg {Number} [stop=length of this ByteArray]
 *  @returns {Number}
 */
Object.defineProperty(ByteArray.prototype,"fill", {
    value : function(value, start, stop) {
	var num = (value instanceof ByteArray ? value[0] : value);
	var from = start || 0;
	var to = stop || this.length;
	for (var i=from; i<to; i++) {
            this[i] = num;
	}
	return this;
    }, writable:true
});

/**
 * Creates a new ByteArray with all elements that pass the test implemented by the provided function
 * @method module:binary.ByteArray.prototype.filter
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @returns {ByteArray}
 * @see Array.prototype.filter
 */
Object.defineProperty(ByteArray.prototype,"filter", {
    value: function(fn, thisObj) {
	return ByteArray((this.toArray()).filter(fn,thisObj));
    }, writable: true
});

/**
 * Executes a provided function once per ByteArray element
 * @method module:binary.ByteArray.prototype.forEach
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see Array.prototype.forEach
 */
Object.defineProperty(ByteArray.prototype, "forEach", {
    value: function(fn, thisObj) {
	return ByteArray((this.toArray()).forEach(fn,thisObj));
    }, writable: true
});

/**
 * Creates a new array with the results of calling a provided function on every element in this array
 * @method module:binary.ByteArray.prototype.map
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see Array.prototype.map
 */
Object.defineProperty(ByteArray.prototype,"map", {
    value: function(fn, thisObj) {
	return ByteArray((this.toArray()).map(fn,thisObj));
    }, writable: true
});

/**
 * Tests whether some element in the array passes the test implemented by the provided function
 * @method module:binary.ByteArray.prototype.some
 * @arg {Function} callbackfn
 * @arg [thisArg]
 * @see Array.prototype.some
 */
Object.defineProperty(ByteArray.prototype,"some", {
    value: function(fn, thisObj) {
	return ByteArray((this.toArray()).some(fn,thisObj));
    }, writable:true
});

/**
 * Apply a function against an accumulator and each value of the array (from left-to-right) as to reduce it to a single value
 * @method module:binary.ByteArray.prototype.reduce
 * @arg {Function} callbackfn
 * @arg [initialValue]
 * @see Array.prototype.reduce
 */
Object.defineProperty(ByteArray.prototype,"reduce",{
    value: function(fn) {
	return ((this.toArray()).reduce(fn));
    }, writable:true
});

/**
 * Apply a function simultaneously against two values of the array (from right-to-left) as to reduce it to a single value
 * @method module:binary.ByteArray.prototype.reduceRight
 * @arg {Function} callbackfn
 * @arg [initialValue]
 * @see Array.prototype.reduceRight
 */
Object.defineProperty(ByteArray.prototype,"reduceRight",{ 
    value: function(fn) {
	return ((this.toArray()).reduceRight(fn));
    }, writable:true
});

/**
 * Sorts the elements of an array in place and returns the array
 * @method module:binary.ByteArray.prototype.slice
 * @arg {Number} beginSlice
 * @arg {Number} [endSlice]
 * @returns {ByteArray}
 * @see Array.prototype.slice
 */
Object.defineProperty(ByteArray.prototype,"sort",{
    value: function(fn) {
	if (fn == undefined) {
            fn =  function(a, b) {return a - b; }
	}

	return this.replace(0,this.length,
                            new ByteArray( this.toArray().sort(fn)));;
    }, writable: true
});

/**
 * Changes the content of an array, adding new elements while removing old elements
 * @method module:binary.ByteArray.prototype.splice
 * @arg start
 * @arg [deleteCount]
 * @arg {Object} [item]
 * @see Array.prototype.splice
 */
Object.defineProperty(ByteArray.prototype,"splice", {
    value: function(arguments) {
	var arr = this.toArray();
	arr.splice(arguments);
	return new ByteArray(arr);
    }, writable:true
});

/**
 * Splits the ByteArray object into an array of ByteArrays using the separator.
  
     There is an optional parameter with the following properties:
     count - Maximum number of elements (ignoring delimiters) to return. The last returned element may contain delimiters.
     includeDelimiter - Whether the delimiter should be included in the result.
 * @method module:binary.ByteArray.prototype.split
 * @arg {ByteString} separator - Can be of arbitrary length
 * @arg {Object} [options] - Optional parameter 
 * @returns {Array} array of ByteArray
 * @see String.prototype.split
 */
Object.defineProperty(ByteArray.prototype,"split", {
    value: function(delimiter, opt){
	var o = {
            count: -1,
            includeDelimiter: false
	}
	
	for (var p in opt ) o[p] = opt[p]; 
	
	var result = [];
	var start = 0;
	var index = -1;
	
	if (delimiter.length == 0)
        return Array(this);
	    
	    do {
            if (result.length == o.count) { return result; }
	        
            index = this.indexOf(delimiter, start);
            if (index == -1) { break; }
	    
            var end = index;
            if (o.includeDelimiter) { end += delimiter.length; }
            result.push(this.slice(start, end));
            start = index + delimiter.length; 
	    } while (index != -1);
	    
	    if (start < this.length) {
            result.push(this.slice(start));
	    }
	    
	    return result;
    }, writable:true
});

/**
 * Returns "ByteArray([])" for a null byte array or "ByteArray([0, 1, 2])" for a byte array of length 3 
 * with bytes 0, 1, and 2. 
 *  @method module:binary.ByteArray.prototype.toSource
 *  @returns {String}
 */
Object.defineProperty(ByteArray.prototype,"toSource", {
    value : function() {
	return "ByteArray([" + this.toArray().join(", ") + "])";
    }, writable:true
});

/**
 * Converts the ByteArray into a debug string like "[ByteArray 10]" where 10 is the length of the byte array.
 * @method module:binary.ByteArray.prototype.toString
 * @returns {String}
 */
Object.defineProperty(ByteArray.prototype,"toString", {
    value : function() {
	return "[ByteString "+this.length + "]";
    }, writable:true
})

/**
 * Splits the ByteString object into an array of ByteStrings using the separator.
  
     There is an optional parameter with the following properties:
     count - Maximum number of elements (ignoring delimiters) to return. The last returned element may contain delimiters.
     includeDelimiter - Whether the delimiter should be included in the result.
 * @method module:binary.ByteString.prototype.split
 * @arg {String} separator - Can be of arbitrary length
 * @arg {Object} [options] - Optional parameter 
 * @returns {Array} array of ByteString
 * @see String.prototype.split
 */
Object.defineProperty(ByteString.prototype,"split", {
    value: function(delimiter, opt){
	var o = {
            count: -1,
            includeDelimiter: false
	}
	
	for (var p in opt ) o[p] = opt[p]; 
	
	var result = [];
	var start = 0;
	var index = -1;
	
	if (delimiter.length == 0)
            return Array(this);
	
	do {
            if (result.length == o.count) { return result; }
	    
            index = this.indexOf(delimiter, start);
            if (index == -1) { break; }
	    
            var end = index;
            if (o.includeDelimiter) { end += delimiter.length; }
            result.push(this.slice(start, end));
            start = index + delimiter.length; 
	} while (index != -1);
	
	if (start < this.length) {
            result.push(this.slice(start));
	}
	
	return result;
    }, writable:true
});

/**
 * Returns "ByteString([])" for a null byte array or "ByteString([0, 1, 2])" for a byte array of length 3 
 * with bytes 0, 1, and 2. 
 *  @method module:binary.ByteString.prototype.toSource
 *  @returns {String}
 */
Object.defineProperty(ByteString.prototype,"toSource", {
    value : function() {
	return "ByteString([" + this.toArray().join(", ") + "])";
    }, writable:true
});

/**
 * Converts the ByteString into a debug string like "[ByteString 10]" where 10 is the length of the byte array.
 * @method module:binary.ByteString.prototype.toString
 * @returns {String}
 */
Object.defineProperty(ByteString.prototype,"toString", {
    value : function() {
	return "[ByteString "+this.length + "]";
    }, writable:true
})
