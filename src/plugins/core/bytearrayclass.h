#ifndef BYTEARRAYCLASS_H
#define BYTEARRAYCLASS_H

#include <QtCore/QObject>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>

#include "binary.h"


#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT 
#else
#define EXPORT 
#endif

class EXPORT ByteArrayClass : public QObject, public QScriptClass
{
 public:
    ByteArrayClass(QScriptEngine *engine);
    ~ByteArrayClass();

    QScriptValue constructor();

    // ByteArray()
    // ByteArray(length) 
    QScriptValue newInstance(int size = 0);

    // ByteArray(byteArray) (copy byteArray) 
    QScriptValue newInstance(const ByteArray &ba);
    
    // ByteArray(byteString) 
    QScriptValue newInstance( const ByteString &bs);
  
    // ByteArray(string, charset) 
    QScriptValue newInstance( QScriptValue string, 
			      QScriptValue charset );
 
    QueryFlags queryProperty(const QScriptValue &object,
			     const QScriptString &name,
			     QueryFlags flags, uint *id);

    QScriptValue property(const QScriptValue &object,
			  const QScriptString &name, uint id);

    void setProperty(QScriptValue &object, const QScriptString &name,
		     uint id, const QScriptValue &value);

    QScriptValue::PropertyFlags propertyFlags(const QScriptValue &object,
				     const QScriptString &name, uint id);

    QScriptClassPropertyIterator *newIterator(const QScriptValue &object);

    QString name() const;

    QScriptValue prototype() const;


    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

    static QScriptValue toScriptValue(QScriptEngine *eng,
				      const ByteArray &ba);

    static void fromScriptValue(const QScriptValue &obj,
				ByteArray &ba);

 private:
    void resize(ByteArray &ba, int newSize);

    QScriptString length_;
    QScriptValue  proto_;
    QScriptValue  ctor_;
};

#endif //BYTEARRAYCLASS_H
