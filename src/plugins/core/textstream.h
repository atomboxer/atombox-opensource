#ifndef TEXT_STREAM_H
#define TEXT_STREAM_H

#include <QtCore/QIODevice>
#include <QtCore/QTextStream>

#include "stream.h"

class TextStream : public QObject {
    Q_OBJECT
    Q_PROPERTY(Stream*  raw READ __internal_getRaw )

    friend class TextStreamPrototype;

public:
    TextStream(Stream *stream, QObject *parent= 0);
    ~TextStream();
    Stream *__internal_getRaw() { return raw_; }

private:
    Stream *raw_;
    QTextStream *ts_;
};

#endif //TEXT_STREAM_H
