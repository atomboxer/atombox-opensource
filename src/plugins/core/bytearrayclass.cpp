#include <QtCore/QTextCodec>
#include <QtCore/QDebug>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptClassPropertyIterator>
#include <QtScript/QScriptValueIterator>

#include "binary.h"
#include "bytearrayclass.h"
#include "bytearrayprototype.h"

#include "utils.h"

#include <stdlib.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString)
Q_DECLARE_METATYPE(ByteArrayClass*)
Q_DECLARE_METATYPE(ByteString*)

class ByteArrayClassPropertyIterator : public QScriptClassPropertyIterator
{
public:
    ByteArrayClassPropertyIterator(const QScriptValue &object);
    ~ByteArrayClassPropertyIterator();

    bool hasNext() const;
    void next();

    bool hasPrevious() const;
    void previous();

    void toFront();
    void toBack();

    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};

static qint32 toArrayIndex(const QString &str)
{
    ByteArray bytes = ByteArray(str.toUtf8());
    char *eptr;
    quint32 pos = strtoul(bytes.constData(), &eptr, 10);
    if ((eptr == bytes.constData() + bytes.size())
        && (ByteArray::number(pos) == bytes)) {
        return pos;
    }
    return -1;
}

ByteArrayClass::ByteArrayClass(QScriptEngine *engine)
    : QObject(engine), QScriptClass(engine)
{
    qScriptRegisterMetaType<ByteArray>(engine,
                                       toScriptValue,
                                       fromScriptValue);

    length_ = engine->toStringHandle(QLatin1String("length"));

    proto_ = engine->newQObject(new ByteArrayPrototype(this),
                                QScriptEngine::ScriptOwnership,
                                QScriptEngine::SkipMethodsInEnumeration
                                | QScriptEngine::ExcludeSuperClassMethods
                                | QScriptEngine::ExcludeSuperClassProperties);
    QScriptValue global = engine->globalObject();
    proto_.setPrototype(global.property("Object").property("prototype"));

    ctor_ = engine->newFunction(construct, proto_);
    ctor_.setData(engine->toScriptValue(this));
}

ByteArrayClass::~ByteArrayClass()
{
}

QScriptClass::QueryFlags
ByteArrayClass::queryProperty(const QScriptValue &object,
                              const QScriptString &name,
                              QueryFlags flags, uint *id)
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(object.data());
    if (!ba)
        return 0;
    if (name == length_) {
        return flags;
    } else {
        qint32 pos = toArrayIndex(name);
        if (pos == -1)
            return 0;
        *id = pos;
        if ((flags & HandlesReadAccess) && (pos >= ba->size()))
            flags &= ~HandlesReadAccess;
        return flags;
    }
}

QScriptValue
ByteArrayClass::property(const QScriptValue &object,
                         const QScriptString &name, uint id)
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(object.data());
    if (!ba)
        return QScriptValue();
    if (name == length_) {
        return ba->length();
    } else {
        qint32 pos = id;
        if ((pos < 0) || (pos >= ba->size()))
            return QScriptValue();
        return uint(ba->at(pos)) & 255;
    }
    return QScriptValue();
}

void
ByteArrayClass::setProperty(QScriptValue &object,
                            const QScriptString &name,
                            uint id, const QScriptValue &value)
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(object.data());
    if (!ba)
        return;
    if (name == length_) {
        resize(*ba, value.toInt32());
    } else {
        qint32 pos = id;
        if (pos < 0)
            return;
        if (ba->size() <= pos)
            resize(*ba, pos + 1);
        (*ba)[pos] = char(value.toInt32());
    }
}

QScriptValue::PropertyFlags
ByteArrayClass::propertyFlags(const QScriptValue &,
                              const QScriptString &name, uint )
{
    if (name == length_) {
        return QScriptValue::Undeletable
            | QScriptValue::SkipInEnumeration;
    }
    return QScriptValue::Undeletable;
}

QScriptClassPropertyIterator *
ByteArrayClass::newIterator(const QScriptValue &object)
{
    return new ByteArrayClassPropertyIterator(object);
}

QString
ByteArrayClass::name() const
{
    return QLatin1String("ByteArray");
}

QScriptValue
ByteArrayClass::prototype() const
{
    return proto_;
}

QScriptValue
ByteArrayClass::constructor()
{
    return ctor_;
}

QScriptValue
ByteArrayClass::newInstance(int size)
{
    engine()->reportAdditionalMemoryCost(size);

    return newInstance(ByteArray(size, /*ch=*/0));
}

QScriptValue
ByteArrayClass::newInstance(const ByteArray &ba)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(ba));
    return engine()->newObject(this, data);
}

QScriptValue
ByteArrayClass::newInstance(const ByteString &bs)
{
    ByteArray alt(bs);
    QScriptValue data = engine()->newVariant(QVariant::fromValue(alt));
    return engine()->newObject(this, data);
}

QScriptValue
ByteArrayClass::newInstance(QScriptValue string, QScriptValue charset)
{
    if (charset.toString().toLower() == "hex") {
        return newInstance(ByteArray(QByteArray::fromHex(string.toString().toLatin1().data())));
    }

    if (charset.toString().toLower() == "base64") {
        return newInstance(ByteArray(QByteArray::fromBase64(string.toString().toLatin1().data())));
    }
    
    if (charset.toString().toLower() == "ascii")
        charset = "utf-8";

    if (charset.toString().toLower() == "ebcdic") {
        QByteArray ba = string.toString().toAscii();
        
        ASCII2EBCDIC(ba.data(), ba.length());
        return newInstance(ByteArray(ba));
    }

    QTextCodec *codec = QTextCodec::codecForName(charset.toString().toLatin1().data());
    
    if (!codec) {
        engine()->currentContext()->throwError("Invalid Codec");
        return QScriptValue();
    }
    
    return newInstance(ByteArray(codec->fromUnicode(string.toString())));
}


QScriptValue
ByteArrayClass::construct(QScriptContext *ctx,
                          QScriptEngine  *eng)
{
    ByteArrayClass *cls = NULL;
    cls = qscriptvalue_cast<ByteArrayClass*>(ctx->callee().data());

    if (!cls)
        return QScriptValue();

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qscriptvalue_cast<ByteArray>(arg));

    //
    //  String and charset
    //
    if (ctx->argumentCount() == 2) {
        return cls->newInstance(ctx->argument(0), ctx->argument(1));
    }
    if (ctx->argumentCount() == 1 && arg.isString()) {
        return cls->newInstance(ctx->argument(0), "utf-8");
    }


    //
    //  Maybe the argument is a ByteString
    //
    ByteString *bs = NULL;
    bs = qscriptvalue_cast<ByteString*>(arg.data());
    if ( bs ) {
        return cls->newInstance(*bs);
    }

    if (arg.isArray()) {
        ByteArray alt;

        QScriptValueIterator it(arg);

        while (it.hasNext()) {
            it.next();
            if (it.flags() & QScriptValue::SkipInEnumeration)
                continue;
            if (it.value().isNumber()) {
                int value = it.value().toInt32();
                if ( value < 0 || value > 255) {
                    ctx->throwError("ByteArray accepts only 0-255 values");
                    return QScriptValue();
                }

                alt.push_back((unsigned char)value);
            }
        }
        return cls->newInstance(alt);
    }

    int size = arg.toInt32();
    return cls->newInstance(size);
}

QScriptValue
ByteArrayClass::toScriptValue( QScriptEngine *eng,
                               const ByteArray &ba)
{
    QScriptValue ctor_ = eng->globalObject().property("__internal__").property("ByteArray");
    ByteArrayClass *cls = qscriptvalue_cast<ByteArrayClass*>(ctor_.data());
    if (!cls)
        return eng->newVariant(QVariant::fromValue(ba));

    return cls->newInstance(ba);
}

void
ByteArrayClass::fromScriptValue(const QScriptValue &obj,
                                ByteArray &ba)
{
    ba = qvariant_cast<ByteArray>(obj.data().toVariant());
}

void
ByteArrayClass::resize(ByteArray &ba, int newSize)
{
    int oldSize = ba.size();

    if (oldSize < newSize) {
        ByteArray newba(newSize,0);
        newba.replace(0,ba.size(),ba);
        ba = newba;
    } else {
        ba.resize(newSize);
    }

    if (newSize > oldSize)
        engine()->reportAdditionalMemoryCost(newSize - oldSize);
}

ByteArrayClassPropertyIterator::ByteArrayClassPropertyIterator(
                                                               const QScriptValue &object)
    : QScriptClassPropertyIterator(object)
{
    toFront();
}

ByteArrayClassPropertyIterator::~ByteArrayClassPropertyIterator()
{
}

bool ByteArrayClassPropertyIterator::hasNext() const
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(object().data());
    return m_index < ba->size();
}

void ByteArrayClassPropertyIterator::next()
{
    m_last = m_index;
    ++m_index;
}

bool ByteArrayClassPropertyIterator::hasPrevious() const
{
    return (m_index > 0);
}

void ByteArrayClassPropertyIterator::previous()
{
    --m_index;
    m_last = m_index;
}

void ByteArrayClassPropertyIterator::toFront()
{
    m_index = 0;
    m_last = -1;
}

void ByteArrayClassPropertyIterator::toBack()
{
    ByteArray *ba = qscriptvalue_cast<ByteArray*>(object().data());
    m_index = ba->size();
    m_last = -1;
}

QScriptString ByteArrayClassPropertyIterator::name() const
{
    return object().engine()->toStringHandle(QString::number(m_last));
}

uint ByteArrayClassPropertyIterator::id() const
{
    return m_last;
}
