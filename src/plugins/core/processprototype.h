#ifndef PROCESSPROTOTYPE_H
#define PROCESSPROTOTYPE_H

#include "binary.h"
#include "process.h"
#include "streamprototype.h"

#include <QtCore/QProcess>
#include <QtCore/QIODevice>


class ProcessPrototype : public StreamPrototype
{

    Q_OBJECT

public:
    ProcessPrototype(QObject *parent = 0);
    ~ProcessPrototype();

public slots:
    void start(QString program, QStringList arguments=QStringList()) const;
    void stop() const;
#ifndef _GUARDIAN_TARGET
    ByteString readAllStandardError();
    ByteString readAllStandardOutput();
#endif
private:
    QProcess *thisProcess() const;
    virtual QIODevice* thisIODevice();
};

#endif //PROCESSPROTOTYPE
