#include "process.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QProcess>
#include <QtCore/QDebug>


#ifdef _GUARDIAN_TARGET
#include <cextdecs.h>
#include <tal.h>
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


Process::Process(QProcess *priv, QObject *parent) :
    QObject(parent)
{
    if (!priv) {
        qFatal("Process::Process programatic error");
    }

    priv_ = priv;

    QObject::connect(priv_, SIGNAL(error(QProcess::ProcessError)), 
                     this,  SLOT(slotError(QProcess::ProcessError))/*, 
                                                                     Qt::DirectConnection*/);

    QObject::connect(priv_, SIGNAL(finished(int, QProcess::ExitStatus)), 
                     this,  SLOT(slotFinished(int, QProcess::ExitStatus))/*,
                                                                           Qt::DirectConnection*/);

#ifndef _GUARDIAN_TARGET
    QObject::connect(priv_, SIGNAL(readyReadStandardError()), 
                     this,  SIGNAL(readyReadStandardError()));

    QObject::connect(priv_, SIGNAL(readyReadStandardOutput()), 
                     this,  SIGNAL(readyReadStandardOutput()));
#endif

    QObject::connect(priv_, SIGNAL(started()), 
                     this,  SLOT(slotStarted())/*, Qt::DirectConnection*/);

}


void 
Process::slotStarted()
{
    emit started();
    //qDebug()<<"STARTED!";
    QCoreApplication::processEvents();
}

void 
Process::slotError(QProcess::ProcessError err)
{
    QString serr;

    switch(err) {
    case (QProcess::FailedToStart): serr = "Process failed to start"; break;
    case (QProcess::Crashed): serr = "Process has crashed"; break;
    case (QProcess::Timedout): serr = "Process timed out"; break;
    case (QProcess::WriteError):serr = "Write error"; break;
    case (QProcess::ReadError): serr = "Read error"; break;
    case (QProcess::UnknownError): serr = "Unknown error"; break;
    }

    emit error(serr);
    QCoreApplication::processEvents();
}

void
Process::slotFinished(int ex, QProcess::ExitStatus stat)
{

    if (stat == QProcess::NormalExit)
        emit finished(0);
    else
        emit finished(1);

    QCoreApplication::processEvents();
}

Process::~Process()
{

    //qDebug()<<"Process::~Process";
    if (priv_)
        delete priv_;
}
