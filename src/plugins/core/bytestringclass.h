#ifndef BYTESTRINGCLASS_H
#define BYTESTRINGCLASS_H

#include <QtCore/QObject>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>

#include "binary.h"

#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT 
#else
#define EXPORT 
#endif

class EXPORT ByteStringClass : public QObject, public QScriptClass
{
 public:
    ByteStringClass(QScriptEngine *engine);
    ~ByteStringClass();

    QScriptValue constructor();

    // ByteString()
    QScriptValue newInstance();

    // ByteString(byteString) (copy byteString) 
    QScriptValue newInstance(const ByteString &ba);
    
    // ByteString(byteArray) (copy byteArray) 
    QScriptValue newInstance(const ByteArray &ba);

    // ByteString(string, charset)
    QScriptValue newInstance(QScriptValue string, QScriptValue charset);
 
    QueryFlags queryProperty(const QScriptValue &object,
			     const QScriptString &name,
			     QueryFlags flags, uint *id);

    QScriptValue property(const QScriptValue &object,
			  const QScriptString &name, uint id);

    void setProperty(QScriptValue &object, const QScriptString &name,
		     uint id, const QScriptValue &value);

    QScriptValue::PropertyFlags propertyFlags(const QScriptValue &object,
				     const QScriptString &name, uint id);

    QScriptClassPropertyIterator *newIterator(const QScriptValue &object);

    QString name() const;

    QScriptValue prototype() const;


    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

    static QScriptValue toScriptValue(QScriptEngine *eng,
				      const ByteString &ba);

    static void fromScriptValue(const QScriptValue &obj, ByteString &ba);
 private:
    void resize(ByteString &ba, int newSize);

    QScriptString length_;
    QScriptValue  proto_;
    QScriptValue  ctor_;
};

#endif //BYTESTRINGCLASS_H
