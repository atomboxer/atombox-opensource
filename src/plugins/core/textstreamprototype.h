#ifndef TEXTSTREAMPROTOTYPE_H
#define TEXTSTREAMPROTOTYPE_H

#include <QtCore/QHash>
#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"
#include "textstream.h"

class TextStreamPrototype : public QObject, public QScriptable
{
    Q_OBJECT
public:
    TextStreamPrototype(QObject *parent = 0);
    ~TextStreamPrototype();

public slots:
    bool hasNext() const;
    void copy(Stream *io) const;
    QString readLine() const;
    QStringList readLines() const;
    QString next() const;
    void print(QScriptValue args) const;
    void write(QString txt) const;
    void writeLine(QString line) const;
    void writeLines(QStringList lines) const;

private:
    QIODevice*   thisIODevice() const;
    QTextStream*  thisStream() const;
};

#endif //TEXTSTREAMPROTOTYPE_H
