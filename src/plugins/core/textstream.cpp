#include "textstream.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

TextStream::TextStream(Stream *stream, QObject *parent) : 
    QObject(parent)
{
    if (!stream) {
        qFatal("TextStream::TextStream programatic error");
    }

    raw_ = stream;
    ts_ = new QTextStream(raw_->priv_);
}

TextStream::~TextStream()
{
    if (ts_)
        delete ts_;

    if (raw_)
        delete raw_;
}

