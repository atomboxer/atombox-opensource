#include "timer.h"

#include <QtCore/QTimer>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Timer::Timer(QObject *parent) : QObject(parent)
{
    QObject::connect(&timer_, SIGNAL(timeout()), this, SIGNAL(timeout()));
}



Timer::~Timer()
{
}
