#include "system.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>

#if defined(_AIX)
#include <sys/procfs.h>
#include <fcntl.h>
#endif

#if  defined(__TANDEM)
#include <unistd.h>
#include <cextdecs.h>
#elif defined(_WIN32) && !defined(_MSC_VER) 
#include <stdlib.h>
#include <windows.h>
#include <process.h>
#include <psapi.h>
#define sleep(n) Sleep(1000 * n)
#else 
#include <unistd.h>
#include <stdlib.h>
#endif

#include <sys/types.h>
#include <unistd.h>

#ifdef __unix__
#include <sys/resource.h>
#endif

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


#if defined(_WIN32) && defined(__GNUC__)
int __getpid(void)
{
    return GetCurrentProcessId();
}
#endif

/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t getPeakRSS( )
{
#if defined(_WIN32)
    /* Windows -------------------------------------------------- */
    PROCESS_MEMORY_COUNTERS info;
    GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
    return (size_t)info.PeakWorkingSetSize;
#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
    /* AIX and Solaris ------------------------------------------ */
    struct psinfo psinfo;
    int fd = -1;
    if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 )
        return (size_t)0L;		/* Can't open? */
    if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
    {
        close( fd );
        return (size_t)0L;		/* Can't read? */
    }
    close( fd );
    return (size_t)(psinfo.pr_rssize * 1024L);

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
    /* BSD, Linux, and OSX -------------------------------------- */
    struct rusage rusage;
    getrusage( RUSAGE_SELF, &rusage );
#if defined(__APPLE__) && defined(__MACH__)
    return (size_t)rusage.ru_maxrss;
#else
    return (size_t)(rusage.ru_maxrss * 1024L);
#endif

#else
    /* Unknown OS ----------------------------------------------- */
    return (size_t)0L;			/* Unsupported. */
#endif
}

QScriptValue 
__system_exit(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1 || !ctx->argument(0).isNumber()) {
        ctx->throwError("<system.exit> requires one numeric argument");
        return QScriptValue();
    }

    QCoreApplication::quit();
    exit(0);
    return QScriptValue();
}


QScriptValue 
__system_sleep(QScriptContext *ctx, QScriptEngine *)
{
    if (ctx->argumentCount() != 1 || !ctx->argument(0).isNumber()) {
        ctx->throwError("<system.sleep> requires one numeric argument");
        return QScriptValue();
    }

    sleep(ctx->argument(0).toNumber());
    return QScriptValue();
}

//system.gid
QString __system_gid()
{
#if defined(_WIN32)
    return "0";
#elif defined (_GUARDIAN_TARGET)
    short handle[10];
    short ret_attr_list[99];
    memset(&ret_attr_list, 0, 99);
    ret_attr_list[0] = 21/*real group id*/;

    short ret_values_list[999];
    short ret_values_len=0;

    PROCESSHANDLE_NULLIT_(handle);
    short error_detail;
    short error = PROCESSHANDLE_GETMINE_(handle);

    if (error != 0) {
        qFatal("programmatic error in __system_uid");
    }

    error = PROCESS_GETINFOLIST_(/*cpu*/,/*pin*/,/*node*/,/*length*/,handle,ret_attr_list,1,ret_values_list,
                                 99/*maxlen*/, &ret_values_len,&error_detail);
    if (error != 0) {
        qFatal("programmatic error __system_uid");
        return QString();
    }

    int id; memcpy((void*)&id, &ret_values_list, sizeof(short)*ret_values_len); 
    return QString::number(id);

#else
    return QString::number(getgid());
#endif
}

//system.uid
QString __system_uid()
{
#if defined(_WIN32)
    return "0";
#elif defined (_GUARDIAN_TARGET)
    short handle[10];
    short ret_attr_list[99];
    memset(&ret_attr_list, 0, 99);
    ret_attr_list[0] = 22 /*real user id*/;

    short ret_values_list[999];
    short ret_values_len=0;

    PROCESSHANDLE_NULLIT_(handle);
    short error_detail;
    short error = PROCESSHANDLE_GETMINE_(handle);

    if (error != 0) {
        qFatal("programmatic error in __system_uid");
    }

    error = PROCESS_GETINFOLIST_(/*cpu*/,/*pin*/,/*node*/,/*length*/,handle,ret_attr_list,1,ret_values_list,
                                 99/*maxlen*/, &ret_values_len,&error_detail);
    if (error != 0) {
        qDebug()<<"error:"<<error<<","<<error_detail;
        qFatal("programmatic error __system_uid");
        return QString();
    }

    int id; memcpy((void*)&id, &ret_values_list, sizeof(short)*ret_values_len); 
    return QString::number(id);
#else
    return QString::number(getuid());
#endif
}


QString __system_loginName()
{
#if defined(_WIN32)
    return "unknown";
#elif defined (_GUARDIAN_TARGET)
    short handle[10];
    short ret_attr_list[99];
    memset(&ret_attr_list, 0, 99);
    ret_attr_list[0] = 82 /*login name*/;

    short ret_values_list[999];
    short ret_values_len=0;

    PROCESSHANDLE_NULLIT_(handle);
    short error_detail;
    short error = PROCESSHANDLE_GETMINE_(handle);

    if (error != 0) {
        qFatal("programmatic error in __system_uid");
    }

    error = PROCESS_GETINFOLIST_(/*cpu*/,/*pin*/,/*node*/,/*length*/,handle,ret_attr_list,1,ret_values_list,
                                 99/*maxlen*/, &ret_values_len,&error_detail);
    if (error != 0) {
        qFatal("programmatic error __system_uid");
        return QString();
    }

    short len;
    memcpy((void*)&len, ret_values_list, sizeof(short));
    char login[33];
    memcpy((void*)login, ret_values_list+1, len+1/*sizeof(short)*ret_values_len*/); 
    return login;
#else
    return QString("unknown");
#endif
}

//system.pid
QString __system_pid()
{
#if defined(_WIN32)
    return QString::number(__getpid());
#elif defined (_GUARDIAN_TARGET)
    short handle[10];
    PROCESSHANDLE_NULLIT_(handle);
    short error = PROCESSHANDLE_GETMINE_(handle);
    if (error != 0) {
        qFatal("programmatic error in __system_uid");
    }

    short cpu;
    short pin;
    error = PROCESSHANDLE_DECOMPOSE_(handle,&cpu, &pin);

    if (error != 0) {
        qFatal("programmatic error __system_uid");
        return QString();
    }

    return QString::number(cpu)+","+QString::number(pin);
#else
    return QString::number(getpid());
#endif
}

QScriptValue __system_processStats(QScriptContext *, QScriptEngine *eng)
{
    QScriptValue ret = eng->newObject();

#ifdef _GUARDIAN_TARGET
    short handle[10];
    short ret_attr_list[99];
    memset(&ret_attr_list, 0, 99);

    ret_attr_list[0] = 55  /*msg_sent, INT32*/;
    ret_attr_list[1] = 56  /*msg_recv, INT32*/;
    ret_attr_list[2] = 103 /*stack_size, INT32*/;
    ret_attr_list[3] = 104 /*max_stack_size, INT32*/;
    ret_attr_list[4] = 111 /*heap_size, INT32*/;
    ret_attr_list[5] = 112 /*max_heap_size, INT32*/;
    
    struct ret_struct {
        int   msg_sent;
        int   msg_recv;
        int   stack_size;
        int   max_stack_size;
        int   heap_size;
        int   max_heap_size;
    };
    
    ret_struct ret_values_list;
    short ret_values_len=0;

    PROCESSHANDLE_NULLIT_(handle);
    short error_detail;
    short error = PROCESSHANDLE_GETMINE_(handle);

    if (error != 0) {
        qFatal("programmatic error in __system_uid");
    }

    error = PROCESS_GETINFOLIST_(/*cpu*/,/*pin*/,/*node*/,/*length*/,handle,ret_attr_list,6,(short*)&ret_values_list,
                                 sizeof(ret_struct), &ret_values_len,&error_detail);
    if (error != 0) {
        qFatal("programmatic error __system_uid");
        return QString();
    }
    

    ret.setProperty("msg_sent", ret_values_list.msg_sent);
    ret.setProperty("msg_recv", ret_values_list.msg_recv);
    ret.setProperty("stack_size", ret_values_list.stack_size);
    ret.setProperty("max_stack_size", ret_values_list.max_stack_size);
    ret.setProperty("heap_size", (qsreal)ret_values_list.heap_size);
    ret.setProperty("max_heap_size", (qsreal)ret_values_list.max_heap_size);

#else
    ret.setProperty("stack_size", (int)getPeakRSS());
#endif
    return ret;
}
