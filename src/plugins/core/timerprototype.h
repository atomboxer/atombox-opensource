#ifndef TIMERPROTOTYPE_H
#define TIMERPROTOTYPE_H

#include "timer.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>

class TimerPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    TimerPrototype(QObject *parent = 0);
    ~TimerPrototype();

public slots:

    int interval() const { return thisTimer()->interval(); }
    bool isActive() const { return thisTimer()->isActive(); }
    bool isSingleShot() const { return thisTimer()->isSingleShot(); }
    void setInterval( int msec ) const { thisTimer()->setInterval( msec ); } 
    void setSingleShot( bool singleShot ) const { thisTimer()->setSingleShot( singleShot ); } 

    void start(int msec)const { thisTimer()->start(msec); }
    void start() const { thisTimer()->start(); }
    void stop() const { thisTimer()->stop(); }


private:
    Timer *thisTimer() const;
};

#endif //TIMERPROTOTYPE_H
