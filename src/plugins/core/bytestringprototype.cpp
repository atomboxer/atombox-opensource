#include "bytestringclass.h"
#include "bytestringprototype.h"

#include <QtCore/QTextCodec>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include "utils.h"
#include "cityhash/city.h"

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

static ByteString arrayToByteString(QScriptValue arg)
{
    ByteString alt;

    QScriptValueIterator it(arg);

    while (it.hasNext()) {
        it.next();
        if (it.flags() & QScriptValue::SkipInEnumeration)
            continue;
        if (it.value().isNumber()) {
            int value = it.value().toInt32();
            if ( value < 0 || value > 255) {
                Q_ASSERT(0);
                return ByteString();
            }
            alt.push_back((unsigned char)value);
        }
    }
    return alt;
}

ByteStringPrototype::ByteStringPrototype(QObject *parent)
: QObject(parent)
{
}

ByteStringPrototype::~ByteStringPrototype()
{
}

ByteString *
ByteStringPrototype::thisByteString() const
{
    return qscriptvalue_cast<ByteString*>(thisObject().data());
}

ByteString
ByteStringPrototype::byteAt(int offset) const
{
    return valueAt(offset);
}

ByteString
ByteStringPrototype::charAt(int offset) const
{
    return byteAt(offset);
}

QScriptValue
ByteStringPrototype::charCodeAt(int offset) const
{
    return get(offset);
}

QScriptValue
ByteStringPrototype::codeAt(int offset) const
{
     ByteString *bs = thisByteString();

     if (offset < 0 || offset >= bs->size()) {
         return QScriptValue();
     }
     return ((unsigned char)bs->at(offset));
}

ByteString
ByteStringPrototype::concat( QScriptValue /*other*/) const
{
    ByteString ba((char*)thisByteString()->data(), thisByteString()->length());
    for (int i = 0; i < context()->argumentCount(); ++i) {
        QScriptValue arg = context()->argument(i);
        ByteString  *bs_arg = 0;
        ByteArray   *ba_arg = 0;
        bs_arg = qscriptvalue_cast<ByteString*>(arg.data());
        if (!bs_arg) {
            ba_arg = qscriptvalue_cast<ByteArray*>(arg.data());
        }
        if ( bs_arg ) {
            ba.append(*bs_arg);
        } else if (ba_arg) {
            ba.append(*ba_arg);
        } else if (arg.isNumber()) {
            ba.push_back((unsigned char)arg.toInt32());
        } else if (arg.isArray()) {
            ba.append(arrayToByteString(arg));
        } else if (arg.isString()) {
            ba.append(arg.toString().toLatin1().data());
        } else {
            Q_ASSERT_X(0,"error","It should never get here.");
        }
    }
    return ba;
}

ByteString
ByteStringPrototype::compress() const
{
    ByteString *ba = thisByteString();
    return qCompress(*ba);
}

ByteString
ByteStringPrototype::uncompress() const
{
    ByteString *ba = thisByteString();
    return qUncompress(*ba);
}

QString
ByteStringPrototype::decodeToString(QScriptValue charset) const
{
    ByteString *bs = thisByteString();

    QString encoding = charset.toString();
    if (encoding.isEmpty() || encoding.toLower() == "ascii") {
        encoding = "utf-8";
    }

    if (encoding.toLower() == "hex") {
        return QString(bs->toHex());
    }

    if (encoding.toLower() == "base64") {
        return QString(bs->toHex());
    }

    if (encoding.toLower() == "ebcdic") {
        QByteArray copy(bs->data(), bs->length());
        EBCDIC2ASCII((char*)copy.data(), bs->length());
        return QString(copy);
    }

    QTextCodec *codec = QTextCodec::codecForName(encoding.toLatin1().data());
    if (!codec) {
        context()->throwError(QString("codec:")+encoding+" not found!");
        return QString();
    }
    return codec->toUnicode(*bs).remove(QChar(0x00));;
}

QScriptValue
ByteStringPrototype::get(int offset) const
{
    return codeAt(offset);
}

ByteString
ByteStringPrototype::hash32() const
{
    ByteString *ba = thisByteString();

    ByteString ret;
    quint32 hash = CityHash32(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}

ByteString
ByteStringPrototype::hash64() const
{
    ByteString *ba = thisByteString();

    ByteString ret;
    quint64 hash = CityHash64(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}

ByteString
ByteStringPrototype::hash64WithSeed(ByteString seed) const
{
    quint64 num_seed=0;
    QDataStream(seed.mid(0))>>num_seed;

    ByteString *ba = thisByteString();
    ByteString ret;
    quint64 hash = CityHash64WithSeed(ba->data(), ba->length(),num_seed);
    QDataStream(&ret,QIODevice::WriteOnly)<<hash;
    return ret;
}


ByteString
ByteStringPrototype::hash128() const
{
    ByteString *ba = thisByteString();

    ByteString ret;
    std::pair<quint64,quint64> hash = CityHash128(ba->data(), ba->length());
    QDataStream(&ret,QIODevice::WriteOnly)<<hash.first<<hash.second;
    return ret;
}

ByteString
ByteStringPrototype::hash128WithSeed(ByteString seed) const
{
    quint64 num_seed1=0;
    quint64 num_seed2=0;
    QDataStream(seed.mid(0))>>num_seed1>>num_seed2;

    ByteString *ba = thisByteString();
    ByteString ret;
    std::pair<quint64,quint64> hash = CityHash128WithSeed(ba->data(), ba->length(),
                                                          std::pair<quint64, quint64>(num_seed1, num_seed2));
    QDataStream(&ret,QIODevice::WriteOnly)<<hash.first<<hash.second;
    return ret;
}

int
ByteStringPrototype::indexOf( QScriptValue arg,
                             int from) const
{
    if (arg.isNumber()) {
        return thisByteString()->indexOf(arg.toInt32(),from);
    }

    ByteString *ba_arg = qscriptvalue_cast<ByteString*>(arg.data());
    if (ba_arg) {
        return thisByteString()->indexOf(*ba_arg, from);
    }

    if (arg.isArray()) {
        return thisByteString()->indexOf(arrayToByteString(arg), from);
    } else {
        context()->throwError("Unsupported argument in ByteString.indexOf");
        return -1;
    }

    return -1;
}

bool
ByteStringPrototype::isPrint( ) const
{
    ByteString *ba = thisByteString();

    for (int i=0; i<ba->length(); i++) {
        const unsigned char c = ba->at(i);
        if (!isprint(c))
            return false;

    }

    return true;
}

int
ByteStringPrototype::lastIndexOf( QScriptValue arg,
                                 int from) const
{
    if (arg.isNumber()) {
        return thisByteString()->lastIndexOf(arg.toInt32(), from);
    }

    ByteString *ba_arg = qscriptvalue_cast<ByteString*>(arg.data());
    if (!ba_arg) {
        context()->throwError("Unsupported argument in ByteString.lastIndexOf");
        return -1;
    }

    return thisByteString()->lastIndexOf(*ba_arg, from);
}

ByteString
ByteStringPrototype::slice() const
{
    return slice(QScriptValue(0));
}

ByteString
ByteStringPrototype::slice(QScriptValue /*args*/) const
{
    QScriptValue begin = context()->argument(0);
    QScriptValue end = context()->argument(1);

    ByteString *ba = thisByteString();

    int from = 0;

    //if (begin.isNumber()) {
    from = begin.toInt32();
    //} else {
    //return ByteString(*thisByteString());
    //}


    if (from < 0) {
        from += ba->length();
    }

    from = qMin(ba->length(), qMax(0, from));
    int to = ba->length();

    //if (end.isNumber()) {
    to = end.toInt32();
    //} else {
    //to = ba->length();
    //}

    if ( to <= 0 ) {
        to += ba->length();
    }

    int len = qMax(0,qMin(ba->length()-from, to - from));

    return ByteString(*thisByteString()).mid(from,len);
}

QList<int>
ByteStringPrototype::toArray(QString charset) const
{
    QList<int> arr;
    QTextCodec *codec = QTextCodec::codecForName(charset.toLatin1().data());

    if (!codec) {
        context()->throwError(QString("codec not found!"));
        return arr;
    }

    QString enc = codec->toUnicode(*thisByteString());
    for (int i=0;i<enc.length();i++) {
        arr.push_back((unsigned char)enc.at(i).unicode());
    }

    return arr;
}

ByteArray
ByteStringPrototype::toByteArray() const
{
     ByteString *bs = thisByteString();
     return ByteArray(*bs);
}

ByteArray
ByteStringPrototype::toByteArray(QString src_charset,
                                 QString dst_charset) const
{
    QTextCodec *sc = QTextCodec::codecForName(src_charset.toLatin1().data());
    QTextCodec *dc = QTextCodec::codecForName(dst_charset.toLatin1().data());
    if (!sc || !dc) {
        context()->throwError(QString("codec not found!"));
        return QByteArray();
    }

    return dc->fromUnicode(sc->toUnicode(*thisByteString()));
}

ByteString
ByteStringPrototype::toByteString() const
{
    ByteString *bs = thisByteString();
    return ByteString(*bs);
}

ByteString
ByteStringPrototype::toByteString(QString src_charset,
                                  QString dst_charset) const
{
    QTextCodec *sc = QTextCodec::codecForName(src_charset.toLatin1().data());
    QTextCodec *dc = QTextCodec::codecForName(dst_charset.toLatin1().data());
    if (!sc || !dc) {
        context()->throwError(QString("codec not found!"));
        return QByteArray();
    }

    return dc->fromUnicode(sc->toUnicode(*thisByteString()));
}

ByteString
ByteStringPrototype::valueAt(int offset) const
{
    ByteString *bs = thisByteString();

    if (offset < 0 || offset >= bs->size()) {
        return ByteString();
    }
    ByteString alt;
    alt.push_back((unsigned char)bs->at(offset));
    return alt;
}

QScriptValue
ByteStringPrototype::valueOf() const
{
     return thisObject().data();
}

//
// Non CommonJS
//
ByteString
ByteStringPrototype::replace( int pos,
			     int len,
			     QScriptValue arg ) const
{
    ByteString *ba = qscriptvalue_cast<ByteString*>(arg.data());
    if (!ba) {
        context()->throwError("Unsupported argument in ByteString.set");
        return ByteString();
    }

    return thisByteString()->replace(pos,len,*ba);
}

QScriptValue
ByteStringPrototype::readInt8( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint8 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteStringPrototype::readInt16( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint16 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteStringPrototype::readInt32( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    qint32 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteStringPrototype::readUInt8( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint8 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;
}

QScriptValue
ByteStringPrototype::readUInt16( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint16 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;

}

QScriptValue
ByteStringPrototype::readUInt32( qint32 offset ) const
{
    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }
    quint16 ret;
    QDataStream(thisByteString()->mid(offset))>>ret;
    return ret;

}

QScriptValue
ByteStringPrototype::writeInt8 ( qint8 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::writeInt16 ( qint16 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::writeInt32 ( qint32 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::writeUInt8 ( quint8 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::writeUInt16( quint16 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::writeUInt32( quint32 value, qint32 offset ) const
{
    QByteArray *ba = thisByteString();

    if (offset>thisByteString()->length()) {
        return QScriptValue(QScriptValue::UndefinedValue);
    }

    if (offset == 0)
      QDataStream(ba,QIODevice::WriteOnly)<<value;
    else {
      QByteArray aux;
      QDataStream(&aux,QIODevice::WriteOnly)<<value;
      ba->replace(offset,sizeof(value), aux);
    }
    return value;
}

QScriptValue
ByteStringPrototype::toNumber() const
{
    QByteArray *ba = thisByteString();
    int length = ba->length();

    if (length == 1) {
        QDataStream str(*ba);
        qint8 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length == 2) {
        QDataStream str(*ba);
        qint16 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length <= 4) {
        if (length == 3)
            ba->push_front((char)0x00);
        QDataStream str(*ba);
        qint32 val;
        str >> val;
        return QScriptValue(val);
    }

    if (length <= 8) {
        for(int i=0;i<8-length;i++)
            ba->push_front((char)0x00);
        QDataStream str(*ba);
        qlonglong val;
        str >> val;
        return QScriptValue((qsreal)val);
    }

    return QScriptValue(QScriptValue::UndefinedValue);
}
