#ifndef PROCESS_H
#define PROCESS_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

#include <QtCore/QObject>
#include <QtCore/QProcess>

class Process : public QObject
{
    friend class ProcessPrototype;
    
    Q_OBJECT

public:
    Process(QProcess *priv, QObject *parent = 0);
    ~Process();

signals:
    void error(QString e);
    void finished(int status);

#ifndef _GUARDIAN_TARGET
    void readyReadStandardError();
    void readyReadStandardOutput();
#endif
    void started();

private slots:
    void slotError(QProcess::ProcessError);
    void slotFinished(int, QProcess::ExitStatus);    
    void slotStarted();

private:
    QProcess *priv_;
};

#endif //PROCESS_H
