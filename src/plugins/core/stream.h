#ifndef STREAM_H
#define STREAM_H

#include "binary.h"
#include <QtCore/QObject>
#include <QtCore/QIODevice>
#include <QtCore/QDebug>

#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT 
#else
#define EXPORT 
#endif

class EXPORT Stream: public QObject
{
    friend class StreamPrototype;
    friend class TextStreamPrototype;
    friend class TextStream;

    Q_OBJECT

public:
    Stream(QIODevice *device, QObject *parent = 0);
    virtual ~Stream();

signals:
    void aboutToClose ();
    void bytesWritten ( qint64 bytes );
    void readStreamFinished ();
    void readyRead();

private:
    QIODevice *priv_;
};

#endif //STREAM_H
