#include "stream.h"

#include <QtCore/QIODevice>
#include "binary.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#define STREAM_DEBUG

Stream::Stream(QIODevice *device, QObject *parent)
    :QObject(parent)
{
    if (!device) {
        qFatal("Stream::Stream programatic error");
    }

    priv_ = device;

    QObject::connect(priv_,SIGNAL(aboutToClose()),
                     this, SIGNAL(aboutToClose()));
    
    QObject::connect(priv_,SIGNAL(bytesWritten(qint64)),
                     this, SIGNAL(bytesWritten(qint64)));
    
    QObject::connect(priv_,SIGNAL(readChannelFinished()),
                     this, SIGNAL(readStreamFinished()));
    
    QObject::connect(priv_,    SIGNAL(readyRead()),
                     this, SIGNAL(readyRead()));
}

Stream::~Stream()
{
#ifndef STREAM_DEBUG
    qDebug()<<"Stream::~Stream()";
#endif

    if (priv_)
        delete priv_;
}
