#include "filesystem.h"
#include "textstream.h"
#include "textstreamprototype.h"
#include "stream.h"
#include "streamprototype.h"

#include <QtScript/QScriptValueIterator>

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QDir>

#ifdef _GUARDIAN_TARGET
#include <guardian/gfsfileengine.h>

#include <cextdecs.h>
#include <tal.h>
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(QIODevice*)

typedef struct s_file_options_ {
    s_file_options_() : read(false),
                        write(false),
                        append(false),
                        update(false),
                        binary(false),
                        exclusive(false),
                        structured(false),
                        canonical(false),
                        charset(""){}
    bool read;
    bool write;
    bool append;
    bool update;
    bool binary;
    bool exclusive;
    bool structured;
    bool canonical;
    QString charset;
} s_file_options_;

static QScriptValue
file_open(QScriptContext *ctx, 
          QScriptEngine *eng, 
          QString fn,
          s_file_options_ &opt)
{
    unsigned io_opt=0;
    if (opt.read)    io_opt |= QIODevice::ReadOnly;
    if (opt.write)   io_opt |= QIODevice::WriteOnly;
    if (opt.append)  io_opt |= QIODevice::Append;
#ifdef _GUARDIAN_TARGET
    if (opt.exclusive)  io_opt |= 0x80;
    if (opt.structured) io_opt |= 0x100;
    else if (opt.update)  io_opt |= 0x40;
#els
    if (opt.update)  io_opt |= 0x40;
#endif

    if (opt.binary == false)  io_opt |= QIODevice::Text;
    if ( opt.canonical ||
#ifndef _GUARDIAN_TARGET
         opt.update     ||
         opt.exclusive  ||
         opt.structured ||
#endif
         !opt.charset.isEmpty()) {

        qWarning("<fs.open> option not supported on this platform");
    }

    QFile *file = new QFile(fn);

    if (!file->open((QIODevice::OpenModeFlag)io_opt)) {
        ctx->throwError(QString("<fs.open>:")+file->errorString());
        delete(file);
        return QScriptValue();
    }


    Q_ASSERT(dynamic_cast<QIODevice*>(file));
    QScriptValue object;


    if (!opt.binary) {
        Stream *s_ptr = new Stream(dynamic_cast<QIODevice*>(file));

        
        return eng->newQObject(new TextStream(s_ptr), QScriptEngine::ScriptOwnership,
                               QScriptEngine::SkipMethodsInEnumeration
                               /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                               /*| QScriptEngine::ExcludeSuperClassProperties*/);
        
    }


    object =  eng->newQObject(new Stream(dynamic_cast<QIODevice*>(file)), QScriptEngine::ScriptOwnership,
                              QScriptEngine::SkipMethodsInEnumeration
                              /*| ,QScriptEngine::ExcludeSuperClassMethods(*/
                              /*| QScriptEngine::ExcludeSuperClassProperties*/);
    return object;
}

QScriptValue 
__fs_move(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    QString newPath = ctx->argument(1).toString();
    if(path.isEmpty() || ctx->argumentCount() != 2) {
        ctx->throwError("<fs.move> invalid arguments");
        return QScriptValue();
    }
    return QFile(path).rename(newPath);
}

QScriptValue 
__fs_open(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 2) {
        ctx->throwError("<fs.open> requires two arguments:path, mode|options");
        return QScriptValue();
    }
    
    QScriptValue sv_path = ctx->argument(0);
    QString file_name = sv_path.toString();

    QScriptValue sv_mode = ctx->argument(1);

    s_file_options_ fo;

    //
    //  Checking options
    //
    if (sv_mode.isString()) {
        QString mode = sv_mode.toString();

        if (mode.contains("r")) fo.read = true;
        if (mode.contains("w")) fo.write = true;
        if (mode.contains("a")) fo.append = true;
        if (mode.contains("b")) fo.binary = true;
        if (mode.contains("+")) fo.update = true;
        if (mode.contains("x")) fo.exclusive = true;
        if (mode.contains("s")) fo.structured = true;
        if (mode.contains("c")) fo.canonical = true;
    } else if (sv_mode.isObject()){
        QScriptValueIterator it(sv_mode);
        while (it.hasNext()) {
            it.next();
            QString name = it.name();
            if (name == "read") fo.read = it.value().toBool();
            else if (name == "write") fo.write = it.value().toBool();
            else if (name == "append") fo.append = it.value().toBool();
            else if (name == "update") fo.update = it.value().toBool();
            else if (name == "binary") fo.binary = it.value().toBool();
            else if (name == "exclusive") fo.exclusive = it.value().toBool();
            else if (name == "structured") fo.structured = it.value().toBool();
            else if (name == "canonical") fo.canonical = it.value().toBool();
            else if (name == "charset") fo.charset = it.value().toString();
            else {
                ctx->throwError("<fs.open> invalid option:"+name);
                return QScriptValue();
            }
        }
    } else {
        Q_ASSERT(0);
    }

    QScriptValue ret;

    ret = file_open(ctx, eng, file_name, fo);

    return ret;
}

QScriptValue 
__fs_remove(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isDirectory> requires one argument");
        return QScriptValue();
    }
    return QFile(path).remove();
}

QScriptValue 
__fs_touch(QScriptContext *ctx, QScriptEngine *eng)
{
    ctx->throwError("<fs.touch> Not implemented!");
    return false;
}

// Tests
QScriptValue 
__fs_exists(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.exists> requires one argument");
        return QScriptValue();
    }
    return QFileInfo(path).exists();
}

QScriptValue 
__fs_isFile(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isFile> requires one argument");
        return QScriptValue();
    }
    return QFileInfo(path).isFile();
}

QScriptValue 
__fs_isDirectory(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isDirectory> requires one argument");
        return QScriptValue();
    }

#if defined(_GUARDIAN_TARGET)
    return QScriptValue();
#endif
    return QFileInfo(path).isDir();
}

QScriptValue 
__fs_isLink(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isLink> requires one argument");
        return QScriptValue();
    }
#if defined(_GUARDIAN_TARGET)
    return QScriptValue();
#endif
    return QFileInfo(path).isSymLink();
}

QScriptValue 
__fs_isReadable(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isReadable> requires one argument");
        return QScriptValue();
    }
    return QFileInfo(path).isReadable();
}

QScriptValue 
__fs_isWritable(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.isWritable> requires one argument");
        return QScriptValue();
    }
    return QFileInfo(path).isWritable();
}

QScriptValue 
__fs_same(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.same> requires one argument");
        return QScriptValue();
    }
    ctx->throwError("<fs.same> Not implemented!");
    return false;
}

//Paths as text
QScriptValue 
__fs_absolute(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.absolute> requires one argument");
        return QScriptValue();
    }
    
    return QFileInfo(path).absoluteFilePath();
}

QScriptValue 
__fs_base(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.base> requires one argument");
        return QScriptValue();
    }

    return QFileInfo(path).baseName();
}

//attrributes
QScriptValue
__fs_lastModified(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.lastModified> requires one argument");
        return QScriptValue();
    }

// #ifdef _GUARDIAN_TARGET
//     short in[1];
//     in[0] = 54; //Aggregate modification time

//     long long  jts = 0;
//     short  error = 0;
    
//     short ts[8];
    
//     qDebug()<<"file is:"<<path.toLatin1().data();
//     // error = FILE_GETINFOLISTBYNAME_(path.toLatin1().data(),
//     //                                 path.toLatin1().length(),
//     //                                 in,
//     //                                 1,
//     //                                 (short*)&ts, sizeof(long long));

//     // if (error < 0) {
//     //     qWarning("error in __fs_lastModified");
//     //     return QScriptValue();
//     // }
    
//     qDebug()<<"jts:"<<jts;
//     // INTERPRETTIMESTAMP(jts, (short*)ts);
//     // qDebug()<<"[0]"<<ts[0]<<"[1]"<<ts[1];
//     return QScriptValue();
// #else
    return eng->newDate(QFileInfo(path).lastModified());
//#endif
}

QScriptValue
__fs_lastRead(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.lastRead> requires one argument");
        return QScriptValue();
    }
    return eng->newDate(QFileInfo(path).lastRead());
}

QScriptValue
__fs_size(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.size> requires one argument");
        return QScriptValue();
    }
    return (qsreal)(QFileInfo(path).size());
}

//listing
QScriptValue
__fs_list(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<fs.list> requires one argument");
        return QScriptValue();
    }

    QStringList list;
#if defined(_GUARDIAN_TARGET)
    path += ".*";

    short sid;
    short error = FILENAME_FINDSTART_(&sid,
                                      path.toLatin1().data(),
                                      path.length());
    if (error != 0) {
        return QScriptValue();
    }
    char name[256];
    short name_len;
    while ( error = FILENAME_FINDNEXT_(sid,
                                       name,
                                       256,
                                       &name_len) == 0 ) {
        list.push_back(QByteArray((const char*)name, name_len));
    }
    FILENAME_FINDFINISH_(sid);
#else
    QStringList filters;
    filters << "*";
    list = QFileInfo(path).dir().entryList(filters);
#endif
    QScriptValue ret = eng->newArray(list.length());
    for (int i=0; i< list.length(); i++) {
        ret.setProperty(i,list.at(i));
    }
    return ret;
}
