#include "process.h"
#include "processprototype.h"

#include <QtCore/QDebug>
#include <QtCore/QProcess>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Process*)
Q_DECLARE_METATYPE(QProcess::ExitStatus)
Q_DECLARE_METATYPE(QProcess::ProcessError)

ProcessPrototype::ProcessPrototype(QObject *parent)
: StreamPrototype(parent)
{
    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");
    qRegisterMetaType<QProcess::ProcessError>("QProcess::ProcessError");
}

ProcessPrototype::~ProcessPrototype()
{

}

void
ProcessPrototype::start(QString program, QStringList arguments) const
{
    thisProcess()->start(program, arguments);
}

void
ProcessPrototype::stop() const
{
    thisProcess()->terminate();
}


#ifndef _GUARDIAN_TARGET
ByteString 
ProcessPrototype::readAllStandardError()
{
    return thisProcess()->readAllStandardError();
}

ByteString
ProcessPrototype::readAllStandardOutput()
{
    return thisProcess()->readAllStandardOutput();
}
#endif

QProcess *
ProcessPrototype::thisProcess() const
{
    Process *p = qscriptvalue_cast<Process*>(thisObject());
    if (!p) {
        qFatal("Programmatic error ProcessPrototype::thisProcess");
    }

    return p->priv_;
}

QIODevice *
ProcessPrototype::thisIODevice()
{
    Process *p = qscriptvalue_cast<Process*>(thisObject());
    if (!p) {
        qFatal("Programmatic error ProcessPrototype::thisIODevice");
    }

    QIODevice *io = dynamic_cast<QIODevice*>(p->priv_);
    if (!io) {
        qFatal("Programmatic error TcpSocketPrototype::thisIODevice");
    }

    return io;
}
