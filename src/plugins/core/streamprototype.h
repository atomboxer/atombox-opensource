#ifndef STREAMPROTOTYPE_H
#define STREAMPROTOTYPE_H

#include <QtCore/QDataStream>
#include <QtCore/QHash>
#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "binary.h"
#include "stream.h"

#ifdef QT_PLUGIN
#define EXPORT Q_DECL_EXPORT 
#else
#define EXPORT 
#endif
class EXPORT StreamPrototype : public QObject, public QScriptable
{
    Q_OBJECT
    Q_PROPERTY(quint32      position READ __internal__getPosition WRITE __internal__setPosition )
    Q_PROPERTY(QScriptValue key      READ getKey                  WRITE setKey )
public:
    StreamPrototype(QObject *parent = 0);
    ~StreamPrototype();

public slots:
    virtual void  close();
    virtual void  copy(QIODevice *io);
    virtual void  flush();
    virtual bool  atEnd();
    virtual bool  isClosed();
    virtual bool  isReadable();
    virtual bool  isSequential();
    virtual bool  isWritable();
    virtual QScriptValue read(unsigned bytes = 4096);
    virtual bool lockRecord();
    virtual bool unlockRecord();
    virtual QScriptValue readInto(QScriptValue buffer, 
                                  QScriptValue begin=QScriptValue(),
                                  QScriptValue end = QScriptValue() );

    virtual bool  skip(int bytes);
    virtual void  write(QScriptValue binary, QScriptValue begin=QScriptValue(), 
                        QScriptValue end = QScriptValue() );

    virtual void   __internal__setPosition( QScriptValue position );
    virtual qint64 __internal__getPosition();

public slots:
    virtual qint64 bytesAvailable();

    virtual bool setKey(QScriptValue key, short spec = 0);
    virtual QScriptValue getKey();

private:
    virtual QString errorString();
    virtual QIODevice*   thisIODevice();
};

#endif //STREAMPROTOTYPE_H
