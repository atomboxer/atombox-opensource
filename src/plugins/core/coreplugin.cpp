
#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QProcess>
#include <QtCore/QProcessEnvironment>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtNetwork/QHostInfo> //hostname

#include "bytearrayclass.h"
#include "bytestringclass.h"

#include "scriptengineapp.h"

#include "console.h"
#include "filesystem.h"      // fs. functions
#include "system.h"

#include "streamprototype.h"
#include "timerprototype.h"
#include "textstream.h"
#include "textstreamprototype.h"
#include "coreplugin.h"
#include "process.h"
#include "processprototype.h"

#ifdef _GUARDIAN_TARGET
#include <cextdecs.h>
#include <tal.h>
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscriptcore_compat, CorePlugin )
#else
Q_EXPORT_PLUGIN2( abscriptcore, CorePlugin )
#endif

Q_DECLARE_METATYPE(Stream*)
Q_DECLARE_METATYPE(TextStream*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)
Q_DECLARE_METATYPE(Timer*)
Q_DECLARE_METATYPE(Process*)


CorePlugin::CorePlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
    
CorePlugin::~CorePlugin()
{
}
 
QScriptValue 
Stream_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    QScriptValue object;
 
    Stream *s_ptr = qscriptvalue_cast<Stream*>(ctx->argument(0));
    if (!s_ptr) {
        //
        //  Maybe is a ByteArray class
        //
        QByteArray *b = NULL;
        b = qscriptvalue_cast<ByteArray*>(ctx->argument(0).data());
        if (!b) {
            b = qscriptvalue_cast<ByteString*>(ctx->argument(0).data());
        }
        
        if (!b) {
            ctx->throwError(QScriptContext::TypeError, "Invalid argument passed to Stream constructor");
            return QScriptValue();
        }

        QBuffer *b_ptr = new QBuffer(b);
        Stream *stream_ptr = new Stream(b_ptr);
        b_ptr->open(QIODevice::ReadWrite);

        object =  eng->newQObject(stream_ptr, QScriptEngine::ScriptOwnership,
                                  QScriptEngine::SkipMethodsInEnumeration
                                  /*| QScriptEngine::ExcludeSuperClassMethods(*/
                                  /*| QScriptEngine::ExcludeSuperClassProperties*/);
        return object;
    }
    
    object =  eng->newQObject(s_ptr, QScriptEngine::ScriptOwnership,
                              QScriptEngine::SkipMethodsInEnumeration
                              /*| QScriptEngine::ExcludeSuperClassMethods(*/
                              /*| QScriptEngine::ExcludeSuperClassProperties*/);
    
    return object;
}

QScriptValue 
Process_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
  
    QProcess *process = new QProcess();
#ifdef _GUARDIAN_TARGET
    QScriptValue options = ctx->argument(0);
    QScriptValueIterator it(options);

    QString guardian_hometerm = "";
    QString guardian_in_file = "";
    QString guardian_out_file = "";
    short   guardian_cpu = -1;
    short   guardian_priority = -1;
    QString guardian_lib = "";
    QString guardian_name = "";

    while (it.hasNext()) {
        it.next();
        bool ok = true;
        if (it.name() == "hometerm") {
            guardian_hometerm = it.value().toString();
            
            char  fname[256];
            short fname_len;
            short err = FILENAME_RESOLVE_ (guardian_hometerm.toLatin1().data(),
                                           guardian_hometerm.size(),
                                           fname,
                                           256,
                                           &fname_len);
            if (err != 0) {
                ctx->throwError("Process constructor. Hometerm does not exist or is invalid");
                return QScriptValue::UndefinedValue;
            }
            guardian_hometerm = QString(QByteArray(fname, fname_len));
        } else if (it.name() == "in") {
            guardian_in_file = it.value().toString();
            
            char  fname[256];
            short fname_len;
            short err = FILENAME_RESOLVE_ (guardian_in_file.toLatin1().data(),
                                           guardian_in_file.size(),
                                           fname,
                                           256,
                                           &fname_len);
            if (err != 0) {
                ctx->throwError("Process constructor. In file does not exist or is invalid");
                return QScriptValue::UndefinedValue;
            }
            guardian_in_file = QString(QByteArray(fname, fname_len));
        } else if (it.name() == "out") {
            guardian_out_file = it.value().toString();
            
            char  fname[256];
            short fname_len;
            short err = FILENAME_RESOLVE_ (guardian_out_file.toLatin1().data(),
                                           guardian_out_file.size(),
                                           fname,
                                           256,
                                           &fname_len);
            if (err != 0) {
                ctx->throwError("Process constructor.Out file does not exist or is invalid");
                return QScriptValue::UndefinedValue;
            }
            guardian_out_file = QString(QByteArray(fname, fname_len));
        } else if (it.name() == "cpu") {
            guardian_cpu = it.value().toUInt32();
        } else if (it.name() == "priority") {
            guardian_priority = it.value().toUInt32();
        } else if (it.name() == "lib") {
            guardian_lib = it.value().toString();
            
            char  fname[256];
            short fname_len;
            short err = FILENAME_RESOLVE_ (guardian_lib.toLatin1().data(),
                                           guardian_lib.size(),
                                           fname,
                                           256,
                                           &fname_len);
            if (err != 0) {
                ctx->throwError("Process constructor.lib does not exist or is invalid");
                return QScriptValue::UndefinedValue;
            }
            guardian_lib = QString(QByteArray(fname, fname_len));
        }  else if (it.name() == "name") {
            guardian_name = it.value().toString();
        } else {
            ctx->throwError("Process constructor. Invalid Guardian process options.");
            return QScriptValue::UndefinedValue;
        }
    }

    process->setGuardianOptions(guardian_hometerm, 
                                guardian_in_file, 
                                guardian_out_file, 
                                guardian_cpu, 
                                guardian_priority,
                                guardian_lib,
                                guardian_name
        );
#endif
    return eng->newQObject(new Process(process), QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue 
TextStream_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    Stream *s_ptr = qscriptvalue_cast<Stream*>(ctx->argument(0));

    if (!s_ptr) {
        ctx->throwError("TextStream constructor, argument(0) not instanceof Stream");
        return QScriptValue();
    }

    QScriptValue options = ctx->argument(1);

    QString c_charset   = options.property("charset").toString();
    QString c_newline   = options.property("newline").toString();
    QString c_delimiter = options.property("delimiter").toString();

    if (!options.isUndefined () &&
        c_charset.isEmpty() &&
        c_newline.isEmpty() &&
        c_delimiter.isEmpty()) {

        ctx->throwError("TextStream constructor: unknown option passed");
        return QScriptValue();
    }

    return eng->newQObject(s_ptr, QScriptEngine::ScriptOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue 
Timer_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    QScriptValue object;
    
    object =  eng->newQObject(new Timer(), QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration
                              /*| QScriptEngine::ExcludeSuperClassMethods(*/
                              /*| QScriptEngine::ExcludeSuperClassProperties*/);
    return object;    
}

    
QScriptValue
_defineProperty(QScriptContext *context, QScriptEngine *engine)
{
    if (context->argumentCount() != 3)
        return QScriptValue();

    QScriptValue obj  = context->argument(0);       //object
    QScriptValue prop = context->argument(1);       //name
    QScriptValue desc = context->argument(2);       //descriptor prop
    
    QScriptValue desc_value    = desc.property("value");
    QScriptValue desc_writable = desc.property("writable");
    QScriptValue desc_get      = desc.property("get");
    QScriptValue desc_set      = desc.property("set");
    QScriptValue desc_configurable      = desc.property("configurable");
    QScriptValue desc_enumerable        = desc.property("enumerable");

    //A data descriptor is a property that has a value
    //Accessor descriptor is a property described by a getter-setter 
    //pair of functions. It CANNOT BE BOTH

    int flg = 0;
    if (!desc_configurable.toBool()) {
        flg |= QScriptValue::Undeletable;
    }
    if (!desc_enumerable.toBool()) {
        flg |= QScriptValue::SkipInEnumeration;
    }

    if (!desc_value.isUndefined()) {
        //
        //  data descriptor
        //
        if (!desc_writable.toBool()) {
            flg |= QScriptValue::ReadOnly;
        }
    
        obj.setProperty(prop.toString(), 
                        desc_value, 
                        (QScriptValue::PropertyFlag)flg);
    } else if (!desc_get.isUndefined() || !desc_get.isUndefined()) {
        if (!desc_get.isUndefined()) {
            obj.setProperty(prop.toString(), 
                            desc_get, 
                            (QScriptValue::PropertyFlag)flg 
                            | QScriptValue::PropertyGetter);
        }
        if (!desc_set.isUndefined()) {
            obj.setProperty(prop.toString(), 
                            desc_set, 
                            (QScriptValue::PropertyFlag)flg 
                            | QScriptValue::PropertySetter);
        }
    } else {
        context->throwError("invalid argument in Object.defineProperty");
    }
 
    return QScriptValue();
}

void
CorePlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("core_compat") ) {
#else
        if ( key == QString("core") ) {
#endif

#ifdef AB_STATIC_PLUGINS
            Q_INIT_RESOURCE(core);
#endif

#ifndef AB_INCLUDE_TESTS
            Q_INIT_RESOURCE(jsfiles);
#else
            Q_INIT_RESOURCE(jsfiles_wtests);
#endif

            ScriptEngineApp *scApp = static_cast<ScriptEngineApp*>(engine);
            Q_ASSERT(scApp);

            engine->globalObject().setProperty("__internal__", engine->newObject());
            engine->globalObject().property("Object").setProperty("defineProperty",
                                                                  engine->newFunction(_defineProperty),
                                                                  QScriptValue::ReadOnly
                                                                  |QScriptValue::Undeletable
                                                                  |QScriptValue::SkipInEnumeration);

            //
            //  binary
            //
            ByteArrayClass  *baClass = new ByteArrayClass(engine);
            engine->globalObject().property("__internal__").
                setProperty("ByteArray", baClass->constructor());

            ByteStringClass *bsClass = new ByteStringClass(engine);
            engine->globalObject().property("__internal__").
                setProperty("ByteString", bsClass->constructor());

            //  fs
            engine->globalObject().property("__internal__").
                setProperty("move", engine->newFunction(__fs_move));
            engine->globalObject().property("__internal__").
                setProperty("open", engine->newFunction(__fs_open));
            engine->globalObject().property("__internal__").
                setProperty("remove", engine->newFunction(__fs_remove));
            engine->globalObject().property("__internal__").
                setProperty("touch", engine->newFunction(__fs_touch));


            // fs tests
            engine->globalObject().property("__internal__").
                setProperty("exists", engine->newFunction(__fs_exists));
            engine->globalObject().property("__internal__").
                setProperty("isFile", engine->newFunction(__fs_isFile));
            engine->globalObject().property("__internal__").
                setProperty("isDirectory", engine->newFunction(__fs_isDirectory));
            engine->globalObject().property("__internal__").
                setProperty("isLink", engine->newFunction(__fs_isLink));
            engine->globalObject().property("__internal__").
                setProperty("isReadable", engine->newFunction(__fs_isReadable));
            engine->globalObject().property("__internal__").
                setProperty("isWritable", engine->newFunction(__fs_isWritable));
            engine->globalObject().property("__internal__").
                setProperty("same", engine->newFunction(__fs_same));

            //fs paths as text
            engine->globalObject().property("__internal__").
                setProperty("absolute", engine->newFunction(__fs_absolute));
            engine->globalObject().property("__internal__").
                setProperty("base", engine->newFunction(__fs_base));

            // fs attributes
            engine->globalObject().property("__internal__").
                setProperty("lastModified", engine->newFunction(__fs_lastModified));
            engine->globalObject().property("__internal__").
                setProperty("lastRead", engine->newFunction(__fs_lastRead));
            engine->globalObject().property("__internal__").
                setProperty("size", engine->newFunction(__fs_size));
    
            //fs listing
            engine->globalObject().property("__internal__").
                setProperty("list", engine->newFunction(__fs_list));

            // io
        
            engine->setDefaultPrototype(qMetaTypeId<Stream*>(),
                                        engine->newQObject(new StreamPrototype(this)));

            engine->globalObject().property("__internal__").
                setProperty("Stream", engine->newFunction(Stream_Constructor));

            engine->globalObject().property("__internal__").property("Stream").
                setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Stream*>()));

            engine->setDefaultPrototype(qMetaTypeId<TextStream*>(),
                                        engine->newQObject(new TextStreamPrototype(this)));

            engine->globalObject().property("__internal__").
                setProperty("TextStream", engine->newFunction(TextStream_Constructor));

            engine->globalObject().property("__internal__").property("TextStream").
                setProperty("prototype", engine->defaultPrototype(qMetaTypeId<TextStream*>()));

            engine->setDefaultPrototype(qMetaTypeId<Timer*>(),
                                        engine->newQObject(new TimerPrototype(this)));

            engine->globalObject().property("__internal__").
                setProperty("Timer", engine->newFunction(Timer_Constructor));

            engine->globalObject().property("__internal__").property("Timer").
                setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Timer*>()));


            //  system
            QFile *file;

            //system.stdin
            file = new QFile();
            if (!file->open(stdin,QIODevice::ReadOnly)) {
                engine->currentContext()->throwError(QString("<creating stdin>:")+file->errorString());
                delete(file);
            }

            Stream *s_ptr = new Stream(dynamic_cast<QIODevice*>(file));
            engine->newQObject(s_ptr, QScriptEngine::QtOwnership,
                               QScriptEngine::SkipMethodsInEnumeration);
    
            QScriptValue s_stdin = engine->newQObject(
                new TextStream(s_ptr), QScriptEngine::QtOwnership,
                QScriptEngine::SkipMethodsInEnumeration);

            engine->globalObject().property("__internal__").
                setProperty("stdin", s_stdin);

            //system.stdout

            file = new QFile();
            if (!file->open(stdout,QIODevice::WriteOnly)) {
                engine->currentContext()->throwError(QString("<creating stdout>:")+file->errorString());
                delete(file);
            }
    
            s_ptr = new Stream(dynamic_cast<QIODevice*>(file));
            engine->newQObject(s_ptr, QScriptEngine::QtOwnership,
                               QScriptEngine::SkipMethodsInEnumeration);
    
            QScriptValue s_stdout = engine->newQObject(
                new TextStream(s_ptr), QScriptEngine::QtOwnership,
                QScriptEngine::SkipMethodsInEnumeration);

            engine->globalObject().property("__internal__").
                setProperty("stdout", s_stdout);


            //system.stderr
            file = new QFile();
            if (!file->open(stderr,QIODevice::WriteOnly)) {
                engine->currentContext()->throwError(QString("<creating stderr>:")+file->errorString());
                delete(file);
            }

            s_ptr = new Stream(dynamic_cast<QIODevice*>(file));
            engine->newQObject(s_ptr, QScriptEngine::QtOwnership,
                               QScriptEngine::SkipMethodsInEnumeration);
    
            QScriptValue s_stderr = engine->newQObject(
                new TextStream(s_ptr), QScriptEngine::QtOwnership,
                QScriptEngine::SkipMethodsInEnumeration);

            engine->globalObject().property("__internal__").
                setProperty("stderr", s_stderr);


            //system.env
            QProcessEnvironment envp = QProcessEnvironment::systemEnvironment();
            QScriptValue env_arr = engine->newArray();
            int i = 0;
            foreach (QString str, envp.toStringList()) {
                env_arr.setProperty(i++,str);
            }

            engine->globalObject().property("__internal__").
                setProperty("env", env_arr);

            //system.args
            QScriptValue args_arr = engine->newArray();
            i = 0;

            QList<QString> args = QCoreApplication::arguments();

            bool start = false;
            for (int j = 0; j < args.length(); j++) {
                if (!start && QString::compare(QString(args[j]), scApp->mainFile(), Qt::CaseInsensitive) == 0) {
                    start = true;
                    continue;
                }
                if (start)
                    args_arr.setProperty(i++,args[j]);
            }

            engine->globalObject().property("__internal__").
                setProperty("args", args_arr);

            //system.exit
            engine->globalObject().property("__internal__").
                setProperty("exit", engine->newFunction(__system_exit));

            //system.sleep
            engine->globalObject().property("__internal__").
                setProperty("sleep", engine->newFunction(__system_sleep));

    
            engine->setDefaultPrototype(qMetaTypeId<Process*>(),
                                        engine->newQObject(new ProcessPrototype(this)));

            engine->globalObject().property("__internal__").
                setProperty("Process", engine->newFunction(Process_Constructor));

            engine->globalObject().property("__internal__").property("Process").
                setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Process*>()));

            //system.platform
            QString platform("Unknown");
#if defined( _GUARDIAN_TARGET )
            platform = "guardian";
#elif defined( __TANDEM )
            platform = "oss";
#elif defined(Q_WS_WIN)
            platform = "windows";
#elif defined(Q_WS_MAC)
            platform = "mac";
#elif defined(Q_OS_UNIX)
            platform = "unix";
#endif
            engine->globalObject().property("__internal__").
                setProperty("gid", __system_gid());

            engine->globalObject().property("__internal__").
                setProperty("uid", __system_uid());

            engine->globalObject().property("__internal__").
                setProperty("pid", __system_pid());

            engine->globalObject().property("__internal__").
                setProperty("login", __system_loginName());

            engine->globalObject().property("__internal__").
                setProperty("processStats", engine->newFunction(__system_processStats));

            engine->globalObject().property("__internal__").
                setProperty("platform", platform);


            engine->globalObject().property("__internal__").
                setProperty("hostname", QHostInfo::localHostName());

            //console.assert
            engine->globalObject().property("__internal__").
                setProperty("__console_assertHelper", 
                            engine->newFunction(__console_assertHelper));

        }
    }
    
    QStringList
        CorePlugin::keys() const
    {
        QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
        keys << QString("core_compat");
#else
        keys << QString("core");
#endif
        return keys;
    }
