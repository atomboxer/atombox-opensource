include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscriptcore
} else {
    TARGET         = abscriptcore_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}


DEPENDPATH += .
INCLUDEPATH += .

HEADERS  = \
           bytearrayclass.h \
           bytearrayprototype.h \
           bytestringclass.h \
           bytestringprototype.h \
           console.h \
           coreplugin.h \
           filesystem.h \
           stream.h \
           streamprototype.h \
           system.h \
           textstream.h \
           textstreamprototype.h \
           timer.h \
           timerprototype.h \
           process.h \
           processprototype.h \
           cityhash\city.h \
           cityhash\citycrc.h \
           cityhash\config.h
           

SOURCES  = \
           bytearrayclass.cpp \
           bytearrayprototype.cpp \
           bytestringclass.cpp \
           bytestringprototype.cpp \
           console.cpp \
           coreplugin.cpp \
           filesystem.cpp \
           stream.cpp \
           streamprototype.cpp \
           system.cpp \
           textstream.cpp \
           textstreamprototype.cpp \
           timer.cpp \
           timerprototype.cpp \
           process.cpp \
           processprototype.cpp \
           cityhash\\city.cc


!isEmpty(ATOMBOXER_INCLUDE_TESTS_SUNSPIDER) {
    RESOURCES += jsfiles_wtests.qrc
} else {
    RESOURCES += jsfiles.qrc
}


win32:!tandem {
    LIBS += -lcrypt32 -lgdi32 -lpsapi
}

static {
    RESOURCES += core.qrc
}
else {
    isEmpty(USE_SCRIPT_CLASSIC) {
        QT += script
    }
    QT += network
    CONFIG   += qt plugin
}
