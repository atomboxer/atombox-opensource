#ifndef SYSTEM_H
#define SYSTEM_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

//system.exit
QScriptValue __system_exit(QScriptContext *, QScriptEngine *);

//system.sleep
QScriptValue __system_sleep(QScriptContext *, QScriptEngine *);

//system.gid
QString __system_gid();

//system.uid
QString __system_uid();

//system.pid
QString __system_pid();

//system.loginName()
QString __system_loginName();

//system.processStats()
QScriptValue __system_processStats(QScriptContext *, QScriptEngine *);

#endif
