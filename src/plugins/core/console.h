#ifndef CONSOLE_H
#define CONSOLE_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

//help not to mess the stack
QScriptValue __console_assertHelper(QScriptContext *, QScriptEngine *);


#endif //CONSOLE_H
