#ifndef COREPLUGIN_H
#define COREPLUGIN_H

#include <QScriptExtensionPlugin>

class CorePlugin : public QScriptExtensionPlugin
{
public:
    CorePlugin( QObject *parent = 0);
    ~CorePlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //COREPLUGIN_H
