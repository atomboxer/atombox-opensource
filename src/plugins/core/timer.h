#ifndef TIMER_H
#define TIMER_H


#include <QtCore/QObject>
#include <QtCore/QTimer>

class Timer : public QObject
{
    Q_OBJECT

public:
    Timer( QObject *parent = 0);
    virtual ~Timer();
    int interval() const { return timer_.interval(); }
    bool isActive() const { return timer_.isActive(); }
    bool isSingleShot() const { return timer_.isSingleShot(); }
    void setInterval( int sec ){ timer_.setInterval( 1000*sec ); } 
    void setSingleShot( bool singleShot ){ timer_.setSingleShot( singleShot ); } 

    // slots
public slots:
    void start(int sec) { timer_.start(1000*sec); }
    void start() { timer_.start(); }
    void stop() { timer_.stop(); }

signals:
    void timeout();

private:
    QTimer& timer() const { return timer_; }
    mutable QTimer timer_;
};

#endif // timer.h 
