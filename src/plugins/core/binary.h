#ifndef BINARY_H
#define BINARY_H

#include <QtCore/QByteArray>
#include <QtCore/QDebug>

class ByteArray  : public QByteArray
{
public:
    ByteArray():QByteArray () {}
    virtual ~ByteArray() { }
    ByteArray ( const char * str ) : QByteArray (str ){}
    ByteArray ( const char * data, int size ):QByteArray ( data, size ){}
    ByteArray ( int size, char ch ):QByteArray(size,ch) {}
    ByteArray ( const QByteArray & other ) : QByteArray(other) {}
};

class ByteString : public QByteArray
{
public:
    ByteString():QByteArray () {}
    virtual ~ByteString() {}
    ByteString ( const char * str ) : QByteArray (str ){}
    ByteString ( const char * data, int size ):QByteArray ( data, size ){}
    ByteString ( int size, char ch ):QByteArray(size,ch) {}
    ByteString ( const QByteArray & other ) : QByteArray(other) {}

};

#endif //BINARY_H
