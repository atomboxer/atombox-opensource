#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

//files
QScriptValue __fs_move(QScriptContext *, QScriptEngine *);
QScriptValue __fs_open(QScriptContext *, QScriptEngine *);
QScriptValue __fs_remove(QScriptContext *, QScriptEngine *);
QScriptValue __fs_touch(QScriptContext *, QScriptEngine *);

//tests
QScriptValue __fs_exists(QScriptContext *, QScriptEngine *);
QScriptValue __fs_isFile(QScriptContext *, QScriptEngine *);
QScriptValue __fs_isDirectory(QScriptContext *, QScriptEngine *);
QScriptValue __fs_isLink(QScriptContext *, QScriptEngine *);
QScriptValue __fs_isReadable(QScriptContext *, QScriptEngine *);
QScriptValue __fs_isWritable(QScriptContext *, QScriptEngine *);
QScriptValue __fs_same(QScriptContext *, QScriptEngine *);

//paths as text
QScriptValue __fs_absolute(QScriptContext *, QScriptEngine *);
QScriptValue __fs_base(QScriptContext *, QScriptEngine *);

//attrributes
QScriptValue __fs_lastModified(QScriptContext *, QScriptEngine *);
QScriptValue __fs_lastRead(QScriptContext *, QScriptEngine *);
QScriptValue __fs_size(QScriptContext *, QScriptEngine *);

//listing
QScriptValue __fs_list(QScriptContext *, QScriptEngine *);

#endif //FILESYSTEM_H
