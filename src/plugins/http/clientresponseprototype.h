#ifndef CLIENTRESPONSEPROTOTYPE_H
#define CLIENTRESPONSEPROTOTYPE_H

#include "clientresponse.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class ClientResponsePrototype : public QObject, public QScriptable
{
    Q_OBJECT

    Q_PROPERTY(QScriptValue headers     READ headers)
    Q_PROPERTY(QScriptValue status      READ status)

public:
    ClientResponsePrototype(QObject *parent = 0);
    ~ClientResponsePrototype();

public slots:
    /* bool         write(QScriptValue chunk) const; */
    /* bool         end  (QScriptValue chunk=QScriptValue()) const; */
    QScriptValue headers() const;
    qint32       status() const;

private:
    ClientResponse *thisClientResponse() const;
};
#endif
