include(../plugins.pri)

isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET         = abscripthttp
} else {
    TARGET         = abscripthttp_compat
}

DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
!isEmpty(USE_SCRIPT_CLASSIC) {
    tandem {
        QT            -= core
    }
} else {
    QT            += core script
}

DEPENDPATH += .
INCLUDEPATH += .

HEADERS  = \
           httpplugin.h \
           server.h \
           serverprototype.h \
           serverrequest.h \
           serverrequestprototype.h \
           serverresponse.h \
           serverresponseprototype.h \
           clientrequest.h \
           clientrequestprototype.h\
           clientresponse.h \
           clientresponseprototype.h

SOURCES  = \
           httpplugin.cpp \
           server.cpp \
           serverprototype.cpp \
           serverrequest.cpp \
           serverrequestprototype.cpp \
           serverresponse.cpp \
           serverresponseprototype.cpp \
           clientrequest.cpp \
           clientrequestprototype.cpp \
           clientresponse.cpp \
           clientresponseprototype.cpp


RESOURCES += jshttpfiles.qrc

static {
    RESOURCES += http.qrc
}
else {
    CONFIG   += qt plugin
    QT       += network

    isEmpty(USE_SCRIPT_CLASSIC) {
         QT += script
         LIBS     +=-labscriptnet
    } else {
         LIBS     +=-labscriptnet_compat
    }
}

PRE_TARGETDEPS  = \
             $$ATOMBOXER_SOURCE_TREE/lib/libtufao.a
LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/ -ltufao

OTHER_FILES += \
    js/http.js
