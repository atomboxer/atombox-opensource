/**
 *  AtomBox `http` module provides a set of APIs for programming applications that use HTTP. The classes in this module allows you to write flexible web applications and services. The central point of this module is the HttpServer class that listens for incoming connections and handles the http requests/responses.


The {@link module:http/url} and {@link module:http/querystring} provide additional utilities for working with urls and query strings. This can be usefull for when you need to implement additional routing logic after the `requestReady` event.

 *  @module http
 *  @see {@link module:http/url}
 *  @see {@link module:http/querystring}
 *  @summary HTTP API
 *  @example

//
// The example below shows how HttpServer class can be used to process Ajax requests
//

//On the server side
var HttpServer = require("http").HttpServer;

var server = new HttpServer();
server.listen(8150);
print(server.isListening());

server.requestReady.connect(function(rq,rs) {
    var date = new Date().toString();
    var json_obj = { field:"hello ajax:"+date};
    rs.writeHeader(200,{ 'Content-Type': 'text/plain'});
    rs.end('callback_('+JSON.stringify(json_obj)+')');
});


//On the browser side:
//.....
//$.ajax({
//    url: 'http://127.0.0.1:8150/',
//    dataType: "jsonp",
//    jsonpCallback: "callback_",
//    cache: false,
//    timeout: 5000,
//    success: function(obj) {
//       document.write(obj.field); //the same field name as on the server
//    },
//    error: function(jqXHR, textStatus, errorThrown) {
//       document.write("ajax request error");
//    }});
//}
//.....
*/

/**
 * @class 
 * @name HttpServer
 * @classdesc The HttpServer class
 * @summary HttpServer class
 * @memberof module:http
 * @requires module:http
 * @example
//
//  Fortune web application
//
var http    = require("http");
var console = require("console");

var fortunes = ["You've been leading a dog's life. Stay off the furniture",
                "You've got to think about tomorrow",
                "You will be surprised by a loud noise",
                "You will feel hungry again in another hour",
                "You might have mail",
                "You cannot kill time without injuring eternity",
                "Computers are not intelligent. They only think they are"];



// Create a new instance of HttpServer
var server =  new http.HttpServer();

// Start listening on port 8150
server.listen(8150);
console.assert(server.isListening());

// Connect the requestReady signal
server.requestReady.connect(function(request,response) {
    //
    //  request  instanceof http.HttpServerRequest
    //  response instanceof http.HttpServerResponse
    //

    //write the header
    response.writeHeader(200,"bla bla",{ 'Content-Type': 'text/plain'});

    //write some data
    response.write("the fortune is:".toByteArray());

    //end the response with the fortune
    response.end(fortunes[Math.floor(Math.random()*7+1)].toByteArray());
});
 */

/**
 * This signal is emitted when the HttpServer got a new request
 * @event module:http.HttpServer.prototype.requestReady
 * @arg {HttpServerRequest} request The request object
 * @arg {HttpServerResponse} response The response object
 */

/**
 * Closes the stream, freeing the resources it is holding
 * @method module:http.HttpServer.prototype.close
 */

/**
 * Returns true if the server is currently listening for incomming connections; otherwise returns false;
 * @method module:http.HttpServer.prototype.isListening
 * @returns {Boolean} listening
 */

/**
 * Tells the server to listen for incomming connections on address and port specified
 * @method module:http.HttpServer.prototype.listen
 * @arg {Number} port The port on which to connect
 * @arg {String} host The host (or IP address) on which to bind the listening port. If ommited, it will bind the port on all the available interfaces
 * @returns {Boolean} success 
 */

/**
 * Returns the server's port if the server is listening for connections
 * @method module:http.HttpServer.prototype.port
 * @returns {Number} port
 */

exports.HttpServer =  __internal__.HttpServer;
exports.request    =  __internal__.request;

var STATUS_CODES = exports.STATUS_CODES = {
  100 : 'Continue',
  101 : 'Switching Protocols',
  102 : 'Processing',                 // RFC 2518, obsoleted by RFC 4918
  200 : 'OK',
  201 : 'Created',
  202 : 'Accepted',
  203 : 'Non-Authoritative Information',
  204 : 'No Content',
  205 : 'Reset Content',
  206 : 'Partial Content',
  207 : 'Multi-Status',               // RFC 4918
  300 : 'Multiple Choices',
  301 : 'Moved Permanently',
  302 : 'Moved Temporarily',
  303 : 'See Other',
  304 : 'Not Modified',
  305 : 'Use Proxy',
  307 : 'Temporary Redirect',
  400 : 'Bad Request',
  401 : 'Unauthorized',
  402 : 'Payment Required',
  403 : 'Forbidden',
  404 : 'Not Found',
  405 : 'Method Not Allowed',
  406 : 'Not Acceptable',
  407 : 'Proxy Authentication Required',
  408 : 'Request Time-out',
  409 : 'Conflict',
  410 : 'Gone',
  411 : 'Length Required',
  412 : 'Precondition Failed',
  413 : 'Request Entity Too Large',
  414 : 'Request-URI Too Large',
  415 : 'Unsupported Media Type',
  416 : 'Requested Range Not Satisfiable',
  417 : 'Expectation Failed',
  418 : 'I\'m a teapot',              // RFC 2324
  422 : 'Unprocessable Entity',       // RFC 4918
  423 : 'Locked',                     // RFC 4918
  424 : 'Failed Dependency',          // RFC 4918
  425 : 'Unordered Collection',       // RFC 4918
  426 : 'Upgrade Required',           // RFC 2817
  500 : 'Internal Server Error',
  501 : 'Not Implemented',
  502 : 'Bad Gateway',
  503 : 'Service Unavailable',
  504 : 'Gateway Time-out',
  505 : 'HTTP Version not supported',
  506 : 'Variant Also Negotiates',    // RFC 2295
  507 : 'Insufficient Storage',       // RFC 4918
  509 : 'Bandwidth Limit Exceeded',
  510 : 'Not Extended'                // RFC 2774
};

/**
 * @class 
 * @name HttpServerRequest
 * @classdesc The class that represents a HttpServer request. An instance of this class is being passed with the `requestReady` event from {@link module:http.HttpServer}. This class is created internaly and is not to be instantiated directly.
 * @summary A class for representing a HttpServer Request
 * @memberof module:http
 * @requires module:http
 * @see module:http.HttpServerResponse
 * @see {@link module:http/url}
 * @see {@link module:http/querystring}
 * @example
var HttpServer = require("http").HttpServer;
var console    = require("console");
var server = new HttpServer();
server.listen(8150);

server.requestReady.connect(function(rq,rs) {
    //Connect a custom function to the the `end` signal
    rq.end.connect(function() {
        //Request has finished parsing
        console.write("url         = "+rq.url+"\n");
        console.write("method      = "+rq.method+"\n");
        console.write("httpVersion = "+rq.httpVersion+"\n");
        console.dir(rq.headers);
        console.dir(rq.trailers);
        
        //end the response
        rs.writeHeader(404,"bla bla",{ 'Content-Type': 'text/plain'});
        rs.end();
    });
});
//url         = /somepage.html
//method      = GET
//httpVersion = 1.1
//{ accept: 'text/html,application/xhtml+xml,application/xml;q=0.9;q=0.8',
//  accept-encoding: 'gzip, deflate',
//  accept-language: 'en-US,en;q=0.5',
//  connection: 'keep-alive',
//  host: '127.0.0.1:8150',
//  user-agent: 'Mozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0' }
//{}
 */

/**
 * dummy
 * @method module:http.HttpServerRequest._
 */

/**
 * Event trigerred when `data` part from the HTTP Request is available
 * @event module:http.HttpServerRequest.data
 * @arg {ByteArray} data
 */

/**
 * Event trigerred when the HTTP Server has finished parsing the request
 * @event module:http.HttpServerRequest.end
 */

/**
 * Event trigerred when the client closed the request before getting a response
 * @event module:http.HttpServerRequest.close
 */


/**
 * The URL passed in the request represented as a String
 * @member {String} module:http.HttpServerRequest.prototype.url
 * @see {@link module:http/url}
 * @see {@link module:http/querystring}

 */

/**
 * The HTTP Method passed in the request
 * @member {String} module:http.HttpServerRequest.prototype.method 
 */

/**
 * The HTTP version passed in the request
 * @member {String} module:http.HttpServerRequest.prototype.httpVersion
 */

/**
 * An Object representing the HTTP headers passed in the request
 * @member {Object} module:http.HttpServerRequest.prototype.headers
 * @example
{ accept: 'text/html,application/xhtml+xml,application/xml;q=0.9;q=0.8',
  accept-encoding: 'gzip, deflate',
  accept-language: 'en-US,en;q=0.5',
  cache-control: 'max-age=0',
  connection: 'keep-alive',
  host: '127.0.0.1:8150',
  user-agent: 'Mozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0' }
 */

/**
 * An Object representing the HTTP trailers passed in the request
 * @member {Object} module:http.HttpServerRequest.prototype.trailers
 */


/**
 * @class 
 * @name HttpServerResponse
 * @classdesc The class that represents a HttpServer response. An instance of this class is being passed with the `requestReady` event from {@link module:http.HttpServer}. This class is created internaly and is not to be instantiated directly.
 * @summary A class for representing a HttpServer Response
 * @memberof module:http
 * @requires module:http
 * @see module:http.HttpServerRequest
 * @example
var HttpServer = require("http").HttpServer;
var console    = require("console");
var server = new HttpServer();
server.listen(8150);

server.requestReady.connect(function(rq,rs) {
    //Connect a custom function to the the `end` signal
    rq.end.connect(function() {       
        //end the response
        rs.writeHeader(100,{ 'Content-Type': 'text/plain'});
        rs.write("Hello world!".toByteArray());
        rs.end();
    });
});
 */

/**
 * An Object representing the HTTP headers associated with this response
 * @member {Object} module:http.HttpServerResponse.prototype.headers
 * @see {@link module:http.HttpServerRequest.prototype.headers}
 */

/**
 * Add trailers
 * @method module:http.HttpServerResponse.prototype.addTrailers
 * @arg {Object} trailers The trailers object
 */

/**
 * End the response
 * @method module:http.HttpServerResponse.prototype.end
 * @arg {ByteArray|ByteString} [data] 
 * @returns {Boolean}
 */

/**
 * Write `data` to the response object
 * @method module:http.HttpServerResponse.prototype.write
 * @arg {ByteArray|ByteString} [data] 
 * @returns {Boolean}
 */

/**
 * Write `continue` to the response object
 * @method module:http.HttpServerResponse.prototype.writeContinue
 * @returns {Boolean}
 */

/**
 * Write the header part of the response object
 * @method module:http.HttpServerResponse.prototype.writeHeader
 * @arg  {Number} status The status code
 * @arg  {String} [reason]  The reason string.(optional)
 * @arg  {Object} headers The header object
 * @returns {Boolean}
 * @example
 var body = 'hello world';
 response.writeHead(200, {
     'Content-Length': body.length,
     'Content-Type': 'text/plain' });
 */

/**
 *  Node.js (adapted for AtomBox) `http/url` module provides a set of utilities for URL resolution and parsing.

Parsed URL objects have some or all of the following fields, depending on whether or not they exist in the URL string. Any parts that are not in the URL string will not be in the parsed object. Examples are shown for the URL
```
'http://user:pass@host.com:8080/p/a/t/h?query=string#hash'
```

```ddl

* href: The full URL that was originally parsed. Both the protocol and host are lowercased.
    Example: 'http://user:pass@host.com:8080/p/a/t/h?query=string#hash'

* protocol: The request protocol, lowercased.
    Example: 'http:'

* host: The full lowercased host portion of the URL, including port information.
    Example: 'host.com:8080'

* auth: The authentication information portion of a URL.
    Example: 'user:pass'

* hostname: Just the lowercased hostname portion of the host.
    Example: 'host.com'

* port: The port number portion of the host.
    Example: '8080'

* pathname: The path section of the URL, that comes after the host and before the query, including the initial slash if present.
    Example: '/p/a/t/h'

* search: The 'query string' portion of the URL, including the leading question mark.
    Example: '?query=string'

* path: Concatenation of pathname and search.
    Example: '/p/a/t/h?query=string'

* query: Either the 'params' portion of the query string, or a querystring-parsed object.
    Example: 'query=string' or {'query':'string'}

* hash: The 'fragment' portion of the URL including the pound-sign.
    Example: '#hash'
```
 *  @module http/url
 *  @summary URL parsing
 *  @example
var console = require('console');
var url     = require('http/url');

var url_string = 'http://user:pass@host.com:8080/p/a/t/h?query=string#hash'

console.dir(url.parse(url_string));

//{ protocol: 'http:',
//  slashes: true,
//  auth: 'user:pass',
//  host: 'host.com:8080',
//  port: '8080',
//  hostname: 'host.com',
//  hash: '#hash',
//  search: '?query=string',
//  query: 'query=string',
//  pathname: '/p/a/t/h',
//  path: '/p/a/t/h?query=string',
//  href: 'http://user:pass@host.com:8080/p/a/t/h?query=string#hash' }
 
 @see {@link http://nodejs.org/api/url.html}
 */

/**
 * Takes the URL string and return an Object
 * @function module:http/url.parse
 * @arg {String} url URL string to be parsed
 * @returns {Object} The parsed object
 */

/**
 * Takes the URL object and return a String
 * @function module:http/url.format
 * @arg {Object} object The URL object to be formatted
 * @returns {String} the URL string
 */

/**
 * Take a base URL, and a href URL, and resolve them as a browser would for an anchor tag.
 * @function module:http/url.resolve
 * @arg {String} from
 * @arg {String} to
 * @returns {String} the URL string
 * @example
url.resolve('http://example.com/', '/one')    // 'http://example.com/one'
 */


/**
 *  The http/querystring module (based on node.js querystring module) provides utility functions for dealing with querystrings.
```
 *  @module http/querystring
 *  @summary URL parsing
 *  @example
var console = require('console');
var url     = require('http/url');
var querystring     = require('http/querystring');

var url_string = 'http://user:pass@host.com:8080/p/a/t/h?query=foo=bar&baz=qux&baz=quux&corge#hash'

console.dir(querystring.parse(url.parse(url_string).query));
//{ query: 'foo=bar', baz: [ 'qux', 'quux' ], corge: '' }

 @see {@link http://nodejs.org/api/querystring.html}
 */


/**
 * Takes the URL string and return an Object
 * @function module:http/querystring.stringify
 * @arg {Object} query Query object to be transformed
 * @returns {String}
 */
/**
 * Deserialize a query string to an object
 * @function module:http/querystring.parse
 * @arg {String} query Query string
 * @returns {Object}
 */



///////////////////////////////////////////////////////////////////////////////////

/**
 * @class 
 * @name HttpClientRequest
 * @classdesc This class incorporates an http request. Only the `http.request` function creates instances of this class.
 * @note  Cannot be instantiated directly
 * @summary  Http client request
 * @memberof module:http
 * @requires module:http
 * @see module:http.request
 * @example

//
// The example below shows how HttpClientRequest class can be used to perform HTTP Requests
//

var console = require("console");
var http    = require("http");

//Create an instance of HttpClientRequest
var rqst    = http.request({host:"10.57.4.72", port:13579, method:'GET', path:"/",
                            headers:{
                                "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0",
                                "Accept-Language":"en-US,en;q=0.5",
                                "Accept-Encoding":"gzip, deflate",
                                "Connection":"keep-alive"}});

rqst.response.connect(function( resp ) {
    
    //resp is instance of HttpClientResponse

    resp.data.connect(function (data) {
        //write the data chunk to the console
        console.write(data.decodeToString());
    });
    
    resp.finished.connect( function() {
        //delete the request (in case the Garbage collector failed)
        rqst.deleteLater();
    });

    resp.error.connect(function(err) {
        console.writeln("Error:"+err);
    });
});


//END THE REQUEST (SENDS THE REQUEST OUT)
rqst.end();
*/

/**
 * Event trigerred when there is a response to the ended request
 * @event module:http.HttpClientRequest.response
 * @arg {HttpClientResponse} resp The HttpClientResponse instance associated to the request
 */

/**
 * Write `data` to the request object. Used for POST requests.
 * @method module:http.HttpClientRequest.prototype.write
 * @arg {ByteArray|ByteString} data
 */

/**
 * Write `data` to the request object AND ends the request. This triggers the sending of the HTTP request through the network, and the `response` event to be triggered.
 * @note Make sure you connect the `response` signal before ending on the request.
 * @method module:http.HttpClientRequest.prototype.end
 * @arg {ByteArray|ByteString} [data] 
 */

/**
 * Returns an Array of Strings with HTTP Raw headers
 * @method module:http.HttpClientRequest.prototype.headers
 * @returns {Array} [headers] 
 */


/**
 * @class 
 * @name HttpClientResponse
 * @classdesc This class incorporates an http response. An instance of this clas can be obtain by connecting the `response` signal of an HttpClientRequest object.
 * @note  Cannot be instantiated directly
 * @summary  Http client response
 * @memberof module:http
 * @requires module:http
 * @see module:http.HttpClientRequest
 */

/**
 * Event trigerred when there is an error associated with the response
 * @event module:http.HttpClientResponse.error
 * @arg {String} error
 */

/**
 * Event trigerred when there is a chunk of data allready available from the response. 
 * @event module:http.HttpClientResponse.data
 * @arg {ByteArray} data
 */

/**
 * Event trigerred when the response has finished
 * @event module:http.HttpClientResponse.finished
 */


// /**
//  * Write `data` to the response object
//  * @method module:http.HttpClientResponse.prototype.write
//  * @arg {ByteArray|ByteString} data
//  */

// /**
//  * Write `data` to the response object AND ends the response.
//  * @method module:http.HttpClientResponse.prototype.end
//  * @arg {ByteArray|ByteString} [data] 
//  */

/**
 * Returns an Array of Strings with HTTP Raw headers
 * @method module:http.HttpClientResponse.prototype.headers
 * @returns {Array} [headers] 
 */

/**
 * Returns the Http Status
 * @method module:http.HttpClientResponse.prototype.status
 * @returns {Number} httpStatus 
 */

/**
 * @method module:http.HttpClientResponse.prototype.close
 * @returns {Number} httpStatus 
 */

/**
 * @method module:http.HttpClientResponse.prototype.url
 * @returns {String} url
 */


/**
 * This function creates an instance of class HttpClientRequest
 * The `arg` parameter is an Object with the following properties

 - host : The host as a String. Default to 'localhost'
 - port : The port as a Number. Defaults to 80
 - method: The HTTP Method as a String. Must be one of: `POST`, `GET`, `PUT`, `DELETE`,`HEAD`. Defaults to `GET`
 - path: The Request path as a String. Defaults to '/'.
 - headers: The Request raw headers as an Object (name:String, value:String)
 - auth_user
 - auth_pass
 - proxy_user
 - proxy_pass

 * @method module:http.request
 * @arg {Object} arg
 * @see module:http.HttpClientRequest for an example
 */
