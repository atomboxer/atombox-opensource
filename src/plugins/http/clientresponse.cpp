#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QMultiMap>
#include <QtCore/QCoreApplication>

#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#include "clientresponse.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define CLIENTRESPONSE_DEBUG

Q_DECLARE_METATYPE(ByteArray)
ClientResponse::ClientResponse(QNetworkReply *r, QObject *parent) :
    QObject(parent)
{
    priv_ = r;
#if defined(CLIENTRESPONSE_DEBUG)
    qDebug()<<"ClientResponse::ClientResponse::"<<r;
#endif
    QObject::connect(priv_, SIGNAL(finished()),   this, SLOT(slot_finished()),Qt::QueuedConnection);
    QObject::connect(priv_, SIGNAL(readyRead()),  this, SLOT(slot_readyRead()));
// QObject::connect(priv_, SIGNAL(downloadProgress ( qint64, qint64)), 
//                  this, SLOT(slot_downloadProgress(qint64, qint64)));

    QObject::connect(priv_, SIGNAL(error(QNetworkReply::NetworkError)), 
                     this, SLOT(slot_error(QNetworkReply::NetworkError)));
}


ClientResponse::~ClientResponse()
{
#if defined(CLIENTRESPONSE_DEBUG)
    qDebug()<<"ClientResponse::~ClientResponse";
#endif

    priv_->deleteLater();
}

qint32
ClientResponse::status() const
{
    return priv_->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
}

// bool
// ClientResponse::write(const QByteArray &chunk)
// {
//     priv_->write(chunk);
//     return true;
// }

// bool
// ClientResponse::end(const QByteArray &chunk)
// {
// #if defined(CLIENTREQUEST_DEBUG)
//     qDebug()<<"ClientResponse::end";
// #endif

//     priv_->write(chunk);
//     priv_->close();
//     return true;
// }

QMultiMap<QString, QString>
ClientResponse::headers() const
{
    QMultiMap<QString, QString> retVal;

    QList<QByteArray> hlist = priv_->rawHeaderList();
    foreach(QByteArray v, hlist) {
        retVal.insert(v, priv_->rawHeader(v));
    }

    return retVal;
}

void
ClientResponse::slot_error(QNetworkReply::NetworkError e)
{
#if defined(CLIENTRESPONSE_DEBUG)
    qDebug()<<"ClientResponse::slot_error"<<e;
#endif
    
    QString serr;
    switch(e) {
    case(QNetworkReply::NoError) :
        serr = QString("no error condition. Note: When the HTTP protocol returns a redirect no error will be reported.");
    break;
    case(QNetworkReply::ConnectionRefusedError):
        serr = QString("the remote server refused the connection (the server is not accepting requests)");
        break;
    case(QNetworkReply::RemoteHostClosedError):
        serr = QString("the remote server closed the connection prematurely, before the entire reply was received and processed");
        break;
    case(QNetworkReply::HostNotFoundError):
        serr = QString("the remote host name was not found (invalid hostname)");
        break;
    case(QNetworkReply::TimeoutError):
        serr = QString("the connection to the remote server timed out");
        break;
    case(QNetworkReply::OperationCanceledError):
        serr = QString("the operation was canceled via calls to abort() or close() before it was finished.");
        break;
    case(QNetworkReply::SslHandshakeFailedError):
        serr = QString("the SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.");
        break;
    case(QNetworkReply::TemporaryNetworkFailureError):
        serr = QString("the connection was broken due to disconnection from the network, however the system has initiated roaming to another access point.");
        break;
    case(QNetworkReply::ProxyConnectionRefusedError):
        serr = QString("the connection to the proxy server was refused (the proxy server is not accepting requests)");
        break;
    case(QNetworkReply::ProxyConnectionClosedError):
        serr = QString("the proxy server closed the connection prematurely, before the entire reply was received and processed");
        break;
    case(QNetworkReply::ProxyNotFoundError):
        serr = QString("the proxy host name was not found (invalid proxy hostname)");
        break;
    case(QNetworkReply::ProxyTimeoutError):
        serr = QString("the connection to the proxy timed out or the proxy did not reply in time to the request sent");
        break;
    case(QNetworkReply::ProxyAuthenticationRequiredError):
        serr = QString("the proxy requires authentication in order to honour the request but did not accept any credentials offered (if any)");
        break;
    case(QNetworkReply::ContentAccessDenied):
        serr = QString("the access to the remote content was denied (similar to HTTP error 401)");
        break;
    case(QNetworkReply::ContentOperationNotPermittedError):
        serr = QString("the operation requested on the remote content is not permitted");
        break;
    case(QNetworkReply::ContentNotFoundError):
        serr = QString("the remote content was not found at the server (similar to HTTP error 404)");
        break;
    case(QNetworkReply::AuthenticationRequiredError):
        serr = QString("the remote server requires authentication to serve the content but the credentials provided were not accepted (if any)");
        break;
    case(QNetworkReply::ContentReSendError):
        serr = QString("the request needed to be sent again, but this failed for example because the upload data could not be read a second time.");
        break;
    case(QNetworkReply::ProtocolUnknownError):
        serr = QString("the Network Access API cannot honor the request because the protocol is not known");
        break;
    case(QNetworkReply::ProtocolInvalidOperationError):
        serr = QString("the requested operation is invalid for this protocol");
        break;
    case(QNetworkReply::UnknownNetworkError):
        serr = QString("an unknown network-related error was detected");
        break;
    case(QNetworkReply::UnknownProxyError):
        serr = QString("an unknown proxy-related error was detected");
        break;
    case(QNetworkReply::UnknownContentError):
        serr = QString("an unknown error related to the remote content was detected");
        break;
    case(QNetworkReply::ProtocolFailure):
        serr = QString("a breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.)");
        break;
    };

    if ( e !=QNetworkReply::ContentAccessDenied &&
         e !=QNetworkReply::ContentNotFoundError &&
         e !=QNetworkReply::UnknownContentError )
        emit error(serr);
}

void
ClientResponse::slot_readyRead()
{
#if defined(CLIENTRESPONSE_DEBUG)
    qDebug()<<"ClientResponse::slot_readyRead";
#endif
    
    emit data(priv_->readAll());
}

// void
// ClientResponse::slot_downloadProgress(qint64 a, qint64 b)
// {
// #if defined(CLIENTRESPONSE_DEBUG)
//     //qDebug()<<"ClientResponse::slot_downloadProgress"<<a<<"/"<<b;
// #endif    
//     emit readyRead();
// }

void
ClientResponse::slot_finished()
{
#if defined(CLIENTRESPONSE_DEBUG)
    qDebug()<<"ClientResponse::slot_finished";
#endif
    emit finished();
}
