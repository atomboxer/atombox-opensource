#include "server.h"

#include "serverrequest.h"
#include "serverresponse.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ServerRequest*)
Q_DECLARE_METATYPE(ServerResponse*)
Q_DECLARE_METATYPE(Tufao::HttpServerRequest*)
Q_DECLARE_METATYPE(Tufao::HttpServerResponse*)

Server::Server(QObject *parent) : rqst_(0), resp_(0)
{
    priv_ =  new Tufao::HttpServer(0);
    QObject::connect( priv_,SIGNAL(requestReady(Tufao::HttpServerRequest*, Tufao::HttpServerResponse*)),
                      this, SLOT(slot_requestReady(Tufao::HttpServerRequest*, Tufao::HttpServerResponse*)),Qt::DirectConnection);

}

Server::~Server()
{
    if (priv_)
        delete priv_;
}

void
Server::slot_requestReady(Tufao::HttpServerRequest *rqst, 
                          Tufao::HttpServerResponse *resp)
{
    Q_ASSERT(rqst);
    Q_ASSERT(resp);

    if (rqst_) {
        rqst_->deleteLater();
        rqst_ = 0;
    }

    rqst_ = new ServerRequest(rqst,this);
   
    if (resp_) {
        resp_->deleteLater();
        resp_ = 0;
    }

    resp_ = new ServerResponse(resp,this);

    QObject::connect(rqst_, SIGNAL(close()), resp_, SLOT(slot_closed()), 
                     Qt::DirectConnection);

    emit requestReady(rqst_, resp_);
}
