#include <QtCore/QDebug>
#include <QtCore/QMultiMap>

#include <QtCore/QCoreApplication>
#include <QtCore/QUrl>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include "httpplugin.h"

#include "server.h"
#include "serverprototype.h"
#include "serverrequest.h"
#include "serverrequestprototype.h"
#include "serverresponse.h"
#include "serverresponseprototype.h"
#include "clientrequest.h"
#include "clientrequestprototype.h"
#include "clientresponse.h"
#include "clientresponseprototype.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#if defined(AB_USE_SCRIPT_CLASSIC)
Q_EXPORT_PLUGIN2( abscripthttp_compat, HttpPlugin )
#else
Q_EXPORT_PLUGIN2( abscripthttp, HttpPlugin )
#endif


Q_DECLARE_METATYPE(Server*)
Q_DECLARE_METATYPE(ServerRequest*)
Q_DECLARE_METATYPE(ServerResponse*)
Q_DECLARE_METATYPE(ClientRequest*)
Q_DECLARE_METATYPE(ClientResponse*)

QScriptValue 
__http_request(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1) {
        ctx->throwError("<http.request> requires one argument of type Object or String");
        return QScriptValue();
    }

    if (ctx->isCalledAsConstructor())
        return ctx->throwError(QScriptContext::SyntaxError, "please don't use the 'new' operator");

    QString host = "localhost";
    unsigned int port = 80;
    QString method = "GET";
    QString path   = "/";
    QMultiMap <QString, QString> headers;
    QString auth_user  = "";
    QString auth_pass  = "";
    QString proxy_user = "";
    QString proxy_pass = "";
    QUrl url;

    if (ctx->argument(0).isObject()) {
        QScriptValueIterator it(ctx->argument(0));
        while (it.hasNext()) {
            it.next();
            QString name = it.name();
            if (name == "host" && it.value().isString()) {
                host = it.value().toString();
            } else if (name == "port") {
                port = it.value().toInt32();
            } else if (name == "method" && it.value().isString()) {
                method = it.value().toString().toUpper();
                if (method != "POST" && method != "GET" && method != "PUT" &&
                    method != "DELETE" && method != "HEAD") {
                    ctx->throwError("<http.request> unknown method property");
                    return QScriptValue();
                }
            }
            else if (name == "path" && it.value().isString()) {
                path = it.value().toString();
            } else if (name == "auth_user" && it.value().isString()) {
                auth_user = it.value().toString();
            } else if (name == "auth_pass" && it.value().isString()) {
                auth_pass = it.value().toString();
            } else if (name == "proxy_user" && it.value().isString()) {
                proxy_user = it.value().toString();
            } else if (name == "proxy_pass" && it.value().isString()) {
                proxy_pass = it.value().toString();
            } else if (name == "headers" && it.value().isObject()) {
                 QScriptValueIterator hit(it.value());
                 while (hit.hasNext()) {
                     hit.next();
                     headers.insert(hit.name(), hit.value().toString());
                 }
            }
        }
        url.setUrl(path);
        url.setScheme("http");
        url.setHost(host);
        url.setPort(port);
        url.setUserName(auth_user);
        url.setPassword(auth_pass);

    } else if (ctx->argument(0).isString()) {
        url = QUrl(ctx->argument(0).toString());
    }
        
    return eng->newQObject(new ::ClientRequest(url, method, headers, 
                                               proxy_user, proxy_pass), 
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

QScriptValue 
HttpServer_Constructor(QScriptContext *context, QScriptEngine *engine)
{
    return engine->newQObject(new ::Server(), QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration
                              /*| QScriptEngine::ExcludeSuperClassMethods*/
                              /*| QScriptEngine::ExcludeSuperClassProperties*/);
}


QScriptValue 
ServerRequest_toScriptValue(QScriptEngine *engine, ServerRequest* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
ServerRequest_fromScriptValue(const QScriptValue &object, ServerRequest* &out)
{ 
    out = qobject_cast<ServerRequest*>(object.toQObject()); 
}

QScriptValue 
ServerResponse_toScriptValue(QScriptEngine *engine, ServerResponse* const &in)
{ 
    return engine->newQObject(in,
                              QScriptEngine::QtOwnership,
                              QScriptEngine::SkipMethodsInEnumeration);
}

void 
ServerResponse_fromScriptValue(const QScriptValue &object, ServerResponse* &out)
{ 
    out = qobject_cast<ServerResponse*>(object.toQObject());
    if (!out) {
        qDebug("httpplugin.cpp. fatal error");
    }
}

QScriptValue 
ClientRequest_toScriptValue(QScriptEngine *engine, ClientRequest* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::AutoOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
ClientRequest_fromScriptValue(const QScriptValue &object, ClientRequest* &out)
{ 
    out = qobject_cast<ClientRequest*>(object.toQObject()); 
}

QScriptValue 
ClientResponse_toScriptValue(QScriptEngine *engine, ClientResponse* const &in)
{ 
    return engine->newQObject(in,QScriptEngine::QtOwnership,
                              QScriptEngine::SkipMethodsInEnumeration); 
}

void 
ClientResponse_fromScriptValue(const QScriptValue &object, ClientResponse* &out)
{ 
    out = qobject_cast<ClientResponse*>(object.toQObject()); 
}


HttpPlugin::HttpPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
    
}
    
HttpPlugin::~HttpPlugin()
{
}

void
HttpPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
#if defined(AB_USE_SCRIPT_CLASSIC)
    if ( key == QString("http_compat") ) {
#else
    if ( key == QString("http") ) {
#endif

#ifdef AB_STATIC_PLUGINS
        Q_INIT_RESOURCE(net);
#endif
        Q_INIT_RESOURCE(jshttpfiles);

        QScriptValue __internal__;

        __internal__ = engine->globalObject().property("__internal__");
        if (!__internal__.isObject()) {
            __internal__ = engine->newObject();
            engine->globalObject()
                .setProperty("__internal__",__internal__);
        }

        engine->setDefaultPrototype(qMetaTypeId<Server*>(),
                                    engine->newQObject(new ServerPrototype(this)));
        
        qScriptRegisterMetaType(engine, ServerRequest_toScriptValue, ServerRequest_fromScriptValue);
        engine->setDefaultPrototype(qMetaTypeId<ServerRequest*>(),
                                    engine->newQObject(new ServerRequestPrototype(this)));

        qScriptRegisterMetaType(engine, ServerResponse_toScriptValue, ServerResponse_fromScriptValue);
        engine->setDefaultPrototype(qMetaTypeId<ServerResponse*>(),
                                    engine->newQObject(new ServerResponsePrototype(this)));

        engine->globalObject().property("__internal__").
            setProperty("HttpServer", engine->newFunction(HttpServer_Constructor));

        engine->globalObject().property("__internal__").
            property("HttpServer").setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Server*>()));

        engine->globalObject().property("__internal__").
            property("ServerRequest").setProperty("prototype", 
                                                  engine->defaultPrototype(qMetaTypeId<ServerRequest*>()));
        engine->globalObject().property("__internal__").
            property("ServerResponse").setProperty("prototype", 
                                                   engine->defaultPrototype(qMetaTypeId<ServerResponse*>()));
        
        qScriptRegisterMetaType(engine, ClientRequest_toScriptValue, ClientRequest_fromScriptValue);
        qScriptRegisterMetaType(engine, ClientResponse_toScriptValue, ClientResponse_fromScriptValue);

        engine->setDefaultPrototype(qMetaTypeId<ClientRequest*>(),
                                    engine->newQObject(new ClientRequestPrototype(this)));

        engine->setDefaultPrototype(qMetaTypeId<ClientResponse*>(),
                                    engine->newQObject(new ClientResponsePrototype(this)));
 
        engine->globalObject().property("__internal__").
            property("HttpClientRequest").setProperty("prototype", 
                                                  engine->defaultPrototype(qMetaTypeId<ClientRequest*>()));
        engine->globalObject().property("__internal__").
            property("HttpClientResponse").setProperty("prototype", 
                                                   engine->defaultPrototype(qMetaTypeId<ClientResponse*>()));

        engine->globalObject().property("__internal__").
            setProperty("request", engine->newFunction(__http_request));
    }
}
    
QStringList
HttpPlugin::keys() const
{
    QStringList keys;
#if defined(AB_USE_SCRIPT_CLASSIC)
    keys << QString("http_compat");
#else
    keys << QString("http");
#endif

    return keys;
}
