#ifndef CLIENTREQUESTPROTOTYPE_H
#define CLIENTREQUESTPROTOTYPE_H

#include "clientrequest.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class ClientRequestPrototype : public QObject, public QScriptable
{
    Q_OBJECT

    Q_PROPERTY(QScriptValue headers     READ headers)

public:
    ClientRequestPrototype(QObject *parent = 0);
    ~ClientRequestPrototype();

public slots:
    bool         write(QScriptValue chunk) const;
    bool         end  (QScriptValue chunk = QScriptValue()) const;
    QScriptValue headers() const;

private:
    ClientRequest *thisClientRequest() const;
};
#endif
