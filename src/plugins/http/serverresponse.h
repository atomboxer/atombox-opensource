#ifndef SERVERRESPONSE_H
#define SERVERRESPONSE_H

#include <QtCore/QIODevice>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QMultiMap>
#include <QtCore/QSharedPointer>
#include <tufao/src/httpserverresponse.h>

class ServerResponse : public QObject
{
    Q_OBJECT
    friend class Tufao::HttpServerResponse;
    friend class ServerRequest;
public:
    ServerResponse(QIODevice *device,
                   Tufao::HttpServerResponse::Options options,
                   QObject *parent = 0);
    ServerResponse(Tufao::HttpServerResponse *resp, QObject *parent=0);

    virtual ~ServerResponse();

    bool addTrailers(QMultiMap<QString, QString> &trailers);
    bool end(const QByteArray &chunk = QByteArray());
    QMultiMap<QString, QString> headers() const;
    bool write(const QByteArray &chunk);
    inline bool writeContinue() const { return priv_->writeContinue(); } 
    bool writeHead(int statusCode, const QString &reasonPhrase, 
                   QMultiMap<QString, QString> headers) const;
    inline bool writeHead(int status, const QString &reason) const {
        return priv_->writeHead(status, reason.toAscii());}

private slots:
    void slot_closed(){closed_ = true;}

signals:
    void finished();

public:
    Tufao::HttpServerResponse* priv_;

private:
    bool closed_;
    
};

#endif // serverresponse.h 
