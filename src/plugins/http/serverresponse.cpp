#include "serverresponse.h"

#include <QtCore/QDebug>

#include <tufao/src/headers.h>
#include <tufao/src/ibytearray.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define SERVERRESPONSE_DEBUG

Q_DECLARE_METATYPE(Tufao::HttpServerResponse*)

// ServerResponse::ServerResponse(QIODevice *device, 
//                                Tufao::HttpServerResponse::Options options, 
//                                QObject *parent):QObject(parent)
// {
//     priv_ = new Tufao::HttpServerResponse(device, options, parent);
//     QObject::connect( priv_,SIGNAL(finished()), this, SIGNAL(finished()));
// }

ServerResponse::ServerResponse(Tufao::HttpServerResponse *resp, QObject *parent) : QObject(parent)
{
    Q_ASSERT(resp);
    closed_ = false;
    priv_ = resp;
    QObject::connect( priv_,SIGNAL(finished()), this, SIGNAL(finished()),Qt::DirectConnection);
}

ServerResponse::~ServerResponse()
{
//#ifdef  SERVERRESPONSE_DEBUG
//    qDebug()<<"ServerResponse::~ServerResponse";
//#endif
    if (priv_) {
        //QObject::disconnect( priv_,SIGNAL(finished()), this, SIGNAL(finished()));
        //priv_->deleteLater();
        //priv_ = 0;
    }
}

bool
ServerResponse::end(const QByteArray &chunk) 
{
    Q_ASSERT(priv_);
    if (closed_)
        return false;

    return priv_->end(chunk);
}

bool 
ServerResponse::write(const QByteArray &chunk)
{
    Q_ASSERT(priv_);
    if (closed_)
        return false;

    return priv_->write(chunk);
}

bool
ServerResponse::addTrailers(QMultiMap<QString, QString> &trailers)
{
    QMapIterator<QString, QString> i(trailers);
    Tufao::Headers trl;
    if (closed_)
        return false;

    Q_ASSERT(priv_);
    while (i.hasNext()) {
        i.next();
        trl.insert(i.key().toAscii(), i.value().toAscii());
    }

    return priv_->addTrailers(trl);
}

QMultiMap<QString, QString>
ServerResponse::headers() const
{
    QMapIterator<Tufao::IByteArray, QByteArray> i(priv_->headers());
    QMultiMap<QString, QString> ret;

    while (i.hasNext()) {
        i.next();
        ret.insert(QString(i.key().data()).toLower(), QString(i.value().data()));
    }

    return ret;
}

bool
ServerResponse::writeHead(int statusCode, const QString &reasonPhrase, QMultiMap<QString, QString> headers) const
{
    Tufao::Headers hdr;
    if (closed_)
        return false;

    QMapIterator<QString, QString> it(headers);
    while(it.hasNext()) {
        it.next();
        hdr.insert(it.key().toAscii(), it.value().toAscii());
    }

    Q_ASSERT(priv_);
    return priv_->writeHead(statusCode, reasonPhrase.toAscii(), hdr);
}
