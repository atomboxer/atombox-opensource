#ifndef SERVERREQUEST_H
#define SERVERREQUEST_H

#include "binary.h"
#include <QtCore/QObject>
#include <QtNetwork/QAbstractSocket>
#include <tufao/src/httpserverrequest.h>

class ServerRequest : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString      url         READ url)
    Q_PROPERTY(QString      method      READ method)
    Q_PROPERTY(QString      httpVersion READ httpVersion)

    friend class Tufao::HttpServerRequest;

public:
    /*ServerRequest(QAbstractSocket *s, QObject *parent = 0);*/
    ServerRequest(Tufao::HttpServerRequest *rqst, QObject *parent = 0);
    virtual ~ServerRequest();

public:
    inline QString url() const { return priv_->url(); }
    inline QString method() const { return priv_->method(); }
    QString httpVersion() const;

    QMultiMap<QString, QString> headers() const;
    QMultiMap<QString, QString> trailers() const;

private slots:
    void slot_close();

signals:
    void data(QByteArray data);
    void end();
    void close();

public:
    Tufao::HttpServerRequest* priv_;

private:
    bool ok_;
};

#endif // serverrequest.h 
