#include "serverresponseprototype.h"
#include "binary.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ServerResponse*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

ServerResponsePrototype::ServerResponsePrototype(QObject *parent)
{
}

ServerResponsePrototype::~ServerResponsePrototype()
{

}

bool
ServerResponsePrototype::addTrailers(QScriptValue trls) const
{
    QMultiMap<QString, QString> t;

    QScriptValueIterator it(trls);
    while (it.hasNext()) {
        it.next();
        t.insert(it.name(), it.value().toString());
    }

    return thisServerResponse()->addTrailers(t);
}

bool
ServerResponsePrototype::end(QScriptValue chunk) const
{
    QByteArray *buf = NULL;
    if (!chunk.isUndefined()) {
        if (chunk.isString())
            return thisServerResponse()->end(chunk.toString().toAscii());

        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());

        if (buf)
            return thisServerResponse()->end(*buf);
    }

    return thisServerResponse()->end();
}

QScriptValue 
ServerResponsePrototype::headers() const
{   
    QMapIterator<QString, QString> it(thisServerResponse()->headers());
    QScriptValue obj = engine()->newObject();

    while(it.hasNext()) {
        it.next();
        obj.setProperty(it.key(), it.value());
    }
    return obj;
}

bool
ServerResponsePrototype::write(QScriptValue chunk) const
{
    QByteArray *buf = NULL;
    if (!chunk.isUndefined()) {
        if (chunk.isString())
            return thisServerResponse()->write(chunk.toString().toAscii());

        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());

        if (buf)
            return thisServerResponse()->write(*buf);
    }
    
    context()->throwError("ServerResponse.prototype.write parameter should be String or instanceof Binary");
    return false;
}

bool
ServerResponsePrototype::writeHeader(int statusCode, QScriptValue arg2, QScriptValue arg3) const
{
    QMultiMap<QString, QString> headers;
    QString reason;
    QScriptValue script_hdrs;

    if (arg2.isString()) {
        reason = arg2.toString();
        script_hdrs = arg3;
    } else
        script_hdrs = arg2;

    if (!script_hdrs.isUndefined()) {
        QScriptValueIterator it(script_hdrs);
        while (it.hasNext()) {
            it.next();
            headers.insert(it.name(), it.value().toString());
        }
    }

    if (headers.size() != 0)
        return thisServerResponse()->writeHead(statusCode, reason, headers);
    else
        return thisServerResponse()->writeHead(statusCode, reason);
}

ServerResponse*
ServerResponsePrototype::thisServerResponse() const
{
    ServerResponse *r = qscriptvalue_cast<ServerResponse*>(thisObject());
    Q_ASSERT(r);
    return r;
}
