#ifndef CLIENTRESPONSE_H
#define CLIENTRESPONSE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtCore/QMultiMap>

#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include "binary.h"

class ClientRequest;
class ClientResponse : public QObject
{
    Q_OBJECT
    
    friend class  ClientRequest;

public:
    ClientResponse( QNetworkReply *r, QObject *parent = 0);
    virtual ~ClientResponse();

Q_INVOKABLE     inline void       close () { priv_->close(); }
Q_INVOKABLE     inline QString    url() const { return priv_->url().toString(); }

    /* virtual bool                        write(const QByteArray &chunk); */
    /* virtual bool                        end(const QByteArray &chunk); */
    virtual QMultiMap<QString, QString> headers() const;
    qint32                              status() const;

signals:
    void error(QString errorString);
    void data(ByteArray);
    void finished();

private slots:
    void slot_error(QNetworkReply::NetworkError);
    void slot_readyRead();
    void slot_finished();
    //void slot_downloadProgress(qint64, qint64);

private:
    QNetworkReply         *priv_;
};
#endif //CLIENTRESPONSE_H
