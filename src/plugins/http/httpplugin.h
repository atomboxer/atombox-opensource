#ifndef HTTPPLUGIN_H
#define HTTPPLUGIN_H

#include <QScriptExtensionPlugin>

class HttpPlugin : public QScriptExtensionPlugin
{
public:
    HttpPlugin( QObject *parent = 0);
    ~HttpPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //HTTPPLUGIN_H
