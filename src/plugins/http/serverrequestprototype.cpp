#include "serverrequestprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <QtCore/QMultiMap>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ServerRequest*)

ServerRequestPrototype::ServerRequestPrototype(QObject *parent)
{
}

ServerRequestPrototype::~ServerRequestPrototype()
{
}

QScriptValue 
ServerRequestPrototype::headers() const
{   
    QMapIterator<QString, QString> it(thisServerRequest()->headers());
    QScriptValue obj = engine()->newObject();

    while(it.hasNext()) {
        it.next();
        obj.setProperty(it.key(), it.value());
    }

    return obj;
}

QScriptValue 
ServerRequestPrototype::trailers() const
{
    QMapIterator<QString, QString> it(thisServerRequest()->trailers());
    QScriptValue obj = engine()->newObject();

    while(it.hasNext()) {
        it.next();
        obj.setProperty(it.key(), it.value());
    }
    return obj;
}

ServerRequest*
ServerRequestPrototype::thisServerRequest() const
{
    ServerRequest *r = qscriptvalue_cast<ServerRequest*>(thisObject());
    if (!r)
        qFatal("Programmatic error ServerRequestPrototype::thisServerRequest");
    
    return r;
}


