#ifndef SERVERRESPONSEPROTOTYPE_H
#define SERVERRESPONSEPROTOTYPE_H

#include "serverresponse.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class ServerResponsePrototype : public QObject, public QScriptable
{
    Q_OBJECT

    Q_PROPERTY(QScriptValue headers     READ headers)

public:
    ServerResponsePrototype(QObject *parent = 0);
    ~ServerResponsePrototype();

public slots:
    bool addTrailers(QScriptValue trls) const;
    bool end(QScriptValue chunk = QScriptValue::UndefinedValue) const;
    QScriptValue headers() const;
    bool write(QScriptValue buf) const;
    inline bool writeContinue() const { return thisServerResponse()->writeContinue(); }
    bool writeHeader(int statusCode, 
                     QScriptValue arg2 = QScriptValue::UndefinedValue, 
                     QScriptValue arg3 = QScriptValue::UndefinedValue) const;
private:
    ServerResponse *thisServerResponse() const;
};

#endif //SERVERRESPONSEPROTOTYPE_H
