
#include "serverprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <tufao/src/httpserver.h>
#include <tufao/src/httpserverresponse.h>
#include <tufao/src/httpserverrequest.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Server*)
Q_DECLARE_METATYPE(ServerRequest*)
Q_DECLARE_METATYPE(ServerResponse*)
Q_DECLARE_METATYPE(Tufao::HttpServerRequest*)
Q_DECLARE_METATYPE(Tufao::HttpServerResponse*)

ServerPrototype::ServerPrototype(QObject *parent)
{
}

ServerPrototype::~ServerPrototype()
{

}

void
ServerPrototype::close()
{
    return thisServer()->close();
}

bool
ServerPrototype::isListening()
{
    return thisServer()->isListening();
}

bool
ServerPrototype::listen( quint16 port, QScriptValue host)
{
    QHostAddress addr = QHostAddress::Any;
    if (!host.isUndefined()) {
        addr = QHostAddress(host.toString());
    }

    if (port == 0) {
        context()->throwError("HttpServer.prototype.listen, invalid port specified");
        return false;
    }

    return thisServer()->listen(addr, port);
}

quint16
ServerPrototype::port()
{
    return thisServer()->serverPort();
}

Server*
ServerPrototype::thisServer() 
{
    Server *s = qscriptvalue_cast<Server*>(thisObject());
    Q_ASSERT(s);
    if (init_flags_.find(s) == init_flags_.end()) {
        // QObject::connect(s, SIGNAL(requestReady(ServerRequest*, ServerResponse*)), 
        //                  this, SIGNAL(requestReady1(ServerRequest*, ServerResponse*)));
        
	init_flags_.insert(s, true);
    }
    return s;
}


