#include "clientrequestprototype.h"
#include "binary.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>
#include <QtNetwork/QNetworkReply>

#include <QtCore/QMultiMap>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ClientRequest*)
Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

ClientRequestPrototype::ClientRequestPrototype(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<QNetworkReply::NetworkError>("QNetworkReply::NetworkError");
   
}


ClientRequestPrototype::~ClientRequestPrototype()
{
}



bool
ClientRequestPrototype::write(QScriptValue chunk) const
{
    QByteArray *buf = NULL;
    if (!chunk.isUndefined()) {
        if (chunk.isString())
            return thisClientRequest()->write(chunk.toString().toAscii());

        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());

        if (buf)
            return thisClientRequest()->write(*buf);
    }
    
    context()->throwError("ClientRequest.prototype.write parameter should be String or instanceof Binary");

    return false;
}

bool
ClientRequestPrototype::end(QScriptValue chunk) const
{
    QByteArray *buf = NULL;

    if (!chunk.isUndefined()) {
        if (chunk.isString())
            return thisClientRequest()->end(chunk.toString().toAscii());
        
        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());
        
        if (buf)
            return thisClientRequest()->end(*buf);
    }
    
    return thisClientRequest()->end();
}

QScriptValue
ClientRequestPrototype::headers() const
{
    QMapIterator<QString, QString> it(thisClientRequest()->headers());
    QScriptValue obj = engine()->newObject();
    
    while(it.hasNext()) {
        it.next();
        obj.setProperty(it.key(), it.value());
    }
    return obj;
}

ClientRequest *
ClientRequestPrototype::thisClientRequest() const
{
    ClientRequest *r = qscriptvalue_cast<ClientRequest*>(thisObject());
    if (!r)
        qFatal("Programmatic error ClientRequestPrototype::thisClientRequest");

    return r;
}
