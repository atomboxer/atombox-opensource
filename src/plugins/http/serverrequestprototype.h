#ifndef SERVERREQUESTPROTOTYPE_H
#define SERVERREQUESTPROTOTYPE_H

#include "serverrequest.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class ServerRequestPrototype : public QObject, public QScriptable
{
    Q_OBJECT

    Q_PROPERTY(QScriptValue headers     READ headers)
    Q_PROPERTY(QScriptValue trailers    READ trailers)

public:
    ServerRequestPrototype(QObject *parent = 0);
    ~ServerRequestPrototype();

public:
    QScriptValue headers() const;
    QScriptValue trailers() const;

private:
    ServerRequest *thisServerRequest() const;
};

#endif //SERVERREQUESTPROTOTYPE_H
