#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <tufao/src/httpserver.h>

#include "serverrequest.h"
#include "serverresponse.h"

class Server : public QObject
{
    Q_OBJECT

public:
    Server(QObject *p = 0);
    ~Server(); 

    inline void close() { priv_->close(); }
    inline bool isListening() { return priv_->isListening(); }
    inline bool listen(const QHostAddress &address, quint16 port) { return priv_->listen(address, port); } 
    inline quint64 serverPort() const { return priv_->serverPort(); }

signals:
    void requestReady( ServerRequest  *rqst,
                       ServerResponse *resp);

public slots:
    void slot_requestReady(Tufao::HttpServerRequest *, Tufao::HttpServerResponse *);

private:
    Tufao::HttpServer *priv_;
    ServerRequest*  rqst_;
    ServerResponse* resp_;
};

#endif // server.h 
