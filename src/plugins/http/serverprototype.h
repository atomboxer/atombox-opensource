#ifndef SERVERPROTOTYPE_H
#define SERVERPROTOTYPE_H

#include "server.h"
#include "serverrequest.h"
#include "serverresponse.h"

#include <QtCore/QObject>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class ServerPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    ServerPrototype(QObject *parent = 0);
    ~ServerPrototype();

public slots:
    void close();
    bool isListening(); 
    bool listen(quint16 port, QScriptValue host=QScriptValue::UndefinedValue);
    quint16 port();

private:
    Server *thisServer();
    QHash<Server*, bool> init_flags_;
};

#endif //SERVERPROTOTYPE_H
