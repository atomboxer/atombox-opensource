#ifndef MQMANAGERPROTOTYPE_H
#define MQMANAGERPROTOTYPE_H

#include <QtCore/QObject>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptable>

class MQManager;
class MQQueue;

class MQManagerPrototype: public QObject, public QScriptable
{
    Q_OBJECT

public:
    MQManagerPrototype(QObject *parent = 0);
    ~MQManagerPrototype();

public slots:
    bool connect(QString qmanager="") const;
    bool disconnect() const;
    
    QScriptValue openQueue(QString qname, QScriptValue options) const;

    bool    isConnected()     const;
    QString lastErrorString() const;
    long    lastReasonCode()  const;

    bool    beginTransaction() const;
    bool    endTransaction()   const;
    bool    abortTransaction() const;

private:
    MQManager *thisMQManager() const;
};

#endif //MQMANAGERPROTOTYPE_H
