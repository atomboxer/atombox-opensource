
/*
* @desc This module provides a simple API for interacting with the Websphere MQ IBM product. The main purpose of this module is to provide a simple but highly customizable API maintain the main functionality of MQ but covering the complexity of the internal API. Currently mq/websphere API supports opening managers, opening and closing queues, receiving datagrams (MQMT_DATAGRAM), receiving requests (MQMT_REQUEST), sending responses to the received requests, sending datagrams, and dealing with unit of works.

Use the [module:mq/websphere.MQManager](websphere.MQManager.html) class to connect to the MQ Manager, and to [open](websphere.MQManager.html#openQueue) a queue (an object of type [module:mq/websphere.MQQueue](websphere.MQQueue.html) ).

An MQQueue object then can [create](websphere.MQQueue.html#createDatagram) either [MQMessage](websphere.MQMessage.html) or [MQRequestMessage](websphere.MQRequestMessage.html) class instances to be sent out through the already opened queue.

The MQQueue object regullarly checks for any income requests or datagrams by emitting the [requestReady](websphere.MQQueue.html#requestReady) and [datagramReady](websphere.MQQueue.html#datagramReady) signals.

@note This module requires the IBM Websphere MQ client to be installed on the machine, and mqic.dll (*mqic.so) to be in %PATH% (respectively $LD_LIBRARY_PATH on UNIX).
 * @module mq/websphere
 * @summary Simple IBM Websphere MQ API
 * @example
var system  = require("system");
var console = require("console");

var MQManager      = require("mq/websphere").MQManager;

var mm = new MQManager();

//Connect to the MQ Manager
mm.connect("EP92.MANAGER");

//Open a queue
var q = mm.openQueue("EP92.ABI",{input:true, output:true});

//Connect the requestReady signal and deal with the request/response objects
q.requestReady.connect( function(rq, rs) {
	console.writeln("requestReady:");
	console.writeln(rq.data().decodeToString());

	rs.write("Hello back".toByteArray());
	rs.end();
});

//Connect the datagramReady signal (no response)
q.datagramReady.connect( function(rq) {
	console.writeln("datagramReady:");
	console.writeln(rq.data());
});

//Connect the error signal
q.error.connect( function(num) {
	console.writeln("error "+num);
});

//Send a request
var r = q.createRequest();
r.ReplyToQ = "EP92.VISA";
r.ReplyToQMgr = "EP92.MANAGER";
r.end("This is a requst".toByteArray());

//Send a datagram
var d = q.createDatagram();
d.end("I am sending something back".toByteArray());
 */

/**
 * Use this class to connect to the MQ Manager, and to [open](websphere.MQManager.html#openQueue) a queue (an object of type [module:mq/websphere.MQQueue](websphere.MQQueue.html) ).
 * @class
 * @name MQManager
 * @classdesc todo
 * @summary MQ Manager connection class
 * @memberof module:mq/websphere
 * @see module:mq/websphere
 * @example
//..
var MQManager      = require("mq/websphere").MQManager;

var mm = new MQManager();

//Connect to the MQ Manager
mm.connect("EP92.MANAGER");

//Open a queue
var q = mm.openQueue("EP92.ABI",{input:true, output:true});
//..
 */

/**
 * Connects to the provided qmanager name. 
 * @note IBM Websphere MQ client connections requires the creation of a client-connection channel externally to the application by setting the MQSERVER environment variable (ex: SET MQSERVER=CHANNEL1/TCP/server_address(port) ) before invoking the connect method.
 * @method module:mq/websphere.MQManager.prototype.connect
 * @arg {String} qmanager
 * @throws {Error}
 */

/**
 * Disconnects the connection to the MQ manager.
 * @method module:mq/websphere.MQManager.prototype.disconnect
 * @throws {Error}
 */


/**
 * This method openes a queue object. The options for the MQOPEN
 * are passed with an object with the following boolean properties:
 * input:true|false, input_exclusive:true|false, output:true|false  
 * @method module:mq/websphere.MQManager.prototype.openQueue
 * @arg {String} qname - The queue name
 * @arg {Object} open_options - An object with properties (input, input_exclusive, output)
 * @throws {Error}
 * @returns {MQQueue} - an instance of module:mq/websphere.MQQueue class
 * @red
 * @example
     //...
     try {
         mq.openQueue('EP92.MANAGER', {input:true, output:true});
     } catch(e) {
        //...
     }
 */


/**
 * Return true if the object is in connected state, false otherwise.
 * @method module:mq/websphere.MQManager.prototype.isConnected
 * @returns {Boolean}
 */

/**
 * Returns the last error in string format.
 * @method module:mq/websphere.MQManager.prototype.lastErrorString
 * @returns {String}
 */

/**
 * Returns the last reason code.
 * @method module:mq/websphere.MQManager.prototype.lastReasonCode
 * @returns {Number}
 */

/**
 * Begins a unit of work
 * @method module:mq/websphere.MQManager.prototype.beginTransaction
 * @returns {Boolean}
 * @throws {Error}
 */

/**
 * Commits the unit of work
 * @method module:mq/websphere.MQManager.prototype.endTransaction
 * @returns {Boolean}
 * @throws {Error}
 */

/**
 * Aborts the unit of work
 * @method module:mq/websphere.MQManager.prototype.abortTransaction
 * @returns {Boolean}
 * @throws {Error}
 */

//...

/**
 * @class
 * @name MQQueue
 * @classdesc An MQQueue object can [create](websphere.MQQueue.html#createDatagram) either [MQMessage](websphere.MQMessage.html) or [MQRequestMessage](websphere.MQRequestMessage.html) class instances to be sent out through the already opened connection.

The MQQueue object regullarly checks for any income requests or datagrams by emitting the [requestReady](websphere.MQQueue.html#requestReady) and [datagramReady](websphere.MQQueue.html#datagramReady) signals.

 * @see module:mq/websphere
 * @summary IBM Websphere MQ Queue
 * @memberof module:mq/websphere
 * @example
//..
var q = mm.openQueue("EP92.ABI",{input:true, output:true});
//..
 */

/**
 * This event is emitted when a request message type (MQMT_REQUEST) is received through the opened queue. By invoking the [end](websphere.MQResponseMessage.html#end) method on the response object, a response will be sent (using MQPUT1 function) to the queue and queue manager specified in the ReplyToQ and ReplyToQMgr.

 * @event module:mq/websphere.MQQueue.prototype.requestReady
 * @arg {MQRequest}  request  - The request  message
 * @arg {MQResponse} response - The response message
 */

/**
 * This event is emitted when a request message type (MQMT_DATAGRAM) is received through the opened queue.
 * @event module:mq/websphere.MQQueue.prototype.datagramReady
 * @arg {MQMessage} message  - The message read from the queue
 */

/**
 * This event is emitted when the queue changes its state.
 * @event module:mq/websphere.MQQueue.prototype.error
 * @arg {Number} reason  - The reason number
 */

/**
 * Closes the queue.
 * @method module:mq/websphere.MQQueue.prototype.close
 * @returns {Boolean}
 * @throws {Error}
 */

/**
 * Creates a new MQ Message of type MQMT_REQUEST. Invoking the [end](websphere.MQRequestMessage.html#end) method will automatically trigger the message to be sent throug the opened queue that created it.
 * @method module:mq/websphere.MQQueue.prototype.createRequest
 * @returns {MQRequest}  an instance of module:mq/websphere.MQRequest class
 * @throws {Error}
 */

/**
 * Creates a new MQ Message of type MQMT_DATAGRAM. Invoking the [end](websphere.MQMessage.html#end) method will automatically trigger the message to be sent through the opened queue that created it.
 * @method module:mq/websphere.MQQueue.prototype.createDatagram
 * @returns {MQMessage}  an instance of module:mq/websphere.MQMessage class
 * @throws {Error}
 */


//......//
/**
 * @class
 * @name MQMessage
 * @classdesc This class represents a generic MQ Message of type MQMT_DATAGRAM. 
Objects of this class cannot be instantiated directly and are created by the MQQueue class objects by invoking the [createDatagram](websphere.MQQueue.html#createDatagram) method.

Invoking the end method, will trigger the MQQueue object to send (MQPUT) the message through its queue connection (and to destroy the message object).
 * @summary IBM Websphere MQ generic message.
 * @memberof module:mq/websphere
 * @see module:mq/websphere.MQQueue.createDatagram
 */

/**
 * Appends a ByteArray to the message beeing built.
 * @method module:mq/websphere.MQMessage.prototype.write
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 * Appends a ByteArray to the message beeing built, and triggers sending of the message through the current connection.
 * @method module:mq/websphere.MQMessage.prototype.end
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 *  Read/Write property
 * @member {ByteArray} module:mq/websphere.MQMessage.prototype.MsgId
 */

/**
 * Read/Write property
 * @member {ByteArray} module:mq/websphere.MQMessage.prototype.CorrelId
 */

/**
 * Returns the last error in string format.
 * @method module:mq/websphere.MQMessage.prototype.lastErrorString
 * @returns {String}
 */

/**
 * Returns the last reason code.
 * @method module:mq/websphere.MQMessage.prototype.lastReasonCode
 * @returns {Number}
 */

/**
 * @class
 * @name MQRequestMessage
 * @classdesc This class represents a MQ Message of type MQMT_REQUEST. 
Objects of this class cannot be instantiated directly and are created by the MQQueue class objects by invoking the [createRequest](websphere.MQQueue.html#createRequest).

Invoking the end method, will trigger the MQQueue object to send (MQPUT) the message through its queue connection (and to destroy the message object).

 * @summary IBM Websphere MQ request message.
 * @memberof module:mq/websphere
 * @see module:mq/websphere.MQQueue.createDatagram
 */

/**
 * Appends a ByteArray to the message beeing built.
 * @method module:mq/websphere.MQRequestMessage.prototype.write
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 * Appends a ByteArray to the message beeing built, and triggers sending of the message through the current connection.
 * @method module:mq/websphere.MQRequestMessage.prototype.end
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 * Read/Write property
 * @member {String} module:mq/websphere.MQRequestMessage.prototype.ReplyToQ
 */

/**
 * Read/Write property
 * @member {String} module:mq/websphere.MQRequestMessage.prototype.ReplyToQMgr
 */

/**
 * Returns the last error in string format.
 * @method module:mq/websphere.MQRequestMessage.prototype.lastErrorString
 * @returns {String}
 */

/**
 * Returns the last reason code.
 * @method module:mq/websphere.MQRequestMessage.prototype.lastReasonCode
 * @returns {Number}
 */

/**
 * @class
 * @name MQResponseMessage
 * @classdesc This class represents a MQ Message of type MQMT_RESPONSE. 
Objects of this class cannot be instantiated directly and are created by the MQQueue class objects when the [requestReady](websphere.MQQueue.html#requestReady) signal is emitted.

Invoking the end method, will trigger the MQ Manager to send (MQPUT1) the message through its connection (and to destroy the message object).

 * @summary IBM Websphere MQ response message
 * @memberof module:mq/websphere
 * @see module:mq/websphere.MQQueue.onRequestReady
 */

/**
 * Appends a ByteArray to the message beeing built.
 * @method module:mq/websphere.MQResponseMessage.prototype.write
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 * Appends a ByteArray to the message beeing built, and triggers sending of the message through the current connection.
 * @method module:mq/websphere.MQResponseMessage.prototype.end
 * @arg {Binary} data
 * @throws {Error}
 */

/**
 * Returns the last error in string format.
 * @method module:mq/websphere.MQResponseManager.prototype.lastErrorString
 * @returns {String}
 */

/**
 * Returns the last reason code.
 * @method module:mq/websphere.MQResponseManager.prototype.lastReasonCode
 * @returns {Number}
 */

exports.MQManager  = MQManager = __internal__.MQManager;
