#ifndef MQMESSAGE_H
#define MQMESSAGE_H

#include <QtCore/QByteArray>
#include <QtCore/QIODevice>
#include <QtCore/QObject>

#include "binary.h"

#if defined(__MINGW64__) || defined (__MINGW32__)
#include <stddef.h>
#define _int64 __int64
#endif

#include <cmqc.h>
#include "binary.h"
#include <QtCore/QDebug>

class MQMessage : public QObject {

    Q_OBJECT
    Q_PROPERTY(ByteArray CorrelId      READ getCorrelId       WRITE setCorrelId )
    Q_PROPERTY(ByteArray MsgId         READ getMsgId          WRITE setMsgId )
    Q_PROPERTY(QString   ReplyToQ      READ getReplyToQ       WRITE setReplyToQ )
    Q_PROPERTY(QString   ReplyToQMgr   READ getReplyToQMgr    WRITE setReplyToQMgr )

public:
    //MQMessage( QObject *parent = 0 );
    MQMessage(MQHCONN &hcon, MQHOBJ &hobj, 
              QByteArray data=QByteArray(), QObject *parent = 0);

    virtual ~MQMessage();

    ByteArray data() const { return data_; };
    bool write(const QByteArray &chunk);
    
    virtual bool end(const QByteArray &chunk = QByteArray());

    QString lastErrorString() const { return last_error_; }
    MQLONG  lastReasonCode()  const { return last_reason_code_; }

    ByteArray getCorrelId() const {
        return QByteArray((char*)(hmd_.CorrelId), sizeof(hmd_.CorrelId)); }
    void setCorrelId(ByteArray n) {
        strncpy((char*)(hmd_.CorrelId), n.data(), sizeof(hmd_.CorrelId));}

    ByteArray getMsgId() const {
        return QByteArray((char*)(hmd_.MsgId), sizeof(hmd_.MsgId)); }
    void setMsgId(ByteArray n) {
        strncpy((char*)(hmd_.MsgId), n.data(), sizeof(hmd_.MsgId));}


    QString getReplyToQ() const {
      return QByteArray(hmd_.ReplyToQ, strlen(hmd_.ReplyToQ)); }
    void setReplyToQ(QString n) {
      strncpy(hmd_.ReplyToQ, n.toLatin1().data(), n.size());
    }

    QString getReplyToQMgr() const {
      return QByteArray(hmd_.ReplyToQMgr, strlen(hmd_.ReplyToQMgr)); }
    void setReplyToQMgr(QString n) {
      strncpy(hmd_.ReplyToQMgr, n.toLatin1().data(), n.size());
    }

protected:
    MQHCONN    hcon_;                   /* connection handle             */
    MQHOBJ     hobj_;                   /* object handle                 */
    MQMD       hmd_ ;                   /* message handle                */
    QByteArray data_;

    QString  last_error_;
    MQLONG   last_reason_code_;
};

class MQRequestMessage : public MQMessage {

    Q_OBJECT

public:
    MQRequestMessage(MQHCONN &hcon, MQHOBJ &hobj,
                     QByteArray data=QByteArray(), QObject *parent = 0);
};

class MQResponseMessage : public MQMessage {

    Q_OBJECT

public:
    //MQResponseMessage( QObject *parent = 0);
    MQResponseMessage(MQHCONN &hcon, MQHOBJ &hobj,
                      QByteArray data=QByteArray(), QObject *parent = 0);

    MQResponseMessage(MQHCONN &hcon, MQHOBJ &hobj, MQMD &md, 
                      QByteArray data=QByteArray(), QObject *parent = 0);
    virtual bool end(const QByteArray &chunk = QByteArray());

private:
    MQMD hrq_;
};

#endif //MQMESSAGE_H
