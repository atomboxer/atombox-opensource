#include "mqqueue.h"
#include "mqqueueprototype.h"

#include "mqmanager.h"
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(MQQueue*)

MQQueuePrototype::MQQueuePrototype(QObject *parent)
    : QObject(parent)
{
    
}

MQQueuePrototype::~MQQueuePrototype()
{
}

bool
MQQueuePrototype::close() const
{
    if (!thisMQQueue()->close()) {
        context()->throwError(lastErrorString());
        return false;
    }
    return true;
}

MQQueue *
MQQueuePrototype::thisMQQueue() const
{
    MQQueue *p = qscriptvalue_cast<MQQueue*>(thisObject());

    if (!p) {
        qFatal("Programmatic error MQQueuePrototype::thisMQQueue");
    }

    return p;
}
