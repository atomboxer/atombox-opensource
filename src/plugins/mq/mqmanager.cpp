#include "mqmanager.h"

#include <QtCore/QString>
#include <QtCore/QByteArray>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif



MQManager::MQManager( QObject *parent ) 
    : QObject(parent),
      is_connected_(false)
{

}

MQManager::~MQManager()
{
    foreach(MQQueue *q, opened_q_list_) {
        if (q)
            delete q;
    }
}

bool
MQManager::connect(QString qmanager)
{
    qmanager_ = qmanager;

    MQLONG ccode;
    MQLONG creason;

    MQCONN(qmanager_.toLatin1().data(),    /* queue manager                  */
           &hcon_,                         /* connection handle              */
           &ccode,                         /* completion code                */
           &creason);                      /* reason code                    */
    
    if (ccode == MQCC_FAILED) {
        last_error_ = "MQCONN ended with reason code "+QString::number(creason);
        last_reason_code_ = creason;
        return false;
    }
    
    is_connected_ = true;
    return true;
}

bool
MQManager::disconnect()
{
    if (!is_connected_)
        return true;
    
    MQLONG ccode;
    MQLONG creason;

    MQDISC(&hcon_,               /* connection handle            */
           &ccode,               /* completion code              */
           &creason);            /* reason code                  */
    
    if (creason != MQRC_NONE) {
        last_error_ = "MQDISC ended with reason code "+QString::number(creason);
        last_reason_code_ = creason;
        is_connected_ = false;
        return false;
    }

    is_connected_ = false;
    return false;
}

MQQueue*
MQManager::openQueue(QString &qname, MQLONG options)
{
    MQOD     od   = { MQOD_DEFAULT };    /* Object Descriptor          */
    od.ObjectType = MQOT_Q;

    strncpy(od.ObjectName, qname.toLatin1().data(), (size_t)MQ_Q_NAME_LENGTH);
    
    MQLONG   ccode;
    MQLONG   creason;

    MQHOBJ   hobj;                     /* object handle                */

    MQQueue  *pmq = new MQQueue(this);

    MQOPEN( hcon_,                     /* connection handle            */
            &od,                       /* object descriptor for queue  */
            options,                   /* open options                 */
            &hobj,                     /* object handle                */
            &ccode,                    /* MQOPEN completion code       */
            &creason );                /* reason code                  */

    if (creason != MQRC_NONE) {
        last_error_ = "MQOPEN ended with reason code " + QString::number(creason);
        last_reason_code_ = creason;
    }

    if (ccode == MQCC_FAILED) {
        last_error_ = "MQOPEN failed with no reason ";
        last_reason_code_ = MQRC_NONE;
        return pmq;
    }
    
    //
    //  Init the MQQueue object, and start the loop
    //
    pmq->init(hcon_, hobj, qname);

    opened_q_list_.push_back(pmq);

    return pmq;
}

bool
MQManager::beginTransaction()
{
    MQBO bo = {MQBO_DEFAULT};     /* begin options                    */
    
    MQLONG ccode;
    MQLONG creason;

    MQBEGIN (hcon_, &bo, &ccode, &creason);
    if (ccode != MQRC_NONE) {
        last_error_ = "MQBEGIN ended with reason code "+QString::number(creason);
        last_reason_code_ = creason;
        return false;
    }
    
    return true;
}

bool
MQManager::endTransaction()
{
    MQLONG ccode;
    MQLONG creason;

    MQCMIT  (hcon_, &ccode, &creason);
    if (ccode != MQRC_NONE) {
        last_error_ = "MQCMIT ended with reason code "+QString::number(creason);
        last_reason_code_ = creason;
        return false;
    }
    
    return true;
}

bool
MQManager::abortTransaction()
{
    MQLONG ccode;
    MQLONG creason;

    MQBACK  (hcon_, &ccode, &creason);
    if (ccode != MQRC_NONE) {
        last_error_ = "MQBACK ended with reason code "+QString::number(creason);
        last_reason_code_ = creason;
        return false;
    }
    
    return true;
}
