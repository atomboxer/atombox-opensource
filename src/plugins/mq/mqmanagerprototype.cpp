#include "mqmanager.h"
#include "mqmanagerprototype.h"
#include "mqqueue.h"
#include "mqqueueprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(MQManager*)
Q_DECLARE_METATYPE(MQQueue*)

MQManagerPrototype::MQManagerPrototype(QObject *parent)
    : QObject(parent)
{
}

MQManagerPrototype::~MQManagerPrototype()
{
}

bool
MQManagerPrototype::connect(QString qmanager) const
{
    if (!thisMQManager()->connect(qmanager)) {
        context()->throwError(lastErrorString());
        return false;
    }
    return true;
}

bool
MQManagerPrototype::disconnect() const
{
    if (!thisMQManager()->disconnect()) {
        context()->throwError(lastErrorString());
        return false;
    }
    return true;
}

QScriptValue 
MQManagerPrototype::openQueue(QString qname, QScriptValue options) const
{
    QScriptValueIterator it(options);
    MQLONG mq_options;
    if (!it.hasNext()) {
        mq_options = MQOO_INPUT_AS_Q_DEF;
    }

    while (it.hasNext()) {
        it.next();
        QString name = it.name();
        if (name == "input" && it.value().toBool()) 
            mq_options |= MQOO_INPUT_SHARED;
        else if (name == "input_exclusive" && it.value().toBool()) 
            mq_options |= MQOO_INPUT_EXCLUSIVE;
        else if (name == "output" && it.value().toBool()) 
            mq_options |= MQOO_OUTPUT;
    }
    
    MQQueue *pmq =  thisMQManager()->openQueue(qname, mq_options);
    if (!pmq) {
        qFatal("programmatic error:MQManagerPrototype::openQueue");
    }

    //connect 
    if (!pmq->isOpened()) {
        context()->throwError( lastErrorString() );
        return QScriptValue();
    }
    
    return engine()->newQObject(pmq, QScriptEngine::AutoOwnership, 
                                QScriptEngine::SkipMethodsInEnumeration);
}

bool
MQManagerPrototype::isConnected()     const
{
    return thisMQManager()->isConnected();
}

QString
MQManagerPrototype::lastErrorString() const
{
    return thisMQManager()->lastErrorString();
}

long
MQManagerPrototype::lastReasonCode()  const
{
    return thisMQManager()->lastReasonCode();
}

bool
MQManagerPrototype::beginTransaction() const
{
    if (!thisMQManager()->beginTransaction()) {
        context()->throwError( lastErrorString() );
        return false;
    }
    return true;
}

bool
MQManagerPrototype::endTransaction() const
{
    if (!thisMQManager()->endTransaction()) {
        context()->throwError( lastErrorString() );
        return false;
    }
    return true;
}

bool
MQManagerPrototype::abortTransaction() const
{
    if (!thisMQManager()->abortTransaction()) {
        context()->throwError( lastErrorString() );
        return false;
    }
    return true;
}


MQManager *
MQManagerPrototype::thisMQManager() const
{
    MQManager *p = qscriptvalue_cast<MQManager*>(thisObject());

    if (!p) {
        qFatal("Programmatic error MQManagerPrototype::thisMQManager");
    }

    return p;
}
