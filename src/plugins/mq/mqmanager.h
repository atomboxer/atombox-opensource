#ifndef MQMANAGER_H
#define MQMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QList>

#if defined(__MINGW64__) || defined (__MINGW32__)
#include <stddef.h>
#define _int64 __int64
#endif

#include <cmqc.h>

#include "mqqueue.h"

class MQManager : public QObject
{
    Q_OBJECT

public:
    MQManager( QObject *parent=0 );
    ~MQManager();

    bool connect(QString qmanager="");
    bool disconnect();

    MQQueue* openQueue(QString &qname, MQLONG options);

    bool    isConnected() const    { return is_connected_; }
    QString lastErrorString()const { return last_error_; }
    MQLONG  lastReasonCode()const  { return last_reason_code_; }

    bool    beginTransaction();
    bool    endTransaction();
    bool    abortTransaction();

private:
    QString qmanager_;
    bool    is_connected_;
    QString last_error_;
    MQLONG  last_reason_code_;

    MQHCONN  hcon_;                   /* connection handle             */

    QList<MQQueue*> opened_q_list_;
    
};
#endif
