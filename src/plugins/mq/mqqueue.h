#ifndef MQQUEUE_H
#define MQQUEUE_H

#include "mqmessage.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>

#if defined(__MINGW64__) || defined (__MINGW32__)
#include <stddef.h>
#define _int64 __int64
#endif

#include <cmqc.h>

class MQQueue : public QObject
{
    Q_OBJECT

    friend class MQManager;
public:
    MQQueue( QObject *parent=0 );
    ~MQQueue();

public slots:
    bool    close();
    MQRequestMessage*   createRequest();
    MQMessage*          createDatagram();

    bool    isOpened() const        { return is_opened_; }
    QString lastErrorString() const { return last_error_; }
    MQLONG  lastReasonCode()  const { return last_reason_code_; }

signals:
    void requestReady(MQMessage *rq, MQResponseMessage *rs);
    void datagramReady(MQMessage *rq);

    void error(long reason);

//
//  Not accesible from script
//
private:
    bool init(MQHCONN &hcon, MQHOBJ &hobj, QString &queue);

private slots:
    void __internal__timeout();

private:
    void requestReady(MQMD md, QByteArray data);
    void datagramReady(MQMD md, QByteArray data);

private:
    QString  queue_;
    bool     is_opened_;
    QString  last_error_;
    MQLONG   last_reason_code_;

    MQHCONN  hcon_;                   /* connection handle             */
    MQHOBJ   hobj_;                   /* object handle                 */

    QTimer  *timer_;
    
};
#endif
