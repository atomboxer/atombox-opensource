#ifndef MQMESSAGEPROTOTYPE_H
#define MQMESSAGEPROTOTYPE_H

#include <QtCore/QObject>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptable>

#include "mqmessage.h"

class MQMessagePrototype: public QObject, public QScriptable
{
    Q_OBJECT

public:
    MQMessagePrototype(QObject *parent = 0);
    ~MQMessagePrototype();

public slots:
    
    ByteArray data() const { return thisMQMessage()->data(); }

    bool write( QScriptValue chunk) const;
    bool end( QScriptValue chunk=QScriptValue::UndefinedValue ) const;

    QString lastErrorString() const { return thisMQMessage()->lastErrorString(); }
    MQLONG  lastReasonCode()  const { return thisMQMessage()->lastReasonCode(); }

private:
    MQMessage *thisMQMessage() const;
};

#endif //MQMESSAGEPROTOTYPE_H
