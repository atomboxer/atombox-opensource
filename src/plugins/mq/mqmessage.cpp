#include "mqmessage.h"

#include <QtCore/QByteArray>
#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

#define MQMESSAGE_DEBUG

MQMessage::MQMessage(MQHCONN &hcon, MQHOBJ &hobj,
                     QByteArray data, QObject *parent):
    QObject(parent),
    hcon_(hcon),
    hobj_(hobj),
    data_(data)
{
#ifdef MQMESSAGE_DEBUG
  qDebug()<<"MqMessage::MQMessage";
#endif
    MQMD hmd = {MQMD_DEFAULT};

    hmd_ = hmd;
    hmd_.MsgType = MQMT_DATAGRAM;
    hmd_.Report =  MQRO_NONE;

    memset(hmd_.ReplyToQ,0, MQ_Q_NAME_LENGTH);
    memset(hmd_.ReplyToQMgr,0, MQ_Q_MGR_NAME_LENGTH);

    memcpy(hmd_.Format,MQFMT_STRING, (size_t)MQ_FORMAT_LENGTH);
    memcpy(hmd_.CorrelId, MQCI_NONE, sizeof(hmd_.CorrelId));
    memcpy(hmd_.MsgId, MQMI_NONE, sizeof(hmd_.MsgId) );
}

MQMessage::~MQMessage()
{

#ifdef MQMESSAGE_DEBUG
    qDebug()<<"MQMessage::~MQMessage";
#endif

}

bool
MQMessage::write(const QByteArray &chunk)
{
    data_.append(chunk);
    return true;
}
    
bool
MQMessage::end(const QByteArray &chunk)
{
    write(chunk);

    MQPMO   pmo = {MQPMO_DEFAULT};   /* put message options           */


    pmo.Options |=  MQPMO_FAIL_IF_QUIESCING;
    
    MQLONG ccode;
    MQLONG rcode;

    MQPUT(hcon_,               /* connection handle               */
          hobj_,               /* object handle                   */
          &hmd_,               /* message descriptor              */
          &pmo,                /* default options (datagram)      */
          data_.length(),      /* message length                  */
          data_.data(),        /* message buffer                  */
          &ccode,              /* completion code                 */
          &rcode);             /* reason code                     */

    if (rcode != MQRC_NONE) {
        last_error_ = "MQPUT failed with reason "+QString::number(rcode);
        last_reason_code_ = rcode;
        return false;
    }

    return true;
}

//...//
MQRequestMessage::MQRequestMessage(MQHCONN &hcon, MQHOBJ &hobj,
                                   QByteArray data,QObject *parent):
    MQMessage(hcon, hobj, data, parent)
{
#ifdef MQMESSAGE_DEBUG
  qDebug()<<"MQRequest::MQRequestMessage";
#endif
    hmd_.MsgType = MQMT_REQUEST;
}


//..//
MQResponseMessage::MQResponseMessage(MQHCONN &hcon, MQHOBJ &hobj,
                                     QByteArray data, QObject *parent):
    MQMessage(hcon, hobj, data, parent)
{
    hmd_.MsgType = MQMT_REPLY;
}

MQResponseMessage::MQResponseMessage(MQHCONN &hcon, MQHOBJ &hobj, 
				     MQMD &md, 
				     QByteArray data, 
				     QObject *parent)
  : MQMessage(hcon, hobj, data, parent)
{
    hrq_ = md;
    hmd_.MsgType = MQMT_REPLY;
}

bool
MQResponseMessage::end(const QByteArray &chunk)
{

    write(chunk);

#ifdef MQMESSAGE_DEBUG
  qDebug()<<"MQResponseMessage::end";
#endif

    MQOD    odR = {MQOD_DEFAULT};    /* Object Descriptor for reply   */
    MQPMO   pmo = {MQPMO_DEFAULT};   /* put message options           */
    
    strncpy(odR.ObjectName, hrq_.ReplyToQ, MQ_Q_NAME_LENGTH);
    strncpy(odR.ObjectQMgrName,hrq_.ReplyToQMgr, MQ_Q_MGR_NAME_LENGTH);
    
    if ( !(hmd_.Report & MQRO_PASS_CORREL_ID) ) {
        memcpy(hmd_.CorrelId, hrq_.MsgId, sizeof(hmd_.MsgId));
    }

    if ( !(hmd_.Report & MQRO_PASS_MSG_ID) ) {
        memcpy(hmd_.MsgId, MQMI_NONE, sizeof(hmd_.MsgId));
    }

    if (hmd_.Report & MQRO_PASS_DISCARD_AND_EXPIRY) {
        if (hmd_.Report & MQRO_DISCARD_MSG) {
            hmd_.Report = MQRO_DISCARD_MSG;
        } else {
            hmd_.Report = MQRO_NONE;         /*    stop further reports */
        }
    } else {
        hmd_.Report = MQRO_NONE;             /*    stop further reports */
    }

    MQLONG ccode;
    MQLONG rcode;

    MQPUT1(hcon_,           /* connection handle                  */
           &odR,          /* object descriptor                  */
           &hmd_,           /* message descriptor                 */
           &pmo,            /* default options                    */
           data_.length(),  /* message length                     */
           data_.data(),    /* message buffer                     */
           &ccode,          /* completion code                    */
           &rcode);         /* reason code                        */

    /* report reason if any  (loop ends if it failed)    */
    if (rcode != MQRC_NONE) {

#ifdef MQMESSAGE_DEBUG
      qDebug()<<"MQResponseMessage::end -  error - "<<rcode;
#endif
        last_error_ = "MQPUT1 failed with reason "+QString::number(rcode);
        last_reason_code_ = rcode;
        return false;
    }

#ifdef MQMESSAGE_DEBUG
    qDebug()<<"MQResponseMessage::end - all OK"<<hmd_.ReplyToQ
	    <<"."<<hmd_.ReplyToQMgr;
#endif

    return true;
}
