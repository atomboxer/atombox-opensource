#include "mqmessage.h"
#include "mqmessageprototype.h"
#include "binary.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptValueIterator>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(MQMessage*)
Q_DECLARE_METATYPE(MQResponseMessage*)

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

MQMessagePrototype::MQMessagePrototype(QObject *parent)
    : QObject(parent)
{
}

MQMessagePrototype::~MQMessagePrototype()
{
}

//QScriptValue
//MQMessagePrototype::data() const 
//{
//  return ByteArray::toScriptValue(engine(), thisMQMessage()->data());
//}

bool
MQMessagePrototype::write( QScriptValue chunk) const
{
    QByteArray *buf = NULL;
    if (!chunk.isUndefined()) {
        if (chunk.isString())
            return thisMQMessage()->write(chunk.toString().toAscii());

        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());

        if (buf)
            return thisMQMessage()->write(*buf);
    }
    
    context()->throwError("MQMessagePrototype.prototype.write parameter should be String or instanceof Binary");
    return false;
}

bool
MQMessagePrototype::end( QScriptValue chunk ) const
{
    QByteArray *buf = NULL;
    if (!chunk.isUndefined()) {
        if (chunk.isString()) {
            if (!thisMQMessage()->end(chunk.toString().toAscii())) {
                context()->throwError(lastErrorString());
                return false;
            }
            return true;
        }

        buf = qscriptvalue_cast<ByteArray*>(chunk.data());
        if ( !buf )
            buf = qscriptvalue_cast<ByteString*>(chunk.data());

        if (buf) {
            if (!thisMQMessage()->end(*buf)) {
                context()->throwError(lastErrorString());
                return false;
            }
            return true;
        }
    }

    if (!thisMQMessage()->end()) {
        context()->throwError(lastErrorString());
        return false;
    }
    return true;
}

MQMessage*
MQMessagePrototype::thisMQMessage() const
{
    MQMessage *r = qscriptvalue_cast<MQMessage*>(thisObject());
    if (!r) {
        qFatal("programmatic error : thisMQMessage()");
    }
    return r;
}
