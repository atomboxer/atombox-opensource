include(../plugins.pri)

!isEmpty(ATOMBOXER_BUILD_MQ) {
    isEmpty(USE_SCRIPT_CLASSIC) {
        TARGET         = abscriptmq
    } else {
        TARGET         = abscriptmq_compat
    }

    #DESTDIR        = $$ATOMBOXER_SOURCE_TREE/plugins/script
    
    DEPENDPATH  += . 
    INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/main/ $$ATOMBOXER_SOURCE_TREE/src/plugins/core . 
    win32 {
	    INCLUDEPATH += $$quote($$MQ_DIR/tools/c/include)
    }

    unix {
	    INCLUDEPATH += $$MQ_DIR/inc
    }

    HEADERS  = \
        mqplugin.h \
        mqmanager.h \
        mqmanagerprototype.h \
        mqqueue.h \
        mqqueueprototype.h \
        mqmessage.h \
        mqmessageprototype.h
    
    SOURCES  = \
	    mqplugin.cpp \
	    mqmanager.cpp \
	    mqmanagerprototype.cpp \
	    mqqueue.cpp \
	    mqqueueprototype.cpp \
	    mqmessage.cpp \
	    mqmessageprototype.cpp

    RESOURCES += mqfiles.qrc

    static {
        RESOURCES += mq.qrc
        LIBS       += -L$$ATOMBOXER_SOURCE_TREE/lib/

    } else {
        CONFIG += qt plugin

        isEmpty(USE_SCRIPT_CLASSIC) {
            QT += script
            LIBS += -labscriptcore
        } else {
            LIBS += -labscriptcore_compat
        }

        win32 {
            LIBS   +=  $$quote($$MQ_DIR/tools/lib/mqic.lib)
        }

        unix {
	    LIBS   +=  -L$$MQ_DIR/lib64/ -lmqic -lmqm
	}
    }
}

