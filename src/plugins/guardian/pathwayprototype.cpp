#include "pathway.h"
#include "pathwayprototype.h"

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(Pathway*)
Q_DECLARE_METATYPE(ByteArray*)

PathwayPrototype::PathwayPrototype(QObject *parent)
: QObject(parent)
{

}

PathwayPrototype::~PathwayPrototype()
{

}

bool
PathwayPrototype::dialogAbort() const
{
    return thisPathway()->dialogAbort();
}

ByteArray
PathwayPrototype::dialogBegin(ByteArray rqst) const
{
    ByteArray ret = thisPathway()->dialogBegin(rqst);
    if (ret.size() == 0 && thisPathway()->errorString() != "") {
        context()->throwError(thisPathway()->errorString());
    }
    return ret;
}

bool
PathwayPrototype::dialogEnd() const
{
    return thisPathway()->dialogEnd();
}

ByteArray
PathwayPrototype::dialogSend(ByteArray rqst) const
{
    ByteArray ret = thisPathway()->dialogSend(rqst);
    if (ret.size() == 0 && thisPathway()->errorString() != "") {
        context()->throwError(thisPathway()->errorString());
    }
    return ret;
}

ByteArray
PathwayPrototype::send(ByteArray rqst) const
{
    ByteArray ret = thisPathway()->send(rqst);
    if (ret.size() == 0 && thisPathway()->errorString() != "") {
        context()->throwError(thisPathway()->errorString());
    }
    return ret;
}

Pathway *
PathwayPrototype::thisPathway() const
{
    Pathway *p = qscriptvalue_cast<Pathway*>(thisObject());
    if (!p) {
        qFatal("Programmatic error TcpSocketPrototype::thisPathway");
    }

    return p;
}
