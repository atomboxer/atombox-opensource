#include "pathway.h"

#include <cextdecs.h>
#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define PATHWAY_DEBUG

Pathway::Pathway(QString processName,
                 QString serverClassName, 
                 QObject *parent) :
    QObject(parent)
{

    id_              = -1;
    errorString_     = QString();
    processName_     = processName;
    serverClassName_ = serverClassName;
}


Pathway::~Pathway()
{

}

bool
Pathway::dialogAbort()
{
    short error  = SERVERCLASS_DIALOG_ABORT_(id_);
    if (error != 0) {
        setErrors();
        return false;
    }
    return true;
}

ByteArray
Pathway::dialogBegin(ByteArray rqst)
{
    if (id_ != -1) {
        errorString_ = "Dialog already started";
        return ByteArray();
    } else 
        errorString_ = "";

    short rqst_len = rqst.size();
    rqst.resize(32767);
    short resp_len = 0;
                                          
    short error  = SERVERCLASS_DIALOG_BEGIN_(&id_, 
                                             processName_.toLatin1().data(),
                                             processName_.size(),
                                             serverClassName_.toLatin1().data(),
                                             serverClassName_.size(),
                                             rqst.data(),
                                             rqst_len, 
                                             32766,
                                             &resp_len);
    if (error != 70) {
#ifdef PATHWAY_DEBUG
        qDebug()<<"SERVERCLASS_DIALOG_BEGIN_ error:"<<error;
#endif
        if (error == 0) {
            id_ = -1;
        } else if (error == 233) {
            setErrors();
            return ByteArray();
        }
    }

    rqst.resize(resp_len);

#ifdef PATHWAY_DEBUG
    qDebug()<<"SERVERCLASS_DIALOG_BEGIN_ resp:"<<rqst.toHex();
#endif
    return rqst;
}

bool
Pathway::dialogEnd()
{
    short error  = SERVERCLASS_DIALOG_END_(id_);
    if (error != 0) {
        setErrors();
        return false;
    }
    return true;
}

ByteArray
Pathway::dialogSend(ByteArray rqst)
{
    if (id_ == -1) {
        errorString_ = "Dialog not started";
        return ByteArray();
    } else 
        errorString_ = "";

    short rqst_len = rqst.size();
    rqst.resize(32767);
    short resp_len = 0;
                                          
    short error  = SERVERCLASS_DIALOG_SEND_(id_, 
                                            rqst.data(),
                                            rqst_len, 
                                            32766,
                                            &resp_len);
    if (error != 70) {
#ifdef PATHWAY_DEBUG
        qDebug()<<"SERVERCLASS_DIALOG_SEND_ error:"<<error;
#endif
        if (error == 0) {
            id_ = -1;
        } else if (error == 233) {
            setErrors();
            return ByteArray();
        }
    }
    rqst.resize(resp_len);

#ifdef PATHWAY_DEBUG
    qDebug()<<"SERVERCLASS_DIALOG_SEND_ resp:"<<rqst.toHex();
#endif
    return rqst;
}


ByteArray 
Pathway::send(ByteArray rqst)
{

    errorString_ = "";
    short rqst_len = rqst.size();
    rqst.resize(32767);
    short resp_len = 0;
                                          
    short error  = SERVERCLASS_SEND_( processName_.toLatin1().data(),
                                      processName_.size(),
                                      serverClassName_.toLatin1().data(),
                                      serverClassName_.size(),
                                      rqst.data(),
                                      rqst_len, 
                                      32766,
                                      &resp_len);
    if (error != 0) {
#ifdef PATHWAY_DEBUG
        qDebug()<<"SERVERCLASS_BEGIN_ error:"<<error;
#endif
        setErrors();
        if (error == 233)
            return ByteArray();
    }

    rqst.resize(resp_len);

#ifdef PATHWAY_DEBUG
    qDebug()<<"SERVERCLASS_SEND_ resp:"<<rqst.toHex();
#endif
    return rqst;
}

void
Pathway::setErrors()
{
    short path_error;
    short file_error;

    if (SERVERCLASS_SEND_INFO_(&path_error, &file_error) == 0) {
        if (path_error != 0 || file_error != 0) {
            errorString_ = "fileError:"+QString::number(file_error) + 
                ":pathwayError:"+QString::number(path_error);
        } else 
            errorString_ = "";
#ifdef PATHWAY_DEBUG
        qDebug()<<"Pathway::"<<errorString();
#endif
    }
}
