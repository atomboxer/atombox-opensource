#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <QtScript/QScriptEngine>

#include "guardianplugin.h"
#include "gfilesystem.h"

#include "pathway.h"
#include "pathwayprototype.h"

#include <cextdecs.h>
#include <tal.h>

#include <gfsfileenginehandler.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_EXPORT_PLUGIN2( abscriptguardian, GuardianPlugin )

GuardianPlugin::GuardianPlugin( QObject *parent )
    : QScriptExtensionPlugin( parent )
{
}
 	
GuardianPlugin::~GuardianPlugin()
{
}

Q_DECLARE_METATYPE(Pathway*)

QScriptValue 
Pathway_Constructor(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 2) {
        ctx->throwError("<<guardian/pathway.Pathway>> constructor require two arguments");
        return QScriptValue();
    }

    QString processName     = ctx->argument(0).toString();
    QString serverClassName = ctx->argument(1).toString();

    return eng->newQObject(new Pathway(processName, 
                                             serverClassName),
                           QScriptEngine::AutoOwnership,
                           QScriptEngine::SkipMethodsInEnumeration
                           /*| QScriptEngine::ExcludeSuperClassMethods(*/
                           /*| QScriptEngine::ExcludeSuperClassProperties*/);
}

void myMessageOutput(QtMsgType type, const char *msg)
{
     //in this function, you can write the message to any stream!
    switch (type) {
     case QtDebugMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtWarningMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtCriticalMsg:
         fprintf(stdout, "%s\n", msg);
         break;
     case QtFatalMsg:
         fprintf(stdout, "%s\n", msg);
         abort();
     }
     fflush(stdout);
     fflush(stderr);
}
 	
void
GuardianPlugin::initialize( const QString &key, QScriptEngine *engine )
{
    QScriptValue globalObject = engine->globalObject();
    if ( key == QString("guardian") ) {
#ifdef AB_STATIC_PLUGINS
	Q_INIT_RESOURCE(guardian);
#endif
	Q_INIT_RESOURCE(guardianfiles);
    }

    TandemEngineHandler::instance();
    qInstallMsgHandler(myMessageOutput);

    // //
    // //  Get the home terminal and install a break handler
    // //
    // char term_name[239];
    // short term_name_len;
    // short error_detail=0;
    
    // short handle = -1;
    // short error = PROCESS_GETINFO_(&handle, /*1*/,
    //     			   /*2*/,
    //     			   /*3*/,
    //     			   /*4*/,
    //     			   /*5*/,
    //     			   term_name  /*6*/,
    //     			   239  /*6*/,
    //     			   &term_name_len);
    // if (!error) {
    //     short filenum;
    //     error = FILE_OPEN_(term_name, term_name_len, &filenum, 1/*read*/);
    //     if (!error) {
    //         short param_arr[4];
    //         param_arr[0] = 1; /* take break*/
    //         param_arr[1] = 0;
    //         param_arr[2] = 0;
    //         param_arr[3] = 0;	    
    //         error = SETPARAM(filenum, 3 /*BREAK Handling*/,param_arr,8);
    //     }
    // }
    
    engine->globalObject().property("__internal__").
	setProperty("isEntrySequenced", engine->newFunction(__fs_g_isEntrySequenced));
    engine->globalObject().property("__internal__").
	setProperty("isKeySequenced", engine->newFunction(__fs_g_isKeySequenced));
    engine->globalObject().property("__internal__").
	setProperty("isUnstructured", engine->newFunction(__fs_g_isUnstructured));
    engine->globalObject().property("__internal__").
	setProperty("isRelative", engine->newFunction(__fs_g_isRelative));
    engine->globalObject().property("__internal__").
	setProperty("isEdit", engine->newFunction(__fs_g_isEdit));
    
    engine->globalObject().property("__internal__").
        setProperty("fileCode", engine->newFunction(__fs_g_fileCode));
    engine->globalObject().property("__internal__").
        setProperty("physicalRecordSize", engine->newFunction(__fs_g_physicalRecordSize));
    engine->globalObject().property("__internal__").
        setProperty("logicalRecordSize", engine->newFunction(__fs_g_logicalRecordSize));
    engine->globalObject().property("__internal__").
        setProperty("blockSize", engine->newFunction(__fs_g_blockSize));

    engine->globalObject().property("__internal__").
        setProperty("beginTransaction", engine->newFunction(__fs_g_beginTransaction));

    engine->globalObject().property("__internal__").
        setProperty("endTransaction", engine->newFunction(__fs_g_endTransaction));

    engine->globalObject().property("__internal__").
        setProperty("abortTransaction", engine->newFunction(__fs_g_abortTransaction));

    engine->globalObject().property("__internal__").
        setProperty("logEMS", engine->newFunction(__fs_g_logEMS));


    engine->setDefaultPrototype(qMetaTypeId<Pathway*>(),
                                engine->newQObject(new PathwayPrototype(this)));

    engine->globalObject().property("__internal__").
        setProperty("Pathway", engine->newFunction(Pathway_Constructor));

    engine->globalObject().property("__internal__").property("Pathway").
        setProperty("prototype", engine->defaultPrototype(qMetaTypeId<Pathway*>()));

}
 	
QStringList
GuardianPlugin::keys() const
{
    QStringList keys;
    keys << QString("guardian");
    return keys;
}
