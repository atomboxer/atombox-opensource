/**
 * @desc This module provides Guardian OS specific functions.
 * @module guardian/fs
 * @summary Guardian specific filesystem
 * @requires module:fs
 * @example
var gfs = require("guardian/fs");

gfs.blockSize("$SYSTEM.SYSTEM.locale"); //4096
gfs.isUnstructured("$SYSTEM.SYSTEM.locale");   //true
 */

//guardian
require("fs");

/**
 * Begins a TMF transaction
 * @method module:guardian/fs.beginTransaction
 */
exports.beginTransaction = __internal__.beginTransaction;
/**
 * Aborts the last TMF transaction
 * @method module:guardian/fs.abortTransaction
 */
exports.abortTransaction = __internal__.abortTransaction;

/**
 * Ends the last TMF transaction
 * @method module:guardian/fs.endTransaction
 */
exports.endTransaction   = __internal__.endTransaction;

/**
 * @method module:guardian/fs.isEntrySequenced
 * @arg {String} path
 * @return {Boolean}
 */
exports.isEntrySequenced = __internal__.isEntrySequenced;

/**
 * @method module:guardian/fs.isKeySequenced
 * @arg {String} path
 * @return {Boolean}
 */
exports.isKeySequenced = __internal__.isKeySequenced;

/**
 * @method module:guardian/fs.isUnstructured
 * @arg {String} path
 * @return {Boolean}
 */
exports.isUnstructured = __internal__.isUnstructured;

/**
 * @method module:guardian/fs.isRelative
 * @arg {String} path
 * @return {Boolean}
 */
exports.isRelative = __internal__.isRelative;

/**
 * @method module:guardian/fs.isEdit
 * @arg {String} path
 * @return {Boolean}
 */
exports.isEdit = __internal__.isEdit;

/**
 * @method module:guardian/fs.fileCode
 * @arg {String} path
 * @return {Number}
 */
exports.fileCode = __internal__.fileCode;

/**
 * @method module:guardian/fs.physicalRecordSize
 * @arg {String} path
 * @return {Number}
 */
exports.physicalRecordSize = __internal__.physicalRecordSize;

/**
 * @method module:guardian/fs.logicalRecordSize
 * @arg {String} path
 * @return {Number}
 */
exports.logicalRecordSize  = __internal__.logicalRecordSize;
exports.blockSize  = __internal__.blockSize;

/**
 *  @method module:guardian/fs.logEMS
 *  @desc Logs an event to Tandem EMS. The severity of the event is based on the event_number value as follows: 0-99 - Informative, 100-199 - Major, 300-399 - Fatal 
 *  @arg {String} collector
 *  @arg {String} text
 *  @arg {Number} event_number
 *  @example
var system  = require("system");
var gfs     = require("guardian/fs");

gfs.logEMS("$0", "Hello world!", 50);

system.exit(0);
 */
exports.logEMS = __internal__.logEMS;
