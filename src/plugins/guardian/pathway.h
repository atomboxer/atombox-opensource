#ifndef PATHWAY_H
#define PATHWAY_H

#include <QtCore/QObject>

#include "binary.h"

class Pathway : public QObject
{
    Q_OBJECT
    friend class PathwayPrototype;

public:
    Pathway(QString processName,
            QString serverClassName, 
            QObject *parent = 0);
    ~Pathway();

public:
    bool      dialogAbort();
    ByteArray dialogBegin(ByteArray rqst);
    bool      dialogEnd();
    ByteArray dialogSend(ByteArray rqst);
    QString   errorString() const {return errorString_; }
    ByteArray send(ByteArray rqst);

private:
    void setErrors();

private:
    qint32  id_;
    QString errorString_;
    QString processName_;
    QString serverClassName_;
    
};

#endif //PATHWAY_H
