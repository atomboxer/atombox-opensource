#ifndef PATHWAYPROTOTYPE_H
#define PATHWAYPROTOTYPE_H

#include "pathway.h"
#include "binary.h"

#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class PathwayPrototype : public QObject, public QScriptable
{
    Q_OBJECT

public:
    PathwayPrototype(QObject *parent = 0);
    ~PathwayPrototype();

public slots:
    bool      dialogAbort() const;
    ByteArray dialogBegin(ByteArray rqst) const;
    bool      dialogEnd() const;
    ByteArray dialogSend(ByteArray rqst) const;
    QString   errorString() const {return thisPathway()->errorString(); }
    ByteArray send(ByteArray rqst) const;

private:
    Pathway * thisPathway() const;
};

#endif //PATHWAYPROTOTYPE_H
