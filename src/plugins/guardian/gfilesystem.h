#ifndef GFILESYSTEM_H
#define GFILESYSTEM_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

#ifdef _GUARDIAN_TARGET
QScriptValue __fs_g_isEntrySequenced(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_isKeySequenced(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_isUnstructured(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_isRelative(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_isEdit(QScriptContext *, QScriptEngine *);

QScriptValue __fs_g_fileCode(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_physicalRecordSize(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_logicalRecordSize(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_blockSize(QScriptContext *, QScriptEngine *);

QScriptValue __fs_g_beginTransaction(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_endTransaction(QScriptContext *, QScriptEngine *);
QScriptValue __fs_g_abortTransaction(QScriptContext *, QScriptEngine *);

QScriptValue __fs_g_logEMS(QScriptContext *, QScriptEngine *);
#endif
#endif //GFILESYSTEM_H
