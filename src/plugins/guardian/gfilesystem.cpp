#include "gfilesystem.h"

#ifdef _GUARDIAN_TARGET
#include "gfsfileengine.h"
#include "gfsfileenginehandler.h"

#include <cextdecs.h>
#include <tal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include "ZSPIC"
#include "ZEMSC"
#include "ZSYSC" 
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//
// guardian shit
//
QScriptValue
__fs_g_physicalRecordSize(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.physicalRecordSize>> requires one argument");
        return QScriptValue();
    }

    short in_list[1];
    in_list[0] = 34;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.physicalRecordSize>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    return out_list;

}

QScriptValue
__fs_g_logicalRecordSize(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.logicalRecordSize>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 43;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.logicalRecordSize>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    return out_list;
}

QScriptValue
__fs_g_blockSize(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.blockSize>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 44;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.blockSize>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    return out_list;
}

QScriptValue
__fs_g_isEntrySequenced(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.isEntrySequenced>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 41;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.isEntrySequenced>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    if (out_list == 2)
        return true;

    return false;
}

QScriptValue
__fs_g_isKeySequenced(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.isKeySequenced>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 41;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.isKeySequenced>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    if (out_list == 3)
        return true;

    return false;
}

QScriptValue
__fs_g_isUnstructured(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.isUnstructured>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 41;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.isUnstructured>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    if (out_list == 0)
        return true;

    return false;
}

QScriptValue
__fs_g_isRelative(QScriptContext *ctx, QScriptEngine *)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.isRelative>> requires one argument");
        return QScriptValue();
    }
    short in_list[1];
    in_list[0] = 41;
    short out_list;

    short error = FILE_GETINFOLISTBYNAME_ (path.toLatin1().data(),
                                           path.size(),
                                           in_list,
                                           1,
                                           (short*)&out_list,
                                           2);
    if (error) {
        ctx->throwError("<<fs/guardian.isRelative>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    if (out_list == 1)
        return true;

    return false;
}

QScriptValue
__fs_g_isEdit(QScriptContext *ctx, QScriptEngine *eng)
{
    if (__fs_g_fileCode(ctx, eng).toInt32() == 101)
        return true;
    
    return false;
}

QScriptValue
__fs_g_fileCode(QScriptContext *ctx, QScriptEngine *eng)
{
    QString path = ctx->argument(0).toString();
    if(path.isEmpty() || ctx->argumentCount() != 1) {
        ctx->throwError("<<fs/guardian.isfileCode>> requires one argument");
        return QScriptValue();
    }
    short out_list[5];

    short error = FILE_GETINFOBYNAME_ (path.toLatin1().data(),
                                       path.size(),
                                       out_list  );
    if (error) {
        ctx->throwError("<<fs/guardian.fileCode>> guardian error:"+QString::number(error));
        return QScriptValue();
    }

    
    return out_list[4];
}

QScriptValue __fs_g_beginTransaction(QScriptContext *, QScriptEngine *)
{
    long tag;
    BEGINTRANSACTION(&tag);
    return (qsreal)tag;
}

QScriptValue __fs_g_endTransaction(QScriptContext *, QScriptEngine *)
{
    ENDTRANSACTION();
    return QScriptValue();
}

QScriptValue __fs_g_abortTransaction(QScriptContext *, QScriptEngine *)
{
    ABORTTRANSACTION();
    return QScriptValue();
}

#ifdef _GUARDIAN_TARGET
bool
send_to_log(char *collector, char *msgbuf, short evnum)
{

#define STOP 0
#define ABEND 1
#define SUCCESS 0
#define WARNING 1
#define FATAL 2

#define   SSID      zspi_ddl_ssid_def
SSID      mysubsys_val_ssid;
SSID      ems_val_ssid;;
#define   pmysubsys_val_ssid   (short _far *)&mysubsys_val_ssid;


	short error;
	short collect;
	short Usedlen;
	short sEventType;
    short   sTemp;
#define   p_sTemp   (char _far *)&sTemp
  short   sActionID = 1;
#define   p_sActionID  (char _far *)&sActionID

	zems_ddl_evt_buffer_def emsbuf;
#define   OMITTED
#define   INFORMATIVE_EVENT    1
#define   MAJOR_EVENT          2
#define   CRITICAL_EVENT       4
	
	memcpy(&ems_val_ssid.z_owner, ZSPI_VAL_TANDEM, sizeof(ems_val_ssid.z_owner));
    ems_val_ssid.z_number  = ZSPI_SSN_ZEMS;
    ems_val_ssid.z_version = ZEMS_VAL_VERSION;

	memcpy(&mysubsys_val_ssid.z_owner, "ATOMBOX ", sizeof("ATOMBOX "));
    mysubsys_val_ssid.z_number  = 1;
    mysubsys_val_ssid.z_version = 1;

    /*
     * Create text to be sent to the collector
     */
	
    char    SubjectFieldName[]={"SUBJECT-FIELD-NAME     "};
	error = EMSINIT( (short *)&emsbuf,ZEMS_VAL_EVT_BUFLEN,
                     (short _far*)&mysubsys_val_ssid,
                     evnum,
                     0x0118270E,
                     SubjectFieldName
                     
		);
	
	if (error)
	{
        return false;
	}
	
    
    if ( 100 > evnum )
        sEventType    = INFORMATIVE_EVENT;
    else
    if ( 200 > evnum )
        sEventType = MAJOR_EVENT;
    else
        sEventType = CRITICAL_EVENT;

/*--------------------------------------------------------------------
-- if this is a critical event, set EMS Emphasis
----------------------------------------------------------------------*/
  if ( CRITICAL_EVENT == sEventType )
    { sTemp = ZSPI_VAL_TRUE;
      error = EMSADDTOKENS ( (short *)&emsbuf, (short *)&ems_val_ssid,
                              ZEMS_TKN_EMPHASIS, p_sTemp );
      if ( error )
          return false;
     }

/*--------------------------------------------------------------------
-- if this event isn't simple Info, then set Action Needed
----------------------------------------------------------------------*/
  if ( MAJOR_EVENT >= sEventType )
    { sTemp = ZSPI_VAL_TRUE;
      error = EMSADDTOKENS ( (short *)&emsbuf, (short *)&ems_val_ssid,
                              ZEMS_TKN_ACTION_NEEDED, p_sTemp, OMITTED,
                              ZEMS_TKN_ACTION_ID, p_sActionID );
      if ( error )
           return false;
     }

	error = EMSADDTOKENS( (short *)&emsbuf,(short *)&ems_val_ssid,
                          ZEMS_TKN_TEXT, msgbuf,  (short )strlen(msgbuf)
		);
	
	if (error)
	{
        return false;
	}
	
	error = SSGETTKN((short *)&emsbuf, ZSPI_TKN_USEDLEN, (char *)&Usedlen);
	
	if (error)
    {
        return false;
	}
	
	error = FILE_OPEN_(collector,  (short )strlen(collector), &collect);
	if (error) {
        return false;
	}
	
	WRITEREADX( collect, (char *)&emsbuf, Usedlen, 0 );
	
	FILE_GETINFO_(collect, (short *)&error);
	
	if ( ! (( error == 0) || (error == 70)))
	{
        return false;
	}
	
	FILE_CLOSE_(collect);
	return true;
}  


QScriptValue
__fs_g_logEMS(QScriptContext *ctx, QScriptEngine *)
{
    QString coll = ctx->argument(0).toString();
    QString text = ctx->argument(1).toString();
    QString event = ctx->argument(2).toString();

    if(coll.isEmpty() || text.isEmpty() || ctx->argumentCount() != 3) {
        ctx->throwError("<<fs/guardian.logEMS>> requires three arguments");
        return QScriptValue();
    }

    if( !send_to_log(coll.toLatin1().data(), text.toLatin1().data(), event.toShort())) {
        ctx->throwError("<<fs/guardian.logEMS>> unable to send msg to EMS");
        return QScriptValue();
    }

    return QScriptValue();
}
#endif //_GUARDIAN_TARGET
