#ifndef GFSFILEENGINE_P_H
#define GFSFILEENGINE_P_H

#include "qplatformdefs.h"

#include "gfsfileengine.h"

#include <QtCore/qfsfileengine.h>
#include <QtCore/QAbstractFileEngine>
#include <QtCore/QHash>
#include <QtCore/QBuffer>


#include <algorithm> // for std::min

#define MAX_BLOCK_SIZE  (20*4096) 

//(1024*52)
//(52*1024) //52

class CircularBuffer
{
public:
    CircularBuffer(size_t capacity=2*MAX_BLOCK_SIZE);
    ~CircularBuffer();

    size_t size() const { return size_; }
    size_t capacity() const { return capacity_; }
    // Return number of bytes written.
    size_t write(const char *data, size_t bytes);
    // Return number of bytes read.
    size_t read(char *data, size_t bytes);

private:
    size_t beg_index_, end_index_, size_, capacity_;
    char *data_;
};

CircularBuffer::CircularBuffer(size_t capacity)
: beg_index_(0)
, end_index_(0)
    , size_(0)
    , capacity_(capacity)
{
    data_ = new char[capacity];
    memset(data_, 41, capacity);
}

CircularBuffer::~CircularBuffer()
{
    delete [] data_;
}

size_t CircularBuffer::write(const char *data, size_t bytes)
{
    if (bytes == 0) return 0;

    size_t capacity = capacity_;
    size_t bytes_to_write = std::min(bytes, capacity - size_);

    // Write in a single step
    if (bytes_to_write <= capacity - end_index_)
    {
        memcpy(data_ + end_index_, data, bytes_to_write);
        end_index_ += bytes_to_write;
        if (end_index_ == capacity) end_index_ = 0;
    }
    // Write in two steps
    else
    {
        size_t size_1 = capacity - end_index_;
        memcpy(data_ + end_index_, data, size_1);
        size_t size_2 = bytes_to_write - size_1;
        memcpy(data_, data + size_1, size_2);
        end_index_ = size_2;
    }

    size_ += bytes_to_write;
    return bytes_to_write;
}

size_t CircularBuffer::read(char *data, size_t bytes)
{
    if (bytes == 0) return 0;

    size_t capacity = capacity_;
    size_t bytes_to_read = std::min(bytes, size_);

    // Read in a single step
    if (bytes_to_read <= capacity - beg_index_)
    {
        memcpy(data, data_ + beg_index_, bytes_to_read);
        beg_index_ += bytes_to_read;
        if (beg_index_ == capacity) beg_index_ = 0;
    }
    // Read in two steps
    else
    {
        size_t size_1 = capacity - beg_index_;
        memcpy(data, data_ + beg_index_, size_1);
        size_t size_2 = bytes_to_read - size_1;
        memcpy(data + size_1, data_, size_2);
        beg_index_ = size_2;
    }

    size_ -= bytes_to_read;
    return bytes_to_read;
}


class QAbstractFileEnginePrivate
{
public:

    inline QAbstractFileEnginePrivate()
        : fileError(QFile::UnspecifiedError)
    {
    }
    inline virtual ~QAbstractFileEnginePrivate() { }

    QFile::FileError fileError;
    QString errorString;

    QAbstractFileEngine *q_ptr;
    Q_DECLARE_PUBLIC(QAbstractFileEngine)
};

class GFSFileEnginePrivate : public QAbstractFileEnginePrivate
{
    Q_DECLARE_PUBLIC(GFSFileEngine)
public:
    struct GuardianStat {
    public:
    GuardianStat() : file_size(0), ts_atime(0), ts_ctime(0), ts_mtime(0), ownerId(0), updateMode(false), prikeyOfst(0), prikeyLen(0), fileType(0), structuredMode(false) {}

        qint64 file_size;
        short  security[4];
        qint64 ts_atime;
        qint64 ts_ctime;
        qint64 ts_mtime;
        QByteArray owner;
        uint       ownerId;

        //guardian
        bool       updateMode;
        short      prikeyOfst;
        short      prikeyLen;
        
        short      fileType; //0 - Unstructured, 1 - relative. 2 - entry seq, 3 - key seq, -1 - nondisk file
        bool       structuredMode;
    };

    static QString canonicalized(const QString &path);

    void nativeInitFileName();
    bool nativeOpen(QIODevice::OpenMode openMode);
    bool openFh(QIODevice::OpenMode flags, FILE *fh_);
    bool openFd(QIODevice::OpenMode flags, int fd_);
    bool nativeClose();
    bool closeFdFh();
    bool nativeFlush();
    bool flushFh();
    qint64 nativeSize() const;
    qint64 sizeFdFh() const;
    qint64 nativePos() const;
    qint64 posFdFh() const;
    bool nativeSeek(qint64);
    bool seekFdFh(qint64);
    qint64 nativeRead(char *data, qint64 maxlen);
    qint64 readFdFh(char *data, qint64 maxlen);
    qint64 nativeReadLine(char *data, qint64 maxlen);
    qint64 readLineFdFh(char *data, qint64 maxlen);
    qint64 nativeWrite(const char *data, qint64 len);
    bool   waitForReadyRead(qint32 msecs);
    qint64 writeFdFh(const char *data, qint64 len);
    int nativeHandle() const;
    bool nativeIsSequential() const;
    bool isSequentialFdFh() const;

public:
    QString filePath;
    mutable QByteArray nativeFilePath;
    QIODevice::OpenMode openMode;
        
    //
    //  Guardian specifics
    //
    CircularBuffer g_circularBufferCache;
    long long      rba_;
    
    FILE *fh_;
    short fd_;
        
    mutable bool  eof_;
    mutable GuardianStat st_;
    
    enum last_io_command_
    {
        IOFlushCommand,
        IOReadCommand,
        IOWriteCommand
    };
        
    last_io_command_  last_io_command_;
    bool last_flush_failed_;
    bool close_file_handle_;

    mutable uint is_sequential_ : 2;
    mutable uint could_stat_ : 1;
    mutable uint tried_stat_ : 1;

    bool doStat() const;

protected:
    GFSFileEnginePrivate();

    void init();

    QAbstractFileEngine::FileFlags getPermissions(QAbstractFileEngine::FileFlags type) const;
};
#endif // GFSFILEENGINE_P_H
