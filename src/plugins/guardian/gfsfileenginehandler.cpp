#include <QtCore/QDebug>

#include "gfsfileenginehandler.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

TandemEngineHandler* TandemEngineHandler::instance_ = 0;

TandemEngineHandler* 
TandemEngineHandler::instance()
{
    if (!instance_) {
	instance_ = new TandemEngineHandler();
    }
    return instance_;
}

QAbstractFileEngine *
TandemEngineHandler::create(const QString &fileName) const
{
    if (fileName.contains(':'))
	return 0;

    return new GFSFileEngine(fileName);
}
