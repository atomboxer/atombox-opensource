#ifndef GFSFILEENGINEHANDLER_H
#define GFSFILEENGINEHANDLER_H

#include <QtCore/QAbstractFileEngine>
#include <QtCore/QAbstractFileEngineHandler>

#include "gfsfileengine.h"

class TandemEngineHandler : public QAbstractFileEngineHandler
{
public:
    static TandemEngineHandler* instance();

    QAbstractFileEngine *create(const QString &fileName) const;

private:
    static TandemEngineHandler *instance_;
};

#endif //GFSFILEENGINEHANDLER_H
