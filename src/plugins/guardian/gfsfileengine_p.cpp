#include "gfsfileengine_p.h"

#include "qplatformdefs.h"

#include <QtCore/QAbstractFileEngine>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QDateTime>
#include <QtCore/QVarLengthArray>

#include <cextdecs.h>
#include <tal.h>

#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define DEBUG_GFSFILEENGINE_P

static inline QByteArray openModeToFopenMode(QIODevice::OpenMode flags,
                                             const QByteArray &fileName)
{
    QByteArray mode;
    if ((flags & QIODevice::ReadOnly) && !(flags & QIODevice::Truncate)) {
        mode = "r";
        if (!(flags & QIODevice::Text)) 
            mode += "b";
        if (flags & QIODevice::WriteOnly) {
            QT_STATBUF statBuf;
            if (!fileName.isEmpty()
                && QT_STAT(fileName, &statBuf) == 0
                && (statBuf.st_mode & S_IFMT) == S_IFREG) {
                mode += '+';
            } else {
                mode = "w";
                if (!(flags & QIODevice::Text)) 
                    mode += "b";
                mode += "+";
            }
        }
    } else if (flags & QIODevice::WriteOnly) {
        mode = "w";
        if (!(flags & QIODevice::Text)) 
            mode += "b";

        if (flags & QIODevice::ReadOnly)
            mode += '+';
    }
    if (flags & QIODevice::Append) {
        mode = "a";
        if (!(flags & QIODevice::Text)) 
            mode += "b";
        if (flags & QIODevice::ReadOnly)
            mode += '+';
    }

    return mode;
}

void
GFSFileEnginePrivate::nativeInitFileName()
{
    nativeFilePath = filePath.toLatin1().data();
}

bool
GFSFileEnginePrivate::nativeOpen(QIODevice::OpenMode openMode)
{
    Q_Q(GFSFileEngine);
    
    short error;
    fd_ = -1;
    fh_ = 0;
    eof_ = false;


    QByteArray fopenMode = openModeToFopenMode(openMode, nativeFilePath.constData());

    //
    //  Guardian...
    //
    nativeFilePath = nativeFilePath.replace("/","\\");
    nativeFilePath = nativeFilePath.replace(" ","");

    //
    //  Guess what kind of file is this
    //
    short vector[5];
    error = FILE_GETINFOBYNAME_(nativeFilePath.constData(), 
                                nativeFilePath.length(),
                                vector);

    if (error && error != 11) { //does not exit
        q->setError( QFile::OpenError,"Guardian error: "+QString::number(error));
        return false; 
    }

    bool disk_file = (vector[3] == -1)?false:true;
    if (error == 11 )
        disk_file = true;

    if (disk_file /*&&
                    openMode & QIODevice::Text*/) {

        fh_ = QT_FOPEN(nativeFilePath.constData(), fopenMode.constData());

        // On failure, return and report the error.
        if (fh_) {
            // Seek to the end when in Append mode.
            if (openMode & QIODevice::Append) {
                int ret;
                do {
                    ret = QT_FSEEK(fh_, 0, SEEK_END);
                } while (ret == -1 && errno == EINTR);
                
                if (ret == -1) {
                    q->setError(errno == EMFILE ? QFile::ResourceError : QFile::OpenError,
                                qt_error_string(int(errno)));
                    return false;
                }
            }

            close_file_handle_ = true;
            this->openMode = openMode;
            st_.structuredMode = false;
            return true;
        } else {
            if (openMode & QIODevice::Text) {
                short vector[5];
                error = FILE_GETINFOBYNAME_(nativeFilePath.constData(), 
                                            nativeFilePath.length(),
                                            vector);
                
                if (error) { //does not exit
                    q->setError( QFile::OpenError,"Guardian error: "+QString::number(error));
                    return false; 
                }
            }      
        }
    }

    //
    //  Let's do the guardian magic
    //
    fh_ = 0;
    fd_ = -1;

    this->openMode = openMode;

    short access;
    if (openMode & QFile::ReadOnly &&
        openMode & QFile::WriteOnly) {
        access = 0;
    } else if (!openMode & QFile::ReadOnly) {
        access = 2;
    } else {
        access = 1; //ReadOnly 
    }

    short exclusion = 0;
    
    if(openMode & 0x40) { //Update
        st_.updateMode = true;
    }

    if(openMode & 0x80) { //Exclusive
        exclusion = 1;
    }

    if(openMode & 0x100) { //Structured
        st_.structuredMode = true;
    } else 
        st_.structuredMode = false;

    unsigned short open_options = 0;

    if (st_.structuredMode == false && disk_file) {
        open_options = 40960;
    }

    unsigned nowait_mask = 16384;

    open_options |= nowait_mask;

    error = FILE_OPEN_(nativeFilePath.constData(),
                       nativeFilePath.length(),
                       &fd_,
                       access,
                       exclusion,   //exclusion:shared
                       1,  //nowait-depth
                       1,  //sync-or-receive-depth
                       open_options
        );

    if (error != 0) {
        q->setError( QFile::OpenError,"Guardian error: "+QString::number(error));
        return false;
    }
    _cc_status CC = AWAITIOX(&fd_);
    if (!_status_eq(CC)) {
        FILE_GETINFO_(fd_, &error);
        q->setError( QFile::OpenError,"Guardian error: "+QString::number(error));
        return false;
    }

    if (fd_ < 0 ) {
        q->setError( QFile::OpenError,"Guardian error: "+QString::number(error));
        return false;
    }

    FILE_GETINFO_(fd_, &error);
    if (error != 0)
        qWarning()<<nativeFilePath.constData()<<" guardian warning:"<<error;

    close_file_handle_ = true;

    doStat();

#ifdef   DEBUG_GFSFILEENGINE_P
    qDebug()<<"gfsfileengine_p.cpp: file:"<<nativeFilePath<<" structured mode:"<<st_.structuredMode;
    qDebug()<<"gfsfileengine_p.cpp: file:"<<nativeFilePath<<" update mode:"<<st_.updateMode;
    qDebug()<<"gfsfileengine_p.cpp: file:"<<nativeFilePath<<" mode:"<<openMode;
#endif

    if ((st_.fileType == -1 ||
         st_.fileType == 0) && 
        st_.structuredMode) {

        qDebug()<<nativeFilePath.constData()<<"Cannot open an unstructured file in structured mode:"<<error;
        q->setError( QFile::OpenError,qt_error_string(int(errno)));
        nativeClose();
        return false;
    }


    //  Large transfer mode
    //
    if (disk_file && 
        !st_.structuredMode) {
        _cc_status status = SETMODE(fd_, 141, 1);
        if ( _status_lt(status) ) {
            qWarning()<<"error setting large transfer mode";
        }

#ifdef   DEBUG_GFSFILEENGINE_P
        qDebug()<<"gfsfileengine_p.cpp: file:"<<nativeFilePath<<" large transfer mode set";
#endif
    }

#ifdef   DEBUG_GFSFILEENGINE_P
    qDebug()<<"gfsfileengine_p.cpp: file:"<<nativeFilePath<<" opened successfully";
#endif

    return true;
}

bool
GFSFileEnginePrivate::nativeClose()
{
    if (!fh_ && fd_ == -1) {
        return false;
    }

    if (fh_) {
        closeFdFh();
    } else {
        FILE_CLOSE_(fd_);
        AWAITIOX(&fd_);
    }

    fh_ = 0;
    fd_ = -1;
    return true;
}

bool
GFSFileEnginePrivate::nativeFlush()
{
    return fh_ ? flushFh() : fd_ != -1;
}

qint64
GFSFileEnginePrivate::nativeRead(char *data, qint64 len)
{
    Q_Q(GFSFileEngine);

    if ( fh_ ) {
        size_t readBytes = 0;
        for (int i = 0; i < 2; ++i) {
            // Cross platform stdlib read
            size_t read = 0;
            do {
                read = fread(data + readBytes, 1, size_t(len - readBytes), fh_);
            } while (read == 0 && !feof(fh_) && errno == EINTR);
            if (read > 0) {
                readBytes += read;
                break;
            } else {
                if (readBytes)
                    break;
                readBytes = read;
            }
        }

        if (readBytes == 0 && !feof(fh_)) {
            // if we didn't read anything and we're not at EOF, it must be an error
            q->setError(QFile::ReadError, qt_error_string(int(errno)));
            return -1;
        }
        return readBytes;
    }

    return readFdFh(data, len); // GUARDIAN
}

qint64 
GFSFileEnginePrivate::nativeReadLine(char *data, qint64 maxlen)
{
    return readLineFdFh(data, maxlen);
}

qint64 
GFSFileEnginePrivate::nativeWrite(const char *data, qint64 len)
{
    return writeFdFh(data, len);
}

qint64 
GFSFileEnginePrivate::nativePos() const
{
    return posFdFh();    
}

bool 
GFSFileEnginePrivate::nativeSeek(qint64 pos)
{
    return seekFdFh(pos);
}

int 
GFSFileEnginePrivate::nativeHandle() const
{
    return fh_ ? gfileno(fh_) : fd_;
}

bool 
GFSFileEnginePrivate::nativeIsSequential() const
{
    return isSequentialFdFh();
}

bool 
GFSFileEngine::remove()
{
    Q_D(GFSFileEngine);
    short error;

    error = FILE_PURGE_( d->nativeFilePath.constData(),
                         d->nativeFilePath.length());

    if (error>0) {
        setError(QFile::RemoveError, "QFile::remove, unable to purge file error:"+
                 QString::number(error));
        return false;
    }

    return true;
}

bool 
GFSFileEngine::copy(const QString &newName)
{
    Q_UNUSED(newName);
    setError(QFile::UnspecifiedError, QLatin1String("Not implemented!"));
    return false;
}

bool 
GFSFileEngine::rename(const QString &newName)
{
    Q_D(GFSFileEngine);

    bool ret = ::rename(d->nativeFilePath.constData(), 
                        QFile::encodeName(newName).constData()) == 0;

    if (!ret)
        setError(QFile::RenameError, qt_error_string(errno));

    return ret;
}

bool 
GFSFileEngine::link(const QString &newName)
{
    Q_D(GFSFileEngine);
    setError(QFile::UnspecifiedError, "QFile::link not implemented on Guardian");
    return false;
}

qint64 
GFSFileEnginePrivate::nativeSize() const
{
    doStat();
    return st_.file_size;
}

bool 
GFSFileEngine::mkdir(const QString &name, bool createParentDirectories) const
{
    return false;
}

bool 
GFSFileEngine::rmdir(const QString &name, bool recurseParentDirectories) const
{
    return false;
}

bool 
GFSFileEngine::caseSensitive() const
{
    return false;
}

bool 
GFSFileEngine::setCurrentPath(const QString &path)
{
    return false;
}

QString 
GFSFileEngine::currentPath(const QString &)
{
    QString curr = QDir::currentPath();
    QStringList list = curr.split('/',QString::SkipEmptyParts);
    return list[1]+"."+list[2];
}

QString 
GFSFileEngine::homePath()
{
    return QString();
}

QString 
GFSFileEngine::rootPath()
{
    return QString();
}

QString 
GFSFileEngine::tempPath()
{
    return QString();
}

QFileInfoList 
GFSFileEngine::drives()
{
    QFileInfoList ret;
    return ret;
}

bool 
GFSFileEnginePrivate::doStat() const
{

    //
    //  Guardian...
    //
    nativeFilePath = nativeFilePath.replace("/","\\");

    if (!tried_stat_) {
        tried_stat_ = true;
        could_stat_ = true;
        
        short in[1];
        in[0] = 191; //size
        short out4[4] = {0,0,0,0};

        short error;
        if (fh_) {
            error = FILE_GETINFOLIST_(gfileno(fh_), in, 1, (short*)out4, 8);
        } else if (fd_ != -1){
            error = FILE_GETINFOLIST_(fd_, in, 1, (short*)out4, 8);
        } else {
            error = FILE_GETINFOLISTBYNAME_(nativeFilePath.constData(),
                                            nativeFilePath.length(),
                                            in,
                                            1,
                                            (short*)out4,
                                            8);
        }

        if (error != 0) {
            could_stat_ = false;
        }

        memcpy(&st_.file_size,out4,8);
        
        char cout4[4] = {0,0,0,0};
        in[0] = 62; //security string;
        if (fh_) {
            error = FILE_GETINFOLIST_(gfileno(fh_), in, 1, (short*)cout4, 4);
        } else if (fd_ != -1){   
            error = FILE_GETINFOLIST_(fd_, in, 1, (short*)cout4, 4);
        } else {
            error = FILE_GETINFOLISTBYNAME_(nativeFilePath.constData(),
                                            nativeFilePath.length(),
                                            in,
                                            1,
                                            (short*)cout4,
                                            4);
        }

        st_.security[0] = cout4[0];
        st_.security[1] = cout4[1];
        st_.security[2] = cout4[2];
        st_.security[3] = cout4[3];

        if (error != 0) {
            could_stat_ = false;
        }

        short in_owner[1];
        in_owner[0] = 58;       //owner
        short out_owner;

        if (fh_) {
            error = FILE_GETINFOLIST_( gfileno(fh_),in_owner, 1, 
                                       (short*)&out_owner, 2);
        } else if (fd_ != -1){   
            error = FILE_GETINFOLIST_(fd_, in_owner, 1, (short*)&out_owner, 2);
        } else {
            error = FILE_GETINFOLISTBYNAME_(nativeFilePath.constData(),
                                            nativeFilePath.length(),
                                            in_owner,
                                            1,
                                            (short*)&out_owner,
                                            2);
        }
        
        if (out_owner > 0) {
            //
            //  Get the UserName
            //
            char username[250];
            memset(username, 249,0);
            short userlen = 0;
            qint32 user = out_owner;
            USER_GETINFO_(username, 250,&userlen, &user);
            st_.owner = QByteArray(username,userlen);
        }

        short in_ts[3];
        in_ts[0] = 54;  //ctime
        in_ts[1] = 56;  //atime
        in_ts[2] = 144; //mtime
        qint64 out_ts[3];

        if (fh_) {
            error = FILE_GETINFOLIST_(gfileno(fh_), in_ts, 3, (short*)&out_ts, 3*8);
        } else if (fd_ != -1){   
            error = FILE_GETINFOLIST_(fd_, in_ts, 3, (short*)&out_ts, 3*8);
        } else {
            error = FILE_GETINFOLISTBYNAME_(nativeFilePath.constData(),
                                            nativeFilePath.length(),
                                            in_ts,
                                            3,
                                            (short*)&out_ts,
                                            3*8);
        }

        if (error != 0) {
            could_stat_ = false;
        }

        st_.ts_ctime = out_ts[0];
        st_.ts_atime = out_ts[1];
        st_.ts_mtime = out_ts[2];

        //
        //  Keys
        //
        short in_key[3];
        in_key[0] = 45; //key offset 2 len
        in_key[1] = 46; //key len    2 len
        char   out_key[250];
        memset(out_key, 250, 0);
        error = 0;
        
        if (fd_ >0) {    
            error = FILE_GETINFOLIST_(fd_, in_key, 2, (short*)&out_key, 250);
            memcpy(&st_.prikeyOfst, out_key, 2);
            memcpy(&st_.prikeyLen, out_key+2, 2);
        }

        //
        //
        //
        short in_ft[1];
        in_ft[0] = 41;
        st_.fileType = -1; /*nondisk file*/

        if (fd_ > 0) {
            error = FILE_GETINFOLIST_ (fd_,
                                       in_ft,
                                       1,
                                       (short*)&st_.fileType,
                                       2);
        }

        if (error != 0) {
            could_stat_ = false;
        } 
            
    }
    return could_stat_;
}

QAbstractFileEngine::FileFlags 
GFSFileEnginePrivate::getPermissions(QAbstractFileEngine::FileFlags type) const
{
    Q_Q(const GFSFileEngine);

    QAbstractFileEngine::FileFlags ret = 0;

    //R W E P(not used)
    //0 - any local ID
    //1 - member of owner�s group (local)
    //2 - owner (local)
    //4 - any network user (local or remote)
    //5 - member of owner�s community
    //6 - local or remote user having same ID as owner
    //7 - local super ID only
    
    //read
    switch(st_.security[0]) {
    case 0: ret |= QAbstractFileEngine::ReadOtherPerm;break;
    case 1: ret |= QAbstractFileEngine::ReadGroupPerm;break;
    case 2: ret |= QAbstractFileEngine::ReadOwnerPerm;break;
    case 4: ret |= QAbstractFileEngine::ReadOtherPerm;break;
    case 5: ret |= QAbstractFileEngine::ReadGroupPerm;break;
    case 6: ret |= QAbstractFileEngine::ReadOwnerPerm;break;
    case 7: break;
    }
    //write
    switch(st_.security[1]) {
    case 0: ret |= QAbstractFileEngine::WriteOtherPerm;break;
    case 1: ret |= QAbstractFileEngine::WriteGroupPerm;break;
    case 2: ret |= QAbstractFileEngine::WriteOwnerPerm;break;
    case 4: ret |= QAbstractFileEngine::WriteOtherPerm;break;
    case 5: ret |= QAbstractFileEngine::WriteGroupPerm;break;
    case 6: ret |= QAbstractFileEngine::WriteOwnerPerm;break;
    case 7: break;
    }
    //execute
    switch(st_.security[2]) {
    case 0: ret |= QAbstractFileEngine::ExeOtherPerm;break;
    case 1: ret |= QAbstractFileEngine::ExeGroupPerm;break;
    case 2: ret |= QAbstractFileEngine::ExeOwnerPerm;break;
    case 4: ret |= QAbstractFileEngine::ExeOtherPerm;break;
    case 5: ret |= QAbstractFileEngine::ExeGroupPerm;break;
    case 6: ret |= QAbstractFileEngine::ExeOwnerPerm;break;
    case 7: break;
    }

    QString unixPath = q->fileName(GFSFileEngine::AbsoluteName);
    unixPath.remove(0,unixPath.lastIndexOf('$'));
    unixPath.replace("$","/G/");
    unixPath.replace(".","/");
    
    // calculate user permissions
    if (type & QAbstractFileEngine::ReadUserPerm) {     
        if (QT_ACCESS(unixPath.toLatin1().constData(), R_OK) == 0) {
            ret |= QAbstractFileEngine::ReadUserPerm;
        }
    }
    if (type & QAbstractFileEngine::WriteUserPerm) {
        if (QT_ACCESS(unixPath.toLatin1().constData(), W_OK) == 0)
            ret |= QAbstractFileEngine::WriteUserPerm;
    }
    if (type & QAbstractFileEngine::ExeUserPerm) {
        if (QT_ACCESS(unixPath.toLatin1().constData(), X_OK) == 0)
            ret |= QAbstractFileEngine::ExeUserPerm;
    }

    return ret;
}

QAbstractFileEngine::FileFlags
GFSFileEngine::fileFlags(FileFlags type) const
{
    Q_D(const GFSFileEngine);

    QAbstractFileEngine::FileFlags ret = 0;

    // Force a stat, so that we're guaranteed to get up-to-date results
    if (type & Refresh) {
        d->tried_stat_ = 0;

    }

    if (type & FlagsMask)
        ret |= LocalDiskFlag;

    d->nativeFilePath = d->nativeFilePath.replace("/","\\");

    bool exists = d->doStat();
    if ( !exists )
        return ret;

    if (exists && (type & PermsMask))
        ret |= d->getPermissions(type);

    if (type & TypesMask) {
        ret |= FileType;
    }

    if (type & FlagsMask) {
        if (exists) {
            ret |= ExistsFlag;
        }
    }

    return ret;;
}

QString
GFSFileEngine::fileName(FileName file) const
{
    Q_D(const GFSFileEngine);

    if (file == BundleName) {
        return QString();
    } else if (file == BaseName) {
        int dot = d->filePath.lastIndexOf(QLatin1Char('.'));
        if (dot != -1)
            return d->filePath.mid(dot + 1);
    } else if (file == PathName) {
        int dot = d->filePath.lastIndexOf(QLatin1Char('.'));
        if (dot == -1)
            return QLatin1String(".");
        return d->filePath.left(dot);

    } else if (file == AbsoluteName || file == AbsolutePathName) {
        char full_name[250];
        short full_name_len;

        FILENAME_RESOLVE_(d->filePath.toLatin1().data(),
                          d->filePath.size(),
                          full_name,
                          250,
                          &full_name_len);
        
        QString f =  QByteArray(full_name, full_name_len);
        if (file == AbsoluteName) {
            return f;
        } else {
            return f.remove(f.mid(f.lastIndexOf('.')));
        }
    } else if (file == CanonicalName || file == CanonicalPathName) {
        char  short_name[250];
        short short_name_len;

        FILENAME_UNRESOLVE_(d->filePath.toLatin1().data(),
                            d->filePath.size(),
                            short_name,
                            250,
                            &short_name_len);
        
        QString f =  QByteArray(short_name, short_name_len);
        if (file == CanonicalPathName) {
            return f;
        } else {
            return f.remove(f.mid(f.lastIndexOf('.')));
        }
    }
    return d->filePath;
}

bool 
GFSFileEngine::isRelativePath() const
{
    Q_D(const GFSFileEngine);
    return (d->filePath.lastIndexOf("$") == -1? true:false);
}

uint 
GFSFileEngine::ownerId(FileOwner own) const
{
    Q_D(const GFSFileEngine);

    d->doStat(); //for system & volume part

    if (!d->could_stat_) {
        return 0;
    }

    return d->st_.ownerId;
}

QString
GFSFileEngine::owner(FileOwner own) const
{
    Q_D(const GFSFileEngine);
    
    d->doStat();

    if (!d->could_stat_) {
        return QString();
    }

    return d->st_.owner;
}

bool
GFSFileEngine::setPermissions(uint perms)
{
    Q_D(GFSFileEngine);
    bool ret = false;
    return ret;
}

bool
GFSFileEngine::setSize(qint64 size)
{
    Q_D(GFSFileEngine);
    bool ret = false;
    if (d->fd_ != -1) {
        return ret;
    }
    else if (d->fh_)
        ret = QT_FTRUNCATE(QT_FILENO(d->fh_), size) == 0;
    if (!ret)
        setError(QFile::ResizeError, qt_error_string(errno));
    return ret;
}

QDateTime 
GFSFileEngine::fileTime(FileTime time) const
{
    Q_D(const GFSFileEngine);
    qint32 error;
    short out[8];

    d->doStat();

    QString stime;
    if (d->doStat()) {
        if (time == CreationTime) {
            error = INTERPRETTIMESTAMP(CONVERTTIMESTAMP(d->st_.ts_ctime), out);
        }   
        else if (time == ModificationTime) {
            error = INTERPRETTIMESTAMP(CONVERTTIMESTAMP(d->st_.ts_mtime), out);
        }
        else if (time == AccessTime) {
            error = INTERPRETTIMESTAMP(CONVERTTIMESTAMP(d->st_.ts_atime), out);
        }

        for (int i=0;i<7;i++) {
            stime += QString::number(out[i]);
            stime += '.';
        }
    }

    return QDateTime::fromString(stime, "yyyy.M.d.h.m.s.z.");;
}
