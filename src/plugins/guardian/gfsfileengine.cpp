#include "gfsfileengine_p.h"

#include <QtCore/QDateTime>
#include <QtCore/QDirIterator>
#include <QtCore/QFileInfo>
#include <QtCore/QSet>
#include <QtCore/QDebug>

#include <errno.h>

#include <tal.h>
#include <cextdecs.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


//#define DEBUG_GFSFILEENGINE

GFSFileEnginePrivate::GFSFileEnginePrivate() : QAbstractFileEnginePrivate()
{
    init();
}

void GFSFileEnginePrivate::init()
{
    is_sequential_ = 0;
    tried_stat_ = 0;

    openMode = QIODevice::NotOpen;

    fd_ = -1;
    fh_ = 0;
    last_io_command_ = IOFlushCommand;
    last_flush_failed_ = false;
    close_file_handle_ = false;

    rba_  = 0;
}

QString GFSFileEnginePrivate::canonicalized(const QString &path)
{
    return path;
}

GFSFileEngine::GFSFileEngine(const QString &file) : QAbstractFileEngine(*new GFSFileEnginePrivate)
{
    Q_D(GFSFileEngine);
    d->filePath = file;
    d->nativeInitFileName();
}

GFSFileEngine::GFSFileEngine() : QAbstractFileEngine(*new GFSFileEnginePrivate)
{
}

GFSFileEngine::GFSFileEngine(GFSFileEnginePrivate &dd)
    : QAbstractFileEngine(dd)
{
}

GFSFileEngine::~GFSFileEngine()
{
    Q_D(GFSFileEngine);

    if (d->close_file_handle_) {
        if (d->fh_) {
            int ret;
            ret = fclose(d->fh_);
        }
        if (d->fd_ != -1) {
            FILE_CLOSE_(d->fd_);
            AWAITIOX(&d->fd_);
        }
    }
}

void
GFSFileEngine::setFileName(const QString &file)
{
    Q_D(GFSFileEngine);
    d->init();
    d->filePath = QDir::fromNativeSeparators(file);
    d->nativeInitFileName();
}

bool
GFSFileEngine::open(QIODevice::OpenMode openMode)
{
    Q_D(GFSFileEngine);

    if (d->filePath.isEmpty()) {
        qWarning("GFSFileEngine::open: No file name specified");
        setError(QFile::OpenError, QLatin1String("No file name specified"));
        return false;
    }

// Append implies WriteOnly.
    if (openMode & QFile::Append)
        openMode |= QFile::WriteOnly;

// WriteOnly implies Truncate if neither ReadOnly nor Append are sent.
    if ((openMode & QFile::WriteOnly) && !(openMode & (QFile::ReadOnly | QFile::Append)))
        openMode |= QFile::Truncate;

    d->openMode = openMode;
    d->last_flush_failed_ = false;
    d->tried_stat_ = 0;
    d->fh_ = 0;
    d->fd_ = -1;

    return d->nativeOpen(openMode);
}

bool
GFSFileEngine::open(QIODevice::OpenMode openMode, FILE *fh)
{
    Q_D(GFSFileEngine);
    
// Append implies WriteOnly.
    if (openMode & QFile::Append)
        openMode |= QFile::WriteOnly;

// WriteOnly implies Truncate if neither ReadOnly nor Append are sent.
    if ((openMode & QFile::WriteOnly) && !(openMode & (QFile::ReadOnly | QFile::Append)))
        openMode |= QFile::Truncate;

    d->openMode = openMode;
    d->last_flush_failed_ = false;
    d->close_file_handle_ = false;
    d->nativeFilePath.clear();
    d->filePath.clear();
    d->tried_stat_ = 0;
    d->fd_ = -1;
    d->fh_ = 0;

    return d->openFh(openMode, fh);
}

bool
GFSFileEnginePrivate::openFh(QIODevice::OpenMode openMode, FILE *fh)
{
    Q_Q(GFSFileEngine);

    this->fh_ = fh;
    this->fd_ = -1;

// Seek to the end when in Append mode.
    if (openMode & QIODevice::Append) {
        int ret;
        do {
            ret = QT_FSEEK(fh, 0, SEEK_END);
        } while (ret != 0 && errno == EINTR);

        if (ret != 0) {
            q->setError(errno == EMFILE ? QFile::ResourceError : QFile::OpenError,
                        qt_error_string(int(errno)));

            this->openMode = QIODevice::NotOpen;
            this->fh_ = 0;

            return false;
        }
    }

    return true;
}

bool
GFSFileEngine::open(QIODevice::OpenMode openMode, int fd)
{
    Q_ASSERT(0);
    return false;
}

bool
GFSFileEnginePrivate::openFd(QIODevice::OpenMode openMode, int fd)
{
    Q_Q(GFSFileEngine);

    Q_ASSERT(0);
    return false;
}

bool
GFSFileEngine::close()
{
    Q_D(GFSFileEngine);
//qDebug()<<d->filePath<<"::"<<"close."; 
    d->openMode = QIODevice::NotOpen;
    return d->nativeClose();
}

bool
GFSFileEnginePrivate::closeFdFh()
{
    Q_Q(GFSFileEngine);

    if (fd_ == -1 && !fh_)
        return false;

// Flush the file if it's buffered, and if the last flush didn't fail.
    bool flushed = !fh_ || (!last_flush_failed_ && q->flush());
    bool closed = true;
    tried_stat_ = 0;

// Close the file if we created the handle.
    if (close_file_handle_) {
        int ret;
        do {
            if (fh_) {
// Close buffered file.
                ret = fclose(fh_) != 0 ? -1 : 0;
            } else {
// Close unbuffered file.
                ret = QT_CLOSE(fd_);
            }
        } while (ret == -1 && errno == EINTR);

// We must reset these guys regardless; calling close again after a
// failed close causes crashes on some systems.
        fh_ = 0;
        fd_ = -1;
        closed = (ret == 0);
    }

// Report errors.
    if (!flushed || !closed) {
        if (flushed) {
// If not flushed, we want the flush error to fall through.
            q->setError(QFile::UnspecifiedError, qt_error_string(errno));
        }
        return false;
    }

    return true;
}

bool GFSFileEngine::flush()
{
    Q_D(GFSFileEngine);
    if ((d->openMode & QIODevice::WriteOnly) == 0) {
// Nothing in the write buffers, so flush succeeds in doing
// nothing.
        return true;
    }
    return d->nativeFlush();
}

bool
GFSFileEnginePrivate::flushFh()
{
    Q_Q(GFSFileEngine);

// Never try to flush again if the last flush failed. Otherwise you can
// get crashes on some systems (AIX).
    if (last_flush_failed_)
        return false;

    int ret = fflush(fh_);

    last_flush_failed_ = (ret != 0);
    last_io_command_ = GFSFileEnginePrivate::IOFlushCommand;

    if (ret != 0) {
        q->setError(errno == ENOSPC ? QFile::ResourceError : QFile::WriteError,
                    qt_error_string(errno));
        return false;
    }
    return true;
}

qint64
GFSFileEngine::size() const
{
    Q_D(const GFSFileEngine);
    return d->nativeSize();
}

qint64
GFSFileEnginePrivate::sizeFdFh() const
{
    Q_ASSERT(0);
    return 0;
}

qint64
GFSFileEngine::pos() const
{
    Q_D(const GFSFileEngine);
    return d->nativePos();
}

qint64
GFSFileEnginePrivate::posFdFh() const
{
    qint64 ret = 0;
    if (fh_)
        ret = qint64(QT_FTELL(fh_));
    return ret;
}

bool
GFSFileEngine::seek(qint64 pos)
{
    Q_D(GFSFileEngine);
    return d->nativeSeek(pos);
}

bool 
GFSFileEnginePrivate::seekFdFh(qint64 pos)
{
    Q_Q(GFSFileEngine);

    if (last_io_command_ != GFSFileEnginePrivate::IOFlushCommand && !q->flush())
        return false;

    if (pos < 0 || pos != qint64(QT_OFF_T(pos)))
        return false;

    if (fh_) {
        // Buffered stdlib mode.
        int ret;
        do {
            ret = QT_FSEEK(fh_, QT_OFF_T(pos), SEEK_SET);
        } while (ret != 0 && errno == EINTR);

        if (ret != 0) {
            q->setError(QFile::ReadError, qt_error_string(int(errno)));
            return false;
        }
    } else {
        // Unbuffered stdio mode.
        qWarning() << "QFile::at: Cannot set file position" << pos;
        q->setError(QFile::PositionError, qt_error_string(errno));
        return false;
    }
    return true;
}

int 
GFSFileEngine::handle() const
{
    Q_D(const GFSFileEngine);
    return d->nativeHandle();
}

qint64
GFSFileEngine::read(char *data, qint64 maxlen)
{
    Q_D(GFSFileEngine);

    if (d->last_io_command_ != GFSFileEnginePrivate::IOReadCommand) {
        flush();
        d->last_io_command_ = GFSFileEnginePrivate::IOReadCommand;
    }

    qint64 ret = d->nativeRead(data, maxlen);
    return ret;
}

qint64
GFSFileEnginePrivate::readFdFh(char *data, qint64 len)
{
    Q_Q(GFSFileEngine);

    if (len < 0 || len != qint64(size_t(len))) {
        q->setError(QFile::ReadError, qt_error_string(EINVAL));
        return -1;
    }

    qint64 readBytes = 0;

    if ( fh_ ) {
        // Buffered stdlib mode.
        size_t result;
        bool retry = true;
        do {
            result = fread(data + readBytes, 1, size_t(len - readBytes), fh_);
            bool eof;
            eof = feof(fh_);
            if (retry && eof && result == 0) {
                QT_FSEEK(fh_, QT_FTELL(fh_), SEEK_SET); // re-sync stream.
                retry = false;
                continue;
            }
            readBytes += result;
        } while (!eof_ && (result == 0 ? errno == EINTR : readBytes < len));

    } else if (fd_ != -1) {
        //  Guardian Magic
        //

        // buffered sequential reads
        if (q->isSequential() || st_.fileType == -1) {
            qint64 requested = len;
            // read some blocks
            if (requested > MAX_BLOCK_SIZE) {
                q->setError(QFile::ReadError,
                            QString("read error. Read requested more data than the block size."));
                //qDebug()<<"read error. Read requested more data than the block size."
                //        <<requested<<">"<<MAX_BLOCK_SIZE;
                eof_ = true;
                return 0;
            }
        
            bool needs_read = true;
            if (st_.fileType == -1) { // we don't do reads here
                needs_read = false;
            }

            eof_ = false;
            bool at_end = false;
            // qDebug()<<"here::requested::"<<requested<<"size:"<<g_circularBufferCache.size();
            while (!at_end && requested >= g_circularBufferCache.size() && needs_read) {
                char block[MAX_BLOCK_SIZE];
                if (!block) {
                    Q_ASSERT(0);
                }
        
                // default
                short error = 40;
                _cc_status status; 

                unsigned int timeout = 0;
                unsigned short bytes_read = 0;

                //
                // when need allways to read at 2048 boundry
                //
                if (rba_!= 0 && rba_%2048 != 0) {
                    short err = FILE_SETPOSITION_(fd_, rba_ - rba_%2048);
                    AWAITIOX(&fd_,,,,-1);
                }
                
                long t = 0;
                status = READX ( fd_,
                                 block,
                                 MAX_BLOCK_SIZE);

                if  (_status_lt(status)) {
                    qDebug()<<"READUPDATEX error"<<status;
                    return -1;
                }

                // if (_status_eq(status)) {
                //     while ( error == 40 /*no time*/) {
                _cc_status aw_status = AWAITIOX(&fd_,, &bytes_read,,-1);
                if ( _status_lt(aw_status) ) {
                    
                }

                FILE_GETINFO_(fd_, &error);
                //         } else
                //             break;
                //     }
                // }


                if (error == 1 ) {
#ifdef DEBUG_GFSFILEENGINE
                        qDebug()<<"EOF!"<<bytes_read;
#endif
                    at_end = true;
                    eof_ = true;
                }

                if ( bytes_read ) {
                    if (rba_%2048 == 0) {
                        rba_ += bytes_read;
#ifdef DEBUG_GFSFILEENGINE
                        qDebug()<<"rba="<<rba_<<"write:"<<bytes_read;
#endif
                        g_circularBufferCache.write((char*)block, bytes_read);
                    } else {

                        if (bytes_read - (rba_%2048)<=0) {
                            //eof_ = true;
                            at_end = true;
                        } else {
#ifdef DEBUG_GFSFILEENGINE
                        qDebug()<<"Not at %2048="<<rba_<<"write:"<<bytes_read;
#endif

                        g_circularBufferCache.write((char*)(block+(rba_%2048)-2), 
                                                    bytes_read - (rba_%2048)+2);
                        rba_ += bytes_read - (rba_%2048);
                        }
                    }
                } else {
                    ;//qDebug()<<"error:"<<error;
                }
                //free(block);
            }

            if (requested > g_circularBufferCache.size())
                requested = g_circularBufferCache.size();

            if (requested)
                readBytes = g_circularBufferCache.read(data, requested);

            if (g_circularBufferCache.size() == 0)
                eof_ = true;
            else 
                eof_ = false;

        } else {
            // default
            short error = 40;
            _cc_status status; 
            eof_ = false;
            unsigned short bytes = 0;
        
            long t =0;
            //BEGINTRANSACTION(&t);
            status = READX ( fd_,
                             data,
                             len);
 
        
#ifdef DEBUG_GFSFILEENGINE
            qDebug()<<filePath<<"read structured, updateMode:"<<q->isUpdateMode()<<" status"<<status;
#endif
            // if (_status_gt(status)) {
            //     eof_ = true;
            // } else if ( _status_lt(status) ) {
            //     FILE_GETINFO_(fd_, &error);
            //     eof_ = true;
            //     ABORTTRANSACTION();
            //     return 0;
            // }
        
            while (error == 40 /*no time*/) {
                _cc_status aw_status = AWAITIOX(&fd_,,&bytes,,0);
        
#ifdef DEBUG_GFSFILEENGINE
                qDebug()<<filePath<<"read awaitio, status"<<aw_status;
                qDebug()<<filePath<<"current key:"<<q->currentKeyValue();
#endif
                if ( _status_lt(aw_status) ) {
                    FILE_GETINFO_(fd_, &error);
#ifdef DEBUG_GFSFILEENGINE
                    qDebug()<<filePath<<"read awaitio, error"<<error;
#endif
                } else {
                    error = 0;
                }
            }

            FILE_GETINFO_(fd_, &error);
            if (error == 1) {
                eof_ = true;
            }

            //ENDTRANSACTION();
            if ( _status_lt(status) ) {
                FILE_GETINFO_(fd_, &error);
                q->setError(QFile::ReadError, QString("read error: %L1").arg(error));
                eof_ = true;
            }

            readBytes = bytes;
        }
    }

    if ( !eof_ && readBytes == 0 ) {
        readBytes = -1;
        q->setError(QFile::ReadError, qt_error_string(errno));
    }

    return readBytes;
}

qint64
GFSFileEngine::readLine(char *data, qint64 maxlen)
{
    Q_D(GFSFileEngine);

    if (d->last_io_command_ != GFSFileEnginePrivate::IOReadCommand) {
        flush();
        d->last_io_command_ = GFSFileEnginePrivate::IOReadCommand;
    }

    return d->nativeReadLine(data, maxlen);
}

qint64
GFSFileEnginePrivate::readLineFdFh(char *data, qint64 maxlen)
{
    Q_Q(GFSFileEngine);
    Q_ASSERT(0);
    return 0;
}

bool
GFSFileEnginePrivate::waitForReadyRead(qint32 msecs) 
{
    Q_Q(GFSFileEngine);

    //
    //  If we have buffered data we are ready to read
    //
    if (g_circularBufferCache.size()!= 0) {
        return true;
    }

    if (fd_ == -1 || !q->isSequential()) {
        return false;
    }

    //
    //  Guardian magic
    //
    const int requested = 2048;

    // read something
    bool eof = false;
    if (requested > g_circularBufferCache.capacity()) {
        q->setError(QFile::ReadError,
                    QString("read error. Read requested more data than the block size."));
        //qDebug()<<"read error. Read requested more data than the block size.";
        eof = true;
        return false;
    }

    char block[requested];
    _cc_status status; 
    unsigned short bytes_read = 0;
    unsigned short bytes = 0;

    short error;

    long tag = 0;
    //BEGINTRANSACTION(&tag);

    //
    // when need allways to read at 2048 boundry
    //
    if (rba_%2048) {
        FILE_SETPOSITION_(fd_, rba_-(rba_%2048));
    }

    if (st_.updateMode || st_.fileType == -1 /*nondisk file*/) {
        status = READUPDATELOCKX ( fd_,
                                   block,
                                   requested);
    } else if (st_.fileType != -1) {
        status = READX ( fd_,
                         block,
                         requested);
    }
    
    if ( _status_lt(status) ) {
        FILE_GETINFO_(fd_, &error);
    } 

    _cc_status aw_status = AWAITIOX(&fd_, ,&bytes_read, ,msecs);
    if ( _status_lt(aw_status) ) {
        FILE_GETINFO_(fd_, &error);
    } 

    if (error == 1) {
        eof_ = true;
    }

    //ENDTRANSACTION();

    if ( bytes_read ) {
        if (rba_%2048 == 0) {
            rba_ += bytes_read;
            g_circularBufferCache.write((char*)block, bytes_read);
        } else {
            rba_ += bytes_read - (rba_%2048);
            g_circularBufferCache.write((char*)(block+(rba_-rba_%2048)), bytes_read - (rba_-rba_%2048));
        }
        return true;
    }

    return false;
}

qint64
GFSFileEngine::write(const char *data, qint64 len)
{
    Q_D(GFSFileEngine);

    if (d->last_io_command_ != GFSFileEnginePrivate::IOWriteCommand) {
        flush();
        d->last_io_command_ = GFSFileEnginePrivate::IOWriteCommand;
    }
    
    qint64 ret = d->nativeWrite(data, len);
    return ret;
}

qint64 
GFSFileEnginePrivate::writeFdFh(const char *data, qint64 len)
{
    Q_Q(GFSFileEngine);

    if (len < 0 || len != qint64(size_t(len))) {
        q->setError(QFile::WriteError, qt_error_string(EINVAL));
        return -1;
    }

    qint64 writtenBytes = 0;

    if (fh_) {
        // Buffered stdlib mode.
        size_t result;

        do {
            result = fwrite(data + writtenBytes, 1, size_t(len - writtenBytes), fh_);
            writtenBytes += result;
        } while (result == 0 ? errno == EINTR : writtenBytes < len);

        if (len &&  writtenBytes == 0) {
            writtenBytes = -1;
            q->setError(errno == ENOSPC ? QFile::ResourceError :
                        QFile::WriteError, 
                        qt_error_string(errno));
        }

    } else if (fd_ != -1) {
        //
        //  Guardian magic
        //
        _cc_status status;

        unsigned short buf_len = len;
        unsigned short completed = 0;

        short error = 40;

        char *writeread_buffer = NULL;
        if (st_.fileType == -1 )
            writeread_buffer = new char[MAX_BLOCK_SIZE];

        
        long t = 0;
        //BEGINTRANSACTION(&t);

        if (q->isUpdateMode() /*&& st_.fileType != -1 */) {
            status = WRITEUPDATEX ( fd_,
                                    data,
                                    buf_len );
        } else if (st_.fileType != -1) {
            status = WRITEX ( fd_,
                              data,
                              buf_len);
        } else {
            memcpy(writeread_buffer, data, buf_len);
            status = WRITEREADX ( fd_,
                                  writeread_buffer,
                                  buf_len,
                                  32755 );
        }
        
        if (_status_lt(status)) {
            FILE_GETINFO_(fd_, &error);
            q->setError(QFile::ReadError, QString("write error: %L1").arg(error));
            //ABORTTRANSACTION();
            return 0;
        }

        while(error == 40 /*no time*/) {
            _cc_status aw_status = AWAITIOX(&fd_, ,&completed,/*nothing*/,0);
            
            if ( _status_lt(aw_status) ) {
                FILE_GETINFO_(fd_, &error);
            } else {
                //
                //  Done
                //
                if (st_.fileType == -1) {
                    g_circularBufferCache.write(writeread_buffer, completed);
                }
                error = 0;
            }
        }

        FILE_GETINFO_(fd_, &error);
        if (error != 0) {
            if (error == 11) {
                status = WRITEX ( fd_,
                                  data,
                                  buf_len);
                if (_status_lt(status)) {
                    FILE_GETINFO_(fd_, &error);
                    return 0;
                }
                _cc_status aw_status = AWAITIOX(&fd_, ,&completed,/*nothing*/,-1);
                if (_status_lt(aw_status))
                    return -1;
                FILE_GETINFO_(fd_, &error);

            } else if (error == 46) {
                char block[MAX_BLOCK_SIZE];
                if (q->isUpdateMode()) {
                    status = READUPDATEX(fd_,block, 4096);
                } else {
                    status = READX(fd_,block, 4096);
                }
                _cc_status aw_status;
                if (status == 0) {
                    aw_status = AWAITIOX(&fd_, ,&completed,/*nothing*/,-1);
                } else { return -1; }

                status = WRITEX ( fd_,
                                  data,
                                  buf_len );

                if (status == 0)
                    aw_status = AWAITIOX(&fd_, ,&completed,/*nothing*/,-1);
                else 
                    return -1;

                FILE_GETINFO_(fd_, &error);
            }
            if (error != 0) {
                q->setError( QFile::WriteError, "write Guardian error:"+
                             QString::number(error));
                writtenBytes = -1;
                return writtenBytes;
            }
        }

        //ENDTRANSACTION();

        if (st_.fileType == -1 )
            delete []writeread_buffer;

        if (error != 0) {
            q->setError( QFile::WriteError, "write Guardian error:"+
                         QString::number(error));
            writtenBytes = -1;
        }

        writtenBytes = buf_len; 
    }

    return writtenBytes;
}

QAbstractFileEngine::Iterator *
GFSFileEngine::beginEntryList(QDir::Filters filters, 
                              const QStringList &filterNames)
{
    QAbstractFileEngine::Iterator *it = NULL;
    return it;
}

QAbstractFileEngine::Iterator *
GFSFileEngine::endEntryList()
{
    return 0;
}

QStringList 
GFSFileEngine::entryList(QDir::Filters filters, 
                         const QStringList &filterNames) const
{
    return QAbstractFileEngine::entryList(filters, filterNames);
}

bool 
GFSFileEngine::isSequential() const
{
    Q_D(const GFSFileEngine);
    return d->isSequentialFdFh();
}

bool 
GFSFileEnginePrivate::isSequentialFdFh() const
{
    return !(st_.structuredMode);
}

bool 
GFSFileEngine::extension(Extension extension, 
                         const ExtensionOption *option, 
                         ExtensionReturn *output)
{
    Q_D(GFSFileEngine);
    if (extension == AtEndExtension && d->fh_ )
        return feof(d->fh_);
    if (extension == AtEndExtension && d->fd_ ) {
        return d->eof_;
    }
    return false;
}

bool
GFSFileEngine::supportsExtension(Extension extension) const
{
    Q_D(const GFSFileEngine);
    if (extension == AtEndExtension && d->fh_)
        return true;
    if (extension == AtEndExtension && d->fd_)
        return true;
    // if (extension == FastReadLineExtension && d->fh_)
    //     return true;
    // if (extension == FastReadLineExtension && d->fd_ != -1 && isSequential())
    //     return true;
    if (extension == UnMapExtension || extension == MapExtension)
        return false;
    return false;
}

bool
GFSFileEngine::atEnd() const
{
    Q_D(const GFSFileEngine);
    return d->eof_;
}

// Tandem specific
QByteArray
GFSFileEngine::currentKeyValue() const
{
    Q_D(const GFSFileEngine);

    if (!isEnscribe()) {
        qFatal("GFSFileEngine::currentKeyValue set key can be used only for enscribe files.");
        return QByteArray();
    }

    //
    //  Keys
    //
    short in_key[2];
    in_key[0] = 16; //key length
    in_key[1] = 17; //key value

    char   out_key[250];
    short  error = 0;
    
    error = FILE_GETINFOLIST_(d->fd_, in_key, 2, (short*)&out_key, 250);
    if (error>0) {
        return QByteArray();
    }
    short len;
    memcpy(&len,out_key,2);
    return QByteArray(out_key+2, len);
}

bool
GFSFileEngine::isEnscribe() const 
{
    Q_D(const GFSFileEngine);
    return (d->fh_ > 0)?false:true;
}

bool
GFSFileEngine::isKeySequenced() const 
{
    Q_D(const GFSFileEngine);
    return (d->st_.fileType == 3)?true:false;
}

bool
GFSFileEngine::isUpdateMode() const 
{
    Q_D(const GFSFileEngine);
    return d->st_.updateMode;
}


bool
GFSFileEngine::setPosition(long long rec_spec) const 
{
    Q_D(const GFSFileEngine);

    short error;
    error = FILE_SETPOSITION_(d->fd_, rec_spec);
    if ( error > 0 ) {
        qWarning(("GFSFileEngine::setPosition error:"
                  +QString::number(error)).toLatin1().data());
        return false;
    }

    char block[MAX_BLOCK_SIZE];
    _cc_status status;

    long t=0;
    //BEGINTRANSACTION(&t);
    status = READUPDATELOCKX(d->fd_, block, 4096);

    if (_status_lt(status)) {
#ifdef DEBUG_GFSFILEENGINE
        FILE_GETINFO_(d->fd_, &error);
        qDebug()<<d->filePath<<"cannot position (readupdatelock). error"<<error;
#endif
        //ABORTTRANSACTION();
        return false;
    }

    unsigned short bytes_read;
    _cc_status aw_status = AWAITIOX((short *)(&(d->fd_)),, &bytes_read,,-1);
    if ( _status_lt(aw_status)) {
#ifdef DEBUG_GFSFILEENGINE
        FILE_GETINFO_(d->fd_, &error);
        qDebug()<<d->filePath<<"cannot position. error"<<error;
#endif
        return false;
    }

    //ENDTRANSACTION();
    return true;
}

bool
GFSFileEngine::setKey(char *key_val, short key_len, short key_spec) const 
{
    Q_D(const GFSFileEngine);
    if (!isEnscribe()) {
        qFatal("GFSFileEngine::setKey set key can be used only for enscribe(key sequenced) files.");
        return false;
    }
    
    short error;
    long tag = 0;
    // if (isUpdateMode()) {
    //     ABORTTRANSACTION();
    //     BEGINTRANSACTION(&tag);
    // }
    
    d->eof_ = false;

    _cc_status aw_status;
    if (isKeySequenced()) {
        error = FILE_SETKEY_(d->fd_, key_val, key_len, key_spec, 0);
    } else {
        if (currentKeyValue() != QByteArray(key_val, key_len)) {
            return false;
        }

        bool ok;
        error = FILE_SETPOSITION_(d->fd_, QString(QByteArray((char*)key_val, 4).toHex()).toLongLong(&ok,16));
        //aw_status = AWAITIOX((short *)(&(d->fd_)),,,,-1);
        if (!ok)
            error = 1; // to fail on the next one
    }

    if ( error != 0 ) {
        qWarning(("GFSFileEngine::setKey error:"
                  +QString::number(error)).toLatin1().data());
        return false;
    }

    char block[MAX_BLOCK_SIZE];
    _cc_status status;
    _cc_status awstatus;
    
    if (isUpdateMode()) {
        if (currentKeyValue() != QByteArray(key_val, key_len)) {
            return false;
        }
    }


#ifdef DEBUG_GFSFILEENGINE
    qDebug()<<d->filePath<<"set Key:"<<QByteArray(key_val, key_len).data()<<" key spec"<<key_spec<<"err:"<<error;
#endif

    return true;
}

bool
GFSFileEngine::setKey(QByteArray key, short key_spec) const
{
    Q_D(const GFSFileEngine);

    if (isKeySequenced() && 
        key_spec == 0) {
        key.resize(d->st_.prikeyLen);
    }

    return setKey(key.data(), d->st_.prikeyLen, key_spec);
}

void
GFSFileEngine::setUpdateMode(bool flg)
{
    Q_D(const GFSFileEngine);
    d->st_.updateMode = flg;
}

bool
GFSFileEngine::waitForReadyRead(qint32 msecs)
{
    Q_D(GFSFileEngine);
    return d->waitForReadyRead(msecs);
}


bool
GFSFileEngine::lockRecord()
{
    Q_D(GFSFileEngine);
    short error;
    _cc_status status = LOCKREC(d->fd_);
    if ( _status_lt(status) ) {
        FILE_GETINFO_(d->fd_, &error);
        return false;
    }

    _cc_status aw_status = AWAITIOX(&d->fd_, ,, , 1000/*1 second*/);
    if ( _status_lt(aw_status) ) {
        FILE_GETINFO_(d->fd_, &error);
        return false;
    }
    return true;
}

bool
GFSFileEngine::unlockRecord()
{
    Q_D(GFSFileEngine);
    short error;
    _cc_status status = UNLOCKREC(d->fd_,);
    if ( _status_lt(status) ) {
        FILE_GETINFO_(d->fd_, &error);
        return false;
    }

    _cc_status aw_status = AWAITIOX(&d->fd_, ,, , 1000/*1 second*/);
    if ( _status_lt(aw_status) ) {
        FILE_GETINFO_(d->fd_, &error);
        return false;
    }
    return true;
}

