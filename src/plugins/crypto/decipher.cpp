#include "decipher.h"

#include <QtCore/QDebug>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>

#include <iostream>

#include <openssl/des.h>
#include <openssl/err.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

QScriptValue Decipher_ctor(QScriptContext *ctx, QScriptEngine *engine)
 {
     QScriptValue object = engine->newQObject( new Decipher(),
					       QScriptEngine::ScriptOwnership,
					       QScriptEngine::ExcludeChildObjects|
					       QScriptEngine::SkipMethodsInEnumeration );

     object.setPrototype(ctx->callee().property("prototype"));
     return object;
 }

Decipher::Decipher() : 
    QObject(0)
{
    initialised_ = false;
}

Decipher::~Decipher()
{
}

bool
Decipher::init(QScriptValue cipherType, QScriptValue skey)
{
    QByteArray *key_ba = NULL;

    key_ba = qscriptvalue_cast<ByteString*>(skey.data());
    if (!key_ba) {
	key_ba = qscriptvalue_cast<ByteArray*>(skey.data());
    }
    if (!key_ba) {
	error_string_ = "key should be instanceof Binary"; 
	return false;
    }

    cipher_ = EVP_get_cipherbyname(cipherType.toString().toAscii().data());
    if(!cipher_) {
	error_string_ = "Unknown cipher: "+ cipherType.toString();
	return false;
    }
 
    unsigned char key[EVP_MAX_KEY_LENGTH];
    memcpy(&key, key_ba->data(), key_ba->length());

    int key_len = key_ba->length();

    EVP_CIPHER_CTX_init(&ctx_);
    EVP_CipherInit_ex(&ctx_, cipher_, NULL, NULL, NULL, false /*decipher*/);

    if (!EVP_CIPHER_CTX_set_key_length(&ctx_, key_len)) {
	error_string_ =  "Invalid key length " + QString::number(key_len);
	EVP_CIPHER_CTX_cleanup(&ctx_);
	return false;
    }
    EVP_DecryptInit_ex(&ctx_, NULL, NULL,
		       (unsigned char *)key,
		       NULL);

    //EVP_CIPHER_CTX_set_padding(&ctx_, false);

    initialised_ = true;

    return true;
}

bool 
Decipher::init(QScriptValue cipherType, QScriptValue skey, QScriptValue siv)
{
    QByteArray *key_ba = NULL;

    key_ba = qscriptvalue_cast<ByteString*>(skey.data());
    if (!key_ba) {
	key_ba = qscriptvalue_cast<ByteArray*>(skey.data());
    }
    if (!key_ba) {
	error_string_ = "key should be instanceof Binary"; 
	return false;
    }
    
    QByteArray *iv_ba = NULL;
    iv_ba = qscriptvalue_cast<ByteString*>(siv.data());
    if (!iv_ba) {
	iv_ba = qscriptvalue_cast<ByteArray*>(siv.data());
    }

    if (!iv_ba) {
	error_string_ = "iv should be instanceof Binary"; 
	return false;
    }

    cipher_ = EVP_get_cipherbyname(cipherType.toString().toAscii().data());
    if(!cipher_) {
	error_string_ = "Unknown cipher: "+ cipherType.toString();
	return false;
    }

    if ((EVP_CIPHER_iv_length(cipher_) != iv_ba->size()) &&
	!(EVP_CIPHER_mode(cipher_) == EVP_CIPH_ECB_MODE && iv_ba->size() == 0)) {
	error_string_ = "Invalid IV length "+QString::number(iv_ba->size());
	return false;
    }

    EVP_CIPHER_CTX_init(&ctx_);

    if (!EVP_CIPHER_CTX_set_key_length(&ctx_, key_ba->size())) {
	error_string_ =  "Invalid key length " + QString::number(key_ba->size());
	EVP_CIPHER_CTX_cleanup(&ctx_);
	return false;
    }

    EVP_DecryptInit_ex(&ctx_, NULL, NULL,
		       (unsigned char *)key_ba->data(),
		       (unsigned char *)iv_ba->data());
    initialised_ = true;
    return true;
}

QString
Decipher::lastError() const
{
    return error_string_;
}

ByteArray
Decipher::update(QScriptValue binary)
{
    if (!initialised_) {
	return QByteArray();
    }

    QByteArray *data = NULL;

    data = qscriptvalue_cast<ByteString*>(binary.data());
    if (!data) {
	data = qscriptvalue_cast<ByteArray*>(binary.data());
    }

    if (!data) {
	error_string_ = QString("data should be instanceof Binary"); 
	return false;
    }
    
    int  out_len=data->size()+EVP_CIPHER_CTX_block_size(&ctx_);
    unsigned char   *out= new unsigned char[out_len];

    if (!EVP_DecryptUpdate((EVP_CIPHER_CTX*)&ctx_, out, &out_len, (unsigned char*)data->constData(), data->size())) {
	ERR_print_errors_fp(stderr);
	qFatal("problems with decipher");
    }

    ByteArray ret((const char*)out, out_len);

    delete[]out;

    return ret;
}

ByteArray
Decipher::final()
{
    if (!initialised_) {
	return false;
    }

    unsigned char *out = new unsigned char[EVP_CIPHER_CTX_block_size(&ctx_)];
    int out_len = 0;
    memset(out, 0, EVP_CIPHER_CTX_block_size(&ctx_));
    if (!EVP_DecryptFinal_ex((EVP_CIPHER_CTX*)&ctx_,out,&out_len)) {
	qWarning("problem with decipher at final");
    }

    QByteArray ret((const char*)out, out_len);
    delete []out;
    EVP_CIPHER_CTX_cleanup((EVP_CIPHER_CTX*)&ctx_);
    initialised_ = false;

    return ret;
}
