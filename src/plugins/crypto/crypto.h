#ifndef CRYPTO_H
#define CRYPTO_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptContext>
#include <QtScript/QScriptValue>

#include <core/binary.h>

QScriptValue __crypto_checkBlockParity(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_fixBlockParity(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_generateRandomByteArray(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_generateSingleKey(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_generateDoubleKey(QScriptContext *, QScriptEngine *);
QScriptValue __crypto_generateTripleKey(QScriptContext *, QScriptEngine *);

#endif
