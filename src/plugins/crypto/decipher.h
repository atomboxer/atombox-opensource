#ifndef DEDECIPHER_H
#define DEDECIPHER_H

#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <openssl/evp.h>

#include <core/binary.h>


QScriptValue Decipher_ctor(QScriptContext *ctx, QScriptEngine *engine);

class Decipher : public QObject
{
    Q_OBJECT
public:
    Decipher();
    ~Decipher();

 public:
    Q_INVOKABLE bool init(QScriptValue cipherType, QScriptValue key);
    Q_INVOKABLE bool init(QScriptValue cipherType, QScriptValue key, QScriptValue iv);
    Q_INVOKABLE QString lastError() const;
    Q_INVOKABLE ByteArray update(QScriptValue binary);
    Q_INVOKABLE ByteArray final();

private:
    mutable QString error_string_;
    EVP_CIPHER_CTX ctx_;
    const EVP_CIPHER *cipher_;
    bool initialised_;
};

#endif //DEDECIPHER_H
