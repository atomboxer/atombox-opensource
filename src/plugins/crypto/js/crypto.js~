/**
 * @module crypto
 * @desc The "crypto" module provides high level API for generic cryptographic functions
 * @summary Generic cryptography
 * @example
    var key = "1234567890ABCDEFFEDCBA0987654321".toByteArray("hex");

    var cipher = crypto.createCipher("aes-128-ecb",key);
    var enc = cipher.update("hello world!  .0".toByteArray());
    enc = enc.concat(cipher.final());

    assert.equal(enc.decodeToString("hex"),
                 "665ad70f51507442e49382e52cab04644357b9f93067ee3a233395339342d7c6");

    var decipher = crypto.createDecipher("aes-128-ecb",key);
    var dec = decipher.update(enc);
    dec = dec.concat(decipher.final());

    assert.equal(dec.decodeToString("ascii"), "hello world!  .0");
 */

var console = require("console");

var ByteArray = require("binary").ByteArray;
var ByteString = require("binary").ByteString;

var Cipher = __internal__.Cipher;
var Decipher = __internal__.Decipher;
var Hash = __internal__.Hash;

/**
 *  Checks the parity of the supplied block
 *  @method module:crypto.checkBlockParity
 *  @arg {ByteArray|ByteString} block
 *  @return {Boolean}
 *  @throws {Error}
 */
exports.checkBlockParity = checkBlockParity = __internal__.checkBlockParity;

/**
 *  Fix the parity of the supplied block
 *  @method module:crypto.fixBlockParity
 *  @arg {ByteArray|ByteString} block
 *  @return {ByteArray}
 *  @throws {Error}
 */
exports.fixBlockParity = fixBlockParity = __internal__.fixBlockParity;

/**
 *  Generates a random byte array having the provided length
 *  @method module:crypto.generateRandomByteArray
 *  @arg {Numeric} length
 *  @return {ByteArray}
 *  @throws {Error}
 */
exports.generateRandomByteArray = generateRandomByteArray = __internal__.generateRandomByteArray;

/**
 *  Similar to generateRandomByteArray(8).fixBlockParity()
 *  @method module:crypto.generateSingleKey
 *  @return {ByteArray}
 *  @throws {Error}
 */

exports.generateSingleKey       = generateSingleKey = __internal__.generateSingleKey;

/**
 *  Similar to generateRandomByteArray(16).fixBlockParity()
 *  @method module:crypto.generateDoubleKey
 *  @return {ByteArray}
 *  @throws {Error}
 */
exports.generateDoubleKey       = generateDoubleKey = __internal__.generateDoubleKey;

/**
 *  Similar to generateRandomByteArray(24).fixBlockParity()
 *  @method module:crypto.generateTripleKey
 *  @return {ByteArray}
 *  @throws {Error}
 */
exports.generateTripleKey       = generateTripleKey = __internal__.generateTripleKey;

/**
 *  @class Cipher
 *  @description The class used for encrypting data. In order to create an instance of this class you need to invoke {@link module:crypto.createCipher} function.
 *  @summary The class used for encrypting data
 *  @memberof module:crypto
 *  @throw {Error}
 */

/**
 *  Updates the cipher with data and returns the enciphered contents, and can be called many times with the new data
 *  @method module:crypto.Cipher.prototype.update
 *  @arg {ByteArray|ByteString} data
 *  @return {ByteArray}
 */

/**
 * Returns the remaining enciphered contents and destroys the Cipher object
 * @method module:crypto.Cipher.prototype.final
 * @return {ByteArray}
 * @note The cipher object cannot be used after the final method has been invoked
 */
exports.Cipher = Cipher;

/**
 *  This static function creates and returns a cipher object.
 *  @note The cipher name is dependent on OpenSSL, examples are 'aes192', 'des-ede', 'des-ede3-cbc', etc. 
 *  @method module:crypto.createCipher
 *  @arg {String} cipher
 *  @arg {ByteArray|ByteString} key
 *  @arg {ByteArray|ByteString} [iv] Initialization vector (for CBC ciphers)
 *  @return {Cipher}
 */
exports.createCipher = createCipher = function(cipher, key, iv) {
    var c = new Cipher();
    if (iv != undefined) {
	
	if (!c.init(cipher,key,iv)) {
	    throw "crypto.createCipher: error:"+c.lastError();
	}
    }
    else {
	if(!c.init(cipher,key)) {
	    throw "crypto.createCipher: error:"+c.lastError();
	}
    }
    return c;
}

/**
 *  @class Decipher
 *  @description The class used for decrypting data. In order to create an instance of this class you need to invoke createDecipher function
 *  @summary The class used for decrypting data
 *  @memberof module:crypto
 */
exports.Decipher = Decipher;

/**
 *  Updates the decipher with data and returns the deciphered contents. It can be called many times with the new data.
 *  @method module:crypto.Decipher.prototype.update
 *  @arg {ByteArray|ByteString} data
 *  @return {ByteArray}
 */

/**
 * Returns the remaining deciphered contents and destroys the Decipher object.
 * @method module:crypto.Decipher.prototype.final
 * @return {ByteArray}
 * @note The decipher object cannot be used after the final method has been invoked
 */

/**
 *  This static function creates and returns a decipher object.
 *  @note The cipher name is dependent on OpenSSL, examples are 'aes192', 'des-ede', 'des-ede3-cbc', etc. 
 *  @method module:crypto.createDecipher
 *  @arg {String} cipher
 *  @arg {ByteArray|ByteString} key
 *  @arg {ByteArray|ByteString} [iv] Initialization vector (for CBC ciphers)
 *  @return {Decipher}
 *  @throw {Error}
 */
exports.createDecipher = createDecipher = function(cipher, key, iv) {
    var c = new Decipher();
    if (iv != undefined) {
	
	if (!c.init(cipher,key,iv)) {
	    throw "crypto.createDecipher: error:"+c.lastError();
	}
    }
    else {
	if(!c.init(cipher,key)) {
	    throw "crypto.createDecipher: error:"+c.lastError();
	}
    }
    return c;
}


/**
 *  @class Hash
 *  @classdes The class used for creating hash digests of data
 *  @summary The class used for creating hash digests of data
 *  @memberof module:crypto
 */

/**
 *  Updates the hash object with data. It can be called many times with the new data.
 *  @method module:crypto.Hash.prototype.update
 *  @arg {ByteArray|ByteString} data
 *  @return {Boolean}
 */

/**
 * Calculates the digest of all of the passed data to the hash object. In order to create an instance of this class you need to invoke {@link module:crypto.createHash} function.
 * @method module:crypto.Hash.prototype.digest
 * @return {ByteArray}
 * @note The hash object cannot be used after the digest() method has been invoked
 */
exports.Hash = Hash;

/**
 *  This static function creates and returns a hash object.
 *  @method module:crypto.createHash
 *  @arg {String} algorithm
 *  @note The algorithm name is dependent on OpenSSL, examples are 'sha1', 'sha256', 'md5', etc. 
 *  @return {Hash}
 *  @throw {Error}
 */
exports.createHash = function(algorithm) {
    var c = new Hash();
    if (!c.init(algorithm)) {
	throw "crypto.createHash: error:"+c.lastError();
    }
    return c;
}