/**
 * @module crypto/cards
 * @desc The "crypto/cards" module provides high level API for EFT cards cryptographic functions
 * @note For UNIX systems, this module is linked dynamically against OpenSSL. If `crypto` plugin does not show as loaded, make sure OpenSSL is installed and the folder containing libssl and libcrypto is included in LD_LIBRARY_PATH.
 * @summary Card/EFT Cryptography
 */

var console = require("console");
require("crypto");

var ByteArray = require("binary").ByteArray;
var ByteString = require("binary").ByteString;

var __internal__getDecimal = function(digits)
{
    var res = "";
    digits = digits.toLowerCase();
    for (var i=0; i<digits.length;i++) {
	    var D = digits[i];
	    switch(D) {
	    case 'a': res = res + "0"; break;
	    case 'b': res = res + "1"; break;
	    case 'c': res = res + "2"; break;
	    case 'd': res = res + "3"; break;
	    case 'e': res = res + "4"; break;
	    case 'f': res = res + "5"; break;
        default: res = res + D; break;
	    }
    }
    return res;
}

var __internal__XORHexString = function(s1, s2)
{
    var s = "";
    for (var i = 0; i< s1.length;i++) {
	    s += (parseInt(s1[i],16) ^ parseInt(s2[i],16)).toString(16);
    }

    return s;
}

/*
 *  PIN Visa PVV
 */
function helperGetDecimal(digits /*hex*/)
{
    var res = "";
    digits = digits.toUpperCase();

    for (var i=0; i<digits.length;i++) {
	    switch(digits[i]) {
	    case 'A': res += "0"; break;
	    case 'B': res += "1"; break;
	    case 'C': res += "2"; break;
	    case 'D': res += "3"; break;
	    case 'E': res += "4"; break;
	    case 'F': res += "5"; break;
	    default: res += digits[i]; break;
	    }
    }
    return res;
}


/**
 *  Static method used for generating a Card Verification Value
 *  @method module:crypto/cards.generateCVV
 *  @arg {ByteArray|ByteString} CVKA the first part of the key
 *  @arg {ByteArray|ByteString} CVKB the second part of the key
 *  @arg {String} acct Account
 *  @arg {String} exp Expiration Date. (4 digits)
 *  @arg {String} svc Service Code (3 digits)
 *  @returns {Number} the CVV value
 *  @example
 var cvv = cc.generateCVV("1234567890ABCDEF".toByteString("hex"),
 "FEDCBA0987654321".toByteString("hex"),
 "1234567890123456789",
 "1111",
 "201" );
 assert.equal(cvv,"237");
*/
exports.generateCVV = function(CVKA, CVKB, acct, exp, svc)
{
    if (!(CVKA instanceof ByteArray  ||
          CVKA instanceof ByteString ||
	      CVKB instanceof ByteArray  ||
	      CVKB instanceof ByteString)) {
	    throw "crypto.generateCVV: error: CVKA/CVKB not instanceof Binary";
	    return undefined;
    }

    if ( acct == undefined || 
	     exp  == undefined || 
	     svc  == undefined ) {
	    throw "crypto.generateCVV: error: invalid arguments";
	    return undefined;
    }


    if (CVKA.length != 8 ||
        CVKB.length != 8) {
	    throw "crypto.generateCVV: error: length of (CVKA and CVKB) != 8";
	    return undefined;
    }

    var acct = acct.replace(/ /g,"").substring(0,19);

    if (exp.length != 4) {
    	throw "crypto.generateCVV: error: length of expiration date != 4";
	    return undefined;
    }

    if (svc.length != 3) {
    	throw "crypto.generateCVV: error: length of SVC != 3";
	    return undefined;
    }

    var block = (acct+exp+svc).rpad('0',32);
    
    var cr1 = createCipher("des",CVKA);
    var r1 = cr1.update(block.substr(0,16).toByteString("hex"));
    cr1.final();
    cr1.deleteLater();

    var xor = __internal__XORHexString(r1.decodeToString("hex"), block.substr(16));

    var cipher = createCipher("des-ede", CVKA.concat(CVKB));
    var result = cipher
	    .update(xor.toByteArray("hex"))
	    .decodeToString("hex");
    cipher.final();
    cipher.deleteLater()

    var CVV = "", i = 0;

    while (CVV.length < 3 && i< result.length) {
     	if (!isNaN(Number(result[i]))) {
     	    CVV += result[i];
     	}
     	i++;
    }    
    return CVV;
}

/**
 *  Static method used for generating a Visa PVV
 *  @method module:crypto/cards.generateVisaPVV
 *  @arg {ByteArray|ByteString} PVKA the first part of the key
 *  @arg {ByteArray|ByteString} PVKB the second part of the key
 *  @arg {String} acct Account
 *  @arg {Number|String} pvki Key index
 *  @returns {String} The PIN value
 *  @example
 var pvv = cc.generateVisaPVV("1234567890ABCDEF".toByteString("hex"),
 "FEDCBA0987654321".toByteString("hex"),
 "1234111111111111111",
 "6",
 "1234");
 assert.equal(pvv, "4637");
*/
exports.generateVisaPVV = function(PVKA, PVKB, acct, pvki, pin)
{
    if (!(PVKA instanceof ByteArray  ||
          PVKA instanceof ByteString ||
	      PVKB instanceof ByteArray  ||
	      PVKB instanceof ByteString)) {
	    throw "crypto.generateVisaPVV: error: PVKA/PVKB not instanceof Binary";
	    return undefined;
    }

    if ( acct == undefined || 
	     pvki == undefined || 
	     pin  == undefined ) {
	    throw "crypto.generateVisaPVV: error: invalid arguments";
	    return undefined;
    }

    if (PVKA.length != 8 ||
        PVKB.length != 8) {
	    throw "crypto.generateVisaPVV: error: length of (PVKA and PVKB) != 8";
	    return undefined;
    }

    if (isNaN(Number(pvki))  ||
	    Number(pvki) > 6 ) {
	    throw "crypto.generateVisaPVV: error: pvki > 6";
	    return undefined;
    }
    
    var lacct = acct.replace(/ /g,"");
    var stage1 = lacct.substring(lacct.length - 12 - 1).substring(1, 12) + pvki + pin;

    var cipher_st2 = createCipher("des-ede",PVKA.concat(PVKB)) 
    var stage2 = cipher_st2.update(stage1.toByteArray("hex"))
	    .decodeToString("hex");
    cipher_st2.final();
    cipher_st2.deleteLater();

    var PVV = "";
    
    while (PVV.length < 4) {
	    var i = 0;
	    while (PVV.length < 4 && i < stage2.length) {
	        if (!isNaN(Number(stage2[i]))) 
		        PVV += stage2[i];
	        i++;
	    }
	    if (PVV.length < 4) {
	        stage2 = helperGetDecimal(stage2);
	    }
    }

    return PVV;
}

exports.getNaturalPin = getNaturalPin = function(key, account, pin_len)
{
    if (account == undefined ||
	    pin_len == undefined) {
	    throw "crypto.getNaturalPin: error: invalid parameters";
	    return undefined;    
    }
    acccount = account.replace(/ /,"");

    account = ("0000000000000000" + account);
    account = account.substring(account.length -16);

    var vdes = undefined;
    var cipher = undefined;
    if (key.length < 16) {
        cipher = createCipher("des",key);
        vdes = cipher.update(account.toByteArray("hex"));
    } else {
        cipher = createCipher("des-ede",key);
        vdes = cipher.update(account.toByteArray("hex"));
    }
    cipher.final();
    cipher.deleteLater();

    var digits = vdes.decodeToString("hex").substring(0,4);
    var ddigits = __internal__getDecimal(digits);

    return ddigits;
}

/**
 *  Static method used for generating an IBM Offset value
 *  @method module:crypto/cards.generateIbmOffset
 *  @arg {ByteArray|ByteString} key 
 *  @arg {String} pin The PIN value
 *  @arg {String} account Account Number
 *  @returns {String} The IBM Offet number
 *  @example
 var offset = cc.generateIbmOffset  ("1234567890ABCDEFFEDCBA0987654321".toByteString("hex"), //key
 "3344",                                                 //pin
 "1234567891111111111");                                 //account
 assert.equal(offset,"2839");
*/
exports.generateIbmOffset = function(key, pin, account)
{
    if (!(key instanceof ByteArray  ||
          key instanceof ByteString )) {
	    throw "crypto.generateIbmOffset: error: key not instanceof Binary";
	    return undefined;
    }

    if ( account == undefined ||
	     pin == undefined ) {
	    throw "crypto.generateIbmOffset: error: invalid parameters";
    }
    acccount = account.replace(/ /,"");

    var natpin = getNaturalPin(key, account, pin.length);
    
    var offset="";
    if (pin == natpin) {
	    offset = "000000000000".substr(0,pin.length);
    } else {
	    for (var i = 0; i<pin.length; i++) {
	        var v = Number(pin[i]) - Number(natpin[i]);
	        if (v<0) 
		        v += 10;
	        offset += v.toString();
	    }
    }
    return offset;
}

/**
 *  Static method used for getting a PIN from an IBM Offset value
 *  @method module:crypto/cards.genPinFromIbmOffset
 *  @arg {ByteArray|ByteString} key 
 *  @arg {String} pin The offset value
 *  @arg {String} account Account Number
 *  @returns {String} The PIN value
 *  @example
 var offset = cc.generateIbmOffset  ("1234567890ABCDEFFEDCBA0987654321".toByteString("hex"), //key
 "3344",                                                 //pin
 "1234567891111111111");                                 //account
 assert.equal(offset,"2839");
*/
exports.genPinFromIbmOffset = function(key, offset, account)
{
    if (!(key instanceof ByteArray  ||
          key instanceof ByteString )) {
	    throw "crypto.generateIbmOffset: error: key not instanceof Binary";
	    return undefined;
    }

    if ( offset  == undefined ||
	     account == undefined ) {
	    throw "crypto.generateIbmOffset: error: invalid parameters";
    }

    acccount = account.replace(/ /,"");
    var natpin = getNaturalPin(key, account, offset.length);
    
    var pin = "";
    if ( offset == "0000" ) {
	    return natpin;
    } else {
	    for (var i = 0; i<natpin.length; i++) {
	        var v = Number(natpin[i]) + Number(offset[i]);
	        if ( v>9 ) 
		        v -= 10;
	        pin += v.toString();
	    }
    }
    return pin;
}

/*
 *  Pin Blocks
 */
var PinBlockRequirements = function(RequiresPadding, RequiresAccount) 
{
    this.RequiresAccount = RequiresAccount;
    this.RequiresPadding = RequiresPadding;
}

var PinBlockFormat = {
    AnsiX98:   0,
    ISO_1 :    1,
    Diebold :  2,
    IBM_3624 : 3
}

var PinBlock = function() {
    this.getPINBlockRequirements = function( PBFormat ) {
	    switch(PBFormat) {
	    case PinBlockFormat.AnsiX98:
            return new PinBlockRequirements(false, true);
        case PinBlockFormat.Diebold:
            return new PinBlockRequirements(false, false);
        case PinBlockFormat.IBM_3624:
            return new PinBlockRequirements(false, false);
        case PinBlockFormat.ISO_1:
            return new PinBlockRequirements(false, false);
	    default: console.assert(false, "Unsupported PIN block");
	    }
    }
    
    this.toPINBlock = function(PBFormat, PIN, padding, account) {
	    acccount = account.replace(/ /,"");
	    switch(PBFormat) {
	    case PinBlockFormat.AnsiX98:
            return this.formatPINBlock_AnsiX98(PIN, account);
	    case PinBlockFormat.Diebold:
            return this.formatPINBlock_Diebold(PIN);
        case PinBlockFormat.IBM_3624:
            return this.formatPINBlock_IBM_3624(PIN);
        case PinBlockFormat.ISO_1:
            return this.formatPINBlock_ISO1(PIN);
	    default: console.assert(false, "Unsupported PIN block");
	    }
    }

    this.toPIN = function(PBFormat, PINBlock, padding, account) {
	    acccount = account.replace(/ /,"");
	    switch(PBFormat) {
	    case PinBlockFormat.AnsiX98:
            return this.getPIN_AnsiX98(PINBlock, account);
	    case PinBlockFormat.Diebold:
            return this.getPIN_Diebold(PINBlock);
        case PinBlockFormat.IBM_3624:
            return this.getPIN_IBM_3624(PINBlock);
        case PinBlockFormat.ISO_1:
            return this.getPIN_ISO1(PINBlock);
	    default: console.assert(false, "Unsupported PIN block");
	    }
    }

    this.formatPINBlock_AnsiX98 = function(PIN, account) {
	    return __internal__XORHexString(
	        (PIN.length.toString().lpad("0",2) + PIN).rpad("F",16)
	        ,account
		        .substring(account.length - 13).substring(0,12)
		        .lpad("0",16));
    }
    
    this.getPIN_AnsiX98 = function(PINBlock, account) {
	    var unXOR = __internal__XORHexString(PINBlock, 
					                         account.substring(account.length - 13).substring(0,12)
					                         .lpad("0",16));
	    return unXOR.substring(2,2 + Number(unXOR.substring(0,2)));
    }

    this.formatPINBlock_Diebold = function(PIN) {
	    return PIN.rpad("F",16);
    }

    this.getPIN_Diebold = function(PINBlock) {
	    return PINBlock.replace(/[Ff]/g,"");
    }

    this.formatPINBlock_IBM_3624 = function(PIN) {
	    return PIN.toUpperCase().rpad("F",16);
    }

    this.getPIN_IBM_3624 = function(PINBlock) {
	    return PINBlock.toUpperCase().replace(/[Ff]/g,"");
    }

    this.formatPINBlock_ISO1 = function(PIN) {
	    var PINChars = "1" + PIN.length + PIN;
	    return PINChars + generateRandomByteArray(16)
	        .decodeToString("hex").substring(0,16 - PINChars.length);
    }

    this.getPIN_ISO1 = function(PINBlock) {
	    return PINBlock.substring(2, 2 + Number(PINBlock.substring(1,2)));
    }

}

/**
 *  Static method used for generating different types of PIN blocks
 *  @method module:crypto/cards.generatePinBlock
 *  @arg {ByteArray|ByteString} key
 *  @arg {String} type The type of PIN block ("AnsiX98","ISO_1","Diebold","IBM_3624")
 *  @arg {String} pad The padding character
 *  @arg {String} account The account number
 *  @returns {ByteArray} The PIN block
 *  @example
 var pb = cc.generatePinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"),
 "AnsiX98",
 "4433",
 "F",
 "1234567891111111111");

 assert.equal(pb.decodeToString("hex"), "C8CECEF0FE741FB9".toLowerCase());

*/
exports.generatePinBlock = function(key, type, PIN, pad, account) {

    if (!(key instanceof ByteArray  ||
          key instanceof ByteString )) {
	    throw "crypto.generatePinBlock: error: key not instanceof Binary";
	    return undefined;
    }

    acccount = account.replace(/ /,"");
    var algo = undefined;
    switch(key.length) {
    case 8: algo = "des"; break;
    case 16: algo = "des-ede"; break;
    case 24: algo = "des-ede3"; break;
    default: throw "generatePinBlock: invalid key length"; break;
    }

    var cipher = createCipher(algo,key); 
    var ret = cipher.update(
	        new PinBlock().toPINBlock(PinBlockFormat[type],PIN,pad,account)
		        .toByteArray("hex"))
    cipher.final();
    cipher.deleteLater();
    return ret;
}

/**
 *  Static method used for getting the PIN out of a PIN block
 *  @method module:crypto/cards.getPinFromPinBlock
 *  @arg {ByteArray|ByteString} key
 *  @arg {String} type The type of PIN block ("AnsiX98","ISO_1","Diebold","IBM_3624")
 *  @arg {ByteArray|ByteString} pinblock
 *  @arg {String} pad The padding character
 *  @arg {String} account The account number
 *  @returns {String} The PIN number
 *  @example

 var pin = cc.getPinFromPinBlock("5D40A4621A8AC4ABF46E5BB3CE98CDB0C15E4679083883B5".toByteArray("hex"),
 "AnsiX98",        
 "C8CECEF0FE741FB9".toByteArray("hex"),   
 "F",               
 "1234567891111111111");
 assert.equal(pin,"4433");
*/
exports.getPinFromPinBlock = function(key, type, pinblock, pad, account) {

    if (!(key instanceof ByteArray  ||
          key instanceof ByteString )) {
	    throw "crypto.generatePinBlock: error: key not instanceof Binary";
	    return undefined;
    }

    if (!(pinblock instanceof ByteArray  ||
          pinblock instanceof ByteString )) {
	    throw "crypto.generatePinBlock: error: pinblock not instanceof Binary";
	    return undefined;
    }

    var algo = undefined;

    switch(key.length) {
    case 8: algo = "des"; break;
    case 16: algo = "des-ede"; break;
    case 24: algo = "des-ede3"; break;
    default: throw "generatePinBlock: invalid key length"; break;
    }

    pinblock = createDecipher(algo,key).update(pinblock.concat("20202020".toByteArray("hex")));
    return new PinBlock().toPIN(PinBlockFormat[type], 
				                pinblock.decodeToString("hex"), pad, account);
}

/*
 * EMV cryptography
 */
getPanSequence = function(pan, seq) {
    var temp = "0000000000" + pan;
    temp = temp.substring(temp.length - 14);
    
    if(seq.length != 2) {
	    throw "crypto.getPanSequence error: length of seq parameter should be 2";
    }

    seq = "0"+seq.toString();
    temp = temp + seq.substring(seq.length - 2);

    return temp;
}

/**
 *  Static method used for generating a Visa 96 ARQC (request cryptogram)
 *  @method module:crypto/cards.generateVisa96Arqc
 *  @arg {ByteArray|ByteString} mk
 *  @arg {String} pan
 *  @arg {String} pan_seq_no PAN Sequence Number (2 digits) 
 *  @arg {String} pad The padding character
 *  @arg {String} account The account number
 *  @arg {String} cdol
 *  @returns {ByteArray} arqc
 *  @example
 var arqc = cc.generateVisa96Arqc( "01010101010101010202020202020202".toByteString("hex"), //mdk
 "4220000000000030", //pan
 "01", //pan seq number
 "000000000100000000000000084080000100000826060412009BADBCAB5c0000FF03A0B000" //cdol
 );

 assert.equal(arqc.decodeToString("hex"), "22abca55a65c3335");
*/
exports.generateVisa96Arqc = function(mk, pan, pan_seq_no, cdol) {

    if ( !mk instanceof ByteArray &&
	     !mk instanceof ByteString ) {
	    throw "crypto.generateVisaEmv96Arqc error: mk should be instanceof Binary";
    }
    
    if ( pan == undefined ||
	     pan_seq_no == undefined ||
	     cdol == undefined ) {
	    throw "crypto.generateVisaEmv96Arqc error: invalid arguments";
    }

    var data = getPanSequence(pan, pan_seq_no);
    if (data.length > 16)
	    data = data.substring(2,2+16);

    var cipher = createCipher("des-ede", mk); 
    var udk = cipher.update(data.toByteArray("hex")).decodeToString("hex");

    data = __internal__XORHexString(data, "FFFFFFFFFFFFFFFF");

    data = cipher.update(data.toByteArray("hex")).decodeToString("hex");
    
    var udka = fixBlockParity(udk.toByteArray("hex"));
    var udkb = fixBlockParity(data.toByteArray("hex"));

    var work = cdol.substring(0, 16);

    for (var i = 16; i< cdol.length; i+=16) {
	    var cipher_a = createCipher("des", udka);
	    var temp1 = cipher_a.update(work.toByteArray("hex")).decodeToString("hex");
	    var temp2 = cdol.substring(i, i+16);
	    temp2 += "0000000000000000";
	    temp2 = temp2.substring(0,16);
	    work = __internal__XORHexString(temp1, temp2);
	    cipher_a.final();
        cipher_a.deleteLater();
    }
    var cipher_a = createCipher("des", udka);
    var arqc = cipher_a.update(work.toByteArray("hex").toByteArray("hex"));
    var ciph = createDecipher("des", udkb); 
    arqc = ciph.update(arqc.concat("20202020".toByteArray("hex")));
    cipher_a.final();
    cipher_a.deleteLater();

    cipher_a = createCipher("des", udka)
    arqc = cipher_a.update(arqc);
    cipher_a.final();
    cipher_a.deleteLater();

    return arqc;
}

/**
 *  Static method used for generating a Visa 96 ARPC (response cryptogram)
 *  @method module:crypto/cards.generateVisa96Arpc
 *  @arg {ByteArray|ByteString} mk
 *  @arg {String} pan
 *  @arg {String} pan_seq_no PAN Sequence Number (2 digits) 
 *  @arg {ByteArray|ByteString} arqc The ARQC value
 *  @arg {String} arc Application Response Code. (4 hex digits)
 *  @returns {ByteArray} arpc
 *  @example
 assert.equal(arqc.decodeToString("hex"), "22abca55a65c3335");

 var arpc = cc.generateVisa96Arpc( "01010101010101010202020202020202".toByteString("hex"), //mdk,
 "4220000000000030", //pan
 "01", //pan seq number
 arqc,
 "0003"
 ); //arc

 assert.equal(arpc.decodeToString("hex"), "fb335eebea22ed6e");
*/
exports.generateVisa96Arpc = function(mk, pan,  pan_seq_no,  arqc, arc) {
    if ( !mk instanceof ByteArray &&
	     !mk instanceof ByteString ) {
	    throw "crypto.generateVisaEmv96Arpc error: mk should be instanceof Binary";
    }

    if ( !arqc instanceof ByteArray &&
	     !arqc instanceof ByteString ) {
	    throw "crypto.generateVisaEmv96Arqc error: arqc should be instanceof Binary";
    }
    
    
    if ( arc == undefined  ||
	     pan == undefined ||
	     pan_seq_no == undefined ||
         arc.length != 4   ||
         arqc.length != 8 )
	    throw "crypto.generateVisaEmv96Arqc error: invalid arguments";
    
    var data = getPanSequence(pan, pan_seq_no);
    if (data.length > 16)
	    data = data.substring(2,2+16);

    var cipher = createCipher("des-ede", mk); 
    var udk = cipher.update(data.toByteArray("hex")).decodeToString("hex");

    data = __internal__XORHexString(data, "FFFFFFFFFFFFFFFF");

    data = cipher.update(data.toByteArray("hex")).decodeToString("hex");
    
    var udka = fixBlockParity(udk.toByteArray("hex")).decodeToString("hex");
    var udkb = fixBlockParity(data.toByteArray("hex")).decodeToString("hex");

    var skey = (udka+udkb).toByteArray("hex");
    
    arc = "0000" + arc;
    arc = arc.substring(arc.length - 4);
    arc = arc + "000000000000";

    var tmp = __internal__XORHexString(arc, arqc.decodeToString("hex"));
    return createCipher("des-ede",skey).update(tmp.toByteArray("hex"));
}


generateMastercard96Arqc = function(mk, atc, un, pan, pan_seq_no, cdol) {

    if ( !mk instanceof ByteArray &&
	     !mk instanceof ByteString ) {
	    throw "crypto.generateVisaEmv96Arqc error: mk should be instanceof Binary";
    }
    console.assert(false,"not implemented yet");
}

generateEmv2000Arqc = function(mk, atc, iv, pan, pan_seq_no, cdol) {

    if ( !mk instanceof ByteArray &&
	     !mk instanceof ByteString ) {
	    throw "crypto.generateVisaEmv96Arqc error: mk should be instanceof Binary";
    }
    console.assert(false,"not implemented yet");
}

