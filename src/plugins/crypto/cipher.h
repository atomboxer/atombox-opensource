#ifndef CIPHER_H
#define CIPHER_H

#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <openssl/evp.h>
#include <openssl/ssl.h>

#include <core/binary.h>


QScriptValue Cipher_ctor(QScriptContext *ctx, QScriptEngine *engine);

class Cipher : public QObject
{
    Q_OBJECT
	public:
    Cipher();
    ~Cipher();

 public:
    Q_INVOKABLE bool init(QScriptValue cipherType, QScriptValue key);
    Q_INVOKABLE bool init(QScriptValue cipherType, QScriptValue key, QScriptValue iv);
    Q_INVOKABLE QString lastError() const;
    Q_INVOKABLE ByteArray update(QScriptValue binary) const;
    Q_INVOKABLE ByteArray final();

private:
    mutable QString error_string_;
    EVP_CIPHER_CTX ctx_;
    const EVP_CIPHER *cipher_;
    bool initialised_;
};

#endif //CIPHER_H
