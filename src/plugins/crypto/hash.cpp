#include "hash.h"

#include <QtCore/QDebug>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>

#include <iostream>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

QScriptValue Hash_ctor(QScriptContext *ctx, QScriptEngine *engine)
 {
     QScriptValue object = engine->newQObject( new Hash(),
				 QScriptEngine::ScriptOwnership,
				 QScriptEngine::SkipMethodsInEnumeration );

     object.setPrototype(ctx->callee().property("prototype"));
     return object;
 }

Hash::Hash() : 
    QObject(0)
{
    initialised_ = false;
}

Hash::~Hash()
{
}

bool
Hash::init(QScriptValue hashType)
{
    md_ = EVP_get_digestbyname(hashType.toString().toAscii().data());
    if(!md_) {
	error_string_ = "Unknown hash: "+ hashType.toString();
	return false;
    }

    EVP_MD_CTX_init(&ctx_);
    EVP_DigestInit_ex(&ctx_, md_, NULL);

    initialised_ = true;

    return true;
}

QString
Hash::lastError() const
{
    return error_string_;
}

bool
Hash::update(QScriptValue binary) const
{
    if (!initialised_) {
	return false;
    }

    QByteArray *data = NULL;

    data = qscriptvalue_cast<ByteString*>(binary.data());
    if (!data) {
    	data = qscriptvalue_cast<ByteArray*>(binary.data());
    }

    if (!data) {
    	error_string_ = QString("data should be instanceof Binary"); 
    	return false;
    }

    EVP_DigestUpdate((EVP_MD_CTX*)&ctx_, (unsigned char*)data->data(), data->size());
    return true;
}

ByteArray
Hash::digest()
{
    if (!initialised_) {
	return false;
    }

    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;

    EVP_DigestFinal_ex(&ctx_, md_value, &md_len);
    EVP_MD_CTX_cleanup(&ctx_);
    initialised_ = false;
    
    return QByteArray((char*)md_value, md_len);
}
