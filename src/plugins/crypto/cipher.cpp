#include "cipher.h"

#include <QtCore/QDebug>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>

#include <iostream>

#include <openssl/des.h>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

QScriptValue Cipher_ctor(QScriptContext *ctx, QScriptEngine *engine)
{
    QScriptValue object = engine->newQObject( new Cipher(),
                                              QScriptEngine::ScriptOwnership,
                                              QScriptEngine::ExcludeChildObjects|
                                              QScriptEngine::SkipMethodsInEnumeration );

    object.setPrototype(ctx->callee().property("prototype"));
    return object;
}

Cipher::Cipher() : 
    QObject(0)
{
    initialised_ = false;
}

Cipher::~Cipher()
{
}

bool
Cipher::init(QScriptValue cipherType, QScriptValue skey)
{
    QByteArray *key_ba = NULL;

    key_ba = qscriptvalue_cast<ByteString*>(skey.data());
    if (!key_ba) {
        key_ba = qscriptvalue_cast<ByteArray*>(skey.data());
    }
    if (!key_ba) {
        error_string_ = "key should be instanceof Binary"; 
        return false;
    }

    cipher_ = EVP_get_cipherbyname(cipherType.toString().toAscii().data());
    if(!cipher_) {
        error_string_ = "Unknown cipher: "+ cipherType.toString();
        return false;
    }
 
    unsigned char key[EVP_MAX_KEY_LENGTH],iv[EVP_MAX_IV_LENGTH];
    memcpy(&key, key_ba->data(), key_ba->length());

    int key_len = key_ba->length();

    EVP_CIPHER_CTX_init(&ctx_);
    EVP_EncryptInit_ex(&ctx_, cipher_, NULL, NULL, NULL);

    if (!EVP_CIPHER_CTX_set_key_length(&ctx_, key_len)) {
        error_string_ =  "Invalid key length " + QString::number(key_len);
        EVP_CIPHER_CTX_cleanup(&ctx_);
        return false;
    }

    EVP_EncryptInit_ex(&ctx_, NULL, NULL,
                       (unsigned char *)key,
                       NULL);

    //EVP_CIPHER_CTX_set_padding(&ctx_, false);
    initialised_ = true;

    return true;
}

bool 
Cipher::init(QScriptValue cipherType, QScriptValue skey, QScriptValue siv)
{
    QByteArray *key_ba = NULL;

    key_ba = qscriptvalue_cast<ByteString*>(skey.data());
    if (!key_ba) {
        key_ba = qscriptvalue_cast<ByteArray*>(skey.data());
    }
    if (!key_ba) {
        error_string_ = "key should be instanceof Binary"; 
        return false;
    }
    
    QByteArray *iv_ba = NULL;
    iv_ba = qscriptvalue_cast<ByteString*>(siv.data());
    if (!iv_ba) {
        iv_ba = qscriptvalue_cast<ByteArray*>(siv.data());
    }

    if (!iv_ba) {
        error_string_ = "iv should be instanceof Binary"; 
        return false;
    }

    cipher_ = EVP_get_cipherbyname(cipherType.toString().toAscii().data());
    if(!cipher_) {
        error_string_ = "Unknown cipher: "+ cipherType.toString();
        return false;
    }

    if ((EVP_CIPHER_iv_length(cipher_) != iv_ba->size()) &&
        !(EVP_CIPHER_mode(cipher_) == EVP_CIPH_ECB_MODE && iv_ba->size() == 0)) {
        error_string_ = "Invalid IV length "+QString::number(iv_ba->size());
        return false;
    }

    EVP_CIPHER_CTX_init(&ctx_);
    EVP_CipherInit_ex(&ctx_, cipher_, NULL, NULL, NULL, true);
    if (!EVP_CIPHER_CTX_set_key_length(&ctx_, key_ba->size())) {
        error_string_ =  "Invalid key length " + QString::number(key_ba->size());
        EVP_CIPHER_CTX_cleanup(&ctx_);
        return false;
    }

    EVP_CipherInit_ex(&ctx_, NULL, NULL,
                      (unsigned char *)key_ba->data(),
                      (unsigned char *)iv_ba->data(), true);
    initialised_ = true;
    return true;
}

QString
Cipher::lastError() const
{
    return error_string_;
}

ByteArray
Cipher::update(QScriptValue binary) const
{
    if (!initialised_) {
        return QByteArray();
    }

    QByteArray *data = NULL;

    data = qscriptvalue_cast<ByteString*>(binary.data());
    if (!data) {
        data = qscriptvalue_cast<ByteArray*>(binary.data());
    }

    if (!data) {
        error_string_ = QString("data should be instanceof Binary"); 
        return false;
    }
    
    int  out_len=data->size()+EVP_CIPHER_CTX_block_size(&ctx_);
    unsigned char   *out= new unsigned char[out_len];

    EVP_CipherUpdate((EVP_CIPHER_CTX*)&ctx_, out, &out_len, (unsigned char*)data->data(), data->size());

    ByteArray ret((const char*)out, out_len);
    delete[]out;

    return ret;
}

ByteArray
Cipher::final()
{
    if (!initialised_) {
        return false;
    }
    
    unsigned char *out = new unsigned char[EVP_CIPHER_CTX_block_size(&ctx_)];
    int out_len = 0;
    memset(out, 0, EVP_CIPHER_CTX_block_size(&ctx_));
    EVP_CipherFinal_ex((EVP_CIPHER_CTX*)&ctx_,out,&out_len);
    
    QByteArray ret((const char*)out, out_len);
    delete []out;
    EVP_CIPHER_CTX_cleanup((EVP_CIPHER_CTX*)&ctx_);
    initialised_ = false;

    return ret;
}
