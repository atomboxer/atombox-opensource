#ifndef CRYPTOPLUGIN_H
#define CRYPTOPLUGIN_H

#include <QScriptExtensionPlugin>

class CryptoPlugin : public QScriptExtensionPlugin
{
public:
    CryptoPlugin( QObject *parent = 0);
    ~CryptoPlugin();

    virtual void initialize(const QString &key, QScriptEngine *engine);
    virtual QStringList keys() const;

private:
    // nothing
};

#endif //CRYPTOPLUGIN_H
