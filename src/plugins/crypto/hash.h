#ifndef HASH_H
#define HASH_H

#include <QtCore/QObject>
#include <QtCore/QString>

#include <QtScript/QScriptContext>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <openssl/evp.h>
#include <openssl/ssl.h>

#include <core/binary.h>


QScriptValue Hash_ctor(QScriptContext *ctx, QScriptEngine *engine);

class Hash : public QObject
{
    Q_OBJECT
	public:
    Hash();
    ~Hash();

 public:
    Q_INVOKABLE bool init(QScriptValue hashType);
    Q_INVOKABLE QString lastError() const;
    Q_INVOKABLE bool update(QScriptValue binary) const;
    Q_INVOKABLE ByteArray digest();

private:
    mutable QString error_string_;
    EVP_MD_CTX  ctx_;
    const EVP_MD *md_;
    bool initialised_;
};

#endif //HASH_H
