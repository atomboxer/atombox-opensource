#include "crypto.h"

#include <core/bytearrayclass.h>
#include <core/bytestringclass.h>

#include <openssl/rand.h>
#include <openssl/des.h>

#include <QtCore/QDebug>
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

Q_DECLARE_METATYPE(ByteArray*)
Q_DECLARE_METATYPE(ByteString*)

static ByteArray
helperRandomBlock(size_t size) 
{
    unsigned char *data = new unsigned char[size];
    
    if (!RAND_bytes(data, size)) {
	qWarning("RAND_bytes error");
    }

    QByteArray ret((const char*)data, size);

    delete []data;
    return ret;
}

QScriptValue 
__crypto_checkBlockParity(QScriptContext *ctx, QScriptEngine *)
{
    QByteArray *key_ba = NULL;
    QScriptValue key = ctx->argument(0);

    key_ba = qscriptvalue_cast<ByteString*>(key.data());
    if (!key_ba) {
	key_ba = qscriptvalue_cast<ByteArray*>(key.data());
    }
    if (!key_ba) {
	ctx->throwError("crypto.checkParity error: argument(0) !instanceof Binary.");
	return false;
    }

    char *k = key_ba->data();
    int ofst = 0;

    DES_cblock cb;
    DES_key_schedule s;
    while (ofst < key_ba->size()) {
	k = k+ofst;
	memcpy((char*)&cb, key_ba->mid(ofst,ofst+8).data(),8);
	DES_key_sched(&cb, &s);
	if (DES_set_key_checked(&cb, &s) == -1) {
	    return false;
	}
	ofst+=8;
    }

    return true;
}

QScriptValue 
__crypto_fixBlockParity(QScriptContext *ctx, QScriptEngine *eng)
{
    QByteArray *key_ba = NULL;
    QScriptValue key = ctx->argument(0);
    
    key_ba = qscriptvalue_cast<ByteString*>(key.data());
    if (!key_ba) {
	key_ba = qscriptvalue_cast<ByteArray*>(key.data());
    }

    if (!key_ba) {
	ctx->throwError("crypto.fixBlockParity error: argument(0) !instanceof Binary.");
	return false;
    }

    char *k = key_ba->data();
    int ofst = 0;
    QByteArray ret;

    while (ofst < key_ba->size()) {
	k = k+ofst;
	DES_cblock cb;
	memcpy(&cb, k, 8);
	DES_set_odd_parity(&cb);
	ret.append((char*)&cb,8);
	ofst += 8;
    }

    return ByteArrayClass::toScriptValue(eng, ret);
}

QScriptValue
__crypto_generateRandomByteArray(QScriptContext *ctx, QScriptEngine *eng)
{
    if (ctx->argumentCount() != 1 ||
	!ctx->argument(0).isNumber()) {
	ctx->throwError("crypto.generateRandomByteArray error: invalid argument.");
	return QScriptValue();
    }
    
    return ByteArrayClass::toScriptValue(eng, helperRandomBlock(ctx->argument(0).toInt32()));
}

QScriptValue
__crypto_generateSingleKey(QScriptContext *ctx, QScriptEngine *eng)
{
    QByteArray key_ba (helperRandomBlock(8));
    unsigned char *k = (unsigned char*)key_ba.data();
    int ofst = 0;
    QByteArray ret;

    while (ofst < key_ba.size()) {
	k = k+ofst;
	DES_cblock cb;
	memcpy(&cb, k, 8);
	DES_set_odd_parity(&cb);
	ret.append((char*)&cb,8);
	ofst += 8;
    }

    return ByteArrayClass::toScriptValue(eng, ret);
}

QScriptValue
__crypto_generateDoubleKey(QScriptContext *ctx, QScriptEngine *eng)
{
    QByteArray key_ba(helperRandomBlock(16));
    unsigned char *k = (unsigned char*)key_ba.data();
    int ofst = 0;
    QByteArray ret;

    while (ofst < key_ba.size()) {
	k = k+ofst;
	DES_cblock cb;
	memcpy(&cb, k, 8);
	DES_set_odd_parity(&cb);
	ret.append((char*)&cb,8);
	ofst += 8;
    }

    return ByteArrayClass::toScriptValue(eng, ret);
}

QScriptValue
__crypto_generateTripleKey(QScriptContext *ctx, QScriptEngine *eng)
{
    QByteArray key_ba(helperRandomBlock(24));
    unsigned char *k = (unsigned char*)key_ba.data();
    int ofst = 0;
    QByteArray ret;

    while (ofst < key_ba.size()) {
	k = k+ofst;
	DES_cblock cb;
	memcpy(&cb, k, 8);
	DES_set_odd_parity(&cb);
	ret.append((char*)&cb,8);
	ofst += 8;
    }

    return ByteArrayClass::toScriptValue(eng, ret);
}
