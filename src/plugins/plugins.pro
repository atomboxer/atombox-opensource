isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../atomboxer.pri)
}

TEMPLATE  = subdirs

SUBDIRS   =  core 

!isEmpty(ATOMBOXER_BUILD_CRYPTO) {
    SUBDIRS += crypto
}

!isEmpty(ATOMBOXER_BUILD_DDL) {
    SUBDIRS += ddl
}

!isEmpty(ATOMBOXER_BUILD_NET) {
    SUBDIRS += net
}

!isEmpty(ATOMBOXER_BUILD_HTTP) {
    SUBDIRS += http
}


!isEmpty(ATOMBOXER_BUILD_SQL) {
    SUBDIRS += sql
}

!isEmpty(ATOMBOXER_BUILD_XML) {
    SUBDIRS += xml
}

tandem {
    SUBDIRS += guardian
}

!tandem:!isEmpty(ATOMBOXER_BUILD_CTREE) {
    SUBDIRS += ctree 
}

!tandem:!isEmpty(ATOMBOXER_BUILD_MQ) {
    SUBDIRS += mq
}
