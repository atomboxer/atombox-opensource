isEmpty(ATOMBOXER_PRI_INCLUDED) {
	include(../../../../atomboxer.pri)
}

QSCRIPTCLASSIC_DIR_TREE = $$ATOMBOXER_SOURCE_TREE/src/3rdparty/qtscriptclassic/

INCLUDEPATH += $$QSCRIPTCLASSIC_DIR_TREE/include $$PWD
DEPENDPATH += $$PWD
TEMPLATE   += fakelib


tandem {
} else {
  mac:LIBS += -F$$PWD/../lib -framework QtScriptClassic
  else:LIBS += -L$$QSCRIPTCLASSIC_DIR_TREE/lib -l$$qtLibraryTarget(QtScriptClassic)
}
TEMPLATE -= fakelib
tandem {       
} else {
  unix:QMAKE_RPATHDIR += $$PWD/../lib
}
