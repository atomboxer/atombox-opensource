isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../../atomboxer.pri)
}

QT          = core
QT         -= gui


QT_NO_GUI = 1
DEFINES += QT_NO_GUI 


DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/
DESTDIR_TARGET = $$ATOMBOXER_SOURCE_TREE/lib/
target.destdir = $$ATOMBOXER_SOURCE_TREE/lib/
dlltarget.path = $$ATOMBOXER_SOURCE_TREE/lib/

tandem {
    isEmpty(USE_SCRIPT_CLASSIC) {
        GUARDIAN_TARGET = $$ATOMBOXER_SOURCE_TREE/lib/ZFDBGDLL
    }
    else {
        GUARDIAN_TARGET = $$ATOMBOXER_SOURCE_TREE/lib/ZSDBGDLL
    }
}

isEmpty(USE_SCRIPT_CLASSIC) {
    QT         += script
}

!isEmpty(USE_SCRIPT_CLASSIC) {
    unix:!tandem {
        LIBS += -lQtScriptClassic
    } else {
        tandem {
            LIBS += -lZSCRIDLL
        } else {
            CONFIG(debug, debug|release) {
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptClassicd1.a
            } else {
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptClassic1.a    
            }
        }
    }
    TARGET     = QtScriptTools_compat
} else {
    TARGET     = QtScriptTools
}



TEMPLATE = lib
QPRO_PWD   = $$PWD

DEFINES   += QT_BUILD_SCRIPTTOOLS_LIB QT_BUILD_INTERNAL
DEFINES   += QT_NO_USING_NAMESPACE
DEFINES   += QT_MAKEDLL

include(debugging/debugging.pri)

symbian:TARGET.UID3=0x2001E625
