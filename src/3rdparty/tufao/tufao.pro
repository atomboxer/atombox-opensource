isEmpty(ATOMBOXER_PRI_INCLUDED) {
	include(../../../atomboxer.pri)
}

TEMPLATE	= lib


ATOMBOXER_SOURCE_TREE = ../../../

DESTDIR = $$ATOMBOXER_SOURCE_TREE/lib/
DESTDIR_TARGET = $$ATOMBOXER_SOURCE_TREE/lib/

CONFIG += static

include(src.pri)

