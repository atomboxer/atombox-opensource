
SOURCES += \ 
    src/httpserver.cpp \
    src/httpserverrequest.cpp \
    src/httpserverresponse.cpp \
    src/httpsserver.cpp \
    src/url.cpp \
    src/querystring.cpp \
    src/priv/tcpserverwrapper.cpp \
    src/priv/reasonphrase.cpp \
    src/priv/http_parser.c \
    src/websocket.cpp \
    src/abstractmessagesocket.cpp \
    src/httpfileserver.cpp \
    src/abstracthttpserverrequesthandler.cpp \
    src/httpserverrequestrouter.cpp \
    src/httppluginserver.cpp \
    src/headers.cpp \
    src/priv/rfc1123.cpp \
    src/priv/rfc1036.cpp \
    src/priv/asctime.cpp \
    src/sessionstore.cpp \
    src/simplesessionstore.cpp 

HEADERS += \
    src/httpserver.h \
    src/httpserverrequest.h \
    src/httpserverresponse.h \
    src/httpsserver.h \
    src/priv/tcpserverwrapper.h \
    src/websocket.h \
    src/abstractmessagesocket.h \
    src/httpfileserver.h \
    src/abstracthttpserverrequesthandler.h \
    src/httpserverrequestrouter.h \
    src/httppluginserver.h \
    src/sessionstore.h \
    src/simplesessionstore.h \
