#include "test1.h"
#include "test2.h"
#include "test3.h"
#include "test4.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

int main()
{
    test1();
    test2();
    test3();
    test4();
    return 0;
}
