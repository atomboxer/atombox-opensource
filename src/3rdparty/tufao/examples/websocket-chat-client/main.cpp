#include <QApplication>
#include "mainwindow.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
