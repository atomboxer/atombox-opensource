#ifndef DEBUG_RUNNER_H
#define DEBUG_RUNNER_H

#include <QtScript/QScriptContextInfo>
#include <QtNetwork/QAbstractSocket>

#include <qscriptdebuggerengine.h>
#include <qscriptdebuggercommand_p.h>
#include <qscriptdebuggerevent_p.h>
#include <qscriptdebuggerresponse_p.h>
#include <qscriptdebuggerbackend_p.h>
#include <qscriptdebuggerobjectsnapshotdelta_p.h>

#include "scriptengineapp.h"

Q_DECLARE_METATYPE(QScriptDebuggerCommand)
Q_DECLARE_METATYPE(QScriptDebuggerResponse)
Q_DECLARE_METATYPE(QScriptDebuggerEvent)
Q_DECLARE_METATYPE(QScriptContextInfo)
Q_DECLARE_METATYPE(QScriptContextInfoList)
Q_DECLARE_METATYPE(QScriptDebuggerValue)
Q_DECLARE_METATYPE(QScriptDebuggerValueList)
Q_DECLARE_METATYPE(QScriptValue::PropertyFlags)
Q_DECLARE_METATYPE(QScriptBreakpointData)
Q_DECLARE_METATYPE(QScriptBreakpointMap)
Q_DECLARE_METATYPE(QScriptScriptData)
Q_DECLARE_METATYPE(QScriptScriptMap)
Q_DECLARE_METATYPE(QScriptScriptsDelta)
Q_DECLARE_METATYPE(QScriptDebuggerValueProperty)
Q_DECLARE_METATYPE(QScriptDebuggerValuePropertyList)
Q_DECLARE_METATYPE(QScriptDebuggerObjectSnapshotDelta)
Q_DECLARE_METATYPE(QList<int>)
Q_DECLARE_METATYPE(QList<qint64>) 

class DebugRunner : public QObject
{
    Q_OBJECT
public:
    DebugRunner(ScriptEngineApp *engine, 
                const QHostAddress &addr, 
                quint16 port, 
                bool connect, 
                QObject *parent = 0);

private slots:
    void onConnected();
    void onDisconnected();

private:
    ScriptEngineApp *scriptEngine_;
    QScriptDebuggerEngine *debuggerEngine_;
};

#endif
