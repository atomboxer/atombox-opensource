
#define QU(x) #x
#define QUH(x) QU(x)

#include <string>

#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QtPlugin>
#include <QtCore/QStringList>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include <QtSql/QSqlDatabase>

#include <tclap/CmdLine.h>
#include "scriptengineapp.h"

#include <stdio.h>
#include <stdlib.h>

#include <QtCore/QCoreApplication>

#include "debugrunner.h"

#ifdef  Q_OS_WIN32
#include <windows.h>
#include <string.h>
#include <tchar.h>
#include <shellapi.h>
#endif

#ifdef AB_HAVE_MQ
#include <qstandardpaths.h>
#endif
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif


#ifdef AB_STATIC_PLUGINS
#if defined(AB_USE_SCRIPT_CLASSIC)

#if defined(ATOMBOXER_BUILD_DDL)
Q_IMPORT_PLUGIN(abscriptddl_compat)
#endif

Q_IMPORT_PLUGIN(abscriptcore_compat)

#if defined(ATOMBOXER_BUILD_CRYPTO)
Q_IMPORT_PLUGIN(abscriptcrypto_compat)
#endif

#if defined(ATOMBOXER_BUILD_NET)
    Q_IMPORT_PLUGIN(abscriptnet_compat)
#endif

#if defined(ATOMBOXER_BUILD_HTTP)
Q_IMPORT_PLUGIN(abscripthttp_compat)
#endif

#if defined(ATOMBOXER_BUILD_SQL)
Q_IMPORT_PLUGIN(abscriptsql_compat)
#endif

#if defined(ATOMBOXER_BUILD_XML)
Q_IMPORT_PLUGIN(abscriptxml_compat)
#endif

#ifdef AB_HAVE_CTREE
Q_IMPORT_PLUGIN(abscriptctree_compat)
#ifdef Q_OS_WIN32 
#ifndef AB_STATIC_PLUGINS
extern int __sse2_available; /*exern*/
#else
int __sse2_available;
#endif
#endif
#endif

#else //JavaScriptCore

#if defined(ATOMBOXER_BUILD_DDL)
Q_IMPORT_PLUGIN(abscriptddl)
#endif

Q_IMPORT_PLUGIN(abscriptcore)

#if defined(ATOMBOXER_BUILD_CRYPTO)
Q_IMPORT_PLUGIN(abscriptcrypto)
#endif

#if defined(ATOMBOXER_BUILD_NET)
Q_IMPORT_PLUGIN(abscriptnet)
#endif

#if defined(ATOMBOXER_BUILD_HTTP)
Q_IMPORT_PLUGIN(abscripthttp)
#endif

#if defined(ATOMBOXER_BUILD_SQL)
Q_IMPORT_PLUGIN(abscriptsql)
#endif

#if defined(ATOMBOXER_BUILD_XML)
Q_IMPORT_PLUGIN(abscriptxml)
#endif

#ifdef AB_HAVE_CTREE
    Q_IMPORT_PLUGIN(abscriptctree)
#ifdef Q_OS_WIN32
#ifdef AB_STATIC_PLUGINS
int __sse2_available; /*exern*/
#endif
#endif
#endif

#endif

#ifdef __TANDEM
Q_IMPORT_PLUGIN(abscriptguardian)

#endif
#endif

#if defined(ATOMBOXER_BUILD_SQL)
#ifdef AB_STATIC_PLUGINS
Q_IMPORT_PLUGIN(qsqlite)
#endif
#endif

#include "license.h"

extern void qScriptDebugRegisterMetaTypes();

void crashMessageOutput(QtMsgType type, const char *msg)
{
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", msg);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", msg);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }
}

void EVAL_TERMINATE() {
    QCoreApplication::exit(0);
    exit(0);
}

QStringList
workaroundAvailableExtensions(int argc, char **argv)
{

    QStringList ret = QScriptEngine().availableExtensions();
    foreach (QString s, ret) {
        if (s.contains("_compat"))
            continue;

        ScriptEngineApp a;
        a.init(argc, argv);

        QScriptValue r = a.importExtension(s);
        if (!r.isUndefined()) {
            ret.removeAll(s);
        }
    }
    return ret;

}

bool 
checkLicense(std::string license) {
    if (license == "helloneo" ||
        license == "dontstop" ) {
        global_is_licensed = true;
        global_license = license;
        return true;
    }
    return false;
}

int *stack;

#ifdef  Q_OS_WIN32
int WinMain(HINSTANCE,HINSTANCE,LPSTR,int)
#else
    int main(int argc, char **argv)
#endif
{

#ifdef Q_OS_WIN32
    LPWSTR *szArglist;
    char *argv[256];
    int argc;

#if defined(AB_STATIC_PLUGINS) && defined(AB_HAVE_CTREE) && defined(Q_OS_WIN32)
#ifdef QT_HAVE_SSE 
    __sse2_available = 1;
#else
    __sse2_available = 0;
#endif
#endif
    szArglist = CommandLineToArgvW(GetCommandLineW(), &argc);

// Free memory allocated for CommandLineToArgvW arguments.
    for( int i=0; i<argc; i++) {
        argv[i] = new char[256];
        sprintf(argv[i], "%ls",szArglist[i]);
    }

    LocalFree(szArglist);
#endif
    //qInstallMsgHandler(crashMessageOutput);

    QCoreApplication *app = new QCoreApplication(argc, argv);
    
    //
    //  Process command line arguments
    //
    TCLAP::CmdLine cmd("AtomBox 0.5(beta) javascript interpreter & DSL\n   2015 @  INDUCTUM SRL (www.inductum.com)", ' ', "0.5");
    TCLAP::UnlabeledValueArg<std::string> mainArg("main","Main script file.",true,"","main script file");
    TCLAP::SwitchArg debugSwitch("d","debug","Start atombox debugger", false);
    cmd.add(debugSwitch);

    TCLAP::ValueArg<short> portArg("p","port","Debugger port to listen on", false, 8150, "port");
    cmd.add(portArg);

    TCLAP::ValueArg<std::string> licenseArg("l","license","Specify a license string", false, "", "license");
    cmd.add(licenseArg);

    
    cmd.add(mainArg);

    //
    //  Find the first unlabeled option (that will be the main script file arg);
    //
    bool in_license = false;
    for (int i=1; i < argc; i++) {
        if (in_license) {
            in_license = false;
            continue;
        }

        if (QByteArray(argv[i],2) == QByteArray("-l") ||
            QByteArray(argv[i],9) == QByteArray("--license")) {
            in_license = true;
            continue;
        } 

        if (argv[i][0] != '-' && !isdigit(argv[i][0])) {
            cmd.parse(i+1, argv);
            break;
        }
    }

#ifdef __TANDEM
    //heap_check_always(1);
#endif

    //@todo - implement license
    checkLicense(licenseArg.getValue());

    ScriptEngineApp *sc_app = new ScriptEngineApp();
    QObject::connect(app, SIGNAL(aboutToQuit()), sc_app, SLOT(deleteLater()));
        
    //
    //  Load all the available extensions
    //
    QDir dir(QCoreApplication::applicationDirPath());
#ifndef AB_STATIC_PLUGINS
    dir.cdUp();

    if (!dir.cd("plugins")) {
        fprintf(stderr, "plugins folder does not exist\n");
        return(-1);
    }

    QStringList paths = app->libraryPaths();
    paths <<  dir.absolutePath();
    app->setLibraryPaths(paths);
#endif

    if (!sc_app->init(argc, argv)) {
        app->quit();
    }


    //Workaround

    QStringList extensions = 
#if defined(AB_USE_SCRIPT_CLASSIC) || defined(_GUARDIAN_TARGET) || defined(AB_STATIC_PLUGINS)
    sc_app->availableExtensions();
#else
    workaroundAvailableExtensions(argc, argv);
#endif
   
    QStringList failExtensions;
    QStringList succExtensions;

    //IBM extensions are ugly and can stop the others load

    for(int i = 0; i<extensions.size(); i++) {
        QString ext = extensions[i];

#if !defined(__TANDEM)
#if defined(AB_USE_SCRIPT_CLASSIC) 
        if (!ext.contains("_compat"))
            continue;
#else
        if (ext.contains("_compat"))
            continue;
#endif
#endif //!__TANDEM

        QScriptValue r = sc_app->importExtension(ext);

#if defined(AB_USE_SCRIPT_CLASSIC)
        if (ext.contains("_compat"))
            ext = ext.replace(QString("_compat"),"");
#endif
        if (!r.isUndefined()) {
            if (sc_app->hasUncaughtException()) {
                int line = sc_app->uncaughtExceptionLineNumber();
                qDebug() << "uncaught exception  "<<line << ":" << r.toString();
            }
            failExtensions.append(ext);
        } else {
            succExtensions.append(ext);
        }
    }


#if !defined(_GUARDIAN_TARGET) && defined(ATOMBOXER_BUILD_SQL)
    if (QSqlDatabase::drivers().contains("QDB2")) {
        if (succExtensions.contains("sql"))
            succExtensions.replace(succExtensions.indexOf("sql"),"sql(+db2)");
    }
#endif

    QString line = "AtomBox 0.5(beta) (non-commercial version)\nPlugins loaded:" + succExtensions.join(",").replace("_compat","");

    if (global_is_licensed) {
        if (global_license == "dontstop") 
            line = "AtomBox 0.5(beta) (non-commercial dontstop version)\nPlugins loaded:" + succExtensions.join(",").replace("_compat","");
        else
            line = "AtomBox 0.5(beta) (licensed version)\nPlugins loaded:" + succExtensions.join(",").replace("_compat","");
    }

    foreach(QString e, failExtensions) {
        line += "," + e + "(FAIL)";
    }

#if defined(REGISTRANT)
    line += "\n";
    line +=  QUH(REGISTRANT);
#endif

    line += "\n";
    std::cout<<line.toLatin1().data()<<std::endl;

    if (!QFileInfo(mainArg.getValue().c_str()).exists()) {
#ifdef Q_OS_WIN32
        if (mainArg.getValue().size()==0) {
            std::stringstream buffer;
            std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());
            cmd.getOutput()->usage(cmd);

            std::string msg = buffer.str();
            size_t pos=0;
            while((pos = msg.find("not_set_yet", pos)) != std::string::npos) {
                msg.replace(pos, std::string("not_set_yet").length(), "ab.exe");
                pos += std::string("ab.exe").length();
            }

            std::wstring wmsg;
            wmsg.assign(msg.begin(), msg.end());
            std::wstring title(L"AtomBox JavaScript interpreter");
            
            MessageBox(NULL,  wmsg.c_str(), title.c_str(), MB_OK | MB_ICONINFORMATION| MB_APPLMODAL | MB_ICONASTERISK );
            //return(0);
        }
#endif

        fprintf(stderr,"\nAtomBox could not open the file \"%s\" for reading.\n",mainArg.getValue().c_str());
        cmd.getOutput()->usage(cmd);
        return 0;
    }

    QScriptValue r;

    if (debugSwitch.getValue() == true) {
        DebugRunner debug(sc_app, QHostAddress(QHostAddress::Any), portArg.getValue(), false /*listen*/);
        return app->exec();
    } else {
        r = sc_app->execute(); //args are already passed in init
    }


    if (sc_app->hasUncaughtException()) {
        int line = sc_app->uncaughtExceptionLineNumber();
        if (line != -1 && r.toString() != "[object Object]") {
            qDebug() << sc_app->uncaughtExceptionBacktrace().first()<<("\n");
            qDebug() << "uncaught exception  "<<line << ":" << r.toString();
        }
    }

    return app->exec();
}
