TEMPLATE = app

isEmpty(ATOMBOXER_PRI_INCLUDED) {
    include(../../atomboxer.pri)
}

QT = core 

!tandem {
    #QT += gui


    !isEmpty(ATOMBOXER_BUILD_HTTP) {
        QT += network
    }

    !isEmpty(ATOMBOXER_BUILD_NET) {
        QT += network
    }

    !isEmpty(ATOMBOXER_BUILD_XML) {
        QT += xml
    }

    !isEmpty(ATOMBOXER_BUILD_SQL) {
        QT += sql
    }

}

unix {
    QT -= gui
}


!isEmpty(AB_STATIC_PLUGINS) {
    !isEmpty(USE_SCRIPT_CLASSIC) {
	    LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcore_compat.a

        !isEmpty(ATOMBOXER_BUILD_DDL) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptddl_compat.a 
        }

        !isEmpty(ATOMBOXER_BUILD_CRYPTO) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcrypto_compat.a 
        }
       !isEmpty(ATOMBOXER_BUILD_NET) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptnet_compat.a 
       }

        !isEmpty(ATOMBOXER_BUILD_HTTP) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscripthttp_compat.a 
        }
        !isEmpty(ATOMBOXER_BUILD_SQL) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptsql_compat.a 
        }
        !isEmpty(ATOMBOXER_BUILD_XML) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptxml_compat.a 
        }

	    !isEmpty(ATOMBOXER_BUILD_CTREE) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptctree_compat.a 
	    }
        
	    !isEmpty(ATOMBOXER_BUILD_MQ) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptmq_compat.a 
	    } 
    } else {
	    LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcore.a

        !isEmpty(ATOMBOXER_BUILD_DDL) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptddl.a 
        }

        !isEmpty(ATOMBOXER_BUILD_CRYPTO) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcrypto.a 
        }
        !isEmpty(ATOMBOXER_BUILD_NET) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptnet.a 
        }

        !isEmpty(ATOMBOXER_BUILD_HTTP) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscripthttp.a 
        }
        !isEmpty(ATOMBOXER_BUILD_SQL) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptsql.a 
        }
        !isEmpty(ATOMBOXER_BUILD_XML) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptxml.a 
        }

	    !isEmpty(ATOMBOXER_BUILD_CTREE) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptctree.a 
	    }
        
	    !isEmpty(ATOMBOXER_BUILD_MQ) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptmq.a 
	    } 
        

	    !isEmpty(ATOMBOXER_BUILD_CTREE) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptctree.a 
	    }

	    !isEmpty(ATOMBOXER_BUILD_MQ) {
            LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptmq.a 
	    }
    }

    tandem {
        LIBS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptguardian.a
    }

    tandem {
        isEmpty(IS_TNSR) {
            isEmpty(USE_SCRIPT_CLASSIC) {
                LIBS       += -b dynamic \ 
                    $$QMAKE_LIBDIR_QT/ZCOREDLL \ 
                    $$QMAKE_LIBDIR_QT/ZSCRFDLL \
                    $$ATOMBOXER_SOURCE_TREE/lib/ZFDBGDLL 
                
                !isEmpty(ATOMBOXER_BUILD_XML) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZXMLDLL
                }
                
                !isEmpty(ATOMBOXER_BUILD_SQL) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZSQLDLL
                }
                
#                !isEmpty(ATOMBOXER_BUILD_NET)|!isEmpty(ATOMBOXER_BUILD_HTTP) {
                    LIBS += $$QMAKE_LIBDIR_QT//ZNETDLL
#                }
                
                !isEmpty(ATOMBOXER_BUILD_XML) {
                    LIBS += $$QMAKE_LIBDIR_QT//ZXMLDLL
                }                
            } else {
                LIBS       += -b dynamic \ 
                    $$QMAKE_LIBDIR_QT/ZCOREDLL \
                    $$ATOMBOXER_SOURCE_TREE/lib/ZSCRIDLL \
                    $$ATOMBOXER_SOURCE_TREE/lib/ZSDBGDLL 
                
                !isEmpty(ATOMBOXER_BUILD_XML) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZXMLDLL
                }
                
                !isEmpty(ATOMBOXER_BUILD_SQL) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZSQLDLL
                }
                
#                !isEmpty(ATOMBOXER_BUILD_NET)|!isEmpty(ATOMBOXER_BUILD_HTTP) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZNETDLL
#                }
                
                !isEmpty(ATOMBOXER_BUILD_XML) {
                    LIBS += $$QMAKE_LIBDIR_QT/ZXMLDLL
                }                
            }
        } else {
            LIBS       += -b dynamic -allow_duplicate_procs \ 
            $$ATOMBOXER_SOURCE_TREE/lib/ZCORESRL \
                $$ATOMBOXER_SOURCE_TREE/lib/ZSDBGSRL \
                $$ATOMBOXER_SOURCE_TREE/lib/ZSCRISRL
            !isEmpty(ATOMBOXER_BUILD_XML) {
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/ZXMLSRL
            }

#            !isEmpty(ATOMBOXER_BUILD_NET)|!isEmpty(ATOMBOXER_BUILD_HTTP) {
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/ZNETSRL
#            }
            
            !isEmpty(ATOMBOXER_BUILD_XML) {
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/ZXMLSRL
            }                
        }

    }

    LIBS       += -L$$ATOMBOXER_SOURCE_TREE/plugins/script/
    !isEmpty(ATOMBOXER_BUILD_DDL) {
        LIBS+= $$ATOMBOXER_SOURCE_TREE/lib/libddl.a \
            $$ATOMBOXER_SOURCE_TREE/lib/libatombox.a
    }
    !isEmpty(ATOMBOXER_BUILD_HTTP) {
        LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libtufao.a
    }

    !isEmpty(ATOMBOXER_BUILD_CRYPTO) {
         !tandem {
                LIBS         += -lssl \
                                -lcrypto
        }
    }
} else { #not AB_STATIC_PLUGINS
    isEmpty(USE_SCRIPT_CLASSIC) {
        QT += script
        !unix {
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptTools.a
        } else {
            LIBS += -lQtScriptTools
        }

    } else {
        !unix {
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptTools_compat.a
        }
    }

    CONFIG(debug, debug|release) {
        unix {
            !isEmpty(USE_SCRIPT_CLASSIC) {
                LIBS += -lQtScriptClassic -lQtScriptTools_compat
            }
        } else {
            !isEmpty(USE_SCRIPT_CLASSIC) {
                LIBS += -lQtScriptClassicd1
            }
        }
    } else { #release
        unix {
            !isEmpty(USE_SCRIPT_CLASSIC) {
                LIBS += -lQtScriptClassic -lQtScriptTools_compat
            }
        } else {
            !isEmpty(USE_SCRIPT_CLASSIC) {
                LIBS += -lQtScriptClassic1
            }
        }
    }
}

INCLUDEPATH += . $$ATOMBOXER_SOURCE_TREE/src/3rdparty/qscripttools/debugging

!isEmpty(USE_SCRIPT_CLASSIC) {
    TARGET = ab_compat
} else { 
    TARGET = ab
}

DESTDIR     = $$ATOMBOXER_SOURCE_TREE/bin/

!isEmpty(AB_STATIC_PLUGINS) {    
    isEmpty(USE_SCRIPT_CLASSIC) {
        PRE_TARGETDEPS  = $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcore.a

        !isEmpty(ATOMBOXER_BUILD_DDL) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptddl.a
        }
        !isEmpty(ATOMBOXER_BUILD_CRYPTO) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcrypto.a
        }
        !isEmpty(ATOMBOXER_BUILD_NET) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptnet.a
        }
        !isEmpty(ATOMBOXER_BUILD_HTTP) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscripthttp.a
        }
        !isEmpty(ATOMBOXER_BUILD_SQL) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptsql.a
        }
        !isEmpty(ATOMBOXER_BUILD_XML) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptxml.a
        }
    } else {
        PRE_TARGETDEPS  = $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcore_compat.a 
        !isEmpty(ATOMBOXER_BUILD_DDL) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptddl_compat.a
        }
        !isEmpty(ATOMBOXER_BUILD_CRYPTO) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptcrypto_compat.a
        }
        !isEmpty(ATOMBOXER_BUILD_NET) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptnet_compat.a
        }
        !isEmpty(ATOMBOXER_BUILD_HTTP) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscripthttp_compat.a
        }
        !isEmpty(ATOMBOXER_BUILD_SQL) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptsql_compat.a
        }
        !isEmpty(ATOMBOXER_BUILD_XML) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptxml_compat.a
        }
    }

    PRE_TARGETDEPS  += \

    !isEmpty(ATOMBOXER_BUILD_CTREE) {
        isEmpty(USE_SCRIPT_CLASSIC) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptctree.a 
        } else {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptctree_compat.a 
        }
    }

    !isEmpty(ATOMBOXER_BUILD_MQ) {
        isEmpty(USE_SCRIPT_CLASSIC) {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptmq.a 
        } else {
            PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptmq_compat.a 
        }
    }

    tandem {
        PRE_TARGETDEPS += $$ATOMBOXER_SOURCE_TREE/plugins/script/libabscriptguardian.a
    }


    !tandem {                
        LIBS += -L$$ATOMBOXER_SOURCE_TREE/lib/ 
        !isEmpty(USE_SCRIPT_CLASSIC) {
            CONFIG(debug, debug|release) {
                unix {
                    LIBS += -lQtScriptClassicd
                } else {
                    LIBS += -lQtScriptClassicd1
                    LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptTools_compat.a
                }
            } else { #CONFIG(...
                LIBS += -lQtScriptClassic1
                LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptTools_compat.a
            }

        } else { #noscriptclasic
	        QT += core script sql network #scripttools
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libQtScriptTools.a
        }

    } } #AB_STATIC_PLUGINS

win32:tandem {
    isEmpty(IS_TNSR) {
        # QMAKE_LFLAGS += $(COMP_ROOT)/usr/lib/ccplmain.o -lcre -lcrtl -lossk -lossf -lsec -li18n -licnv -losse -lossh -lossc -linet -lcppc -lcpp3 -lrld

        # QT         -= core
        
        # QMAKE_LINK   = $(COMP_ROOT)/usr/bin/eld -all
    } else {
        QT         -= core
        QMAKE_LFLAGS = 
        QMAKE_LINK = $(COMP_ROOT)/bin/ld -L$(COMP_ROOT)/lib/ $(COMP_ROOT)/lib/CCPPMAIN.o -set systype guardian -set highpin on  -obey $(COMP_ROOT)/lib/libc.obey -linet -lstl -verbose -linet -lstl -verbose -warn -lrld -UNRES_SYMBOLS warn -set rld_unresolved ignore
    }
}


HEADERS     += \
    scriptengineapp.h \
    debugrunner.h

SOURCES     += \
    main.cpp \
    scriptengineapp.cpp \
    debugrunner.cpp

include (debuggerengine.pri)


win32:!tandem {
    LIBS += -lcrypt32 -lgdi32 -lpsapi
}

!isEmpty(AB_STATIC_PLUGINS) {
    !isEmpty(ATOMBOXER_BUILD_CTREE) {
	    unix::!win32 {
            LIBS         += /usr/lib/64/libssl.so \
		        /usr/lib/64/libcrypto.so
	    } 
	    
	    win32 {
            LIBS         += $$ATOMBOXER_SOURCE_TREE/lib/libssl.a \
		        $$ATOMBOXER_SOURCE_TREE/lib/libcrypto.a
	    }	    
    }	
    isEmpty(USE_SCRIPT_CLASSIC) {
        tandem {
            QT -= script
        } else {
            LIBS += -labscriptcore
            QT   +=  script
        }
    } 

    unix:!tandem {
        LIBS += -labscriptcore_compat
    }

    !isEmpty(ATOMBOXER_BUILD_MQ) {

        win32:!tandem {
            LIBS +=  $$quote($$CTREE_DIR/lib/ctree.isam/Microsoft Visual Studio 2010/mtclient.lib)
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/runtmchk.lib  
            LIBS += $$ATOMBOXER_SOURCE_TREE/lib/ftol2.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/lldiv.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/ulldiv.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/ullshr.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/llmul.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/llrem.obj \
	            $$ATOMBOXER_SOURCE_TREE/lib/lldvrm.obj 
	        
            LIBS +=-lmingw32 -lmoldname -lmingwex
            LIBS +=  -ladvapi32 -lshell32 -luser32 -lkernel32  -lcrypt32 -lwsock32 -lgdi32 -lpsapi
	        
        }
        
        unix {
	        LIBS += $$CTREE_DIR/lib/ctree.isam/multithreaded/static/libmtclient.a
        }
    }
}

!isEmpty(AB_HAVE_MPATROL) {
#    LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libmpatrolmt_mingw64_32.a -lbfd -limagehlp -liberty -lz
    LIBS += $$ATOMBOXER_SOURCE_TREE/lib/libmpatrolmt_mingw64_32.a -lbfd -limagehlp -liberty -lz  -Wl,--allow-multiple-definition -lpsapi
}
