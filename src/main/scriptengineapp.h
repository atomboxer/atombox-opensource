#ifndef SCRIPTENGINEAPP_H
#define SCRIPTENGINEAPP_H

#include <QtCore/QHash>
#include <QtCore/QFileInfo>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVector>
#include <QtCore/QTimer>

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

class RequireCache;

class ScriptEngineApp : public QScriptEngine 
{
    Q_OBJECT
public:
    ScriptEngineApp();
    virtual ~ScriptEngineApp();

    void addToCache(QString &n, QScriptValue v) { require_cache_[n]=v; }
    void addToPaths(QString path) { paths_.push_back(path); }

    QHash<QString,QScriptValue> cache() const { return require_cache_; }
    void removeFromCache(QString key) { require_cache_.remove(key); }
    virtual void collectGarbage();

    QScriptValue execute();
    bool init(int argc, char **argv);
    QString mainFile() const { return mainFile_; }
    QStringList mainFileArgs() const { return mainFileArgs_; }
    QScriptValue mainModule() const { return mainModule_; }
    QStringList paths() const { return paths_; }
    QScriptValue require(QString name,
			 QString type=QString());
    
    QString resolveModule(QString &name);

public slots:
    void terminateEvaluation();

private:
    bool processArgs(int argc, char **argv);
    void setup();

private:
#ifdef AB_EVALUATION
    QTimer *eval_timer_;
#endif
    QString  mainFile_;
    QScriptValue mainModule_;

    QStringList paths_;
    QList<QString> mainFileArgs_;
    QVector<QString> resolvedModules_;
    
    QHash<QString,QScriptValue> require_cache_;
};

#endif //SCRIPTENGINEAPP_H
