#include "scriptengineapp.h"

#include <QtCore/QEventLoop>
#include <QtCore/QRegExp>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QTimer>
#include <QtCore/QCoreApplication>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#ifdef ATOMBOXER_BUILD_DDL
#include "ddlengine.h"
#endif

#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

//#define DEBUG_REQUIRE

#if  !defined(_GUARDIAN_TARGET) || !defined(ATOMBOXER_BUILD_DDL)
static QScriptValue _require(QScriptContext *context, QScriptEngine *engine);
#else
QScriptValue _require(QScriptContext *context, QScriptEngine *engine);
#endif

extern bool global_is_licensed;

#ifdef AB_STATIC_PLUGINS
extern QScriptValue
_defineProperty(QScriptContext *context, QScriptEngine *engine);
#else

QScriptValue
_defineProperty(QScriptContext *context, QScriptEngine *engine)
{
    if (context->argumentCount() != 3)
        return QScriptValue();

    QScriptValue obj  = context->argument(0);       //object
    QScriptValue prop = context->argument(1);       //name
    QScriptValue desc = context->argument(2);       //descriptor prop

    QScriptValue desc_value    = desc.property("value");
    QScriptValue desc_writable = desc.property("writable");
    QScriptValue desc_get      = desc.property("get");
    QScriptValue desc_set      = desc.property("set");
    QScriptValue desc_configurable      = desc.property("configurable");
    QScriptValue desc_enumerable        = desc.property("enumerable");

    //A data descriptor is a property that has a value
    //Accessor descriptor is a property described by a getter-setter 
    //pair of functions. It CANNOT BE BOTH

    int flg = 0;
    if (!desc_configurable.toBool()) {
        flg |= QScriptValue::Undeletable;
    }
    if (!desc_enumerable.toBool()) {
        flg |= QScriptValue::SkipInEnumeration;
    }

    if (!desc_value.isUndefined()) {
        //
        //  data descriptor
        //
        if (!desc_writable.toBool()) {
            flg |= QScriptValue::ReadOnly;
        }
        
        obj.setProperty(prop.toString(), 
                        desc_value, 
                        (QScriptValue::PropertyFlag)flg);
    } else if (!desc_get.isUndefined() || !desc_get.isUndefined()) {
        if (!desc_get.isUndefined()) {
            obj.setProperty(prop.toString(), 
                            desc_get, 
                            (QScriptValue::PropertyFlag)flg 
                            | QScriptValue::PropertyGetter);
            }
        if (!desc_set.isUndefined()) {
            obj.setProperty(prop.toString(), 
                            desc_set, 
                            (QScriptValue::PropertyFlag)flg 
                            | QScriptValue::PropertySetter);
        }
    } else {
        context->throwError("invalid argument in Object.defineProperty");
    }
 
    return QScriptValue();
}
#endif

#if  !defined(_GUARDIAN_TARGET) || !defined(ATOMBOXER_BUILD_DDL)
QScriptValue
_require(QScriptContext *context, QScriptEngine *engine)
{
     ScriptEngineApp *ptr = dynamic_cast<ScriptEngineApp*>(engine);
     Q_ASSERT(ptr);

     if (context->argumentCount() == 1) {
         return ptr->require(context->argument(0).toString(), ".");
     } else if (context->argumentCount() == 2) {
         return ptr->require( context->argument(0).toString(),
                              context->argument(1).toString() );
     }
     return QScriptValue();
}
#endif

QScriptValue
_processEvents(QScriptContext *context, QScriptEngine *engine)
{
    static QEventLoop loop;
    loop.processEvents();
    return QScriptValue();
}


ScriptEngineApp::ScriptEngineApp() :
    QScriptEngine()
{
    paths_.push_back("c:/");

#ifdef AB_EVALUATION
    if (!global_is_licensed) {
        eval_timer_ = new QTimer(this);
        connect(eval_timer_, SIGNAL(timeout()), this, SLOT(terminateEvaluation()));
        eval_timer_->start(10800000); /*3 hours*/
    }
#endif

}

void
ScriptEngineApp::terminateEvaluation() {
    std::cout<<"AtomBox evaluation ended (3 hour limit reached)"<<std::endl;
    QCoreApplication::exit(0);
    exit(0);
}

ScriptEngineApp::~ScriptEngineApp()
{
#ifdef AB_HAVE_MPATROL
    delete DDLEngine::instance();
#endif
}

void
ScriptEngineApp::collectGarbage()
{
#ifdef SCRIPTENGINEAPP_DEBUG
    qDebug()<<"*** COLLECT GARBAGE ***";
#endif

    QScriptEngine::collectGarbage();
}

QScriptValue
ScriptEngineApp::execute()
{
    setup();

#ifdef DEBUG_REQUIRE
    qDebug()<<"[execute] main file:"<<mainFile_;
#endif

    mainModule_ = newObject();

    if (mainFile_.size() == 0) {
        fprintf(stderr,"no main file. Nothing to execute\n");
        return QScriptValue();
    }

    return require(mainFile_/*, QDir::currentPath()*/);
}

bool
ScriptEngineApp::init(int argc, char **argv)
{
    if(argc && !processArgs(argc, argv)) {
        fprintf(stderr, "[init] Unable to process the arguments\n");
        return false;
    }
    //
    //  Process events everty 250 milliseconds
    //

    //this->setProcessEventsInterval(250);

    return true;
}

QScriptValue
ScriptEngineApp::require( QString name,
                          QString type )
{
    QScriptValue g = currentContext()->activationObject();

    if (g.property("require").isValid() ) {
#ifdef DEBUG_REQUIRE
        qDebug()<<"[require]"<<"Already defined.";
#endif
        //  This is to allow extension plugins to redefine require.
        QScriptValueList args;
        args << name;
        args << type;

        return g.property("require").call(g, args);
    }

#ifdef DEBUG_REQUIRE
    qDebug()<<"[require]("<<name<<","<<type<<")";
#endif
    QString  moduleName = this->resolveModule(name);
    if (!moduleName.size()) {
        return currentContext()->throwError("(require) Unable to locate module:"+QString(name));
    }
    
    //Search in cache first
    if ( cache().contains(moduleName) ) {
#ifdef DEBUG_REQUIRE
        qDebug()<<"[require](returned from cache)";
#endif
        return cache().find(moduleName).value();
    }

    QScriptValue sv_require = newFunction(_require);


    //  Build the require function
    if ( !g.property("require").isFunction()) {
        g.setProperty("require", sv_require);

        QScriptValue arr = newArray();
        for (int i=0;i<paths_.size();i++) {
            arr.setProperty(QString::number(i),paths_[i]);
        }
        sv_require.setProperty("paths",arr);
        sv_require.setProperty("main",
                 QFileInfo(mainFile()).absoluteFilePath());            
    }

    // Add to cache
    QScriptValue sv_exports = newObject();
    addToCache(moduleName, sv_exports);

    QScriptValue sv_module;

    if (name == mainFile_) {
        sv_module = mainModule();
    } else {
        sv_module = newObject();
    }

    sv_module.setProperty("id", moduleName);
    
    QFile scriptFile;

    paths_.push_back(QFileInfo(moduleName).absolutePath());
    scriptFile.setFileName(moduleName);
    scriptFile.open(QIODevice::ReadOnly|QIODevice::Text);
    
    QTextStream stream(&scriptFile);
    QString code = stream.readAll();
    scriptFile.close();

    QString closure = "(function(require,exports,module){";
    closure += code;
    closure += ";})";

    QScriptValue req = evaluate( closure, moduleName, 1);
    QScriptValueList args;

    args << sv_require;
    args << sv_exports;
    args << sv_module;

    req.call(sv_exports, args);

    if (sv_module.property("exports").isValid()) {
        addToCache(moduleName, sv_module.property("exports"));
        return sv_module.property("exports");
    } else {
        return sv_exports;
    }

    return sv_exports;
}

//PRIVATE
bool
ScriptEngineApp::processArgs(int argc, char **argv)
{

    bool in_license = false;
    for (int i=1; i < argc; i++) {
        if (in_license) {
            in_license = false;
            continue;
        }

        if (QByteArray(argv[i],2) == QByteArray("-l") ||
            QByteArray(argv[i],9) == QByteArray("--license")) {
            in_license = true;
            continue;
        } 

        if (argv[i][0] != '-' && !isdigit(argv[i][0])) {
            this->mainFile_ = argv[i];
            break;
        }
    }

    for(int i=2; i<argc; i++) {
        this->mainFileArgs_.push_back(QString(argv[i]));
    }

    return true;
}

QString
ScriptEngineApp::resolveModule(QString &name)
{
    QString ret;
    QFileInfo f;

    if (QFileInfo(name).isDir()) {
        fprintf(stderr,"[resolveModule] directories are not supported in require(...)\n");
        return QString();
    }

    if (!QFileInfo(QString(":/atombox/")+name).isDir() &&
        (f = QFileInfo(QString(":/atombox/")+name)).exists() ||
        (f = QFileInfo(QString(":/atombox/")+name+".js")).exists()) {
        return f.absoluteFilePath();
    }

    //
    // Look into resources first
    //
    if ((f = QFileInfo(name)).exists() ||
        (f = QFileInfo(name+".js")).exists() ||
        (f = QFileInfo(name+"js")).exists() ||
        (f = QFileInfo("js"+name)).exists()) {
        if (f.absoluteFilePath().isEmpty())
            ret = name;
        else
            ret = f.absoluteFilePath();

    } else { // we need to search it in .paths
        for(int i=0;i<paths_.size();i++) {
            QFileInfoList li= QDir(paths_[i]).entryInfoList();
            QRegExp rx ("(ddl){0,1}"+name+"[\\.]{1}(js){0,1}");
            for (int j=0;j<li.size();j++) {
                if (rx.exactMatch(li[j].baseName()+"."+li[j].completeSuffix())) {
                    ret = li[j].absoluteFilePath();
                    break;
                }
            }
        }

        for(int i=0;i<paths_.size();i++) {
            if (QFileInfo(paths_[i]+"/"+name).exists()) {
                ret = paths_[i]+"/"+name;
                break;
            }
            if (QFileInfo(paths_[i]+"/"+name+".js").exists()) {
                ret = paths_[i]+"/"+name+".js";
                break;
            }
        }
    }

    if (!ret.isEmpty() && !paths_.contains(QFileInfo(ret).absolutePath()))
        paths_.push_back(QFileInfo(ret).absolutePath());

#ifdef DEBUG_REQUIRE
    qDebug()<<"[resolveModule] name="<<name<<"resolved to:"<<ret<<"paths:"<<paths_;
#endif


    return ret;
}

void
ScriptEngineApp::setup()
{
    //
    //  @todo setup_system
    //
    QScriptValue o = globalObject().property("Object");
    Q_ASSERT(!o.isUndefined());
    o.setProperty("defineProperty",
                  newFunction(_defineProperty),
                  QScriptValue::ReadOnly
                  |QScriptValue::Undeletable
                  |QScriptValue::SkipInEnumeration);


    globalObject().setProperty("processEvents",
                               newFunction(_processEvents),
                               QScriptValue::ReadOnly
                               |QScriptValue::Undeletable
                               |QScriptValue::SkipInEnumeration);

    //
    //  OK_STATE, WARNING_STATE, ERROR_STATE, DEFAULT_STATE
    //
    globalObject().setProperty("OK_STATE",0);
    globalObject().setProperty("WARNING_STATE",1);
    globalObject().setProperty("ERROR_STATE",2);
    globalObject().setProperty("DEFAULT_STATE",3);

    globalObject().setProperty("BIG_ENDIAN",0);
    globalObject().setProperty("LITTLE_ENDIAN",1);
}
