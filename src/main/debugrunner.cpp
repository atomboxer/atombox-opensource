#include <QtCore/QCoreApplication>

#include "debugrunner.h"
#ifdef AB_HAVE_MPATROL
#include <mpatrol.h>
#endif

void qScriptDebugRegisterMetaTypes()
{

    qRegisterMetaType<QScriptDebuggerEngine::Error>("<QScriptDebuggerEngine::Error>");
    qMetaTypeId<QScriptDebuggerCommand>();
    qMetaTypeId<QScriptDebuggerResponse>();
    qMetaTypeId<QScriptDebuggerEvent>();
    qMetaTypeId<QScriptContextInfo>();
    qMetaTypeId<QScriptContextInfoList>();
    qMetaTypeId<QScriptDebuggerValue>();
    qMetaTypeId<QScriptDebuggerValueList>();
    qMetaTypeId<QScriptValue::PropertyFlags>();
    qMetaTypeId<QScriptBreakpointData>();
    qMetaTypeId<QScriptBreakpointMap>();
    qMetaTypeId<QScriptScriptData>();
    qMetaTypeId<QScriptScriptMap>();
    qMetaTypeId<QScriptScriptsDelta>();
    qMetaTypeId<QScriptDebuggerValueProperty>();
    qMetaTypeId<QScriptDebuggerValuePropertyList>();
    qMetaTypeId<QScriptDebuggerObjectSnapshotDelta>();
    qMetaTypeId<QList<int> >();
    qMetaTypeId<QList<qint64> >();

    qRegisterMetaTypeStreamOperators<QScriptDebuggerCommand>("QScriptDebuggerCommand");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerResponse>("QScriptDebuggerResponse");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerEvent>("QScriptDebuggerEvent");
    qRegisterMetaTypeStreamOperators<QScriptContextInfo>("QScriptContextInfo");
    qRegisterMetaTypeStreamOperators<QScriptContextInfoList>("QScriptContextInfoList");
    qRegisterMetaTypeStreamOperators<QScriptBreakpointData>("QScriptBreakpointData");
    qRegisterMetaTypeStreamOperators<QScriptBreakpointMap>("QScriptBreakpointMap");
    qRegisterMetaTypeStreamOperators<QScriptScriptData>("QScriptScriptData");
    qRegisterMetaTypeStreamOperators<QScriptScriptMap>("QScriptScriptMap");
    qRegisterMetaTypeStreamOperators<QScriptScriptsDelta>("QScriptScriptsDelta");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerValue>("QScriptDebuggerValue");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerValueList>("QScriptDebuggerValueList");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerValueProperty>("QScriptDebuggerValueProperty");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerValuePropertyList>("QScriptDebuggerValuePropertyList");
    qRegisterMetaTypeStreamOperators<QScriptDebuggerObjectSnapshotDelta>("QScriptDebuggerObjectSnapshotDelta");
    qRegisterMetaTypeStreamOperators<QList<int> >("QList<int>");
    qRegisterMetaTypeStreamOperators<QList<qint64> >("QList<qint64>");
}

DebugRunner::DebugRunner(ScriptEngineApp *engine, 
                         const QHostAddress &addr, 
                         quint16 port, 
                         bool connect, 
                         QObject *parent)
    : QObject(parent)
{
    Q_ASSERT(engine != 0);

    qScriptDebugRegisterMetaTypes();

    scriptEngine_ = engine;
    debuggerEngine_ = new QScriptDebuggerEngine(this);
    QObject::connect(debuggerEngine_, SIGNAL(connected()), this, SLOT(onConnected()), Qt::QueuedConnection);
    QObject::connect(debuggerEngine_, SIGNAL(disconnected()),this, SLOT(onDisconnected()), Qt::QueuedConnection);
    debuggerEngine_->setTarget(scriptEngine_);

    if (connect) {
        qDebug("attempting to connect to debugger at %s:%d", qPrintable(addr.toString()), port);
        debuggerEngine_->connectToDebugger(addr, port);
    } else {
        if (debuggerEngine_->listen(addr, port))
            qDebug("waiting for debugger to connect at %s:%d", qPrintable(addr.toString()), port);
        else {
            qWarning("Failed to listen!");
            QCoreApplication::quit();
        }
    }
}

void DebugRunner::onConnected()
{
    qDebug("debugger connected");
    QScriptValue r = scriptEngine_->execute();
}

void DebugRunner::onDisconnected()
{
    qDebug("debugger disconnected");
    debuggerEngine_->disconnectFromDebugger();
    QCoreApplication::quit();
}

