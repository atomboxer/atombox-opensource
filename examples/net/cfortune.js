//
//  TCP/Client Fortune example
//

var TcpSocket     = require("net").TcpSocket;
var console       = require("console");

//Create a new TcpSocket object
var socket = new TcpSocket();

//Connect the readyRead signal
socket.readyRead.connect( function() {
    //Read what the server sends us
    var fortune = socket.read();

    console.write(fortune.decodeToString());
});

//Connect the disconnected signal
socket.disconnected.connect( function() {
    console.write("disconnected\n");
});

socket.connectToHost("localhost", 8150);
