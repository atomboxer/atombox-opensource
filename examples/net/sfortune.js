//
//  TCP/Server Fortune Server example
//

var net     = require("net");
var console = require("console");

var fortunes = ["You've been leading a dog's life. Stay off the furniture",
                "You've got to think about tomorrow",
                "You will be surprised by a loud noise",
                "You will feel hungry again in another hour",
                "You might have mail",
                "You cannot kill time without injuring eternity",
                "Computers are not intelligent. They only think they are"];


// Create a new instance of TcpServer
var server =  new net.TcpServer();

// Start listening on port 8150
server.listen(8150);

// Connect the newConnection signal
server.newConnection.connect(function() {

    // Accept the client connection
    var client = server.nextPendingConnection();

    // Send the fortune 
    client.write(fortunes[Math.floor(Math.random()*7+1)].toByteArray());
    
    // Wait for the server to send the message and then disconnect
    client.bytesWritten.connect(function() {
        console.write("fortune delivered");

        client.disconnectFromHost(); /* close the connection */
    });
});
