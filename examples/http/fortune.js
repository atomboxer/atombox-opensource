//
//  Fortune web application
//
var http    = require("http");
var console = require("console");

var fortunes = ["You've been leading a dog's life. Stay off the furniture",
                "You've got to think about tomorrow",
                "You will be surprised by a loud noise",
                "You will feel hungry again in another hour",
                "You might have mail",
                "You cannot kill time without injuring eternity",
                "Computers are not intelligent. They only think they are"];



// Create a new instance of HttpServer
var server =  new http.HttpServer();

// Start listening on port 8150
server.listen(8150);
console.assert(server.isListening());

// Connect the requestReady signal
server.requestReady.connect(function(request,response) {
    //
    //  request  instanceof http.HttpServerRequest
    //  response instanceof http.HttpServerResponse
    //

    //write the header
    response.writeHeader(200,"bla bla",{ 'Content-Type': 'text/plain'});

    //write some data
    response.write("the fortune is:".toByteArray());

    //end the response with the fortune
    response.end(fortunes[Math.floor(Math.random()*7+1)].toByteArray());
});
