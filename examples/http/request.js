var HttpServer = require("http").HttpServer;
var console    = require("console");
var server = new HttpServer();
server.listen(8150);

server.requestReady.connect(function(rq,rs) {
    //Connect a custom function to the the `end` signal
    rq.end.connect(function() {
        //Request has finished parsing
        console.write("url         = "+rq.url+"\n");
        console.write("method      = "+rq.method+"\n");
        console.write("httpVersion = "+rq.httpVersion+"\n");
        console.dir(rq.headers);
        console.dir(rq.trailers);
        
        //end the response
        rs.writeHeader(404,"bla bla",{ 'Content-Type': 'text/plain'});
        rs.end();
    });
});
