var console = require("console");
var Process = require("process").Process;

//create a new Process object
var p = new Process();

//connect a function to the 'finished' signal
p.finished.connect(function(exit) {
	console.write("finished event:"+exit+"\n");
});

//
//  Start the Windows cmd process with some parameters
//
p.start("cmd",["/C echo Hello world"]);

