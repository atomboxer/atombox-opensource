#!/bin/bash

linux=false
if [ `uname` == "Linux" ]
then
    linux=true
fi

qt_dir="/opt/qt48"

install_dir="atombox_exec"

src_dir="../../atombox_opensource"

if [ -e $install_dir ]
then
    echo "removing dir"
#    rm -rf $install_dir
else
    echo "create dir"
    mkdir $install_dir
    mkdir $install_dir/lib
    mkdir $install_dir/bin
    mkdir $install_dir/plugins
fi

echo "copy qt lib"
if [ "$linux" = true ] ; then
    cp $qt_dir/lib/*.so.4 $install_dir/lib/
else
    cp $qt_dir/lib/*.a $install_dir/lib/
fi

echo "copy qt plugins"
cp -rf $qt_dir/plugins/sqldrivers $install_dir/plugins

echo "copy bin"
if [ -e $src_dir/bin/ab_compat ] 
then
    cp $src_dir/bin/ab_compat $install_dir/bin/ 
else
    cp $src_dir/bin/ab $install_dir/bin/ 
fi
cp $src_dir/bin/hellojs $install_dir/bin/ 
cp $src_dir/bin/setenv $install_dir/bin/ 

echo "copy ab lib"
if [ "$linux" = true ] ;then
    cp $src_dir/lib/libQtScriptClassic.so.1 $install_dir/lib/
    cp $src_dir/lib/libQtScriptTools_compat.so.1 $install_dir/lib/
    cp $src_dir/lib/libQtScriptTools.so.1 $install_dir/lib/
else
    cp $src_dir/lib/libQtScriptClassic.a $install_dir/lib/
    cp $src_dir/lib/libQtScriptTools_compat.a $install_dir/lib/
    cp $src_dir/lib/libQtScriptTools.a $install_dir/lib/
fi

echo "copy ab plugins"
cp -rf $src_dir/plugins/* $install_dir/plugins/
cp -rf $src_dir/plugins/script/libabscriptcore*.so $install_dir/lib/
cp -rf $src_dir/plugins/script/libabscriptnet*.so $install_dir/lib/

cp $src_dir/lib/libmtclient.so $install_dir/lib

echo "copy ab doc"
mkdir $install_dir/doc
touch $install_dir/doc/www.atombox.org

ymmdd=`date +"%y_%m_%d"`
if [ "$linux" = true ] ; then
    tar -cf atombox_$yymmdd.tar $install_dir
    gzip atombox_$yymmdd.tar
else
    tar -cf atombox_aix_$yymmdd.tar $install_dir
    gzip -f atombox_aix_$yymmdd.tar
fi

#echo "copy ab tests"
#cp -rf $src_dir/tests $install_dir/


