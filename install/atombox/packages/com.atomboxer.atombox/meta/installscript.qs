function Component()
{
    // constructor
}

Component.prototype.isDefault = function()
{
    // select the component by default
    return true;
}


Component.prototype.createOperations = function()
{
    // call default implementation to actually install README.txt!
    component.createOperations();

    if (installer.value("os") === "win") {
        component.addOperation("CreateShortcut", "@TargetDir@/bin/ab.exe", "@StartMenuDir@/ab.lnk");
        component.addOperation("CreateShortcut", "@TargetDir@/bin/ab_nocompat.exe", "@StartMenuDir@/ab_no_compat.lnk");
        component.addOperation("CreateShortcut", "@TargetDir@/bin/abinspect.exe", "@StartMenuDir@/AtomBox Inspector.lnk");
        component.addOperation("CreateShortcut", "@TargetDir@/html/index.html", "@StartMenuDir@/AtomBox API.lnk");
        //component.addOperation("CreateShortcut", "http://www.atombox.org/jsdoc", "@StartMenuDir@/AtomBox API.lnk");
        component.addOperation("EnvironmentVariable", "ATOMBOX_DIR", "@TargetDir@");
    }
}

